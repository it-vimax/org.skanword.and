Задачи:
1. При входе в приложение, идут 4 запроса, которые нужно настроить
	- https://skanword.org/rest/config/google
		auth_key=311af268e99bc748686535a713f03419
		user_id=4411022
		vendor_id=755229023b2ec91f3cd18f667a59b2c9
		last_sync_type=mobile_internet
		vendor_key=c0024365c4777655be37a8cecdacf8ce
		google_advertising_id=
		google_android_id=ceb1341ab86b3d55
		google_device_id=358240051111110
		device_info=eyJkZXZpY2Vfc3lzdGVtX3ZlcnNpb24iOiIzLjEwLjArIiwiYXBwX2J1aWxkIjoiMTQ1IiwiYXBw%0AX3ZlcnNpb24iOiIxLjEwLjQ1In0%3D%0A
		updates=e30=

	- https://skanword.org/rest/sync/google
		auth_key=311af268e99bc748686535a713f03419
		user_id=4411022
		vendor_id=755229023b2ec91f3cd18f667a59b2c9
		last_sync_type=mobile_internet
		vendor_key=c0024365c4777655be37a8cecdacf8ce
		google_advertising_id=
		google_android_id=ceb1341ab86b3d55
		google_device_id=358240051111110
		device_info=eyJkZXZpY2Vfc3lzdGVtX3ZlcnNpb24iOiIzLjEwLjArIiwiYXBwX2J1aWxkIjoiMTQ1IiwiYXBw%0AX3ZlcnNpb24iOiIxLjEwLjQ1In0%3D%0A
        updates=eyI0NTg2MmNkNWQ4ODNmMGFiMjMzZTJjZDI5OGFlOWUzMSI6eyJrZXl3b3JkIjp0cnVlLCJ1Y20i%0AOnt9LCJfdW90IjoxNTYxOTQ4MDMzLCJ1aGIiOnt9LCJ1b3UiOnt9LCJ1dXMiOnsibHUiOjEsImhj%0AbyI6MCwiaGx1IjowLCJsYyI6MSwibGYiOnsiZGVza3RvcCI6MX0sInRjX29mZiI6MCwidGNfb24i%0AOjAsInRtX29mZiI6MCwidG1fb24iOjB9fX0%3D%0A

	- https://skanword.org/rest/cross_list_data/google
		auth_key=311af268e99bc748686535a713f03419
		user_id=4411022
		vendor_id=755229023b2ec91f3cd18f667a59b2c9
		last_sync_type=mobile_internet
		vendor_key=c0024365c4777655be37a8cecdacf8ce
		google_advertising_id=36267fae-08c9-4df0-b678-6a82e9e564b2
		google_android_id=ceb1341ab86b3d55
		google_device_id=358240051111110
		device_info=eyJkZXZpY2Vfc3lzdGVtX3ZlcnNpb24iOiIzLjEwLjArIiwiYXBwX2J1aWxkIjoiMTQ1IiwiYXBw%0AX3ZlcnNpb24iOiIxLjEwLjQ1In0%3D%0A
		updates=e30%3D%0A
		page=current
		type=new
		set_id_first=0
		set_id_last=0

	- https://skanword.org/rest/info/google
		auth_key=311af268e99bc748686535a713f03419
		user_id=4411022
		vendor_id=755229023b2ec91f3cd18f667a59b2c9
		last_sync_type=mobile_internet
		vendor_key=c0024365c4777655be37a8cecdacf8ce
		google_advertising_id=36267fae-08c9-4df0-b678-6a82e9e564b2
		google_android_id=ceb1341ab86b3d55
		google_device_id=358240051111110
		device_info=eyJkZXZpY2Vfc3lzdGVtX3ZlcnNpb24iOiIzLjEwLjArIiwiYXBwX2J1aWxkIjoiMTQ1IiwiYXBw%0AX3ZlcnNpb24iOiIxLjEwLjQ1In0%3D%0A
		mask=2
		news_hash=d41d8cd98f00b204e9800998ecf8427e
		shop_hash=7d495825b38f1a00d0de3db3ab6990f2
		rating_hash=
		updates=e30%3D%0A



