package org.skanword.and.scanwordgame;

import java.util.logging.*;
import android.content.*;
import android.widget.*;
import android.annotation.*;
import android.graphics.drawable.*;
import android.graphics.*;
import android.support.v4.view.*;
import android.view.*;
import android.util.*;
import android.support.v7.widget.AppCompatImageView;

public class TouchImageView extends AppCompatImageView
{
    protected static Logger LOG;
    private boolean canScale;
    private int clickSlop;
    private boolean couldBeLongClick;
    private ImageViewDelegate delegate;
    private PointF last;
    private long lastClickTime;
    private LongClickDetector lastLongClickDetector;
    private float[] m;
    private ScaleGestureDetectorProxy mScaleDetector;
    private OverScroller mScroller;
    private VelocityTracker mVelocityTracker;
    protected Matrix matrix;
    private float maxScale;
    private float minScale;
    private Mode mode;
    private int oldMeasuredHeight;
    private int oldMeasuredWidth;
    protected float origHeight;
    protected float origWidth;
    private float scale;
    private PointF start;
    private int viewHeight;
    private int viewWidth;
    
    static {
        TouchImageView.LOG = Logger.getLogger("com.adamrosenfield.wordswithcrosses");
    }
    
    public TouchImageView(final Context context) {
        super(context);
        this.mode = Mode.NONE;
        this.last = new PointF();
        this.start = new PointF();
        this.canScale = true;
        this.scale = 1.0f;
        this.minScale = 1.0f;
        this.maxScale = 3.0f;
        this.clickSlop = 3;
        this.lastClickTime = -1L;
        this.mVelocityTracker = null;
        this.sharedConstructing(context);
    }
    
    public TouchImageView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mode = Mode.NONE;
        this.last = new PointF();
        this.start = new PointF();
        this.canScale = true;
        this.scale = 1.0f;
        this.minScale = 1.0f;
        this.maxScale = 3.0f;
        this.clickSlop = 3;
        this.lastClickTime = -1L;
        this.mVelocityTracker = null;
        this.sharedConstructing(context);
    }
    
    static /* synthetic */ ScaleGestureDetectorProxy access000(final TouchImageView touchImageView) {
        return touchImageView.mScaleDetector;
    }
    
    static /* synthetic */ VelocityTracker access100(final TouchImageView touchImageView) {
        return touchImageView.mVelocityTracker;
    }
    
    static /* synthetic */ ImageViewDelegate access1000(final TouchImageView touchImageView) {
        return touchImageView.delegate;
    }
    
    static /* synthetic */ VelocityTracker access102(final TouchImageView touchImageView, final VelocityTracker mVelocityTracker) {
        return touchImageView.mVelocityTracker = mVelocityTracker;
    }
    
    static /* synthetic */ long access1100(final TouchImageView touchImageView) {
        return touchImageView.lastClickTime;
    }
    
    static /* synthetic */ long access1102(final TouchImageView touchImageView, final long lastClickTime) {
        return touchImageView.lastClickTime = lastClickTime;
    }
    
    static /* synthetic */ boolean access1200(final TouchImageView touchImageView) {
        return touchImageView.canScale;
    }
    
    static /* synthetic */ float access1300(final TouchImageView touchImageView) {
        return touchImageView.scale;
    }
    
    static /* synthetic */ float access1302(final TouchImageView touchImageView, final float scale) {
        return touchImageView.scale = scale;
    }
    
    static /* synthetic */ float access1400(final TouchImageView touchImageView) {
        return touchImageView.maxScale;
    }
    
    static /* synthetic */ float access1500(final TouchImageView touchImageView) {
        return touchImageView.minScale;
    }
    
    static /* synthetic */ int access1600(final TouchImageView touchImageView) {
        return touchImageView.viewWidth;
    }
    
    static /* synthetic */ int access1700(final TouchImageView touchImageView) {
        return touchImageView.viewHeight;
    }
    
    static /* synthetic */ OverScroller access1800(final TouchImageView touchImageView) {
        return touchImageView.mScroller;
    }
    
    static /* synthetic */ PointF access200(final TouchImageView touchImageView) {
        return touchImageView.last;
    }
    
    static /* synthetic */ PointF access300(final TouchImageView touchImageView) {
        return touchImageView.start;
    }
    
    static /* synthetic */ Mode access400(final TouchImageView touchImageView) {
        return touchImageView.mode;
    }
    
    static /* synthetic */ Mode access402(final TouchImageView touchImageView, final Mode mode) {
        return touchImageView.mode = mode;
    }
    
    static /* synthetic */ void access500(final TouchImageView touchImageView) {
        touchImageView.endLongClickDetection();
    }
    
    static /* synthetic */ boolean access600(final TouchImageView touchImageView) {
        return touchImageView.couldBeLongClick;
    }
    
    static /* synthetic */ boolean access602(final TouchImageView touchImageView, final boolean couldBeLongClick) {
        return touchImageView.couldBeLongClick = couldBeLongClick;
    }
    
    static /* synthetic */ LongClickDetector access700(final TouchImageView touchImageView) {
        return touchImageView.lastLongClickDetector;
    }
    
    static /* synthetic */ LongClickDetector access702(final TouchImageView touchImageView, final LongClickDetector lastLongClickDetector) {
        return touchImageView.lastLongClickDetector = lastLongClickDetector;
    }
    
    static /* synthetic */ int access900(final TouchImageView touchImageView) {
        return touchImageView.clickSlop;
    }
    
    private void endLongClickDetection() {
        if (this.couldBeLongClick) {
            this.couldBeLongClick = false;
            this.lastLongClickDetector.disable();
            this.lastLongClickDetector = null;
        }
    }
    
    private void fixTrans() {
        this.matrix.getValues(this.m);
        final float[] m = this.m;
        final float n = m[2];
        final float n2 = m[5];
        final float fixTrans = this.getFixTrans(n, (float)this.viewWidth, this.origWidth * this.scale);
        final float fixTrans2 = this.getFixTrans(n2, (float)this.viewHeight, this.origHeight * this.scale);
        if (fixTrans != 0.0f || fixTrans2 != 0.0f) {
            this.matrix.postTranslate(fixTrans, fixTrans2);
        }
    }
    
    private float getFixDragTrans(float n, final float n2, final float n3) {
        if (n3 <= n2) {
            n = 0.0f;
        }
        return n;
    }
    
    private float getFixTrans(final float n, float n2, float n3) {
        if (n3 <= n2) {
            n3 = n2 - n3;
            n2 = 0.0f;
        }
        else {
            n2 -= n3;
            n3 = 0.0f;
        }
        if (n < n2) {
            return -n + n2;
        }
        if (n > n3) {
            return -n + n3;
        }
        return 0.0f;
    }

    private void sharedConstructing(final Context context) {
        super.setClickable(true);
        final GestureDetector gestureDetector = new GestureDetector(context, (GestureDetector.OnGestureListener)new TouchImageView.GestureListener(this, (View)this));
        this.mScroller = new OverScroller(context);
        this.mScaleDetector = ScaleGestureDetectorProxy.create(context, this);
        this.matrix = new Matrix();
        this.m = new float[9];
        this.setImageMatrix(this.matrix);
        this.setScaleType(ImageView.ScaleType.MATRIX);
        this.setOnTouchListener((View.OnTouchListener)new TouchImageView.TouchImageView1(this, gestureDetector));
    }
    
    public void centerImage() {
        final Drawable drawable = this.getDrawable();
        if (drawable != null && drawable.getIntrinsicWidth() != 0) {
            if (drawable.getIntrinsicHeight() == 0) {
                return;
            }
            final int intrinsicWidth = drawable.getIntrinsicWidth();
            final int intrinsicHeight = drawable.getIntrinsicHeight();
            final float n = (float)this.viewWidth;
            final float origWidth = (float)intrinsicWidth;
            final float n2 = n / origWidth;
            final float n3 = (float)this.viewHeight;
            final float origHeight = (float)intrinsicHeight;
            this.scale = Math.min(n2, n3 / origHeight);
            final Matrix matrix = this.matrix;
            final float scale = this.scale;
            matrix.setScale(scale, scale);
            final float n4 = (float)this.viewWidth;
            final float scale2 = this.scale;
            this.matrix.postTranslate((n4 - scale2 * origWidth) * 0.5f, (this.viewHeight - scale2 * origHeight) * 0.5f);
            this.origWidth = origWidth;
            this.origHeight = origHeight;
            this.setImageMatrix(this.matrix);
            this.fixTrans();
        }
    }
    
    @SuppressLint({ "NewApi" })
    public void computeScroll() {
        super.computeScroll();
    }
    
    protected void onClick(final PointF pointF) {
        this.performClick();
    }
    
    protected void onDoubleClick(final PointF pointF) {
        this.performClick();
    }
    
    protected void onDraw(final Canvas canvas) {
        if (this.mScroller.computeScrollOffset()) {
            final float[] array = new float[9];
            this.matrix.getValues(array);
            this.translate(this.mScroller.getCurrX() - array[2], this.mScroller.getCurrY() - array[5]);
        }
        super.onDraw(canvas);
    }
    
    protected void onLongClick(final PointF pointF) {
        this.performLongClick();
    }
    
    public void onMatrixChanged() {
        this.fixTrans();
        this.setImageMatrix(this.matrix);
        this.invalidate();
    }
    
    protected void onMeasure(int viewWidth, int viewHeight) {
        super.onMeasure(viewWidth, viewHeight);
        this.viewWidth = View.MeasureSpec.getSize(viewWidth);
        this.viewHeight = View.MeasureSpec.getSize(viewHeight);
        if (this.oldMeasuredWidth != this.viewWidth || this.oldMeasuredHeight != this.viewHeight) {
            viewWidth = this.viewWidth;
            if (viewWidth != 0) {
                viewHeight = this.viewHeight;
                if (viewHeight == 0) {
                    return;
                }
                this.oldMeasuredHeight = viewHeight;
                this.oldMeasuredWidth = viewWidth;
            }
        }
    }
    
    protected void onScaleEnd(final float n) {
    }
    
    public PointF pixelToBitmapPos(final float n, final float n2) {
        final Matrix matrix = new Matrix();
        if (this.matrix.invert(matrix)) {
            final float[] array = { n, n2 };
            matrix.mapPoints(array);
            return new PointF(array[0], array[1]);
        }
        TouchImageView.LOG.warning("pixelToBitmapPos: Failed to invert matrix!");
        return new PointF(0.0f, 0.0f);
    }
    
    public void setCanScale(final boolean canScale) {
        this.canScale = canScale;
    }
    
    public void setClickSlop(final int clickSlop) {
        this.clickSlop = clickSlop;
    }
    
    public void setDelegate(final ImageViewDelegate delegate) {
        this.delegate = delegate;
    }
    
    public void setImageDrawable(final Drawable imageDrawable) {
        super.setImageDrawable(imageDrawable);
        this.origWidth = (float)imageDrawable.getIntrinsicWidth();
        this.origHeight = (float)imageDrawable.getIntrinsicHeight();
    }
    
    public void setMaxScale(final float maxScale) {
        this.maxScale = maxScale;
    }
    
    public void setMinScale(final float minScale) {
        this.minScale = minScale;
    }
    
    public void setScaleAndTranslate(final float scale, final float n, final float n2) {
        this.scale = scale;
        this.matrix.setScale(scale, scale);
        this.matrix.postTranslate(n, n2);
        this.onMatrixChanged();
    }
    
    public void setTranslate(final float n, final float n2) {
        this.setScaleAndTranslate(this.scale, n, n2);
    }
    
    public void translate(float fixDragTrans, float fixDragTrans2) {
        fixDragTrans = this.getFixDragTrans(fixDragTrans, (float)this.viewWidth, this.origWidth * this.scale);
        fixDragTrans2 = this.getFixDragTrans(fixDragTrans2, (float)this.viewHeight, this.origHeight * this.scale);
        this.matrix.postTranslate(fixDragTrans, fixDragTrans2);
        this.onMatrixChanged();
    }
    
    private class GestureListener extends GestureDetector.SimpleOnGestureListener
    {
        private final View mView;
        final /* synthetic */ TouchImageView this0;
        
        public GestureListener(final TouchImageView this0, final View mView) {
            super();
            this.this0 = this0;
            this.mView = mView;
        }
        
        public boolean onDown(final MotionEvent motionEvent) {
            this.this0.mScroller.forceFinished(true);
            ViewCompat.postInvalidateOnAnimation(this.mView);
            return true;
        }
        
        @SuppressLint({ "NewApi" })
        public boolean onFling(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
            final float[] array = new float[9];
            this.this0.matrix.getValues(array);
            this.this0.mScroller.fling((int)array[2], (int)array[5], (int)n, (int)n2, -(int)(array[0] * this.this0.origWidth), 0, -(int)(array[4] * this.this0.origHeight), 0);
            ViewCompat.postInvalidateOnAnimation(this.mView);
            return true;
        }
        
        public boolean onScroll(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
            ViewCompat.postInvalidateOnAnimation(this.mView);
            return true;
        }
    }
    
    public interface ImageViewDelegate
    {
        void scaleStarted();
        
        void scrollStarted();
    }
    
    private class LongClickDetector implements Runnable
    {
        private boolean enabled;
        final /* synthetic */ TouchImageView this0;
        
        private LongClickDetector(final TouchImageView this0) {
            super();
            this.this0 = this0;
            this.enabled = true;
        }
        
        LongClickDetector(final TouchImageView touchImageView, final TouchImageView1 viewOnTouchListener) {
            this(touchImageView);
        }
        
        public void disable() {
            this.enabled = false;
        }
        
        @Override
        public void run() {
            if (this.enabled) {
                this.this0.mode = Mode.NONE;
                final TouchImageView this0 = this.this0;
                this0.onLongClick(this0.pixelToBitmapPos(this0.last.x, this.this0.last.y));
            }
        }
    }

    private enum Mode {
        NONE,
        DRAG,
        ZOOM

    }
    
    public class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener
    {
        final /* synthetic */ TouchImageView this0;
        
        public ScaleListener(final TouchImageView this0) {
            super();
            this.this0 = this0;
        }
        
        public boolean onScale(final ScaleGestureDetector scaleGestureDetector) {
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            final float access1300 = this.this0.scale;
            final TouchImageView this0 = this.this0;
            this0.scale *= scaleFactor;
            Label_0128: {
                float n;
                if (this.this0.scale > this.this0.maxScale) {
                    final TouchImageView this2 = this.this0;
                    this2.scale = this2.maxScale;
                    n = this.this0.maxScale;
                }
                else {
                    if (this.this0.scale >= this.this0.minScale) {
                        break Label_0128;
                    }
                    final TouchImageView this3 = this.this0;
                    this3.scale = this3.minScale;
                    n = this.this0.minScale;
                }
                scaleFactor = n / access1300;
            }
            if (this.this0.origWidth * this.this0.scale > this.this0.viewWidth && this.this0.origHeight * this.this0.scale > this.this0.viewHeight) {
                this.this0.matrix.postScale(scaleFactor, scaleFactor, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
            }
            else {
                this.this0.matrix.postScale(scaleFactor, scaleFactor, this.this0.viewWidth / 2.0f, this.this0.viewHeight / 2.0f);
            }
            if (this.this0.delegate != null) {
                this.this0.delegate.scaleStarted();
            }
            this.this0.onMatrixChanged();
            return true;
        }
        
        public boolean onScaleBegin(final ScaleGestureDetector scaleGestureDetector) {
            Log.v("", "onScaleBeginonScaleBegin");
            if (!this.this0.canScale) {
                return false;
            }
            this.this0.mode = Mode.ZOOM;
            TouchImageView.this.endLongClickDetection();
            return true;
        }
        
        public void onScaleEnd(final ScaleGestureDetector scaleGestureDetector) {
            final TouchImageView this0 = this.this0;
            this0.onScaleEnd(this0.scale);
        }
    }

    class TouchImageView1 implements View.OnTouchListener {
        final /* synthetic */ TouchImageView this0;
        final /* synthetic */ GestureDetector valgdt;

        TouchImageView1(final TouchImageView this0, final GestureDetector valgdt) {
            super();
            this.this0 = this0;
            this.valgdt = valgdt;
        }

        @SuppressLint({ "NewApi" })
        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            TouchImageView.access000(this.this0).onTouchEvent(motionEvent);
            this.valgdt.onTouchEvent(motionEvent);
            final PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
            if (TouchImageView.access100(this.this0) == null) {
                TouchImageView.access102(this.this0, VelocityTracker.obtain());
            }
            TouchImageView.access100(this.this0).addMovement(motionEvent);
            final int action = motionEvent.getAction();
            if (action != 0) {
                if (action != 1) {
                    if (action != 2) {
                        if (action != 3 && action != 6) {
                            return true;
                        }
                        TouchImageView.access402(this.this0, TouchImageView.Mode.NONE);
                        TouchImageView.access500(this.this0);
                        return true;
                    }
                    else if (TouchImageView.access400(this.this0) == TouchImageView.Mode.DRAG) {
                        final float x = pointF.x;
                        final float x2 = TouchImageView.access200(this.this0).x;
                        final float y = pointF.y;
                        final float y2 = TouchImageView.access200(this.this0).y;
                        if (TouchImageView.access600(this.this0) && ((int)Math.abs(pointF.x - TouchImageView.access200(this.this0).x) > TouchImageView.access900(this.this0) || (int)Math.abs(pointF.y - TouchImageView.access200(this.this0).y) > TouchImageView.access900(this.this0))) {
                            TouchImageView.access500(this.this0);
                        }
                        if (!TouchImageView.access600(this.this0)) {
                            TouchImageView.access200(this.this0).set(pointF.x, pointF.y);
                            if (TouchImageView.access1000(this.this0) != null) {
                                TouchImageView.access1000(this.this0).scrollStarted();
                            }
                            this.this0.translate(x - x2, y - y2);
                            return true;
                        }
                    }
                }
                else {
                    TouchImageView.access100(this.this0).computeCurrentVelocity(1000);
                    if (TouchImageView.access400(this.this0) != TouchImageView.Mode.DRAG) {
                        TouchImageView.access402(this.this0, TouchImageView.Mode.NONE);
                        return true;
                    }
                    TouchImageView.access402(this.this0, TouchImageView.Mode.NONE);
                    final int n = (int)Math.abs(pointF.x - TouchImageView.access300(this.this0).x);
                    final int n2 = (int)Math.abs(pointF.y - TouchImageView.access300(this.this0).y);
                    if (n <= TouchImageView.access900(this.this0) && n2 <= TouchImageView.access900(this.this0)) {
                        final long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis - TouchImageView.access1100(this.this0) < ViewConfiguration.getDoubleTapTimeout()) {
                            final TouchImageView this0 = this.this0;
                            this0.onDoubleClick(this0.pixelToBitmapPos(pointF.x, pointF.y));
                        }
                        else {
                            final TouchImageView this2 = this.this0;
                            this2.onClick(this2.pixelToBitmapPos(pointF.x, pointF.y));
                        }
                        TouchImageView.access1102(this.this0, currentTimeMillis);
                    }
                    TouchImageView.access500(this.this0);
                    if (TouchImageView.access100(this.this0) != null) {
                        TouchImageView.access100(this.this0).recycle();
                        TouchImageView.access102(this.this0, (VelocityTracker)null);
                        return true;
                    }
                }
            }
            else {
                TouchImageView.access200(this.this0).set(pointF);
                TouchImageView.access300(this.this0).set(TouchImageView.access200(this.this0));
                TouchImageView.access402(this.this0, TouchImageView.Mode.DRAG);
                TouchImageView.access500(this.this0);
                TouchImageView.access602(this.this0, true);
                final TouchImageView this3 = this.this0;
                TouchImageView.access702(this3, new TouchImageView.LongClickDetector(this3, (TouchImageView1)null));
                final TouchImageView this4 = this.this0;
                this4.postDelayed((Runnable)TouchImageView.access700(this4), (long)ViewConfiguration.getLongPressTimeout());
            }
            return true;
        }
    }
}
