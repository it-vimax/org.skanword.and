package org.skanword.and.scanwordgame;

import android.widget.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class PlayLayout extends RelativeLayout
{
    private boolean isViewKeyBoard;
    private IChandeKeyboardViewListener listener;
    
    public PlayLayout(final Context context) {
        super(context);
    }
    
    public PlayLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public PlayLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.isViewKeyBoard = (this.getHeight() > View.MeasureSpec.getSize(n2));
        final IChandeKeyboardViewListener listener = this.listener;
        if (listener != null) {
            listener.onChange(this.isViewKeyBoard);
        }
        super.onMeasure(n, n2);
    }
    
    public void setOnChangeKeyboadListener(final IChandeKeyboardViewListener listener) {
        this.listener = listener;
    }
    
    public interface IChandeKeyboardViewListener
    {
        void onChange(final boolean p0);
    }
}
