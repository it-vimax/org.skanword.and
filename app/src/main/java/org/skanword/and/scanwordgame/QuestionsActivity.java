package org.skanword.and.scanwordgame;

import org.skanword.and.etc.*;
import org.skanword.and.datamanager.*;
import com.astuetz.*;
import org.skanword.and.*;
import android.os.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.*;
import android.view.*;
import android.app.*;
import java.io.*;
import android.net.*;
import android.content.*;
import android.util.*;
import java.util.*;
import android.widget.*;
import android.graphics.*;

public class QuestionsActivity extends FragmentActivity implements ActionBar.TabListener, TopActivityManager.TopActivity
{
    public static final String EXTRA_CROSS = "cross";
    public static final String EXTRA_QUESTIONS = "EXTRA_QUESTIONS";
    public static final String EXTRA_SOLVE_STATE = "EXTRA_SOLVE_STATE";
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    private Skanword mSkanword;
    ViewPager mViewPager;
    private PagerSlidingTabStrip tabs;
    
    public QuestionsActivity() {
        super();
    }
    
    public static SkanwordBox[][] getBoxes() {
        if (SkanwordGameActivity.sGetSkanwordBoard() != null) {
            return SkanwordGameActivity.sGetSkanwordBoard().getBoxes();
        }
        return null;
    }
    
    public void buildKeyword(final LinearLayout linearLayout) {
        if (this.mSkanword.getKeyword() != null && this.mSkanword.getKeyword().getCells().size() >= 1) {
            final LayoutInflater from = LayoutInflater.from(SmappsScanwords.getContext());
            linearLayout.removeAllViewsInLayout();
            final SkanwordBox[][] boxes = getBoxes();
            for (final Skanword.Keyword.KeywordCell keywordCell : this.mSkanword.getKeyword().getCells()) {
                SkanwordBox skanwordBox;
                if (boxes != null) {
                    skanwordBox = boxes[keywordCell.getX()][keywordCell.getY()];
                }
                else {
                    skanwordBox = null;
                }
                if (skanwordBox == null) {
                    this.finish();
                    return;
                }
                final String latterResponse = skanwordBox.getLatterResponse();
                int n = 0;
                Label_0161: {
                    if (skanwordBox != null) {
                        if (!skanwordBox.isHintOpen()) {
                            n = n;
                            if (!skanwordBox.isComplete()) {
                                break Label_0161;
                            }
                        }
                        n = 1;
                    }
                }
                final View inflate = from.inflate(R.layout.question_letter, (ViewGroup)null);
                linearLayout.addView(inflate);
                final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)inflate.getLayoutParams();
                layoutParams.rightMargin = 10;
                layoutParams.bottomMargin = 2;
                inflate.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                inflate.findViewById(R.id.letter_view).setBackgroundResource(R.drawable.unguessed_keyword_letter_cell);
                if (n != 0) {
                    inflate.findViewById(R.id.letter_view).setBackgroundResource(R.drawable.guessed_keyword_letter_cell);
                }
                if (latterResponse.equalsIgnoreCase(" ")) {
                    continue;
                }
                ((TextView)inflate.findViewById(R.id.letter_text)).setText((CharSequence)latterResponse);
            }
            return;
        }
        linearLayout.setVisibility(View.GONE);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(R.layout.questions);
        this.mSkanword = (Skanword)this.getIntent().getSerializableExtra("EXTRA_SCANWORD_OBJECT");
        this.getWindow().setSoftInputMode(3);
        this.mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(this.getSupportFragmentManager(), this.mSkanword);
        this.tabs = (PagerSlidingTabStrip)this.findViewById(R.id.tabs_questions);
        (this.mViewPager = (ViewPager)this.findViewById(R.id.pager_questions)).setAdapter(this.mAppSectionsPagerAdapter);
        this.tabs.setViewPager(this.mViewPager);
        this.mAppSectionsPagerAdapter.notifyDataSetChanged();
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Список вопросов");
        this.buildKeyword((LinearLayout)this.findViewById(R.id.keywordLayout));
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.finish();
        return true;
    }
    
    public void onTabReselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
    }
    
    public void onTabSelected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
        this.mViewPager.setCurrentItem(actionBarTab.getPosition());
    }
    
    public void onTabUnselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
    }

    public static class AppSectionsPagerAdapter extends FragmentStatePagerAdapter
    {
        private Skanword mScanword;

        public AppSectionsPagerAdapter(final FragmentManager fragmentManager, final Skanword mScanword) {
            super(fragmentManager);
            this.mScanword = mScanword;
        }

        private List<Skanword.Question> getQuestionsForList(final int n) {
            final ArrayList<Skanword.Question> list = new ArrayList<Skanword.Question>();
            for (final Skanword.Question skanwordQuestion : this.mScanword.getQuestions()) {
                if (n == 0 && !skanwordQuestion.isComplete()) {
                    list.add(skanwordQuestion);
                }
                else {
                    if (n != 1 || !skanwordQuestion.isComplete()) {
                        continue;
                    }
                    list.add(skanwordQuestion);
                }
            }
            return list;
        }

        public int getCount() {
            return 2;
        }

        public Fragment getItem(final int n) {
            final QuestionsActivity.QuestionsFragment fragment = new QuestionsActivity.QuestionsFragment();
            final Bundle arguments = new Bundle();
            arguments.putSerializable("EXTRA_QUESTIONS", (Serializable)this.getQuestionsForList(n));
            arguments.putSerializable("cross", (Serializable)this.mScanword);
            fragment.setArguments(arguments);
            if (n == 0) {
                fragment.setEmptyLabelText("Ура! Все решено!");
                return (Fragment)fragment;
            }
            if (n != 1) {
                return (Fragment)fragment;
            }
            fragment.setEmptyLabelText("У вас нет ни одного решенного вопроса");
            return (Fragment)fragment;
        }

        public int getItemPosition(final Object o) {
            return -2;
        }

        public CharSequence getPageTitle(final int n) {
            int n2;
            if (this.mScanword.isFinished()) {
                n2 = this.mScanword.getQuestions().size();
            }
            else {
                n2 = this.mScanword.getFinishedQuestionsCount();
            }
            if (n == 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("РЕШЁННЫЕ(");
                sb.append(this.mScanword.getQuestions().size() - n2);
                sb.append(")");
                return sb.toString();
            }
            if (n != 1) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Section ");
                sb2.append(n + 1);
                return sb2.toString();
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("НЕРЕШЁННЫЕ(");
            sb3.append(n2);
            sb3.append(")");
            return sb3.toString();
        }
    }


    private interface ItemAction
    {
        void clickItem(final Skanword.Question p0);
    }
    
    public static class QuestionsFragment extends Fragment
    {
        private ItemAction mActions;
        private QuestionsListAdapter mAdapter;
        private TextView mEmptyListLabel;
        private String mEmptyText;
        private ListView mListView;
        private List<Skanword.Question> mQuestions;
        private Skanword mScanword;
        private String mScanwordImagesDir;
        
        public QuestionsFragment() {
            super();
            this.mListView = null;
            this.mAdapter = null;
            this.mQuestions = null;
            this.mActions = null;
            this.mEmptyListLabel = null;
            this.mEmptyText = "";
            this.mScanwordImagesDir = "";
        }
        
        static /* synthetic */ List access000(final QuestionsFragment questionsFragment) {
            return questionsFragment.mQuestions;
        }
        
        static /* synthetic */ void access100(final QuestionsFragment questionsFragment, final Skanword.Question question) {
            questionsFragment.openQuestion(question);
        }
        
        private void openQuestion(final Skanword.Question question) {
            final Intent intent = new Intent("android.intent.action.EDIT", (Uri)null, (Context)this.getActivity(), (Class)SkanwordGameActivity.class);
            intent.putExtra("cross_id", this.mScanword.getId());
            intent.putExtra("sel_question", (Serializable)question);
            this.getActivity().setResult(-1, intent);
            this.getActivity().finish();
        }
        
        @Override
        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, Bundle arguments) {
            arguments = this.getArguments();
            final View inflate = layoutInflater.inflate(R.layout.questions_list, viewGroup, false);
            (this.mListView = (ListView)inflate.findViewById(R.id.questions_list)).setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
            this.mQuestions = (List<Skanword.Question>)arguments.getSerializable(QuestionsActivity.EXTRA_QUESTIONS);
            Log.v("SkanwordsTime", "question sort  1");
            for (final Skanword.Question question : this.mQuestions) {
                question.setUnguessedCellsCount(0);
                if (question.isComplete()) {
                    continue;
                }
                final boolean verticalOrientation = question.getQuestionData().isVerticalOrientation();
                for (int i = 0; i < question.getAnswer().getLength(); ++i) {
                    final int x = question.getAnswer().getX();
                    int n;
                    if (verticalOrientation) {
                        n = 0;
                    }
                    else {
                        n = i;
                    }
                    final int y = question.getAnswer().getY();
                    int n2;
                    if (verticalOrientation) {
                        n2 = i;
                    }
                    else {
                        n2 = 0;
                    }
                    SkanwordBox skanwordBox;
                    if (QuestionsActivity.getBoxes() != null) {
                        skanwordBox = QuestionsActivity.getBoxes()[x + n][y + n2];
                    }
                    else {
                        skanwordBox = null;
                    }
                    if (skanwordBox == null || (!skanwordBox.isHintOpen() && !skanwordBox.isComplete())) {
                        question.setUnguessedCellsCount(question.getUnguessedCellsCount() + 1);
                    }
                }
            }
            Collections.sort(this.mQuestions, new QuestionsFragment1(this));
            Log.v("SkanwordsTime", "question sort  2");
            this.mScanword = (Skanword)arguments.getSerializable("cross");
            this.mScanwordImagesDir = this.mScanword.getImageDir();
            this.mEmptyListLabel = (TextView)inflate.findViewById(R.id.empty_quesstions_list);
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mAdapter);
            sb.append("onCreateView questionsActivity ");
            sb.append(this.mScanword);
            Log.v("", sb.toString());
            this.mAdapter = new QuestionsActivity.QuestionsListAdapter((Context)this.getActivity(), R.layout.question_row_item, this.mQuestions, this.mScanwordImagesDir, this.mScanword, this.mActions);
            this.mListView.setAdapter((ListAdapter)this.mAdapter);
            if (this.mQuestions.size() > 0) {
                this.mEmptyListLabel.setVisibility(View.INVISIBLE);
            }
            else {
                this.mEmptyListLabel.setVisibility(View.VISIBLE);
            }
            this.mEmptyListLabel.setText((CharSequence)this.mEmptyText);
            this.mListView.setOnItemClickListener((AdapterView.OnItemClickListener)new QuestionsFragment2(this));
            return inflate;
        }
        
        @Override
        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            Log.v("", "  onSaveInstanceState ");
        }
        
        public void setEmptyLabelText(final String mEmptyText) {
            this.mEmptyText = mEmptyText;
        }

        class QuestionsFragment1
                implements Comparator<Skanword.Question> {
            final /* synthetic */ QuestionsActivity.QuestionsFragment this0;

            QuestionsFragment1(QuestionsActivity.QuestionsFragment questionsFragment) {
                this.this0 = questionsFragment;
            }

            @Override
            public int compare(Skanword.Question question, Skanword.Question question2) {
                if (question.hasKeywordCell() && !question2.hasKeywordCell()) {
                    return -1;
                }
                if (!question.hasKeywordCell() && question2.hasKeywordCell()) {
                    return 1;
                }
                if (question.getUnguessedCellsCount() < question2.getUnguessedCellsCount()) {
                    return -1;
                }
                if (question2.getUnguessedCellsCount() < question.getUnguessedCellsCount()) {
                    return 1;
                }
                if (question.getGuessedCellsCount() > question2.getGuessedCellsCount()) {
                    return -1;
                }
                if (question2.getGuessedCellsCount() <= question.getGuessedCellsCount()) return 0;
                return 1;
            }
        }

        class QuestionsFragment2 implements AdapterView.OnItemClickListener {
            final /* synthetic */ QuestionsFragment this0;

            QuestionsFragment2(final QuestionsFragment this0) {
                super();
                this.this0 = this0;
            }

            public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                final QuestionsFragment this0 = this.this0;
                QuestionsFragment.access100(this0, (Skanword.Question)QuestionsFragment.access000(this0).get(n));
            }
        }
    }
    
    public static class QuestionsListAdapter extends ArrayAdapter<Skanword.Question>
    {
        private SkanwordBox[][] mBoxes;
        private final int mResourceId;
        private Skanword mScanword;
        private final String mScanwordImagesDir;
        
        public QuestionsListAdapter(final Context context, final int mResourceId, final List<Skanword.Question> list, final String mScanwordImagesDir, final Skanword mScanword, final ItemAction itemAction) {
            super(context, mResourceId, (List)list);
            this.mScanwordImagesDir = mScanwordImagesDir;
            this.mResourceId = mResourceId;
            this.mScanword = mScanword;
            this.mBoxes = QuestionsActivity.getBoxes();
        }
        
        private View createRowView(final ViewGroup viewGroup) {
            return LayoutInflater.from(this.getContext()).inflate(this.mResourceId, viewGroup, false);
        }
        
        private void rebuildView(final View view, final Skanword.Question question) {
            ((TextView)view.findViewById(R.id.question_text)).setText((CharSequence)question.getQuestionData().getQuestionValue().getOriginal());
            if (!question.getQuestionData().getQuestionValue().isTextQuestion()) {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.mScanwordImagesDir);
                sb.append("/");
                sb.append(question.getQuestionData().getQuestionValue().getUrl());
                final Bitmap decodeFile = BitmapFactory.decodeFile(sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(this.mScanwordImagesDir);
                sb2.append("/");
                sb2.append(question.getQuestionData().getQuestionValue().getUrl());
                Log.v("", sb2.toString());
                ((ImageView)view.findViewById(R.id.question_image)).setImageBitmap(decodeFile);
                view.findViewById(R.id.question_image).setVisibility(View.GONE);
                view.findViewById(R.id.question_text).setVisibility(View.VISIBLE);
            }
            else {
                view.findViewById(R.id.question_image).setVisibility(View.VISIBLE);
                view.findViewById(R.id.question_text).setVisibility(View.GONE);
            }
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            final LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.letters);
            linearLayout.removeAllViewsInLayout();
            final boolean verticalOrientation = question.getQuestionData().isVerticalOrientation();
            for (int i = 0; i < question.getAnswer().getLength(); ++i) {
                final int x = question.getAnswer().getX();
                int n;
                if (verticalOrientation) {
                    n = 0;
                }
                else {
                    n = i;
                }
                final int n2 = x + n;
                final int y = question.getAnswer().getY();
                int n3;
                if (verticalOrientation) {
                    n3 = i;
                }
                else {
                    n3 = 0;
                }
                final int n4 = y + n3;
                final SkanwordBox[][] mBoxes = this.mBoxes;
                SkanwordBox skanwordBox;
                if (mBoxes != null) {
                    skanwordBox = mBoxes[n2][n4];
                }
                else {
                    skanwordBox = null;
                }
                final int n5 = n2 * this.mScanword.getSettings().getHeight() + n4;
                final String substring = this.mScanword.getAnswers().substring(n5, n5 + 1);
                final boolean b = skanwordBox != null && (skanwordBox.isHintOpen() || skanwordBox.isComplete());
                final View inflate = from.inflate(R.layout.question_letter, (ViewGroup)null);
                linearLayout.addView(inflate);
                final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)inflate.getLayoutParams();
                layoutParams.rightMargin = 10;
                layoutParams.bottomMargin = 2;
                inflate.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                if (!question.isComplete() && !b) {
                    final View viewById = inflate.findViewById(R.id.letter_text);
                    int backgroundResource;
                    if (skanwordBox.getKeywordLetterNumber() > 0) {
                        backgroundResource = R.drawable.guessed_keyword_letter_cell;
                    }
                    else {
                        backgroundResource = R.drawable.guessed_simple_letter_cell;
                    }
                    viewById.setBackgroundResource(backgroundResource);
                }
                else {
                    final View viewById2 = inflate.findViewById(R.id.letter_text);
                    int backgroundResource2;
                    if (skanwordBox.getKeywordLetterNumber() > 0) {
                        backgroundResource2 = R.drawable.guessed_keyword_letter_cell;
                    }
                    else {
                        backgroundResource2 = R.drawable.guessed_simple_letter_cell;
                    }
                    viewById2.setBackgroundResource(backgroundResource2);
                }
                if (!substring.equalsIgnoreCase(" ")) {
                    ((TextView)inflate.findViewById(R.id.letter_text)).setText((CharSequence)substring);
                }
            }
        }
        
        public View getView(final int n, View rowView, final ViewGroup viewGroup) {
            if (rowView != null) {
                this.rebuildView(rowView, (Skanword.Question)this.getItem(n));
                return rowView;
            }
            rowView = this.createRowView(viewGroup);
            this.rebuildView(rowView, (Skanword.Question)this.getItem(n));
            return rowView;
        }
    }
}
