package org.skanword.and.scanwordgame;

import android.app.*;

import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import org.skanword.and.libs.qustom.QustomDialogBuilder;
import org.skanword.and.network.*;
import org.skanword.and.network.socialnetworks.*;
import com.nostra13.universalimageloader.core.*;
import android.widget.*;
import org.skanword.and.datamanager.*;
import org.skanword.and.etc.*;
import android.content.*;
import android.os.*;
import org.skanword.and.*;
import android.view.*;

public class GameFinishActivity extends Activity
{
    private EventListener mProgressUpdateListener;
    private Skanword mScanword;
    private EventListener mUserInfoEventListener;
    
    public GameFinishActivity() {
        super();
    }
    
    static /* synthetic */ void access000(final GameFinishActivity gameFinishActivity) {
        gameFinishActivity.showAuthDialog();
    }
    
    static /* synthetic */ void access100(final GameFinishActivity gameFinishActivity) {
        gameFinishActivity.updateUserInfo();
    }

    private void showAuthDialog() {
        final View inflate = LayoutInflater.from((Context)this).inflate(R.layout.auth_dialog_layout, (ViewGroup)null);
        final QustomDialogBuilder setTitleColor = new QustomDialogBuilder((Context)this).setDividerColor("#44000000").setTitle("Авторизироваться сейчас:").setTitleColor("#000000");
        setTitleColor.setPositiveButton("Отмена", (DialogInterface.OnClickListener)new GameFinishActivity.GameFinishActivity6(this));
        setTitleColor.setCustomView(R.layout.auth_dialog_layout, inflate.getContext());
        final GameFinishActivity.GameFinishActivity7 onClickListener = new GameFinishActivity.GameFinishActivity7(this, setTitleColor.show());
        ((Button)inflate.findViewById(R.id.okAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
        ((Button)inflate.findViewById(R.id.fbAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
        ((Button)inflate.findViewById(R.id.vkAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
    }
    
    private void updateUserInfo() {
        final View viewById = this.findViewById(R.id.authLayout);
        final View viewById2 = this.findViewById(R.id.authedLayout);
        final View viewById3 = this.findViewById(R.id.noNetworkLabel);
        if (!MainNetworkManager.getInstance().hasNetworkConnection()) {
            viewById.setVisibility(View.GONE);
            viewById2.setVisibility(View.GONE);
            viewById3.setVisibility(View.VISIBLE);
            return;
        }
        viewById3.setVisibility(View.GONE);
        if (MainNetworkManager.getInstance().isUserAuthorised()) {
            viewById.setVisibility(View.GONE);
            viewById2.setVisibility(View.VISIBLE);
            if (MainNetworkManager.getInstance().getUserAuth().getAvatarUrl() != null) {
                ImageLoader.getInstance().displayImage(MainNetworkManager.getInstance().getUserAuth().getAvatarUrl(), (ImageView)this.findViewById(R.id.profile_image), new DisplayImageOptions.Builder().cacheInMemory(true).showImageForEmptyUri(R.drawable.avatar).showImageOnFail(R.drawable.avatar).build());
            }
            ((TextView)this.findViewById(R.id.userNameLabel)).setText((CharSequence)MainNetworkManager.getInstance().getUserAuth().getFullName());
            final TextView textView = (TextView)this.findViewById(R.id.placeLabel);
            final StringBuilder sb = new StringBuilder();
            sb.append(MainDataManager.getInstance().getUserData().getPlaceScanwords());
            sb.append(" ");
            sb.append(getResources().getString(R.string.place_label));
            textView.setText((CharSequence)sb.toString());
            final TextView textView2 = (TextView)this.findViewById(R.id.pointsLabel);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(MainDataManager.getInstance().getUserData().getScoreScanwords());
            sb2.append(" ");
            sb2.append(Utils.spellVariantForNumber(MainDataManager.getInstance().getUserData().getScoreScanwords(), "очко", "очка", "очков"));
            textView2.setText((CharSequence)sb2.toString());
            return;
        }
        viewById.setVisibility(View.VISIBLE);
        viewById2.setVisibility(View.GONE);
    }
    
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        MainNetworkManager.getInstance().onActivityResult(this, n, n2, intent);
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(R.layout.game_finish_activity);
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Поздравляем!");
        this.getWindow().setSoftInputMode(3);
        this.mScanword = (Skanword)this.getIntent().getSerializableExtra("EXTRA_SCANWORD_OBJECT");
        int n;
        if (this.mScanword.getLevel() == 1) {
            n = 150;
        }
        else if (this.mScanword.getLevel() == 2) {
            n = 200;
        }
        else if (this.mScanword.getLevel() == 3) {
            n = 250;
        }
        else {
            n = 0;
        }
        final TextView textView = (TextView)this.findViewById(R.id.difficultyEarnedPointsLabel);
        final StringBuilder sb = new StringBuilder();
        sb.append(n);
        sb.append(" ");
        sb.append(Utils.spellVariantForNumber(n, "очков", "очка", "очко"));
        sb.append(" ");
        sb.append(R.string.difficalty_points_label);
        textView.setText((CharSequence)sb.toString());
        final TextView textView2 = (TextView)this.findViewById(R.id.answersEarnedPointsLabel);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.mScanword.getQuestions().size());
        sb2.append(" ");
        sb2.append(Utils.spellVariantForNumber(this.mScanword.getQuestions().size(), "очков", "очка", "очко"));
        sb2.append(" ");
        sb2.append(getResources().getString(R.string.difficalty_points_label));
        textView2.setText((CharSequence)sb2.toString());
        final TextView textView3 = (TextView)this.findViewById(R.id.answersEarnedPointsLabel);
        final int n2 = n + this.mScanword.getQuestions().size();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(n2);
        sb3.append(" ");
        sb3.append(Utils.spellVariantForNumber(n2, "очков", "очка", "очко"));
        sb3.append(" ");
        sb3.append(R.string.answers_points_label);
        textView3.setText(sb3.toString());

        this.findViewById(R.id.authLayout).setOnClickListener((View.OnClickListener)new GameFinishActivity.GameFinishActivity1(this));
        this.findViewById(R.id.noNetworkLabel).setOnClickListener((View.OnClickListener)new GameFinishActivity.GameFinishActivity2(this));
        ((Button)this.findViewById(R.id.newScanwordButton)).setOnClickListener((View.OnClickListener)new GameFinishActivity.GameFinishActivity3(this));
        this.mUserInfoEventListener = (EventListener)new GameFinishActivity.GameFinishActivity4(this);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_USER_INFO_UPDATE", this.mUserInfoEventListener);
        this.mProgressUpdateListener = (EventListener)new GameFinishActivity.GameFinishActivity5(this);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_PROGRESS_UPDATE", this.mProgressUpdateListener);
        this.updateUserInfo();
    }
    
    protected void onDestroy() {
        super.onDestroy();
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_USER_INFO_UPDATE", this.mUserInfoEventListener);
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_PROGRESS_UPDATE", this.mProgressUpdateListener);
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.setResult(0, (Intent)null);
        this.finish();
        return true;
    }

    class GameFinishActivity6 implements DialogInterface.OnClickListener {
        final /* synthetic */ GameFinishActivity this0;

        GameFinishActivity6(final GameFinishActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    class GameFinishActivity7 implements View.OnClickListener {
        final /* synthetic */ GameFinishActivity this0;
        final /* synthetic */ AlertDialog valdialog;

        GameFinishActivity7(final GameFinishActivity this0, final AlertDialog valdialog) {
            super();
            this.this0 = this0;
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            if (view.getId() == R.id.fbAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB, (Activity)this.this0, true);
            }
            else if (view.getId() == R.id.okAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK, (Activity)this.this0, true);
            }
            else if (view.getId() == R.id.vkAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK, (Activity)this.this0, true);
            }
            this.valdialog.dismiss();
        }
    }

    class GameFinishActivity1 implements View.OnClickListener {
        final /* synthetic */ GameFinishActivity this0;

        GameFinishActivity1(final GameFinishActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            GameFinishActivity.access000(this.this0);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_gamefinish_login");
        }
    }

    class GameFinishActivity2 implements View.OnClickListener {
        final /* synthetic */ GameFinishActivity this0;

        GameFinishActivity2(final GameFinishActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
        }
    }


    class GameFinishActivity3 implements View.OnClickListener {
        final /* synthetic */ GameFinishActivity this0;

        GameFinishActivity3(final GameFinishActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            this.this0.setResult(-1, (Intent)null);
            this.this0.finish();
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_gamefinish_chooseanother");
        }
    }

    class GameFinishActivity4 implements EventListener {
        final /* synthetic */ GameFinishActivity this0;

        GameFinishActivity4(final GameFinishActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new GameFinishActivity4.GameFinishActivity41(this));
        }

        class GameFinishActivity41 implements Runnable {
            final /* synthetic */ GameFinishActivity4 this1;

            GameFinishActivity41(final GameFinishActivity4 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                GameFinishActivity.access100(this.this1.this0);
            }
        }
    }

    class GameFinishActivity5 implements EventListener {
        final /* synthetic */ GameFinishActivity this0;

        GameFinishActivity5(final GameFinishActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new GameFinishActivity5.GameFinishActivity51(this));
        }

        class GameFinishActivity51 implements Runnable {
            final /* synthetic */ GameFinishActivity5 this1;

            GameFinishActivity51(final GameFinishActivity5 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                this.this1.this0.setResult(555, (Intent)null);
                this.this1.this0.finish();
            }
        }
    }
}
