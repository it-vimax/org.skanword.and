package org.skanword.and.scanwordgame;

import java.io.*;
import android.content.res.*;
import org.skanword.and.datamanager.*;
import java.util.*;
import android.support.v4.view.ViewCompat;
import android.util.*;
import android.graphics.*;

public class SkanwordBox implements Serializable
{
    public static final int BOX_SIZE = 30;
    public static final float SELECTED_SCALE = 1.1f;
    private static AssetManager assetManager;
    private static final long serialVersionUID = 1L;
    private List<ArrowType> arrowTypes;
    private BoxType boxType;
    private boolean isActiveWorld;
    private boolean isAddedArrow;
    private boolean isComplete;
    private boolean isHintOpen;
    private boolean isSelected;
    private int keywordLetterNumber;
    private Paint questionText;
    private final List<Skanword.Question> questions;
    private String response;
    private final String skanwordDirName;
    private Bitmap tempBitmap;
    private final int x;
    private final int y;
    
    public SkanwordBox(final int x, final int y, final String skanwordDirName, final AssetManager assetManager) {
        super();
        this.keywordLetterNumber = 0;
        this.x = x;
        this.y = y;
        this.skanwordDirName = skanwordDirName;
        if (SkanwordBox.assetManager == null) {
            SkanwordBox.assetManager = assetManager;
        }
        this.boxType = BoxType.LETTER;
        this.questions = new ArrayList<Skanword.Question>(2);
        (this.questionText = new Paint()).setTextAlign(Paint.Align.CENTER);
        this.questionText.setColor(-16777216);
        this.questionText.setAntiAlias(true);
        this.questionText.setTypeface(this.boxType.getQuestionFont());
        this.isAddedArrow = false;
        this.arrowTypes = new ArrayList<ArrowType>(8);
        this.isSelected = false;
        this.isActiveWorld = false;
        this.response = " ";
    }
    
    static /* synthetic */ AssetManager access100() {
        return SkanwordBox.assetManager;
    }
    
    private boolean isCompleteCheck() {
        final boolean isComplete = this.isComplete;
        final List<Skanword.Question> questions = this.questions;
        boolean b = isComplete;
        if (questions != null && !(b = isComplete)) {
            final Iterator<Skanword.Question> iterator = questions.iterator();
            do {
                b = isComplete;
                if (iterator.hasNext()) {
                    continue;
                }
                return b;
            } while (!iterator.next().isComplete());
            b = true;
            this.isComplete = true;
        }
        return b;
    }
    
    public void addArrow(final ArrowType arrowType) {
        this.isAddedArrow = true;
        this.arrowTypes.add(arrowType);
    }
    
    public void addQuestionBox(final Skanword.Question question, final Boolean b) {
        BoxType boxType;
        if (b) {
            boxType = BoxType.MAIN_QUESTION;
        }
        else {
            boxType = BoxType.SLAVE_QUESTION;
        }
        this.boxType = boxType;
        this.addQuestionRelation(question);
    }
    
    public void addQuestionRelation(final Skanword.Question question) {
        if (this.getQuestions().size() < 2) {
            this.getQuestions().add(question);
            return;
        }
        throw new RuntimeException("full questions List");
    }
    
    public void drawArrow(final Canvas canvas, final int n, final int n2, final float n3) {
        if (this.isAddedArrow) {
            final Iterator<ArrowType> iterator = this.arrowTypes.iterator();
            while (iterator.hasNext()) {
                iterator.next().draw(canvas, n, n2, n3);
            }
        }
    }
    
    public void drawBackground(final Canvas canvas, final int n, int n2, float borderSize) {
        if (canvas == null) {
            return;
        }
        final int n3 = (int)(30.0f * borderSize);
        final int n4 = SkanwordBox1.boxType[this.boxType.ordinal()];
        if (n4 != 1) {
            if (n4 != 2) {
                return;
            }
            final List<Skanword.Question> questions = this.getQuestions();
            final int n5 = 0;
            if (questions.get(0).getQuestionData().getQuestionValue().isTextQuestion()) {
                this.boxType.drawTextBackground(canvas, n, n2, borderSize);
                borderSize *= 5.0f;
                this.questionText.setTextSize(borderSize);
                final double n6 = borderSize;
                Double.isNaN(n6);
                final int n7 = (int)(n6 * 1.2);
                if (this.getQuestions().size() != 1) {
                    throw new RuntimeException("Wrong Question data in Box");
                }
                final String[] split = this.getQuestions().get(0).getQuestionData().getQuestionValue().getFormatted().split("_");
                final float n8 = (float)(n3 / 2);
                final int n9 = n2 + Math.round(n8 - borderSize);
                int i = n5;
                n2 = n9;
                if (split.length % 2 == 0) {
                    n2 = n9 - n7 / 2;
                    i = n5;
                }
                while (i < split.length) {
                    canvas.drawText(split[i], (float)(Math.round(n8) + n), (float)n2, this.questionText);
                    n2 += n7;
                    ++i;
                }
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.skanwordDirName);
                sb.append("/");
                sb.append(this.getQuestions().get(0).getQuestionData().getQuestionValue().getUrl());
                final String string = sb.toString();
                if (this.getTempBitmap() == null) {
                    this.tempBitmap = BitmapFactory.decodeFile(string);
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(this.skanwordDirName);
                sb2.append(" tempBitmap ");
                sb2.append(this.tempBitmap);
                Log.v("SkanwordsFunc", sb2.toString());
                borderSize = this.getBorderSize(borderSize);
                final int round = Math.round(this.getQuestions().get(0).getQuestionData().getQuestionValue().getWidth() * n3 - borderSize);
                final int round2 = Math.round(n3 * this.getQuestions().get(0).getQuestionData().getQuestionValue().getHeight() - borderSize);
                if (this.getTempBitmap() != null) {
                    canvas.drawBitmap(Bitmap.createScaledBitmap(this.getTempBitmap(), round, round2, false), (float)Math.round(n + borderSize), (float)Math.round(n2 + borderSize), (Paint)null);
                }
            }
        }
        else {
            this.drawLetterBackground(canvas, n, n2, borderSize);
        }
    }
    
    public void drawKeywordFrame(final Canvas canvas, final int n, final int n2, final float n3, final int n4) {
        this.boxType.drawKeywordFrame(canvas, n, n2, n3, n4);
    }
    
    public void drawLetterBackground(final Canvas canvas, final int n, final int n2, final float n3) {
        this.isCompleteCheck();
        if (SkanwordBox1.boxType[this.boxType.ordinal()] != 1) {
            return;
        }
        if (this.isSelected()) {
            if (!this.isComplete && !this.isHintOpen) {
                this.boxType.drawLetterSelected(canvas, n, n2, n3);
                return;
            }
            this.boxType.drawCompleteLetterSelected(canvas, n, n2, n3);
        }
        else if (this.isActiveWorld()) {
            if (!this.isComplete && !this.isHintOpen) {
                this.boxType.drawLetterWordSelected(canvas, n, n2, n3);
                return;
            }
            this.boxType.drawCompleteLetterWordSelected(canvas, n, n2, n3);
        }
        else {
            if (!this.isComplete && !this.isHintOpen) {
                this.boxType.drawBackground(canvas, n, n2, n3);
                return;
            }
            this.boxType.drawCompleteBackground(canvas, n, n2, n3);
        }
    }
    
    public void drawResultLetters(final Canvas canvas, final int n, final int n2, final float n3) {
        this.boxType.drawResponseLettter(canvas, n, n2, this.response, this.isComplete || this.isHintOpen, n3);
    }
    
    public float getBorderSize(final float n) {
        return getBorderSize(n);
    }
    
    public BoxType getBoxType() {
        return this.boxType;
    }
    
    public int getKeywordLetterNumber() {
        return this.keywordLetterNumber;
    }
    
    public String getLatterResponse() {
        return this.response;
    }
    
    public List<Skanword.Question> getQuestions() {
        return this.questions;
    }
    
    public Bitmap getTempBitmap() {
        return this.tempBitmap;
    }
    
    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
    
    public boolean isActiveWorld() {
        return this.isActiveWorld;
    }
    
    public boolean isComplete() {
        return this.isComplete;
    }
    
    public boolean isHintOpen() {
        return this.isHintOpen;
    }
    
    public boolean isSelected() {
        return this.isSelected;
    }
    
    public void setCompletition() {
        this.isComplete = true;
    }
    
    public void setHighlightLetter(final boolean isSelected) {
        this.isSelected = isSelected;
    }
    
    public void setHighlightdWord(final boolean isActiveWorld) {
        this.isActiveWorld = isActiveWorld;
    }
    
    public void setHintOpen(final boolean isHintOpen) {
        this.isHintOpen = isHintOpen;
    }
    
    public void setKeywordLetterNumber(final int keywordLetterNumber) {
        this.keywordLetterNumber = keywordLetterNumber;
    }
    
    public void setResponse(final String response) {
        if (!this.isComplete && !this.isHintOpen) {
            this.response = response;
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BOX[");
        sb.append(this.x);
        sb.append(", ");
        sb.append(this.y);
        sb.append("] type =");
        sb.append(this.boxType);
        sb.append("  letter =");
        sb.append(this.response);
        return sb.toString();
    }

    // ---- Classes

    public enum BoxType {
        LETTER(Integer.valueOf(0)),
        MAIN_QUESTION(Integer.valueOf(1)),
        SLAVE_QUESTION(Integer.valueOf(2));

        private static final float BORDER_SIZE = 0.75f;
        private static final int[][] GRARIENTS_COLORS;
        private final Rect boxRect;
        private final Paint completeLetterdBox;
        private final Paint completeSelecteLetterWordBox;
        private final Paint completeSelecteLetterdBox;
        private final Typeface[] fonts;
        private final int id;
        private final Paint responsePaint;
        private final Paint selecteLetterWordBox;
        private final Paint selecteLetterdBox;
        private final Paint textBox;
        private final Paint whiteBox;

        static {
            int[][] iArr = new int[5][];
            iArr[0] = new int[]{Color.parseColor("#c2e0f4"), Color.parseColor("#6eb0dc")};
            iArr[1] = new int[]{Color.parseColor("#d3f958"), Color.parseColor("#9cbc35")};
            iArr[2] = new int[]{Color.parseColor("#e9fcaf"), Color.parseColor("#c5e169")};
            iArr[3] = new int[]{Color.parseColor("#bce296"), Color.parseColor("#5ea659")};
            iArr[4] = new int[]{Color.parseColor("#fff4d3"), Color.parseColor("#ffeeba")};
            GRARIENTS_COLORS = iArr;
        }

        private BoxType(Integer num) {
            this.id = num.intValue();
            this.whiteBox = new Paint();
            this.whiteBox.setColor(-1);
            this.textBox = new Paint();
            this.textBox.setDither(true);
            this.selecteLetterdBox = new Paint();
            this.selecteLetterdBox.setColor(Color.parseColor("#ffea94"));
            this.completeLetterdBox = new Paint();
            this.completeLetterdBox.setDither(true);
            this.completeSelecteLetterdBox = new Paint();
            this.completeSelecteLetterdBox.setDither(true);
            this.completeSelecteLetterWordBox = new Paint();
            this.completeSelecteLetterWordBox.setDither(true);
            this.boxRect = new Rect();
            this.selecteLetterWordBox = new Paint();
            this.selecteLetterWordBox.setDither(true);
            if (num.intValue() == 0) {
                this.responsePaint = new Paint();
                this.responsePaint.setTextAlign(Paint.Align.CENTER);
                this.responsePaint.setColor(ViewCompat.MEASURED_STATE_MASK);
                this.responsePaint.setAntiAlias(true);
                this.fonts = new Typeface[]{Typeface.createFromAsset(SkanwordBox.assetManager, "fonts/SEGOEUIB.TTF"), Typeface.createFromAsset(SkanwordBox.assetManager, "fonts/SEGOEUI.TTF")};
                return;
            }
            this.responsePaint = null;
            this.fonts = null;
        }

        public void drawKeywordFrame(Canvas canvas, int i, int i2, float f, int i3) {
            int i4 = (int) (30.0f * f);
            int round = Math.round(2.0f * f);
            int i5 = round / 2;
            int i6 = i + i4;
            Rect rect = new Rect(i + i5, i5 + i2, i6, i2 + i4);
            Paint paint = new Paint();
            paint.setColor(Color.parseColor("#f38d02"));
            paint.setStrokeWidth((float) round);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(rect, paint);
            paint.setStyle(Paint.Style.FILL);
            Path path = new Path();
            float f2 = (float) i6;
            float i7 = (float) i2;
            path.moveTo(f2, i7);
            float f3 = (float) i4;
            float f4 = 0.4f * f3;
            path.lineTo(f2, i2 + f4);
            path.lineTo(f2 - f4, i2);
            path.close();
            canvas.drawPath(path, paint);
            this.whiteBox.setTextSize(f * 5.0f);
            this.whiteBox.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
            StringBuilder f5 = new StringBuilder();
            f5.append("");
            f5.append(i3);
            canvas.drawText(f5.toString(), ((float) i) + (1062836634 * f3), i2 + (f3 * 1045220557), this.whiteBox);
        }

        public void drawTextBackground(Canvas canvas, int i, int i2, float f) {
            drawLetterBox(createGradient(this.textBox, 0, i, i2, f), canvas, i, i2, f);
        }

        public void drawCompleteBackground(Canvas canvas, int i, int i2, float f) {
            if (this.id == 0) {
                drawLetterBox(createGradient(this.completeLetterdBox, 3, i, i2, f), canvas, i, i2, f);
            }
        }

        protected void drawBackground(Canvas canvas, int i, int i2, float f) {
            drawLetterBox(this.whiteBox, canvas, i, i2, f);
        }

        protected void drawLetterSelected(Canvas canvas, int i, int i2, float f) {
            if (this.id == 0) {
                drawLetterBox(this.selecteLetterdBox, canvas, i, i2, f);
            }
        }

        protected void drawLetterWordSelected(Canvas canvas, int i, int i2, float f) {
            if (this.id == 0) {
                drawLetterBox(createGradient(this.selecteLetterWordBox, 4, i, i2, f), canvas, i, i2, f);
            }
        }

        protected void drawCompleteLetterSelected(Canvas canvas, int i, int i2, float f) {
            if (this.id == 0) {
                drawLetterBox(createGradient(this.completeSelecteLetterdBox, 1, i, i2, f), canvas, i, i2, f);
            }
        }

        protected void drawCompleteLetterWordSelected(Canvas canvas, int i, int i2, float f) {
            if (this.id == 0) {
                drawLetterBox(createGradient(this.completeSelecteLetterWordBox, 2, i, i2, f), canvas, i, i2, f);
            }
        }

        private void drawLetterBox(Paint paint, Canvas canvas, int i, int i2, float f) {
            int i3 = (int) (30.0f * f);
            int f2 = Math.round(getBorderSize(f));
            this.boxRect.set(i + f2, f2 + i2, i + i3, i2 + i3);
            canvas.drawRect(this.boxRect, paint);
        }

        protected void drawResponseLettter(Canvas canvas, int i, int i2, String str, boolean z, float f) {
            int i3 = (int) (30.0f * f);
            if (this.id == 0 && str != " ") {
                char[] toCharArray = str.toCharArray();
                this.responsePaint.setTextSize(f * 15.0f);
                this.responsePaint.setTypeface(this.fonts[1]);
                canvas.drawText(toCharArray, 0, toCharArray.length, (float) (i + Math.round((float) (i3 / 2))), (float) (i2 + Math.round((((float) i3) + this.responsePaint.getTextSize()) / 1073741824)), this.responsePaint);
            }
        }

        protected Typeface getQuestionFont() {
            return this.fonts[1];
        }

        private Paint createGradient(Paint paint, int i, int i2, int i3, float f) {
            if (paint != null && i >= 0) {
                int[][] iArr = GRARIENTS_COLORS;
                if (i < iArr.length) {
                    f = (int) (f * 30.0f);
                    paint.setShader(new LinearGradient((float) i2, (float) i3, (float) (i2 + f), (float) (i3 + f), iArr[i][0], iArr[i][1], Shader.TileMode.CLAMP));
                }
            }
            return paint;
        }

        private static float getBorderSize(float f) {
            return Math.max(1.0f, f * BORDER_SIZE);
        }
    }

    static class SkanwordBox1 {
    static final int[] boxType;

    static {
        boxType = new int[SkanwordBox.BoxType.values().length];
        try {
            SkanwordBox1.boxType[SkanwordBox.BoxType.LETTER.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            SkanwordBox1.boxType[SkanwordBox.BoxType.MAIN_QUESTION.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError) {
        }
    }
}

}
