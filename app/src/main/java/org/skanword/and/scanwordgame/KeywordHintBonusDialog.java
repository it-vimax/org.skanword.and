package org.skanword.and.scanwordgame;

import org.skanword.and.libs.qustom.QustomDialogBuilder;
import org.skanword.and.menu.*;
import android.content.*;
import android.app.*;
import android.graphics.*;
import org.skanword.and.etc.*;
import org.skanword.and.*;
import android.widget.*;
import android.view.*;

public class KeywordHintBonusDialog extends QustomDialogBuilder
{
    private static HintsBonusDialog.HintsBonusDialogDelegate mDelegate;
    private static AlertDialog mDialog;
    public View.OnClickListener doubleClickListener;
    public DialogInterface.OnCancelListener mCompleteDialogCancelListener;
    private View mDialogView;
    private boolean mHadRewardedVideo;
    public View.OnClickListener takeClickListener;
    public int layoutResId;

    public KeywordHintBonusDialog(final Context context) {
        super(context);
        this.mHadRewardedVideo = false;
        this.mDialogView = null;
        this.takeClickListener = (View.OnClickListener)new KeywordHintBonusDialog.KeywordHintBonusDialog2(this);
        this.doubleClickListener = (View.OnClickListener)new KeywordHintBonusDialog.KeywordHintBonusDialog3(this);
        this.mCompleteDialogCancelListener = (DialogInterface.OnCancelListener)new KeywordHintBonusDialog.KeywordHintBonusDialog4(this);
    }

    public KeywordHintBonusDialog(final Context context, final int n) {
        super(context);
        this.mHadRewardedVideo = false;
        this.mDialogView = null;
        this.takeClickListener = (View.OnClickListener)new KeywordHintBonusDialog.KeywordHintBonusDialog2(this);
        this.doubleClickListener = (View.OnClickListener)new KeywordHintBonusDialog.KeywordHintBonusDialog3(this);
        this.mCompleteDialogCancelListener = (DialogInterface.OnCancelListener)new KeywordHintBonusDialog.KeywordHintBonusDialog4(this);
    }

    static /* synthetic */ AlertDialog access000() {
        return KeywordHintBonusDialog.mDialog;
    }

    static /* synthetic */ AlertDialog access002(final AlertDialog mDialog) {
        return KeywordHintBonusDialog.mDialog = mDialog;
    }

    static /* synthetic */ HintsBonusDialog.HintsBonusDialogDelegate access100() {
        return KeywordHintBonusDialog.mDelegate;
    }

    static /* synthetic */ boolean access200(final KeywordHintBonusDialog keywordHintBonusDialog) {
        return keywordHintBonusDialog.mHadRewardedVideo;
    }

    public static AlertDialog getDialog() {
        return KeywordHintBonusDialog.mDialog;
    }

    public static boolean isShowing() {
        return getDialog() != null && getDialog().isShowing();
    }

    public static void showCompleteBonusDialogInContext(final Activity activity, final boolean b, final boolean b2, final DialogInterface.OnCancelListener onCancelListener) {
        if (!activity.isFinishing()) {
            final View inflate = LayoutInflater.from((Context)activity).inflate(R.layout.bonus_complete_dialog_view, (ViewGroup)null);
            final KeywordHintBonusDialog keywordHintBonusDialog = new KeywordHintBonusDialog((Context)activity, R.style.Scanword_style_dialog);
            keywordHintBonusDialog.setCustomView(R.style.Scanword_style_dialog, inflate.getContext()); //TODO не понятно что передавать
            final AlertDialog show = keywordHintBonusDialog.show();
            keywordHintBonusDialog.setDialogView(inflate);
            keywordHintBonusDialog.setDialog(show);
            final TextView textView = (TextView)inflate.findViewById(R.id.headLabel);
            int n;
            if (b && b2) {
                n = 2;
            }
            else {
                n = 1;
            }
            if (!b) {
                textView.setText("Начислено ");
                inflate.findViewById(R.id.simpleHintsCountsArea).setVisibility(View.VISIBLE); //TODO не точно
                final TextView textView2 = (TextView)inflate.findViewById(R.id.hintsReceivedLabel); //TODO не точно
                final StringBuilder sb = new StringBuilder();
                sb.append("Поздравляем!");
                sb.append(n);
                sb.append(" ");
                sb.append(Utils.spellVariantForNumber(n, "подсказок", "подсказки", "подсказку"));
                textView2.setText((CharSequence)sb.toString());
            }
            else {
                inflate.findViewById(R.id.videoHintsCountsArea).setVisibility(View.VISIBLE);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Вам начислено ");
                sb2.append(n);
                sb2.append(" ");
                sb2.append(Utils.spellVariantForNumber(n, "подсказок", "подсказки", "подсказку"));
                sb2.append(":");
                textView.setText((CharSequence)sb2.toString());
                ((TextView)inflate.findViewById(R.id.hintsCountLabel)).setText((CharSequence)"1");
                final TextView textView3 = (TextView)inflate.findViewById(R.id.doubledLabel);
                String text;
                if (b2) {
                    text = "да";
                }
                else {
                    text = " нет";
                }
                textView3.setText((CharSequence)text);
                final TextView textView4 = (TextView)inflate.findViewById(R.id.doubledLabel);
                String s;
                if (b2) {
                    s = "#99cc00";
                }
                else {
                    s = "#ff4444";
                }
                textView4.setTextColor(Color.parseColor(s));
            }
            inflate.findViewById(R.id.doubledLabel).setVisibility(View.GONE); // TODO все id примерны с файла bonus_complete_dialog_view.xml
            inflate.findViewById(R.id.notificationArea).setVisibility(View.GONE);
            inflate.findViewById(R.id.closeButton).setOnClickListener((View.OnClickListener)new KeywordHintBonusDialog.KeywordHintBonusDialog1(show));
            show.setOnCancelListener(onCancelListener);
        }
    }

    public static void showDialogInContext(final Context context, final HintsBonusDialog.HintsBonusDialogDelegate mDelegate, final String s) {
        KeywordHintBonusDialog.mDelegate = mDelegate;
        final View inflate = LayoutInflater.from(context).inflate(R.layout.keyword_bonus_dialog_view, (ViewGroup)null);
        final KeywordHintBonusDialog keywordHintBonusDialog = new KeywordHintBonusDialog(context, R.style.Scanword_style_dialog);
        keywordHintBonusDialog.setCustomView(R.style.Scanword_style_dialog, context);
        final AlertDialog show = keywordHintBonusDialog.show();
        keywordHintBonusDialog.setDialogView(inflate);
        show.setCancelable(false);
        keywordHintBonusDialog.setDialog(show);
        keywordHintBonusDialog.setHadRewardedVideo(AdsManager.getInstance().hasRewardedVideo());
        if (!keywordHintBonusDialog.isHadRewardedVideo()) {
            inflate.findViewById(R.id.closePanel).setVisibility(View.VISIBLE);
            inflate.findViewById(R.id.closeButton).setOnClickListener(keywordHintBonusDialog.takeClickListener);
        }
        else {
            inflate.findViewById(R.id.bonusPanel).setVisibility(View.VISIBLE);
            inflate.findViewById(R.id.getButton).setOnClickListener(keywordHintBonusDialog.takeClickListener);
            inflate.findViewById(R.id.doubleButton).setOnClickListener(keywordHintBonusDialog.doubleClickListener);
        }
        keywordHintBonusDialog.buildKeyword((LinearLayout)inflate.findViewById(R.id.keywordLayout), s);
    }

    public void buildKeyword(final LinearLayout linearLayout, final String s) {
        final LayoutInflater from = LayoutInflater.from(SmappsScanwords.getContext());
        linearLayout.removeAllViewsInLayout();
        int n;
        for (int i = 0; i < s.length(); i = n) {
            n = i + 1;
            final String upperCase = s.substring(i, n).toUpperCase();
            final View inflate = from.inflate(R.layout.question_letter, (ViewGroup)null);
            linearLayout.addView(inflate);
            final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)inflate.getLayoutParams();
            layoutParams.rightMargin = 15;
            layoutParams.bottomMargin = 2;
            inflate.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            inflate.findViewById(R.id.letter_view).setBackgroundResource(R.drawable.guessed_keyword_letter_cell);
            if (!upperCase.equalsIgnoreCase(" ")) {
                ((TextView)inflate.findViewById(R.id.letter_text)).setText((CharSequence)upperCase);
            }
        }
    }

    public View getDialogView() {
        return this.mDialogView;
    }

    protected int getLayoutresId() {
        this.layoutResId = R.layout.custom_dialog_layout;
        return this.layoutResId;
    }

    public boolean isHadRewardedVideo() {
        return this.mHadRewardedVideo;
    }

    public void setDialog(final AlertDialog mDialog) {
        KeywordHintBonusDialog.mDialog = mDialog;
    }

    public void setDialogView(final View mDialogView) {
        this.mDialogView = mDialogView;
    }

    public void setHadRewardedVideo(final boolean mHadRewardedVideo) {
        this.mHadRewardedVideo = mHadRewardedVideo;
    }

    // ---- Classes

    static final class KeywordHintBonusDialog1 implements View.OnClickListener {
        final /* synthetic */ AlertDialog valdialog;

        KeywordHintBonusDialog1(final AlertDialog valdialog) {
            super();
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            KeywordHintBonusDialog.access002((AlertDialog)null);
            this.valdialog.cancel();
        }
    }

    class KeywordHintBonusDialog2 implements View.OnClickListener {
        final /* synthetic */ KeywordHintBonusDialog this0;

        KeywordHintBonusDialog2(final KeywordHintBonusDialog this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            final AlertDialog access000 = KeywordHintBonusDialog.access000();
            KeywordHintBonusDialog.access002((AlertDialog)null);
            if (KeywordHintBonusDialog.access100() == null) {
                return;
            }
            KeywordHintBonusDialog.access100().onTake(KeywordHintBonusDialog.access200(this.this0));
            if (access000 != null) {
                access000.dismiss();
            }
        }
    }

    class KeywordHintBonusDialog3 implements View.OnClickListener {
        final /* synthetic */ KeywordHintBonusDialog this0;

        KeywordHintBonusDialog3(final KeywordHintBonusDialog this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            final AlertDialog access000 = KeywordHintBonusDialog.access000();
            KeywordHintBonusDialog.access002((AlertDialog)null);
            KeywordHintBonusDialog.access100().onDouble();
            if (access000 != null) {
                access000.dismiss();
            }
        }
    }

    class KeywordHintBonusDialog4 implements DialogInterface.OnCancelListener {
        final /* synthetic */ KeywordHintBonusDialog this0;

        KeywordHintBonusDialog4(final KeywordHintBonusDialog this0) {
            super();
            this.this0 = this0;
        }

        public void onCancel(final DialogInterface dialogInterface) {
        }
    }
}