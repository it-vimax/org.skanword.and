package org.skanword.and.scanwordgame;

import android.content.*;
import android.os.*;
import java.lang.reflect.*;
import android.annotation.*;
import android.view.*;

public abstract class ScaleGestureDetectorProxy
{
    public ScaleGestureDetectorProxy() {
        super();
    }
    
    public static ScaleGestureDetectorProxy create(final Context context, final TouchImageView touchImageView) {
        if (Build.VERSION.SDK_INT >= 8) {
            try {
                return (ScaleGestureDetectorProxy)Class.forName("org.skanword.and.scanwordgame.ScaleGestureDetectorProxyScaleGestureDetectorProxyFroyo").getConstructor(Context.class, Class.forName("android.view.ScaleGestureDetectorOnScaleGestureListener")).newInstance(context, Class.forName("org.skanword.and.scanwordgame.TouchImageViewScaleListener").getConstructor(TouchImageView.class).newInstance(touchImageView));
            }
            catch (InvocationTargetException ex) {
                ex.printStackTrace();
            }
            catch (IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
            catch (InstantiationException ex3) {
                ex3.printStackTrace();
            }
            catch (SecurityException ex4) {
                ex4.printStackTrace();
            }
            catch (NoSuchMethodException ex5) {
                ex5.printStackTrace();
            }
            catch (ClassNotFoundException ex6) {
                ex6.printStackTrace();
            }
        }
        return new ScaleGestureDetectorProxyDefault();
    }
    
    public abstract boolean onTouchEvent(final MotionEvent p0);
    
    public static class ScaleGestureDetectorProxyDefault extends ScaleGestureDetectorProxy
    {
        public ScaleGestureDetectorProxyDefault() {
            super();
        }
        
        @Override
        public boolean onTouchEvent(final MotionEvent motionEvent) {
            return false;
        }
    }
    
    @TargetApi(8)
    public static class ScaleGestureDetectorProxyFroyo extends ScaleGestureDetectorProxy
    {
        private ScaleGestureDetector mDetector;
        
        public ScaleGestureDetectorProxyFroyo(final Context context, final ScaleGestureDetector.OnScaleGestureListener scaleGestureDetectorOnScaleGestureListener) {
            super();
            this.mDetector = new ScaleGestureDetector(context, scaleGestureDetectorOnScaleGestureListener);
        }
        
        @Override
        public boolean onTouchEvent(final MotionEvent motionEvent) {
            return this.mDetector.onTouchEvent(motionEvent);
        }
    }
}
