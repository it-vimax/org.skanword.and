package org.skanword.and.scanwordgame;

import android.graphics.*;

public enum ArrowType {
    RIGHT(Integer.valueOf(0)),
    BOTTOM(Integer.valueOf(1)),
    RIGHT_BOTTOM(Integer.valueOf(2)),
    LEFT_BOTTOM(Integer.valueOf(3)),
    TOP_RIGHT(Integer.valueOf(4)),
    BOTTOM_RIGHT(Integer.valueOf(5)),
    TOP_LEFT_BOTTOM(Integer.valueOf(6)),
    TOP_RIGHT_BOTTOM(Integer.valueOf(7)),
    LEFT_BOTTOM_RIGHT(Integer.valueOf(8)),
    TOP_LEFT_RIGHT(Integer.valueOf(9)),
    SMALL_TOP_RIGHT(Integer.valueOf(10)),
    SMALL_BOTTOM_RIGHT(Integer.valueOf(11)),
    SMALL_LEFT_BOTTOM(Integer.valueOf(12)),
    SMALL_RIGHT_BOTTOM(Integer.valueOf(13));

    private static final int ARROW_SIZE = 10;
    private static final int COLOR = -16777216;
    private static final float END_FEATHERING_RATIO = 0.65f;
    private static final float START_FEATHERING_RATIO = 0.5f;
    private static final float STICK_HEIGHT_RATIO = 0.1f;
    private static final float STICK_SMALL_RATIO = 0.3f;
    private static final float VERTICAL_SHIFT_FEATHERING_RATIO = 0.25f;
    private static final int[] verticesColors;
    private final Paint arrowPaint;
    private final int id;

    static {
        verticesColors = new int[]{-16777216, -16777216, -16777216, -16777216, -16777216, -16777216};
    }

    private ArrowType(Integer n2) {
        this.id = n2;
        this.arrowPaint = new Paint();
        this.arrowPaint.setColor(-16777216);
    }

    public static ArrowType create(int n) {
        ArrowType arrowType = RIGHT;
        ArrowType[] arrarrowType = ArrowType.values();
        int n2 = arrarrowType.length;
        int n3 = 0;
        while (n3 < n2) {
            ArrowType arrowType2 = arrarrowType[n3];
            if (arrowType2.id == n - 1) {
                return arrowType2;
            }
            ++n3;
        }
        return arrowType;
    }

    private void drawCurvedLine(Canvas canvas, Point point, Point point2, Point point3, Paint paint, float f) {
        paint.setStrokeWidth((float)((int)(f * 10.0f)) * 0.1f);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        Path path = new Path();
        path.moveTo((float)point.x, (float)point.y);
        path.lineTo((float)point2.x, (float)point2.y);
        path.moveTo((float)point2.x, (float)point2.y);
        path.lineTo((float)point3.x, (float)point3.y);
        path.close();
        canvas.drawPath(path, paint);
    }

    private void drawFeathering(Canvas canvas, Point point, Boolean bl, float f) {
        int n = (int)(f * 10.0f);
        if (bl.booleanValue()) {
            int n2 = point.x;
            int n3 = point.y;
            float f2 = n;
            Point point1 = new Point(n2, n3 + Math.round(0.35000002f * f2));
            n = point.x;
            f = 0.25f * f2;
            n2 = Math.round(f);
            n3 = point.y;
            this.drawTriangle(canvas, point, (Point)point1, new Point(n + n2, n3 - Math.round(f2 *= 0.14999998f)));
            this.drawTriangle(canvas, point, (Point)point1, new Point(point.x - Math.round(f), point.y - Math.round(f2)));
            return;
        }
        int n4 = point.x;
        float f3 = n;
        Point point2 = new Point(n4 + Math.round(0.35000002f * f3), point.y);
        n = point.x;
        f = 0.14999998f * f3;
        n4 = Math.round(f);
        int n5 = point.y;
        this.drawTriangle(canvas, point, (Point)point2, new Point(n - n4, n5 - Math.round(f3 *= 0.25f)));
        this.drawTriangle(canvas, point, (Point)point2, new Point(point.x - Math.round(f), point.y + Math.round(f3)));
    }

    private void drawTriangle(Canvas canvas, Point point, Point point2, Point point3) {
        float[] arrf = new float[]{point.x, point.y, point2.x, point2.y, point3.x, point3.y};
        canvas.drawVertices(Canvas.VertexMode.TRIANGLES, arrf.length, arrf, 0, null, 0, verticesColors, 0, null, 0, 0, new Paint());
    }

    protected void draw(Canvas canvas, int n, int n2, float f) {
        int n3 = (int)(10.0f * f);
        int n4 = (int)(30.0f * f);
        int n5 = this.id;
        boolean bl = true;
        boolean bl2 = true;
        boolean bl3 = true;
        boolean bl4 = true;
        boolean bl5 = true;
        boolean bl6 = true;
        boolean bl7 = true;
        boolean bl8 = true;
        switch (n5) {
            default: {
                return;
            }
            case 13: {
                float f2 = n3;
                float f3 = 0.3f * f2;
                n3 = n + Math.round(f3);
                n4 = n2 + Math.round(f3);
                n5 = n4 + Math.round(f2 * 0.65f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n3, n4), new Point(n3, n5), this.arrowPaint, f);
                Point point = new Point(n3, n5);
                if (n5 == n4) {
                    bl8 = false;
                }
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 12: {
                float f4 = n3;
                float f5 = 0.3f * f4;
                n3 = (n += n4) - Math.round(f5);
                n4 = n2 + Math.round(f5);
                n5 = n4 + Math.round(f4 * 0.65f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n3, n4), new Point(n3, n5), this.arrowPaint, f);
                Point point = new Point(n3, n5);
                bl8 = n5 != n4 ? bl : false;
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 11: {
                float f6 = n3;
                float f7 = 0.3f * f6;
                n3 = Math.round(f7) + n;
                n4 = n2 + Math.round(f7);
                n5 = n3 + Math.round(f6 * 0.65f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n3, n4), new Point(n5, n4), this.arrowPaint, f);
                Point point = new Point(n5, n4);
                this.drawFeathering(canvas, point, Boolean.valueOf(false), f);
                return;
            }
            case 10: {
                n2 += n4;
                float f8 = n3;
                float f9 = 0.3f * f8;
                n3 = Math.round(f9) + n;
                n4 = n2 - Math.round(f9);
                n5 = n3 + Math.round(f8 * 0.65f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n3, n4), new Point(n5, n4), this.arrowPaint, f);
                Point point = new Point(n5, n4);
                this.drawFeathering(canvas, point, Boolean.valueOf(false), f);
                return;
            }
            case 9: {
                n += n4;
                n2 += n4;
                n4 = n - n3;
                n5 = n2 - n3;
                n3 = n4 + Math.round((float)n3 * 0.65f / 1.5f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n4, n5), new Point(n3, n5), this.arrowPaint, f);
                Point point = new Point(n3, n5);
                this.drawFeathering(canvas, point, Boolean.valueOf(false), f);
                return;
            }
            case 8: {
                n += n4;
                float f10 = n3;
                n4 = n - n3;
                n3 = (n2 += Math.round(0.1f * f10 / 2.0f)) + n3;
                n5 = n4 + Math.round(f10 * 0.65f / 1.5f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n4, n3), new Point(n5, n3), this.arrowPaint, f);
                Point point = new Point(n5, n3);
                this.drawFeathering(canvas, point, Boolean.valueOf(false), f);
                return;
            }
            case 7: {
                float f11 = n3;
                n2 += n4;
                n4 = (n += Math.round(0.1f * f11 / 2.0f)) + n3;
                n3 = n2 - n3;
                n5 = n3 + Math.round(f11 * 0.65f / 1.5f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n4, n3), new Point(n4, n5), this.arrowPaint, f);
                Point point = new Point(n4, n5);
                bl8 = n5 != n3 ? bl2 : false;
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 6: {
                n += n4;
                n2 += n4;
                n4 = n - n3;
                n5 = n2 - n3;
                n3 = n5 + Math.round((float)n3 * 0.65f / 1.5f);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n4, n5), new Point(n4, n3), this.arrowPaint, f);
                Point point = new Point(n4, n3);
                bl8 = n3 != n5 ? bl3 : false;
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 5: {
                float f12 = n3;
                float f13 = 0.1f * f12;
                n += Math.round(((float)n4 + f13) / 2.0f);
                n3 = (n2 += Math.round(f13 / 2.0f)) + Math.round(f12 *= 0.65f);
                n4 = n + Math.round(f12);
                n5 = n2 + Math.round(f12);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n, n3), new Point(n4, n5), this.arrowPaint, f);
                Point point = new Point(n4, n5);
                bl8 = n5 != n3 ? bl4 : false;
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 4: {
                float f14 = n3;
                n += Math.round((0.1f * f14 + (float)n4) / 2.0f);
                n3 = (n2 += n4) - Math.round(f14 *= 0.65f);
                n4 = n + Math.round(f14);
                n5 = n2 - Math.round(f14);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n, n3), new Point(n4, n5), this.arrowPaint, f);
                Point point = new Point(n4, n5);
                bl8 = n5 != n3 ? bl5 : false;
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 3: {
                n += n4;
                float f15 = n3;
                n2 += Math.round((0.1f * f15 + (float)n4) / 2.0f);
                n3 = Math.round(f15 *= 0.65f);
                n4 = n - Math.round(f15);
                n5 = n2 + Math.round(f15);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n - n3, n2), new Point(n4, n5), this.arrowPaint, f);
                Point point = new Point(n4, n5);
                bl8 = n5 != n2 ? bl6 : false;
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 2: {
                float f16 = n3;
                float f17 = 0.1f * f16;
                n2 += Math.round((f17 + (float)n4) / 2.0f);
                n3 = Math.round(f16 *= 0.65f);
                n4 = (n += Math.round(f17 / 2.0f)) + Math.round(f16);
                n5 = n2 + Math.round(f16);
                this.drawCurvedLine(canvas, new Point(n, n2), new Point(n3 + n, n2), new Point(n4, n5), this.arrowPaint, f);
                Point point = new Point(n4, n5);
                bl8 = n5 != n2 ? bl7 : false;
                this.drawFeathering(canvas, point, Boolean.valueOf(bl8), f);
                return;
            }
            case 1: {
                float f18 = n3;
                float f19 = 0.1f * f18;
                n3 = n2 + Math.round(f18 * 0.65f);
                this.arrowPaint.setStrokeWidth(f19);
                f18 = n += Math.round(((float)n4 + f19) / 2.0f);
                canvas.drawLine(f18, (float)n2, f18, (float)n3, this.arrowPaint);
                this.drawFeathering(canvas, new Point(n, n3), Boolean.valueOf(true), f);
                return;
            }
            case 0:
        }
        float f20 = n3;
        float f21 = 0.1f * f20;
        n2 += Math.round(((float)n4 + f21) / 2.0f);
        n3 = n + Math.round(f20 * 0.65f);
        this.arrowPaint.setStrokeWidth(f21);
        f20 = n;
        f21 = n2;
        canvas.drawLine(f20, f21, (float)n3, f21, this.arrowPaint);
        this.drawFeathering(canvas, new Point(n3, n2), Boolean.valueOf(false), f);
    }
}
