package org.skanword.and.scanwordgame;

import android.content.*;
import android.view.animation.*;
import android.view.*;
import android.util.*;

public class OverScroller
{
    private static final int DEFAULT_DURATION = 250;
    private static final int FLING_MODE = 1;
    private static final int SCROLL_MODE = 0;
    private final boolean mFlywheel;
    private Interpolator mInterpolator;
    private int mMode;
    private final SplineOverScroller mScrollerX;
    private final SplineOverScroller mScrollerY;
    
    public OverScroller(final Context context) {
        this(context, null);
    }
    
    public OverScroller(final Context context, final Interpolator interpolator) {
        this(context, interpolator, true);
    }
    
    public OverScroller(final Context context, final Interpolator interpolator, final float n, final float n2) {
        this(context, interpolator, true);
    }
    
    public OverScroller(final Context context, final Interpolator interpolator, final float n, final float n2, final boolean b) {
        this(context, interpolator, b);
    }
    
    public OverScroller(final Context context, final Interpolator mInterpolator, final boolean mFlywheel) {
        super();
        this.mInterpolator = mInterpolator;
        this.mFlywheel = mFlywheel;
        this.mScrollerX = new SplineOverScroller();
        this.mScrollerY = new SplineOverScroller();
        SplineOverScroller.initFromContext(context);
    }
    
    public void abortAnimation() {
        this.mScrollerX.finish();
        this.mScrollerY.finish();
    }
    
    public boolean computeScrollOffset() {
        if (this.isFinished()) {
            return false;
        }
        final int mMode = this.mMode;
        if (mMode != 0) {
            if (mMode != 1) {
                return true;
            }
            if (!this.mScrollerX.mFinished && !this.mScrollerX.update() && !this.mScrollerX.continueWhenFinished()) {
                this.mScrollerX.finish();
            }
            if (!this.mScrollerY.mFinished && !this.mScrollerY.update() && !this.mScrollerY.continueWhenFinished()) {
                this.mScrollerY.finish();
                return true;
            }
        }
        else {
            final long n = AnimationUtils.currentAnimationTimeMillis() - this.mScrollerX.mStartTime;
            final int access500 = this.mScrollerX.mDuration;
            if (n < access500) {
                float interpolation = n / (float)access500;
                final Interpolator mInterpolator = this.mInterpolator;
                if (mInterpolator != null) {
                    interpolation = mInterpolator.getInterpolation(interpolation);
                }
                this.mScrollerX.updateScroll(interpolation);
                this.mScrollerY.updateScroll(interpolation);
                return true;
            }
            this.abortAnimation();
        }
        return true;
    }
    
    @Deprecated
    public void extendDuration(final int n) {
        this.mScrollerX.extendDuration(n);
        this.mScrollerY.extendDuration(n);
    }
    
    public void fling(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8) {
        this.fling(n, n2, n3, n4, n5, n6, n7, n8, 0, 0);
    }
    
    public void fling(final int n, final int n2, int n3, int n4, final int n5, final int n6, final int n7, final int n8, final int n9, final int n10) {
        if (this.mFlywheel && !this.isFinished()) {
            final float access200 = this.mScrollerX.mCurrVelocity;
            final float access201 = this.mScrollerY.mCurrVelocity;
            final float n11 = (float)n3;
            if (Math.signum(n11) == Math.signum(access200)) {
                final float n12 = (float)n4;
                if (Math.signum(n12) == Math.signum(access201)) {
                    n3 = (int)(n11 + access200);
                    n4 = (int)(n12 + access201);
                }
            }
        }
        this.mMode = 1;
        this.mScrollerX.fling(n, n3, n5, n6, n9);
        this.mScrollerY.fling(n2, n4, n7, n8, n10);
    }
    
    public final void forceFinished(final boolean b) {
        this.mScrollerX.mFinished = (this.mScrollerY.mFinished = b);
    }
    
    public float getCurrVelocity() {
        return (float)Math.sqrt(this.mScrollerX.mCurrVelocity * this.mScrollerX.mCurrVelocity + this.mScrollerY.mCurrVelocity * this.mScrollerY.mCurrVelocity);
    }
    
    public final int getCurrX() {
        return this.mScrollerX.mCurrentPosition;
    }
    
    public final int getCurrY() {
        return this.mScrollerY.mCurrentPosition;
    }
    
    @Deprecated
    public final int getDuration() {
        return Math.max(this.mScrollerX.mDuration, this.mScrollerY.mDuration);
    }
    
    public final int getFinalX() {
        return this.mScrollerX.mFinal;
    }
    
    public final int getFinalY() {
        return this.mScrollerY.mFinal;
    }
    
    public final int getStartX() {
        return this.mScrollerX.mStart;
    }
    
    public final int getStartY() {
        return this.mScrollerY.mStart;
    }
    
    public final boolean isFinished() {
        return this.mScrollerX.mFinished && this.mScrollerY.mFinished;
    }
    
    public boolean isOverScrolled() {
        return (!this.mScrollerX.mFinished && this.mScrollerX.mState != 0) || (!this.mScrollerY.mFinished && this.mScrollerY.mState != 0);
    }
    
    public boolean isScrollingInDirection(final float n, final float n2) {
        final int access400 = this.mScrollerX.mFinal;
        final int access401 = this.mScrollerX.mStart;
        final int access402 = this.mScrollerY.mFinal;
        final int access403 = this.mScrollerY.mStart;
        return !this.isFinished() && Math.signum(n) == Math.signum((float)(access400 - access401)) && Math.signum(n2) == Math.signum((float)(access402 - access403));
    }
    
    public void notifyHorizontalEdgeReached(final int n, final int n2, final int n3) {
        this.mScrollerX.notifyEdgeReached(n, n2, n3);
    }
    
    public void notifyVerticalEdgeReached(final int n, final int n2, final int n3) {
        this.mScrollerY.notifyEdgeReached(n, n2, n3);
    }
    
    @Deprecated
    public void setFinalX(final int finalPosition) {
        this.mScrollerX.setFinalPosition(finalPosition);
    }
    
    @Deprecated
    public void setFinalY(final int finalPosition) {
        this.mScrollerY.setFinalPosition(finalPosition);
    }
    
    public final void setFriction(final float n) {
        this.mScrollerX.setFriction(n);
        this.mScrollerY.setFriction(n);
    }
    
    void setInterpolator(final Interpolator mInterpolator) {
        this.mInterpolator = mInterpolator;
    }
    
    public boolean springBack(final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
        boolean b = true;
        this.mMode = 1;
        final boolean springback = this.mScrollerX.springback(n, n3, n4);
        final boolean springback2 = this.mScrollerY.springback(n2, n5, n6);
        if (!springback) {
            if (springback2) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    public void startScroll(final int n, final int n2, final int n3, final int n4) {
        this.startScroll(n, n2, n3, n4, 250);
    }
    
    public void startScroll(final int n, final int n2, final int n3, final int n4, final int n5) {
        this.mMode = 0;
        this.mScrollerX.startScroll(n, n3, n5);
        this.mScrollerY.startScroll(n2, n4, n5);
    }
    
    public int timePassed() {
        return (int)(AnimationUtils.currentAnimationTimeMillis() - Math.min(this.mScrollerX.mStartTime, this.mScrollerY.mStartTime));
    }
    
    static class SplineOverScroller
    {
        private static final int BALLISTIC = 2;
        private static final int CUBIC = 1;
        private static float DECELERATION_RATE = 0.0f;
        private static final float END_TENSION = 1.0f;
        private static final float GRAVITY = 2000.0f;
        private static final float INFLEXION = 0.35f;
        private static final int NB_SAMPLES = 100;
        private static final float P1 = 0.175f;
        private static final float P2 = 0.35000002f;
        private static float PHYSICAL_COEF = 0.0f;
        private static final int SPLINE = 0;
        private static final float[] SPLINE_POSITION;
        private static final float[] SPLINE_TIME;
        private static final float START_TENSION = 0.5f;
        private float mCurrVelocity;
        private int mCurrentPosition;
        private float mDeceleration;
        private int mDuration;
        private int mFinal;
        private boolean mFinished;
        private float mFlingFriction;
        private int mOver;
        private int mSplineDistance;
        private int mSplineDuration;
        private int mStart;
        private long mStartTime;
        private int mState;
        private int mVelocity;
        
        static {
            SplineOverScroller.DECELERATION_RATE = (float)(Math.log(0.78) / Math.log(0.9));
            SPLINE_POSITION = new float[101];
            SPLINE_TIME = new float[101];
            float n = 0.0f;
            int i = 0;
            float n2 = 0.0f;
            while (i < 100) {
                final float n3 = i / 100.0f;
                float n4 = 1.0f;
                float n5;
                float n6;
                float n7;
                float n8;
                while (true) {
                    n5 = (n4 - n) / 2.0f + n;
                    n6 = 1.0f - n5;
                    n7 = n5 * 3.0f * n6;
                    n8 = n5 * n5 * n5;
                    final float n9 = (n6 * 0.175f + n5 * 0.35000002f) * n7 + n8;
                    if (Math.abs(n9 - n3) < 1.0E-5) {
                        break;
                    }
                    if (n9 > n3) {
                        n4 = n5;
                    }
                    else {
                        n = n5;
                    }
                }
                SplineOverScroller.SPLINE_POSITION[i] = n7 * (n6 * 0.5f + n5) + n8;
                float n10 = 1.0f;
                float n11;
                float n12;
                float n13;
                float n14;
                while (true) {
                    n11 = (n10 - n2) / 2.0f + n2;
                    n12 = 1.0f - n11;
                    n13 = n11 * 3.0f * n12;
                    n14 = n11 * n11 * n11;
                    final float n15 = (n12 * 0.5f + n11) * n13 + n14;
                    if (Math.abs(n15 - n3) < 1.0E-5) {
                        break;
                    }
                    if (n15 > n3) {
                        n10 = n11;
                    }
                    else {
                        n2 = n11;
                    }
                }
                SplineOverScroller.SPLINE_TIME[i] = n13 * (n12 * 0.175f + n11 * 0.35000002f) + n14;
                ++i;
            }
            SplineOverScroller.SPLINE_POSITION[100] = (SplineOverScroller.SPLINE_TIME[100] = 1.0f);
        }
        
        SplineOverScroller() {
            super();
            this.mFlingFriction = ViewConfiguration.getScrollFriction();
            this.mState = 0;
            this.mFinished = true;
        }
        
        static /* synthetic */ boolean access000(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mFinished;
        }
        
        static /* synthetic */ boolean access002(final SplineOverScroller splineOverScroller, final boolean mFinished) {
            return splineOverScroller.mFinished = mFinished;
        }
        
        static /* synthetic */ int access100(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mCurrentPosition;
        }
        
        static /* synthetic */ float access200(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mCurrVelocity;
        }
        
        static /* synthetic */ int access300(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mStart;
        }
        
        static /* synthetic */ int access400(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mFinal;
        }
        
        static /* synthetic */ int access500(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mDuration;
        }
        
        static /* synthetic */ long access600(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mStartTime;
        }
        
        static /* synthetic */ int access700(final SplineOverScroller splineOverScroller) {
            return splineOverScroller.mState;
        }
        
        private void adjustDuration(int n, int n2, final int n3) {
            final float abs = Math.abs((n3 - n) / (float)(n2 - n));
            n = (int)(abs * 100.0f);
            if (n < 100) {
                final float n4 = n / 100.0f;
                n2 = n + 1;
                final float n5 = n2 / 100.0f;
                final float[] spline_TIME = SplineOverScroller.SPLINE_TIME;
                final float n6 = spline_TIME[n];
                this.mDuration *= (int)(n6 + (abs - n4) / (n5 - n4) * (spline_TIME[n2] - n6));
            }
        }
        
        private void fitOnBounceCurve(final int n, final int mStart, final int n2) {
            final float n3 = (float)(-n2);
            final float mDeceleration = this.mDeceleration;
            final float n4 = n3 / mDeceleration;
            final double n5 = n2 * n2 / 2.0f / Math.abs(mDeceleration) + Math.abs(mStart - n);
            Double.isNaN(n5);
            final double n6 = Math.abs(this.mDeceleration);
            Double.isNaN(n6);
            final float n7 = (float)Math.sqrt(n5 * 2.0 / n6);
            this.mStartTime -= (int)((n7 - n4) * 1000.0f);
            this.mStart = mStart;
            this.mVelocity = (int)(-this.mDeceleration * n7);
        }
        
        private static float getDeceleration(final int n) {
            if (n > 0) {
                return -2000.0f;
            }
            return 2000.0f;
        }
        
        private double getSplineDeceleration(final int n) {
            return Math.log(Math.abs(n) * 0.35f / (this.mFlingFriction * SplineOverScroller.PHYSICAL_COEF));
        }
        
        private double getSplineFlingDistance(final int n) {
            final double splineDeceleration = this.getSplineDeceleration(n);
            final float deceleration_RATE = SplineOverScroller.DECELERATION_RATE;
            final double n2 = deceleration_RATE;
            Double.isNaN(n2);
            final double n3 = this.mFlingFriction * SplineOverScroller.PHYSICAL_COEF;
            final double n4 = deceleration_RATE;
            Double.isNaN(n4);
            final double exp = Math.exp(n4 / (n2 - 1.0) * splineDeceleration);
            Double.isNaN(n3);
            return n3 * exp;
        }
        
        private int getSplineFlingDuration(final int n) {
            final double splineDeceleration = this.getSplineDeceleration(n);
            final double n2 = SplineOverScroller.DECELERATION_RATE;
            Double.isNaN(n2);
            return (int)(Math.exp(splineDeceleration / (n2 - 1.0)) * 1000.0);
        }
        
        static void initFromContext(final Context context) {
            SplineOverScroller.PHYSICAL_COEF = context.getResources().getDisplayMetrics().density * 160.0f * 386.0878f * 0.84f;
        }
        
        private void onEdgeReached() {
            final int mVelocity = this.mVelocity;
            final float n = mVelocity * mVelocity / (Math.abs(this.mDeceleration) * 2.0f);
            final float signum = Math.signum((float)this.mVelocity);
            final int mOver = this.mOver;
            float n2 = n;
            if (n > mOver) {
                final float n3 = -signum;
                final int mVelocity2 = this.mVelocity;
                this.mDeceleration = n3 * mVelocity2 * mVelocity2 / (mOver * 2.0f);
                n2 = (float)mOver;
            }
            this.mOver = (int)n2;
            this.mState = 2;
            final int mStart = this.mStart;
            if (this.mVelocity <= 0) {
                n2 = -n2;
            }
            this.mFinal = mStart + (int)n2;
            this.mDuration = -(int)(this.mVelocity * 1000.0f / this.mDeceleration);
        }
        
        private void startAfterEdge(final int n, int n2, int n3, final int n4) {
            boolean b = true;
            if (n > n2 && n < n3) {
                Log.e("OverScroller", "startAfterEdge called from a valid position");
                this.mFinished = true;
                return;
            }
            final boolean b2 = n > n3;
            int n5;
            if (b2) {
                n5 = n3;
            }
            else {
                n5 = n2;
            }
            final int n6 = n - n5;
            if (n6 * n4 < 0) {
                b = false;
            }
            if (b) {
                this.startBounceAfterEdge(n, n5, n4);
                return;
            }
            if (this.getSplineFlingDistance(n4) > Math.abs(n6)) {
                if (!b2) {
                    n2 = n;
                }
                if (b2) {
                    n3 = n;
                }
                this.fling(n, n4, n2, n3, this.mOver);
                return;
            }
            this.startSpringback(n, n5, n4);
        }
        
        private void startBounceAfterEdge(final int n, final int n2, final int n3) {
            int n4;
            if (n3 == 0) {
                n4 = n - n2;
            }
            else {
                n4 = n3;
            }
            this.mDeceleration = getDeceleration(n4);
            this.fitOnBounceCurve(n, n2, n3);
            this.onEdgeReached();
        }
        
        private void startSpringback(int mStart, final int mFinal, final int n) {
            this.mFinished = false;
            this.mState = 1;
            this.mStart = mStart;
            this.mFinal = mFinal;
            mStart -= mFinal;
            this.mDeceleration = getDeceleration(mStart);
            this.mVelocity = -mStart;
            this.mOver = Math.abs(mStart);
            final double n2 = mStart;
            Double.isNaN(n2);
            final double n3 = this.mDeceleration;
            Double.isNaN(n3);
            this.mDuration = (int)(Math.sqrt(n2 * -2.0 / n3) * 1000.0);
        }
        
        boolean continueWhenFinished() {
            final int mState = this.mState;
            if (mState != 0) {
                if (mState == 1) {
                    return false;
                }
                if (mState == 2) {
                    this.mStartTime += this.mDuration;
                    this.startSpringback(this.mFinal, this.mStart, 0);
                }
            }
            else {
                if (this.mDuration >= this.mSplineDuration) {
                    return false;
                }
                this.mStart = this.mFinal;
                this.mVelocity = (int)this.mCurrVelocity;
                this.mDeceleration = getDeceleration(this.mVelocity);
                this.mStartTime += this.mDuration;
                this.onEdgeReached();
            }
            this.update();
            return true;
        }
        
        void extendDuration(final int n) {
            this.mDuration = (int)(AnimationUtils.currentAnimationTimeMillis() - this.mStartTime) + n;
            this.mFinished = false;
        }
        
        void finish() {
            this.mCurrentPosition = this.mFinal;
            this.mFinished = true;
        }
        
        void fling(final int n, final int mVelocity, final int mFinal, final int mFinal2, int splineFlingDuration) {
            this.mOver = splineFlingDuration;
            this.mFinished = false;
            this.mVelocity = mVelocity;
            final float mCurrVelocity = (float)mVelocity;
            this.mCurrVelocity = mCurrVelocity;
            this.mSplineDuration = 0;
            this.mDuration = 0;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mStart = n;
            this.mCurrentPosition = n;
            if (n <= mFinal2 && n >= mFinal) {
                this.mState = 0;
                double splineFlingDistance = 0.0;
                if (mVelocity != 0) {
                    splineFlingDuration = this.getSplineFlingDuration(mVelocity);
                    this.mSplineDuration = splineFlingDuration;
                    this.mDuration = splineFlingDuration;
                    splineFlingDistance = this.getSplineFlingDistance(mVelocity);
                }
                final double n2 = Math.signum(mCurrVelocity);
                Double.isNaN(n2);
                this.mSplineDistance = (int)(splineFlingDistance * n2);
                this.mFinal = this.mSplineDistance + n;
                final StringBuilder sb = new StringBuilder();
                sb.append(n);
                sb.append(" flingScroller ");
                sb.append(mVelocity);
                sb.append(" ");
                sb.append(mFinal);
                sb.append("  ");
                sb.append(mFinal2);
                sb.append(" ");
                sb.append(this.mFinal);
                Log.v("", sb.toString());
                if (this.mFinal < mFinal) {
                    Log.v("", " block 1 ");
                    this.adjustDuration(this.mStart, this.mFinal, mFinal);
                    this.mFinal = mFinal;
                }
                if (this.mFinal > mFinal2) {
                    Log.v("", " block 2 ");
                    this.adjustDuration(this.mStart, this.mFinal, mFinal2);
                    this.mFinal = mFinal2;
                }
                return;
            }
            this.startAfterEdge(n, mFinal, mFinal2, mVelocity);
        }
        
        void notifyEdgeReached(final int n, final int n2, final int mOver) {
            if (this.mState == 0) {
                this.mOver = mOver;
                this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
                this.startAfterEdge(n, n2, n2, (int)this.mCurrVelocity);
            }
        }
        
        void setFinalPosition(final int mFinal) {
            this.mFinal = mFinal;
            this.mFinished = false;
        }
        
        void setFriction(final float mFlingFriction) {
            this.mFlingFriction = mFlingFriction;
        }
        
        boolean springback(final int n, final int n2, final int n3) {
            this.mFinished = true;
            this.mFinal = n;
            this.mStart = n;
            this.mVelocity = 0;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mDuration = 0;
            if (n < n2) {
                this.startSpringback(n, n2, 0);
            }
            else if (n > n3) {
                this.startSpringback(n, n3, 0);
            }
            return this.mFinished ^ true;
        }
        
        void startScroll(final int mStart, final int n, final int mDuration) {
            this.mFinished = false;
            this.mStart = mStart;
            this.mFinal = mStart + n;
            this.mStartTime = AnimationUtils.currentAnimationTimeMillis();
            this.mDuration = mDuration;
            this.mDeceleration = 0.0f;
            this.mVelocity = 0;
        }
        
        boolean update() {
            final long n = AnimationUtils.currentAnimationTimeMillis() - this.mStartTime;
            final int mDuration = this.mDuration;
            if (n > mDuration) {
                return false;
            }
            double n2 = 0.0;
            final int mState = this.mState;
            if (mState != 0) {
                if (mState != 1) {
                    if (mState == 2) {
                        final float n3 = n / 1000.0f;
                        final int mVelocity = this.mVelocity;
                        final float n4 = (float)mVelocity;
                        final float mDeceleration = this.mDeceleration;
                        this.mCurrVelocity = n4 + mDeceleration * n3;
                        n2 = mVelocity * n3 + mDeceleration * n3 * n3 / 2.0f;
                        final StringBuilder sb = new StringBuilder();
                        sb.append(n2);
                        sb.append(" BALLISTIC ");
                        Log.v("", sb.toString());
                    }
                }
                else {
                    final float n5 = n / (float)mDuration;
                    final float n6 = n5 * n5;
                    final float signum = Math.signum((float)this.mVelocity);
                    final int mOver = this.mOver;
                    n2 = mOver * signum * (3.0f * n6 - 2.0f * n5 * n6);
                    this.mCurrVelocity = signum * mOver * 6.0f * (-n5 + n6);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(n2);
                    sb2.append(" CUBIC ");
                    Log.v("", sb2.toString());
                }
            }
            else {
                final float n7 = n / (float)this.mSplineDuration;
                final int n8 = (int)(n7 * 100.0f);
                float n9 = 1.0f;
                float n10 = 0.0f;
                if (n8 < 100) {
                    final float n11 = n8 / 100.0f;
                    final int n12 = n8 + 1;
                    final float n13 = n12 / 100.0f;
                    final float[] spline_POSITION = SplineOverScroller.SPLINE_POSITION;
                    final float n14 = spline_POSITION[n8];
                    n10 = (spline_POSITION[n12] - n14) / (n13 - n11);
                    n9 = n14 + (n7 - n11) * n10;
                }
                final int mSplineDistance = this.mSplineDistance;
                n2 = n9 * mSplineDistance;
                this.mCurrVelocity = n10 * mSplineDistance / this.mSplineDuration * 1000.0f;
            }
            this.mCurrentPosition = this.mStart + (int)Math.round(n2);
            return true;
        }
        
        void updateScroll(final float n) {
            final int mStart = this.mStart;
            this.mCurrentPosition = mStart + Math.round(n * (this.mFinal - mStart));
        }
    }
}
