package org.skanword.and.scanwordgame;

import java.util.logging.*;
import org.skanword.and.R;
import org.skanword.and.etc.versions.*;
import android.os.Handler;
import android.util.*;
import android.net.*;
import java.io.*;
import org.skanword.and.*;
import android.content.*;
import org.skanword.and.datamanager.*;
import java.util.concurrent.*;
import me.grantland.widget.*;
import org.skanword.and.inappbilling.*;
import android.os.*;
import android.preference.*;
import android.opengl.*;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import org.skanword.and.libs.qustom.QustomDialogBuilder;
import org.skanword.and.network.*;
import java.util.*;
import android.annotation.*;
import android.graphics.*;
import android.view.*;
import org.skanword.and.etc.*;
import org.skanword.and.menu.*;
import android.app.*;
import android.widget.*;
import org.skanword.and.menu.shopmenu.*;

public class SkanwordGameActivity extends Activity implements TopActivityManager.TopActivity, TouchImageView.ImageViewDelegate, IAdsRewardedVideoActivity, IAdsInterstitialActivity
{
    public static final String ALPHA = "йцукенгшщзхъфывапролджэёячсмитьбю";
    public static final String EXTRA_SCANWORD_BOXES_OBJECT = "EXTRA_SCANWORD_BOXES_OBJECT";
    public static final String EXTRA_SELETED_QUESTION = "sel_question";
    public static final String EXTRA_SET_ID = "set_id";
    public static final String EXTRA_SKANWORD_OBJECT = "EXTRA_SCANWORD_OBJECT";
    public static final String EXTRA_SKAN_ID = "cross_id";
    public static final String EXTRA_SOLVE_STATE_OBJECT = "EXTRA_SOLVE_STATE_OBJECT";
    private static final Logger LOG;
    public static boolean isClickOpenKeyboard;
    private static SkanwordBoard mBoard;
    private static boolean mHasCompleteWatchDialog;
    private static boolean mIsStartingShowVideo;
    private static int[] maxTextureSizes;
    private boolean adbotStep;
    private boolean hasSetInitialZoom;
    private int heightKeyboard;
    private View keyboardView;
    private ProgressDialog loadProgressDialog;
    private View mActionBarView;
    private SkanwordImageView mBoardView;
    private Toast mHintToast;
    private TextView mHintsCountTextView;
    private boolean mIsRunning;
    private boolean mKeywordGuessed;
    private MinishopListAdapter mMinishopListAdapter;
    private Skanword.Question mQuestionToOpen;
    private TextView mQuestionsLeftCountTextView;
    private boolean mScanwordFinished;
    private Skanword mSkanword;
    private int mSkanwordId;
    private EventListener mTimerEventListener;
    private EventListener mTutorialStateListener;
    private EventListener mUserInfoEventListener;
    private DisplayMetrics metrics;
    private PlayLayout playLayout;
    protected SharedPreferences prefs;
    protected AndroidVersionUtils utils;
    
    static {
        LOG = Logger.getLogger("com.adamrosenfield.wordswithcrosses");
    }
    
    public SkanwordGameActivity() {
        super();
        this.mHintsCountTextView = null;
        this.mQuestionsLeftCountTextView = null;
        this.mBoardView = null;
        this.mHintToast = null;
        this.adbotStep = true;
        this.hasSetInitialZoom = false;
        this.heightKeyboard = 0;
        this.mQuestionToOpen = null;
        this.mScanwordFinished = false;
        this.utils = AndroidVersionUtils.Factory.getInstance();
        this.mKeywordGuessed = false;
        Log.v("", "ScanwordGameActivity  constructor");
    }
    
    static /* synthetic */ void access000(final SkanwordGameActivity skanwordGameActivity, final View view) {
        skanwordGameActivity.showViewToast(view);
    }
    
    static /* synthetic */ void access100(final SkanwordGameActivity skanwordGameActivity) {
        skanwordGameActivity.goToQuestionsList();
    }
    
    static /* synthetic */ SkanwordBoard access1000() {
        return SkanwordGameActivity.mBoard;
    }
    
    static /* synthetic */ void access1100(final SkanwordGameActivity skanwordGameActivity, final boolean b) {
        skanwordGameActivity.saveScanwordState(b);
    }
    
    static /* synthetic */ boolean access1200(final SkanwordGameActivity skanwordGameActivity) {
        return skanwordGameActivity.checkScanwordFinished();
    }
    
    static /* synthetic */ SkanwordImageView access1400(final SkanwordGameActivity skanwordGameActivity) {
        return skanwordGameActivity.mBoardView;
    }
    
    static /* synthetic */ View access1500(final SkanwordGameActivity skanwordGameActivity) {
        return skanwordGameActivity.keyboardView;
    }
    
    static /* synthetic */ Logger access1600() {
        return SkanwordGameActivity.LOG;
    }
    
    static /* synthetic */ int access1700(final SkanwordGameActivity skanwordGameActivity) {
        return skanwordGameActivity.heightKeyboard;
    }
    
    static /* synthetic */ int access1702(final SkanwordGameActivity skanwordGameActivity, final int heightKeyboard) {
        return skanwordGameActivity.heightKeyboard = heightKeyboard;
    }
    
    static /* synthetic */ void access1800(final SkanwordGameActivity skanwordGameActivity) {
        skanwordGameActivity.showKeyboard();
    }
    
    static /* synthetic */ void access200(final SkanwordGameActivity skanwordGameActivity, final boolean b) {
        skanwordGameActivity.openShop(b);
    }
    
    static /* synthetic */ void access300(final SkanwordGameActivity skanwordGameActivity) {
        skanwordGameActivity.updateTimers();
    }
    
    static /* synthetic */ boolean access400(final SkanwordGameActivity skanwordGameActivity) {
        return skanwordGameActivity.mIsRunning;
    }
    
    static /* synthetic */ boolean access502(final boolean mIsStartingShowVideo) {
        return SkanwordGameActivity.mIsStartingShowVideo = mIsStartingShowVideo;
    }
    
    static /* synthetic */ boolean access600() {
        return SkanwordGameActivity.mHasCompleteWatchDialog;
    }
    
    static /* synthetic */ boolean access602(final boolean mHasCompleteWatchDialog) {
        return SkanwordGameActivity.mHasCompleteWatchDialog = mHasCompleteWatchDialog;
    }
    
    static /* synthetic */ Skanword access700(final SkanwordGameActivity skanwordGameActivity) {
        return skanwordGameActivity.mSkanword;
    }
    
    static /* synthetic */ void access900(final SkanwordGameActivity skanwordGameActivity) {
        skanwordGameActivity.openLetter();
    }
    
    private boolean checkKeywordComplete() {
        return this.mSkanword.getKeyword() != null && !this.mKeywordGuessed && (this.mKeywordGuessed = SkanwordGameActivity.mBoard.isKeywordGuessed());
    }
    
    private boolean checkScanwordFinished() {
        final boolean mScanwordFinished = this.mScanwordFinished;
        if (mScanwordFinished) {
            return mScanwordFinished;
        }
        this.mScanwordFinished = SkanwordGameActivity.mBoard.getCurrentSkanword().isFinished();
        if (this.mScanwordFinished) {
            final Intent intent = new Intent("android.intent.action.EDIT", (Uri)null, (Context)this, (Class)GameFinishActivity.class);
            intent.putExtra("cross_id", this.mSkanword.getId());
            intent.putExtra("EXTRA_SCANWORD_OBJECT", (Serializable)this.mSkanword);
            this.startActivityForResult(intent, 99);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_victory_enter");
            MainNetworkManager.getInstance().requestSyncExec();
        }
        return this.mScanwordFinished;
    }

    private void delayshowKeyboard() {
        new Handler(Looper.getMainLooper()).postDelayed((Runnable)new SkanwordGameActivity.SkanwordGameActivity25(this), 250L);
    }
    
    private void enterLetter(final String s) {
        boolean b = false;
        this.letterEntered(false);
        final int inputLetter = SkanwordGameActivity.mBoard.inputLetter(s, false);
        this.updateQuestionCountLable(inputLetter);
        final boolean checkKeywordComplete = this.checkKeywordComplete();
        if (inputLetter > 0 || checkKeywordComplete) {
            b = true;
        }
        this.render(b);
        if (SkanwordGameActivity.mBoard.getHighlightLetter() != null) {
            this.mBoardView.ensureVisible(SkanwordGameActivity.mBoard.getHighlightLetter());
        }
        this.updateQuestionTextView();
        if (checkKeywordComplete) {
            this.showKeywordBonus();
            return;
        }
        if (this.checkScanwordFinished()) {
            SoundsManager.playSound((Context)this, R.raw.win);
            return;
        }
        if (inputLetter > 0) {
            SoundsManager.playSound((Context)this, R.raw.word_correct);
            return;
        }
        SoundsManager.playSound((Context)this, R.raw.hint_letter);
    }
    
    private void goToQuestionsList() {
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
            TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_COMPLETE);
        }
        final Intent intent = new Intent("android.intent.action.EDIT", (Uri)null, (Context)this, (Class)QuestionsActivity.class);
        intent.putExtra("cross_id", this.mSkanwordId);
        final Skanword mSkanword = this.mSkanword;
        if (mSkanword != null) {
            intent.putExtra("EXTRA_SCANWORD_OBJECT", (Serializable)mSkanword);
        }
        SkanwordGameActivity.isClickOpenKeyboard = false;
        this.startActivityForResult(intent, 1);
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_questions_enter");
    }
    
    private void hideKeyboard() {
        SkanwordGameActivity.LOG.info("Keyboard hide");
        SkanwordGameActivity.isClickOpenKeyboard = false;
        if (this.keyboardView.getVisibility() != View.GONE) {
            this.keyboardView.setVisibility(View.GONE);
            this.updateBoardViewPosition();
            Log.v("SkanwordsGame", "hidekeyboard");
        }
    }
    
    private void hintTutorialStep() {
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
            TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_SKANWORD_COMPLETE);
        }
    }
    
    private void initKeyboard() {
        this.updateKeyboardFromPrefs();
    }
    
    private void initPlayboard() {
        (this.mBoardView = (SkanwordImageView)this.findViewById(R.id.board)).setDelegate((TouchImageView.ImageViewDelegate)this);
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mSkanword);
        sb.append("  ");
        sb.append(this.mSkanword.getImageDir());
        Log.v("", sb.toString());
        SkanwordGameActivity.mBoard = new SkanwordBoard(this.mSkanword, this.getResources().getConfiguration().orientation, this.getAssets());
        this.mKeywordGuessed = SkanwordGameActivity.mBoard.isKeywordGuessed();
        this.mBoardView.setBoard(SkanwordGameActivity.mBoard, this.metrics, Integer.valueOf(SkanwordGameActivity.maxTextureSizes[0]));
        this.registerForContextMenu((View)this.mBoardView);
        this.mBoardView.setClickListener((SkanwordImageView.ClickListener)new SkanwordGameActivity.SkanwordGameActivity21(this));
        this.playLayout.setOnChangeKeyboadListener((PlayLayout.IChandeKeyboardViewListener)new SkanwordGameActivity.SkanwordGameActivity22(this));
        this.playLayout.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new SkanwordGameActivity.SkanwordGameActivity23(this));
        this.updateQuestionCountLable(-1);
    }
    
    public static boolean isGameActive() {
        return sGetSkanwordBoard() != null;
    }
    
    private void letterEntered(final boolean b) {
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_WORD_SELECTED || (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED && b)) {
            final TutorialManager instance = TutorialManager.getInstance();
            TutorialManager.TutorialStep tutorialStep;
            if (b) {
                tutorialStep = TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT;
            }
            else {
                tutorialStep = TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED;
            }
            instance.setTutorialStep(tutorialStep);
        }
    }
    
    private void openLetter() {
        final boolean b = true;
        this.letterEntered(true);
        final int openLetter = SkanwordGameActivity.mBoard.openLetter();
        this.updateQuestionCountLable(openLetter);
        final boolean checkKeywordComplete = this.checkKeywordComplete();
        boolean b2 = b;
        if (openLetter <= 0) {
            b2 = (checkKeywordComplete && b);
        }
        this.render(b2);
        this.mBoardView.ensureVisible(SkanwordGameActivity.mBoard.getHighlightLetter());
        this.setVolumeControlStream(3);
        this.updateQuestionTextView();
        if (checkKeywordComplete) {
            this.showKeywordBonus();
            return;
        }
        if (this.checkScanwordFinished()) {
            SoundsManager.playSound((Context)this, R.raw.win);
            return;
        }
        if (openLetter > 0) {
            SoundsManager.playSound((Context)this, R.raw.word_correct);
            return;
        }
        SoundsManager.playSound((Context)this, R.raw.hint_letter);
    }

    private void openShop(final boolean b) {
        final boolean boolean1 = SmappsScanwords.getAppSharedPreferences("GENERAL_PREFERENCES_NAME").getBoolean("show_hints_offers", false);
        final boolean b2 = SkanwordGameActivity.mBoard.getHighlightLetter() != null && !SkanwordGameActivity.mBoard.isHighlightedBoxGuessed() && boolean1 && MainDataManager.getInstance().getUserData().getHints() < 1;
        if (AdsManager.getInstance().hasRewardedVideo() && b && b2) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_hints4video_hintuse_show");
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this, R.style.Scanword_style_dialog);
            alertDialogBuilder.setMessage("Открыть букву за просмотр видео?").setPositiveButton("Ок", (DialogInterface.OnClickListener)new SkanwordGameActivity.SkanwordGameActivity17(this)).setNegativeButton("Отмена", (DialogInterface.OnClickListener)null).setOnCancelListener((DialogInterface.OnCancelListener)new SkanwordGameActivity.SkanwordGameActivity16(this));
            alertDialogBuilder.create().show();
            return;
        }
        final LayoutInflater from = LayoutInflater.from((Context)this);
        final QustomDialogBuilder setTitleColor = new QustomDialogBuilder(this).setDividerColor("#8fb815").setTitle("Покупка подсказок").setTitleColor("#000000");
        setTitleColor.setPositiveButton("Закрыть", (DialogInterface.OnClickListener)new SkanwordGameActivity.SkanwordGameActivity18(this));
        final View inflate = from.inflate(R.layout.mini_shop_layout, (ViewGroup)null);
        final ListView listView = (ListView)inflate.findViewById(R.id.miniShopList);
        final List shopItemsOfType = MainDataManager.getInstance().getShopDataManager().getShopItemsOfType(ShopDataManager.ShopProductType.MINI_SHOP);
        setTitleColor.setCustomView(R.layout.shop_menu_layout,this);
        final StringBuilder sb = new StringBuilder();
        sb.append("openShop  ");
        sb.append(boolean1);
        Log.v("SkanwordsFunc", sb.toString());
        listView.setAdapter((ListAdapter)(this.mMinishopListAdapter = new SkanwordGameActivity.MinishopListAdapter((Context)this, R.layout.shop_item_row, shopItemsOfType, (Activity)this, b2, setTitleColor.show())));
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_minishop_enter");
    }
    
    private void postPuzzleLoaded() {
        this.initPlayboard();
        this.initKeyboard();
        final int intExtra = this.getIntent().getIntExtra("sel_question", -1);
        if (intExtra >= 0 && intExtra < this.mSkanword.getQuestions().size()) {
            final Skanword.Question question = this.mSkanword.getQuestions().get(intExtra);
            if (question != null) {
                SkanwordGameActivity.mBoard.setHighlightLetter(new SkanwordBoard.Position(question.getQuestionData().getX(), question.getQuestionData().getY()));
                this.delayshowKeyboard();
            }
        }
    }

    private void recalcField() {
        float f;
        block4 : {
            if (this.hasSetInitialZoom) return;
            String string = this.prefs.getString("initialZoom", "0");
            try {
                f = Float.parseFloat(string);
                break block4;
            }
            catch (NumberFormatException numberFormatException) {}
            f = 0.0f;
        }
        if (Math.abs(f) < 0.01f) {
            this.mBoardView.fitToScreen();
        } else {
            this.mBoardView.setRenderScale(f);
        }
        this.hasSetInitialZoom = true;
        this.render(false);
    }
    
    private void render(final boolean b) {
        Log.v("", "render  Play crossword activity");
        this.mBoardView.render(b);
        this.mBoardView.requestFocus();
    }
    
    public static SkanwordBoard sGetSkanwordBoard() {
        return SkanwordGameActivity.mBoard;
    }
    
    private void saveScanwordState(final boolean b) {
        if (SkanwordGameActivity.mBoard != null) {
            MainDataManager.getInstance().getSkanwordsDataManager().saveSkanwordState(SkanwordGameActivity.mBoard.getCurrentSkanword(), b);
        }
    }
    
    private void showKeyboard() {
        Log.v("SkanwordsFunc", "Keyboard show");
        SkanwordGameActivity.isClickOpenKeyboard = true;
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_SKANWORD_OPENED) {
            TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_WORD_SELECTED);
        }
        if (this.keyboardView.getVisibility() != View.VISIBLE) {
            this.keyboardView.setVisibility(View.VISIBLE);
            this.updateBoardViewPosition();
            Log.v("SkanwordsGame", "showkeyboard");
        }
    }

    private void showKeywordBonus() {
        KeywordHintBonusDialog.showDialogInContext((Context)this, (HintsBonusDialog.HintsBonusDialogDelegate)new SkanwordGameActivity.SkanwordGameActivity20(this), (String)mBoard.getKeywordString());
    }
    
    private void showViewToast(final View view) {
        CharSequence charSequence = null;
        switch (view.getId()) {
            case R.id.open_shop_button: {
                charSequence ="Магазин";
                break;
            }
            case R.id.questions_list_button: {
                charSequence = "Список вопросов";
                break;
            }
            case R.id.use_hint_button: {
                charSequence = "Подсказка";
                break;
            }
            case R.id.watchVideoButton: {
                charSequence = "Бесплатная подсказка";
                break;
            }
            default: {
                charSequence = null;
                break;
            }
        }
        if (charSequence == null) {
            return;
        }
        final Toast text = Toast.makeText(this.getApplicationContext(), charSequence, Toast.LENGTH_SHORT);
        final float density = this.getApplicationContext().getResources().getDisplayMetrics().density;
        text.setGravity(51, (int)(view.getLeft() * density + 0.5f), (int)(48.0f * density + 0.5f));
        text.show();
    }

    private void tutorialComplete() {
        SmappsScanwords.getScheduleExecutorService().schedule((Runnable)new SkanwordGameActivity.SkanwordGameActivity13(this), 3L, TimeUnit.SECONDS);
    }


    private void updateBoardViewPosition() {
        new Handler(Looper.getMainLooper()).postDelayed((Runnable)new SkanwordGameActivity.SkanwordGameActivity24(this), 1L);
    }
    
    private void updateKeyboardFromPrefs() {
        (this.keyboardView = this.findViewById(R.id.playKeyboard)).setVisibility(View.GONE);
    }
    
    private void updateQuestionTextView() {
        final StringBuilder sb = new StringBuilder();
        sb.append("updateQuestionTextView ");
        sb.append(SkanwordGameActivity.mBoard.getHightlightQuestion().getQuestionData().getQuestionValue().getOriginal());
        Log.v("SkanwordsGame", sb.toString());
        final SkanwordBoard mBoard = SkanwordGameActivity.mBoard;
        if (mBoard != null) {
            if (mBoard.getHightlightQuestion() == null) {
                return;
            }
            String original;
            if (SkanwordGameActivity.mBoard.getHightlightQuestion().getQuestionData().getQuestionValue().isTextQuestion()) {
                original = SkanwordGameActivity.mBoard.getHightlightQuestion().getQuestionData().getQuestionValue().getOriginal();
            }
            else {
                original = "Картинка";
            }
            ((AutofitTextView)this.findViewById(R.id.questionText)).setText((CharSequence)original);
        }
    }
    
    private void updateTimers() {
        final MinishopListAdapter mMinishopListAdapter = this.mMinishopListAdapter;
        if (mMinishopListAdapter != null) {
            mMinishopListAdapter.updateTimerView();
        }
    }
    
    public void cellClicked(final SkanwordBoard.Position highlightLetter) {
        final SkanwordBoard mBoard = SkanwordGameActivity.mBoard;
        if (mBoard != null && this.mBoardView != null) {
            final boolean b = highlightLetter != null && !highlightLetter.equals(mBoard.getHighlightLetter());
            if (highlightLetter != null && !b && !SkanwordGameActivity.isClickOpenKeyboard) {
                this.showKeyboard();
            }
            else {
                final boolean b2 = true ^ SkanwordGameActivity.isClickOpenKeyboard;
                if (b2) {
                    this.showKeyboard();
                }
                if (highlightLetter != null) {
                    SkanwordGameActivity.mBoard.setHighlightLetter(highlightLetter);
                }
                this.mBoardView.zoomIn();
                this.render(false);
                if (!b2 || b) {
                    this.mBoardView.ensureVisible(SkanwordGameActivity.mBoard.getHighlightLetter());
                }
            }
            this.updateQuestionTextView();
            return;
        }
        this.finish();
    }

    protected void checkTutorialState() {
        final TutorialManager.TutorialStep currentTutorialStep = TutorialManager.getInstance().currentTutorialStep();
        final TutorialManager.TutorialStep tutorial_SKANWORD_OPENED = TutorialManager.TutorialStep.TUTORIAL_SKANWORD_OPENED;
        final boolean b = true;
        final boolean b2 = currentTutorialStep == tutorial_SKANWORD_OPENED;
        boolean b3 = b;
        if (TutorialManager.getInstance().currentTutorialStep() != TutorialManager.TutorialStep.TUTORIAL_WORD_SELECTED) {
            b3 = b;
            if (TutorialManager.getInstance().currentTutorialStep() != TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED) {
                b3 = b;
                if (TutorialManager.getInstance().currentTutorialStep() != TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT) {
                    b3 = b;
                    if (TutorialManager.getInstance().currentTutorialStep() != TutorialManager.TutorialStep.TUTORIAL_WORD_ENTERED) {
                        b3 = b;
                        if (TutorialManager.getInstance().currentTutorialStep() != TutorialManager.TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
                            b3 = (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_SKANWORD_COMPLETE && b);
                        }
                    }
                }
            }
        }
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_SKANWORD_COMPLETE) {
            this.tutorialComplete();
        }
        this.runOnUiThread((Runnable)new SkanwordGameActivity.SkanwordGameActivity12(this, b2, b3));
    }
    
    protected void checkVideoOfferAvailability() {
        final boolean hasRewardedVideo = AdsManager.getInstance().hasRewardedVideo();
        final boolean hasNetworkConnection = MainNetworkManager.getInstance().hasNetworkConnection();
        this.mActionBarView.findViewById(R.id.newVideoIcon).setVisibility(View.INVISIBLE);
        this.mActionBarView.findViewById(R.id.faqIcon).setVisibility(View.INVISIBLE);
        if (!hasNetworkConnection) {
            this.mActionBarView.findViewById(R.id.faqIcon).setVisibility(View.VISIBLE);
            return;
        }
        if (hasRewardedVideo) {
            this.mActionBarView.findViewById(R.id.newVideoIcon).setVisibility(View.VISIBLE);
        }
    }
    
    public void keyboardButtonClicked(final View view) {
        this.enterLetter(((Button)view).getText().toString().toUpperCase());
    }
    
    public void keyboardDeleteButtonClicked(final View view) {
        this.enterLetter(null);
    }
    
    public void keyboardHideButtonClicked(final View view) {
        this.hideKeyboard();
    }
    
    public void keyboardSpaceButtonClicked(final View view) {
        this.enterLetter(" ");
    }
    
    protected void onActivityResult(final int n, final int result, final Intent intent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(n);
        sb.append(" ---------onActivityResult---------  ");
        sb.append(result);
        Log.v("", sb.toString());
        InAppManager.getInstance().onActivityResult(n, result, intent);
        if (n == 99 && result == -1) {
            this.finish();
            return;
        }
        if (n == 99 && result == 555) {
            this.setResult(result);
            this.finish();
            return;
        }
        if (intent != null) {
            if (result == 0) {
                return;
            }
            this.mQuestionToOpen = (Skanword.Question)intent.getExtras().getSerializable("sel_question");
        }
    }
    
    public void onBackPressed() {
        if (this.keyboardView.getVisibility() != View.GONE) {
            this.hideKeyboard();
            return;
        }
        super.onBackPressed();
    }
    
    @SuppressLint({ "NewApi" })
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final DailyBonusObject dailyBonus = MainDataManager.getInstance((Context)this).getDailyBonus();
        if (dailyBonus != null) {
            dailyBonus.setScratchedPosition(0);
        }
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_ISSUE_OPENED) {
            TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_SKANWORD_OPENED);
        }
        this.prefs = PreferenceManager.getDefaultSharedPreferences((Context)this);
        this.mSkanwordId = this.getIntent().getIntExtra("cross_id", -1);
        this.mSkanword = (Skanword)this.getIntent().getSerializableExtra("EXTRA_SCANWORD_OBJECT");
        if (this.mSkanword == null) {
            this.mSkanword = SmappsScanwords.getDatabaseHelper((Context)this).getFullskanwordData(Integer.valueOf(this.mSkanwordId));
        }
        this.mScanwordFinished = this.mSkanword.isFinished();
        if (!this.mSkanword.hasFiles()) {
            this.setResult(556);
            this.finish();
            return;
        }
        this.setVolumeControlStream(3);
        if (this.mSkanwordId != -1) {
            final Iterator<Skanword.Question> iterator = this.mSkanword.getQuestions().iterator();
            while (iterator.hasNext()) {
                iterator.next().setComplete(false);
            }
            if (SkanwordGameActivity.maxTextureSizes == null) {
                GLES20.glGetIntegerv(3379, SkanwordGameActivity.maxTextureSizes = new int[1], 0);
                final int[] maxTextureSizes = SkanwordGameActivity.maxTextureSizes;
                if (maxTextureSizes[0] == 0) {
                    GLES11.glGetIntegerv(3379, maxTextureSizes, 0);
                }
                final int[] maxTextureSizes2 = SkanwordGameActivity.maxTextureSizes;
                if (maxTextureSizes2[0] == 0) {
                    maxTextureSizes2[0] = 3379;
                }
            }
            this.getWindow().setSoftInputMode(2);
            this.metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(this.metrics);
            final ActionBar actionBar = this.getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
            (this.mActionBarView = ((LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.scanwords_game_action_bar, (ViewGroup)null)).setLayoutParams((ViewGroup.LayoutParams)new ActionBar.LayoutParams(-1, -1));
            actionBar.setCustomView(this.mActionBarView);
            this.mActionBarView.findViewById(R.id.use_hint_button).setOnClickListener((View.OnClickListener)new SkanwordGameActivity.SkanwordGameActivity1(this));
            this.mHintsCountTextView = (TextView)this.mActionBarView.findViewById(R.id.hints_count_label);
            this.mActionBarView.findViewById(R.id.use_hint_button).setOnLongClickListener((View.OnLongClickListener)new SkanwordGameActivity.SkanwordGameActivity2(this));
            this.mActionBarView.findViewById(R.id.watchVideoButton).setOnClickListener((View.OnClickListener)new SkanwordGameActivity.SkanwordGameActivity3(this));
            this.mActionBarView.findViewById(R.id.watchVideoButton).setOnLongClickListener((View.OnLongClickListener)new SkanwordGameActivity.SkanwordGameActivity4(this));
            this.mActionBarView.findViewById(R.id.questions_list_button).setOnClickListener((View.OnClickListener)new SkanwordGameActivity.SkanwordGameActivity5(this));
            this.mActionBarView.findViewById(R.id.questions_list_button).setOnLongClickListener((View.OnLongClickListener)new SkanwordGameActivity.SkanwordGameActivity6(this));
            this.mQuestionsLeftCountTextView = (TextView)this.mActionBarView.findViewById(R.id.questions_count_label);
            this.mActionBarView.findViewById(R.id.open_shop_button).setOnClickListener((View.OnClickListener)new SkanwordGameActivity.SkanwordGameActivity7(this));
            this.mActionBarView.findViewById(R.id.open_shop_button).setOnLongClickListener((View.OnLongClickListener)new SkanwordGameActivity.SkanwordGameActivity8(this));
            this.checkVideoOfferAvailability();
            this.setContentView(R.layout.play);
            this.utils.holographic((Activity)this);
            this.setTitle((CharSequence)"");
            this.playLayout = (PlayLayout)this.findViewById(R.id.gameLayout);
            this.postPuzzleLoaded();
            this.mTimerEventListener = (EventListener)new SkanwordGameActivity.SkanwordGameActivity9(this);
            if (this.mTimerEventListener != null) {
                SmappsScanwords.getEventsDispatcher().addListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
            }
            this.updateHintsCountLabel();
            this.mUserInfoEventListener = (EventListener)new SkanwordGameActivity.SkanwordGameActivity10(this);
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_USER_INFO_UPDATE", this.mUserInfoEventListener);
            this.mTutorialStateListener = (EventListener)new SkanwordGameActivity.SkanwordGameActivity11(this);
            SmappsScanwords.getEventsDispatcher().addListener("TUTORIAL_STATE_CHANGED", this.mTutorialStateListener);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("WRONG ID crosswordId = ");
        sb.append(this.mSkanwordId);
        throw new RuntimeException(sb.toString());
    }
    
    protected void onDestroy() {
        Log.v("", "onDestroy ----");
        final SkanwordBoard mBoard = SkanwordGameActivity.mBoard;
        if (mBoard != null) {
            mBoard.getCurrentSkanword();
        }
        SkanwordGameActivity.mBoard = null;
        final SkanwordImageView mBoardView = this.mBoardView;
        if (mBoardView != null) {
            mBoardView.setDelegate(null);
            this.mBoardView.setBoard(null);
            this.mBoardView.setImageBitmap((Bitmap)null);
        }
        this.mBoardView = null;
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
            TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_COMPLETE);
        }
        SmappsScanwords.getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putBoolean("game_played", true).commit();
        if (this.mTimerEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        }
        if (this.mUserInfoEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_USER_INFO_UPDATE", this.mUserInfoEventListener);
        }
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_USER_INFO_UPDATE", this.mUserInfoEventListener);
        final ProgressDialog loadProgressDialog = this.loadProgressDialog;
        if (loadProgressDialog != null) {
            loadProgressDialog.dismiss();
            this.loadProgressDialog = null;
        }
        super.onDestroy();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.hideKeyboard();
        this.finish();
        return true;
    }
    
    protected void onPause() {
        Log.v("", " ------------------   onPause");
        if (SkanwordGameActivity.mBoard != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onPause saveScanwordState ");
            sb.append(SkanwordGameActivity.mBoard.isProgressChanged());
            Log.v("SkanwordsFunc", sb.toString());
            this.saveScanwordState(SkanwordGameActivity.mBoard.isProgressChanged());
        }
        final boolean isClickOpenKeyboard = SkanwordGameActivity.isClickOpenKeyboard;
        this.hideKeyboard();
        final Toast mHintToast = this.mHintToast;
        if (mHintToast != null) {
            mHintToast.cancel();
        }
        SkanwordGameActivity.isClickOpenKeyboard = isClickOpenKeyboard;
        super.onPause();
        this.mIsRunning = false;
    }
    
    protected void onRestart() {
        super.onRestart();
        Log.v("", " ------------------   onRestart");
    }
    
    protected void onResume() {
        this.mIsRunning = true;
        super.onResume();
        AdsManager.getInstance().activityResumed(this);
        SkanwordGameActivity.isClickOpenKeyboard = false;
        final Skanword.Question mQuestionToOpen = this.mQuestionToOpen;
        if (mQuestionToOpen != null) {
            this.cellClicked(new SkanwordBoard.Position(mQuestionToOpen.getQuestionData().getX(), this.mQuestionToOpen.getQuestionData().getY()));
            this.mQuestionToOpen = null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(" ----- onResume: mHasCompleteWatchDialog =");
        sb.append(SkanwordGameActivity.mHasCompleteWatchDialog);
        sb.append(" mIsStartingShowVideo =");
        sb.append(SkanwordGameActivity.mIsStartingShowVideo);
        Log.v("", sb.toString());
        if (SkanwordGameActivity.mHasCompleteWatchDialog) {
            SkanwordGameActivity.mHasCompleteWatchDialog = false;
            HintForVideoDialog.showCompleteWatchDialogInContext((Context)this);
        }
    }
    
    protected void onStart() {
        super.onStart();
        final StringBuilder sb = new StringBuilder();
        sb.append(" -- onStart: mHasCompleteWatchDialog = ");
        sb.append(SkanwordGameActivity.mHasCompleteWatchDialog);
        sb.append(" mIsStartingShowVideo =");
        sb.append(SkanwordGameActivity.mIsStartingShowVideo);
        Log.v("", sb.toString());
        this.checkTutorialState();
        if (this.adbotStep) {
            if (!this.mSkanword.isOpened()) {
                if (!SkanwordGameActivity.mHasCompleteWatchDialog && !SkanwordGameActivity.mIsStartingShowVideo) {
                    AdsManager.getInstance().showAds(new AdsManager.AdsEventsListener(), 11);
                }
                this.mSkanword.setOpened(true);
                this.mSkanword.setChanged(true);
                SkanwordGameActivity.mBoard.setProgressChanged(true);
            }
            else if (!SkanwordGameActivity.mHasCompleteWatchDialog && !SkanwordGameActivity.mIsStartingShowVideo) {
                AdsManager.getInstance().showInterstitial(this);
            }
            this.adbotStep = false;
        }
    }
    
    protected void onStop() {
        super.onStop();
        Log.v("", " ------------------   onStop");
    }
    
    public void onWindowFocusChanged(final boolean b) {
        this.recalcField();
    }

    public void openCellForOffer() {
        this.hideKeyboard();
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_hints4video_video_enter");
        final AdsManager instance = AdsManager.getInstance();
        final SkanwordGameActivity.SkanwordGameActivity15 skanwordGameActivity15 = new SkanwordGameActivity.SkanwordGameActivity15(this);
        final StringBuilder sb = new StringBuilder();
        sb.append("hc");
        sb.append(this.mSkanword.getId());
        instance.showVideoOffer((Activity)this, (AdsManager.AdsEventsListener)skanwordGameActivity15, sb.toString(), true);
    }
    
    public void questionNextButtonClicked(final View view) {
        if (SkanwordGameActivity.mBoard.getHightlightQuestion() == null) {
            return;
        }
        final Skanword.Question nextToQuestion = this.mSkanword.getNextToQuestion(SkanwordGameActivity.mBoard.getHightlightQuestion());
        if (nextToQuestion == null) {
            return;
        }
        this.cellClicked(new SkanwordBoard.Position(nextToQuestion.getQuestionData().getX(), nextToQuestion.getQuestionData().getY()));
    }
    
    public void questionPreviousButtonClicked(final View view) {
        if (SkanwordGameActivity.mBoard.getHightlightQuestion() == null) {
            return;
        }
        final Skanword.Question previousToQuestion = this.mSkanword.getPreviousToQuestion(SkanwordGameActivity.mBoard.getHightlightQuestion());
        if (previousToQuestion == null) {
            return;
        }
        this.cellClicked(new SkanwordBoard.Position(previousToQuestion.getQuestionData().getX(), previousToQuestion.getQuestionData().getY()));
    }
    
    public void scaleStarted() {
        this.hideKeyboard();
    }
    
    public void scrollStarted() {
        this.hideKeyboard();
    }

    protected void showVideoForHintDialog() {
        final boolean hasRewardedVideo = AdsManager.getInstance().hasRewardedVideo();
        if (!MainNetworkManager.getInstance().hasNetworkConnection()) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this, R.style.Theme_AppCompat_DayNight);
            alertDialogBuilder.setMessage("Для получения подсказки необходимо подключение к сети Интернет.").setPositiveButton("Ок", (DialogInterface.OnClickListener)null);
            alertDialogBuilder.create().show();
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hfv_dialog_no_network_click");
            return;
        }
        if (hasRewardedVideo) {
            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_COMPLETE);
            }
            SkanwordGameActivity.mIsStartingShowVideo = true;
            HintForVideoDialog.showsDialogInContext((Context)this, (HintForVideoDialog.HintForVideoDialogDelegate)new SkanwordGameActivity.SkanwordGameActivity14(this));
            return;
        }
        final AlertDialog.Builder alertDialogBuilder2 = new AlertDialog.Builder((Context)this, R.style.Theme_AppCompat_DayNight);
        alertDialogBuilder2.setMessage("В данный момент, к сожалению, нет видео. Попробуйте позже.").setPositiveButton("Ок", (DialogInterface.OnClickListener)null);
        alertDialogBuilder2.create().show();
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hfv_dialog_no_offer_click");
    }
    
    public void updateHintsCountLabel() {
        final TextView mHintsCountTextView = this.mHintsCountTextView;
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(MainDataManager.getInstance().getUserData().getHints());
        mHintsCountTextView.setText((CharSequence)sb.toString());
    }
    
    public void updateQuestionCountLable(final int n) {
        if (n > 0) {
            final Context applicationContext = this.getApplicationContext();
            final StringBuilder sb = new StringBuilder();
            sb.append("+");
            sb.append(n);
            sb.append(" ");
            sb.append(Utils.spellVariantForNumber(n, "очко", "очка", "очков"));
            final String string = sb.toString();
            final Toast mHintToast = this.mHintToast;
            if (mHintToast != null) {
                mHintToast.cancel();
            }
            (this.mHintToast = Toast.makeText(applicationContext, (CharSequence)string, Toast.LENGTH_SHORT)).setGravity(16, 0, 0);
            this.mHintToast.show();
        }
        if (n != 0) {
            final TextView mQuestionsLeftCountTextView = this.mQuestionsLeftCountTextView;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(this.mSkanword.getQuestions().size() - SkanwordGameActivity.mBoard.getCurrentSkanword().getFinishedQuestionsCount());
            mQuestionsLeftCountTextView.setText((CharSequence)sb2.toString());
        }
    }

    public void useHint() {
        if (MainDataManager.getInstance().getUserData().getHints() <= 0) {
            this.openShop(true);
            return;
        }
        if (SkanwordGameActivity.mBoard.getHighlightLetter() == null) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this, R.style.Theme_AppCompat_DayNight);
            alertDialogBuilder.setMessage("Выберите клетку сканворда, чтобы использовать подсказку.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new SkanwordGameActivity.SkanwordGameActivity19(this));
            alertDialogBuilder.create().show();
            return;
        }
        if (!SkanwordGameActivity.mBoard.isHighlightedBoxGuessed()) {
            MainDataManager.getInstance().addUserHints(-1);
            final Context applicationContext = this.getApplicationContext();
            final Toast mHintToast = this.mHintToast;
            if (mHintToast != null) {
                mHintToast.cancel();
            }
            (this.mHintToast = Toast.makeText(applicationContext, "-1 подсказка", Toast.LENGTH_SHORT)).setGravity(16, 0, 0);
            this.mHintToast.show();
            this.updateHintsCountLabel();
            UserUpdatesManager.getInstance().usedHint(1);
            this.hintTutorialStep();
        }
        this.openLetter();
    }
    
    public static class MinishopListAdapter extends ArraySectionedListAdapter<ShopDataManager.ShopItem>
    {
        private final Activity mActivity;
        private List<ShopDataManager.ShopItem> mObjectsList;
        private boolean mOffersEnabled;
        private int mResourceId;
        private View mSectionHeaderView;
        private AlertDialog miniShopDialog;
        
        public MinishopListAdapter(final Context context, final int mResourceId, final List<ShopDataManager.ShopItem> mObjectsList, final Activity mActivity, final boolean mOffersEnabled, final AlertDialog miniShopDialog) {
            super(context, mResourceId, mObjectsList);
            this.mResourceId = mResourceId;
            this.mActivity = mActivity;
            this.mObjectsList = mObjectsList;
            this.mOffersEnabled = mOffersEnabled;
            this.miniShopDialog = miniShopDialog;
        }
        
        static /* synthetic */ View access1900(final MinishopListAdapter minishopListAdapter) {
            return minishopListAdapter.mSectionHeaderView;
        }
        
        static /* synthetic */ AlertDialog access2000(final MinishopListAdapter minishopListAdapter) {
            return minishopListAdapter.miniShopDialog;
        }
        
        static /* synthetic */ Activity access2100(final MinishopListAdapter minishopListAdapter) {
            return minishopListAdapter.mActivity;
        }
        
        static /* synthetic */ void access800(final MinishopListAdapter minishopListAdapter) {
            minishopListAdapter.updateTimerView();
        }
        
        private View createRowView(final ViewGroup viewGroup) {
            return LayoutInflater.from(this.getContext()).inflate(this.mResourceId, viewGroup, false);
        }

        private void rebuildView(final View view, final ShopDataManager.ShopItem shopDataManagerShopShopItemsSetShopItem) {
            final Button button = (Button)view.findViewById(R.id.buyButton);
            final TextView textView = (TextView)view.findViewById(R.id.itemNameLabel);
            view.setClickable(true);
            textView.setTextColor(-16777216);
            final int n = 8;
            if (shopDataManagerShopShopItemsSetShopItem == null && this.mOffersEnabled) {
                textView.setText("Открыть\nза видео");
                ((LinearLayout)view.findViewById(R.id.sale_label)).setVisibility(View.GONE);
                ((ImageView)view.findViewById(R.id.itemTypeIcon)).setBackgroundResource(R.drawable.icon_open_for_video);
                button.setText("Открыть");
                button.setBackgroundResource(R.drawable.orange_btn_d);
                button.setOnClickListener((View.OnClickListener)new MinishopListAdapter.SkanwordGameActivityMinishopListAdapter2(this));
                return;
            }
            textView.setText((CharSequence)shopDataManagerShopShopItemsSetShopItem.getName());
            final LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.sale_label);
            int visibility = n;
            if (shopDataManagerShopShopItemsSetShopItem.isSale()) {
                visibility = n;
                if (MainDataManager.getInstance().getShopDataManager().saleEnabled()) {
                    visibility = 0;
                }
            }
            linearLayout.setVisibility(visibility);
            ((ImageView)view.findViewById(R.id.itemTypeIcon)).setBackgroundResource(MenuShop.ShopItemsAdapter.getIconId(shopDataManagerShopShopItemsSetShopItem, ShopDataManager.ShopProductType.HINTS));
            button.setOnClickListener((View.OnClickListener)new SkanwordGameActivityMinishopListAdapter3(this, shopDataManagerShopShopItemsSetShopItem));
            String priceForProductId;
            if ((priceForProductId = InAppManager.getInstance().getPriceForProductId(shopDataManagerShopShopItemsSetShopItem.getId())).equals("")) {
                priceForProductId = "\u043a\u0443\u043f\u0438\u0442\u044c";
            }
            if (priceForProductId != null) {
                button.setText((CharSequence)priceForProductId);
            }
        }

        private void updateTimerView() {
            this.mActivity.runOnUiThread((Runnable)new SkanwordGameActivityMinishopListAdapter1(this));
        }

        protected void bindRowViewInSection(Context context, int i, int i2, View view) {
            rebuildView(view, getItemInSectionAndRow(i, i2));
        }

        @Override
        protected void bindSectionView(final Context context, final int n, final View view) {
            Log.v("", " bindSectionView");
            view.setClickable(true);
            this.updateTimerView();
        }

        @Override
        public ShopDataManager.ShopItem getItemInSectionAndRow(final int n, final int n2) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mOffersEnabled);
            sb.append(" getItemInSectionAndRow ");
            sb.append(n2);
            Log.v("", sb.toString());
            if (!this.mOffersEnabled) {
                return this.mObjectsList.get(n2);
            }
            if (n2 == 0) {
                return null;
            }
            return this.mObjectsList.get(n2 - 1);
        }
        
        @Override
        protected int getNumberOfRowsInSection(int i) {
            return this.mObjectsList.size();
        }
        
        @Override
        protected int getNumberOfSections() {
            return 1;
        }
        
        @Override
        protected View newRowViewInSection(final Context context, final int n, final int n2, final ViewGroup viewGroup) {
            return this.createRowView(viewGroup);
        }
        
        @Override
        protected View newSectionView(final Context context, final int n, final ViewGroup viewGroup) {
            if (!MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(ShopDataManager.ShopProductType.HINTS)) {
                return new View(this.getContext());
            }
            if (this.mSectionHeaderView == null) {
                this.mSectionHeaderView = ((LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header, viewGroup, false);
            }
            return this.mSectionHeaderView;
        }

        class SkanwordGameActivityMinishopListAdapter2 implements View.OnClickListener {
            final /* synthetic */ MinishopListAdapter this0;

            SkanwordGameActivityMinishopListAdapter2(final MinishopListAdapter this0) {
                super();
                this.this0 = this0;
            }

            public void onClick(final View view) {
                MinishopListAdapter.access2000(this.this0).dismiss();
                ((SkanwordGameActivity)MinishopListAdapter.access2100(this.this0)).openCellForOffer();
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_minishop_hint4video");
            }
        }

        class SkanwordGameActivityMinishopListAdapter3 implements View.OnClickListener {
            final /* synthetic */ MinishopListAdapter this0;
            final /* synthetic */ ShopDataManager.ShopItem valshopItem;

            SkanwordGameActivityMinishopListAdapter3(final MinishopListAdapter this0, final ShopDataManager.ShopItem valshopItem) {
                super();
                this.this0 = this0;
                this.valshopItem = valshopItem;
            }

            public void onClick(final View view) {
                InAppManager.getInstance().purchaseProduct(this.valshopItem.getId(), MinishopListAdapter.access2100(this.this0), InAppManager.PurchaseSource.MINISHOP);
                final SmappsScanwords.ActionType user_ACTION = SmappsScanwords.ActionType.USER_ACTION;
                final StringBuilder sb = new StringBuilder();
                sb.append("click_minishop_buy_");
                sb.append(this.valshopItem.getId());
                SmappsScanwords.sendGAEvent(user_ACTION, sb.toString());
            }
        }

        class SkanwordGameActivityMinishopListAdapter1 implements Runnable {
            final /* synthetic */ MinishopListAdapter this0;

            SkanwordGameActivityMinishopListAdapter1(final MinishopListAdapter this0) {
                super();
                this.this0 = this0;
            }

            @Override
            public void run() {
                if (MinishopListAdapter.access1900(this.this0) != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Акция! Осталось: ");
                    sb.append(Utils.secondsToTimerType(MainDataManager.getInstance().getShopDataManager().getSaleTimeLeft()));
                    ((TextView)MinishopListAdapter.access1900(this.this0).findViewById(R.id.setLabel)).setText((CharSequence)sb.toString());
                    MinishopListAdapter.access1900(this.this0).invalidate();
                }
            }
        }
    }

    class SkanwordGameActivity25 implements Runnable {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity25(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (SkanwordGameActivity.access1400(this.this0) != null && SkanwordGameActivity.access1000() != null) {
                SkanwordGameActivity.access1800(this.this0);
                SkanwordGameActivity.access1400(this.this0).ensureVisible(SkanwordGameActivity.access1000().getHighlightLetter());
            }
        }
    }

    class SkanwordGameActivity21 implements SkanwordImageView.ClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity21(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final SkanwordBoard.Position skanwordBoardPosition) {
            this.this0.cellClicked(skanwordBoardPosition);
        }

        public void onDoubleClick(final SkanwordBoard.Position skanwordBoardPosition) {
        }

        public void onLongClick(final SkanwordBoard.Position skanwordBoardPosition) {
        }
    }

    class SkanwordGameActivity22 implements PlayLayout.IChandeKeyboardViewListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity22(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onChange(final boolean isClickOpenKeyboard) {
            if (SkanwordGameActivity.access1400(this.this0) != null && SkanwordGameActivity.access1000() != null && SkanwordGameActivity.access1000().getHighlightLetter() != null) {
                SkanwordGameActivity.isClickOpenKeyboard = isClickOpenKeyboard;
            }
        }
    }

    class SkanwordGameActivity23 implements ViewTreeObserver.OnGlobalLayoutListener {
        private boolean isChangedKeyboard = true;
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity23(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onGlobalLayout() {
            if (SkanwordGameActivity.access1000() != null) {
                if (SkanwordGameActivity.access1400(this.this0) == null) {
                    return;
                }
                Log.v("", "onGlobalLayout");
                if (this.isChangedKeyboard && SkanwordGameActivity.access1000().getHighlightLetter() != null) {
                    this.isChangedKeyboard = false;
                    SkanwordGameActivity.access1400(this.this0).ensureVisible(SkanwordGameActivity.access1000().getHighlightLetter());
                }
                final Rect rect = new Rect();
                ((View)SkanwordGameActivity.access1500(this.this0).getParent()).getWindowVisibleDisplayFrame(rect);
                final int n = ((View)SkanwordGameActivity.access1400(this.this0).getParent()).getRootView().getHeight() - (rect.bottom - rect.top);
                final Logger access1600 = SkanwordGameActivity.access1600();
                final StringBuilder sb = new StringBuilder();
                sb.append("heightDifference2 =");
                sb.append(n);
                access1600.info(sb.toString());
                if (n > 0 && SkanwordGameActivity.access1700(this.this0) != n) {
                    SkanwordGameActivity.access1702(this.this0, n);
                    if (SkanwordGameActivity.access1400(this.this0) != null && SkanwordGameActivity.access1000() != null && SkanwordGameActivity.access1000().getHighlightLetter() != null) {
                        SkanwordGameActivity.access1400(this.this0).refresh();
                        SkanwordGameActivity.access1400(this.this0).ensureVisible(SkanwordGameActivity.access1000().getHighlightLetter());
                    }
                    this.isChangedKeyboard = true;
                }
                if (n == 0 && SkanwordGameActivity.access1700(this.this0) != n) {
                    SkanwordGameActivity.access1702(this.this0, n);
                    if (SkanwordGameActivity.access1400(this.this0) != null && SkanwordGameActivity.access1000() != null) {
                        SkanwordGameActivity.access1400(this.this0).refresh();
                        if (SkanwordGameActivity.access1000().getHighlightLetter() != null) {
                            SkanwordGameActivity.access1400(this.this0).ensureVisible(SkanwordGameActivity.access1000().getHighlightLetter());
                        }
                    }
                    this.isChangedKeyboard = true;
                }
            }
        }
    }

    class SkanwordGameActivity17 implements DialogInterface.OnClickListener {
        final SkanwordGameActivity this0;

        SkanwordGameActivity17(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_hints4video_hintuse_approve");
            this.this0.openCellForOffer();
        }
    }

    class SkanwordGameActivity16 implements DialogInterface.OnCancelListener {
        final SkanwordGameActivity this0;

        SkanwordGameActivity16(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onCancel(final DialogInterface dialogInterface) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_hints4video_hintuse_cancel");
        }
    }

    class SkanwordGameActivity18 implements DialogInterface.OnClickListener {
        final SkanwordGameActivity this0;

        SkanwordGameActivity18(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_minishop_close");
        }
    }

    static class SkanwordGameActivity20 implements HintsBonusDialog.HintsBonusDialogDelegate {
        final SkanwordGameActivity this0;

        SkanwordGameActivity20(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        static void access1300(final SkanwordGameActivity20 skanwordGameActivity20, final boolean b, final boolean b2, final boolean b3) {
            skanwordGameActivity20.showCompl(b, b2, b3);
        }

        private void showCompl(final boolean b, final boolean b2, final boolean b3) {
            this.this0.runOnUiThread((Runnable)new SkanwordGameActivity20.SkanwordGameActivity201(this, b, b2, b3));
        }

        public void onCancel() {
            Log.v("SkanwordsFunc", "showKeywordBonus onCancel");
            SkanwordGameActivity.access1200(this.this0);
        }

        public void onDouble() {
            final AdsManager instance = AdsManager.getInstance();
            final SkanwordGameActivity this0 = this.this0;
            final SkanwordGameActivity20.SkanwordGameActivity202 skanwordGameActivity202 = new SkanwordGameActivity20.SkanwordGameActivity202(this);
            final StringBuilder sb = new StringBuilder();
            sb.append("ck");
            sb.append(SkanwordGameActivity.access700(this.this0).getId());
            instance.showVideoOffer((Activity)this0, (AdsManager.AdsEventsListener)skanwordGameActivity202, sb.toString(), true);
        }

        public void onTake(final boolean b) {
            this.showCompl(false, b, false);
        }

        class SkanwordGameActivity201 implements Runnable {
            final /* synthetic */ SkanwordGameActivity20 this1;
            final /* synthetic */ boolean valdoubled;
            final /* synthetic */ boolean valhadRewardedVideo;
            final /* synthetic */ boolean valisShowDialog;

            SkanwordGameActivity201(final SkanwordGameActivity20 this1, final boolean valdoubled, final boolean valhadRewardedVideo, final boolean valisShowDialog) {
                super();
                this.this1 = this1;
                this.valdoubled = valdoubled;
                this.valhadRewardedVideo = valhadRewardedVideo;
                this.valisShowDialog = valisShowDialog;
            }

            @Override
            public void run() {
                MainDataManager.getInstance().addUserHints(1);
                UserUpdatesManager.getInstance().bonusRecieved(this.valdoubled, this.valhadRewardedVideo, false);
                if (this.valisShowDialog) {
                    KeywordHintBonusDialog.showCompleteBonusDialogInContext((Activity)this.this1.this0, this.valdoubled, this.valhadRewardedVideo, (DialogInterface.OnCancelListener)new SkanwordGameActivity201.SkanwordGameActivity2011(this));
                }
                else {
                    SkanwordGameActivity.access1200(this.this1.this0);
                }
                MainNetworkManager.getInstance().requestSyncExec();
            }

            class SkanwordGameActivity2011 implements DialogInterface.OnCancelListener {
                final /* synthetic */ SkanwordGameActivity201 this2;

                SkanwordGameActivity2011(final SkanwordGameActivity201 this2) {
                    super();
                    this.this2 = this2;
                }

                public void onCancel(final DialogInterface dialogInterface) {
                    SkanwordGameActivity.access1200(this.this2.this1.this0);
                }
            }
        }

        class SkanwordGameActivity202 extends AdsManager.AdsEventsListener {
            final /* synthetic */ SkanwordGameActivity20 this1;

            SkanwordGameActivity202(final SkanwordGameActivity20 this1) {
                super();
                this.this1 = this1;
            }

            public void onComplete() {
                super.onComplete();
                Log.v("SkanwordsAds", "onComplete listener");
                SkanwordGameActivity20.access1300(this.this1, true, true, true);
            }

            public void onNoAds() {
                SkanwordGameActivity20.access1300(this.this1, true, false, true);
                Log.v("SkanwordsAds", "onNoAds listener");
            }

            public void onSkipped() {
                super.onSkipped();
                Log.v("SkanwordsAds", "videoSkipped listener");
                SkanwordGameActivity20.access1300(this.this1, true, false, true);
            }
        }
    }

    class SkanwordGameActivity13 implements Runnable {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity13(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_SKANWORD_COMPLETE) {
                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_COMPLETE);
            }
        }
    }

    class SkanwordGameActivity24 implements Runnable {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity24(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (SkanwordGameActivity.access1400(this.this0) != null && SkanwordGameActivity.access1000() != null) {
                SkanwordGameActivity.access1400(this.this0).ensureVisible(SkanwordGameActivity.access1000().getHighlightLetter());
            }
        }
    }

    class SkanwordGameActivity12 implements Runnable {
        final /* synthetic */ SkanwordGameActivity this0;
        final /* synthetic */ boolean valfShowBig;
        final /* synthetic */ boolean valfShowSmall;

        SkanwordGameActivity12(final SkanwordGameActivity this0, final boolean valfShowBig, final boolean valfShowSmall) {
            super();
            this.this0 = this0;
            this.valfShowBig = valfShowBig;
            this.valfShowSmall = valfShowSmall;
        }

        @Override
        public void run() {
            final View viewById = this.this0.findViewById(R.id.tutorialView);
            final boolean valfShowBig = this.valfShowBig;
            final int n = 0;
            int visibility;
            if (valfShowBig) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
            final View viewById2 = this.this0.findViewById(R.id.tutorialViewSmall);
            int visibility2;
            if (this.valfShowSmall) {
                visibility2 = 0;
            }
            else {
                visibility2 = 8;
            }
            viewById2.setVisibility(visibility2);
            final View viewById3 = this.this0.findViewById(R.id.tutorialImageSmall);
            int visibility3 = n;
            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_WORD_SELECTED) {
                visibility3 = 8;
            }
            viewById3.setVisibility(visibility3);
            if (TutorialManager.getInstance().currentTutorialStep() != TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED && TutorialManager.getInstance().currentTutorialStep() != TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT) {
                if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_WORD_ENTERED) {
                    ((ImageView)this.this0.findViewById(R.id.tutorialImageSmall)).setImageResource(R.drawable.tutorial_img_cell_select);
                    ((TextView)this.this0.findViewById(R.id.tutorialTextSmall)).setText("В трудной ситуации выручат\nподсказки. Выберите клетку.");
                    return;
                }
                if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
                    ((ImageView)this.this0.findViewById(R.id.tutorialImageSmall)).setImageResource(R.drawable.tutorial_img_hint_use);
                    ((TextView)this.this0.findViewById(R.id.tutorialTextSmall)).setText("Теперь нажмите на иконку с подсказками.");
                    return;
                }
                if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_SKANWORD_COMPLETE) {
                    ((ImageView)this.this0.findViewById(R.id.tutorialImageSmall)).setImageResource(R.drawable.tutorial_img_complete);
                    ((TextView)this.this0.findViewById(R.id.tutorialTextSmall)).setText("Теперь разгадывать сканворд не составит труда!");
                }
            }
            else {
                ((TextView)this.this0.findViewById(R.id.tutorialTextSmall)).setText("Правильный ответ будет выделен зеленым.");
            }
        }
    }

    class SkanwordGameActivity1 implements View.OnClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity1(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            this.this0.useHint();
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_playscan_hintsbtn");
        }
    }

    class SkanwordGameActivity2 implements View.OnLongClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity2(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public boolean onLongClick(final View view) {
            SkanwordGameActivity.access000(this.this0, view);
            return false;
        }
    }

    class SkanwordGameActivity3 implements View.OnClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity3(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            this.this0.showVideoForHintDialog();
        }
    }

    class SkanwordGameActivity4 implements View.OnLongClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity4(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public boolean onLongClick(final View view) {
            SkanwordGameActivity.access000(this.this0, view);
            return false;
        }
    }

    class SkanwordGameActivity6 implements View.OnLongClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity6(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public boolean onLongClick(final View view) {
            SkanwordGameActivity.access000(this.this0, view);
            return false;
        }
    }

    class SkanwordGameActivity5 implements View.OnClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity5(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            SkanwordGameActivity.access100(this.this0);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_playscan_questionsbtn");
        }
    }

    class SkanwordGameActivity7 implements View.OnClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity7(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            SkanwordGameActivity.access200(this.this0, false);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_playscan_openshop");
        }
    }

    class SkanwordGameActivity8 implements View.OnLongClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity8(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public boolean onLongClick(final View view) {
            SkanwordGameActivity.access000(this.this0, view);
            return false;
        }
    }

    class SkanwordGameActivity9 implements EventListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity9(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new SkanwordGameActivity9.SkanwordGameActivity91(this));
        }

        class SkanwordGameActivity91 implements Runnable {
            final /* synthetic */ SkanwordGameActivity9 this1;

            SkanwordGameActivity91(final SkanwordGameActivity9 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                if (MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(ShopDataManager.ShopProductType.HINTS)) {
                    SkanwordGameActivity.access300(this.this1.this0);
                }
                this.this1.this0.checkVideoOfferAvailability();
            }
        }
    }

    class SkanwordGameActivity10 implements EventListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity10(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new SkanwordGameActivity10.SkanwordGameActivity101(this));
        }

        class SkanwordGameActivity101 implements Runnable {
            final /* synthetic */ SkanwordGameActivity10 this1;

            SkanwordGameActivity101(final SkanwordGameActivity10 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                this.this1.this0.updateHintsCountLabel();
            }
        }
    }

    class SkanwordGameActivity11 implements EventListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity11(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.checkTutorialState();
        }
    }

    class SkanwordGameActivity15 extends AdsManager.AdsEventsListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity15(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onComplete() {
            super.onComplete();
            SkanwordGameActivity.access900(this.this0);
            if (SkanwordGameActivity.access1000() != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("openCellForOffer ");
                sb.append(SkanwordGameActivity.access1000().isProgressChanged());
                Log.v("SkanwordsFunc", sb.toString());
                SkanwordGameActivity.access1100(this.this0, SkanwordGameActivity.access1000().isProgressChanged());
            }
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_hints4video_video_shown");
        }

        public void onNoAds() {
            super.onNoAds();
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_hints4video_video_nowshown");
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.this0, R.attr.dialogTheme);
            alertDialogBuilder.setMessage("Извините, сейчас у нас нет предложений для вас. Пожалуйста, попробуйте позже.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new SkanwordGameActivity15.SkanwordGameActivity151(this));
            alertDialogBuilder.create().show();
        }

        class SkanwordGameActivity151 implements DialogInterface.OnClickListener {
            final /* synthetic */ SkanwordGameActivity15 this1;

            SkanwordGameActivity151(final SkanwordGameActivity15 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class SkanwordGameActivity14 implements HintForVideoDialog.HintForVideoDialogDelegate {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity14(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onCancel() {
        }

        public void onWatch() {
            final AdsManager instance = AdsManager.getInstance();
            final SkanwordGameActivity this0 = this.this0;
            final SkanwordGameActivity14.SkanwordGameActivity141 skanwordGameActivity141 = new SkanwordGameActivity14.SkanwordGameActivity141(this);
            final StringBuilder sb = new StringBuilder();
            sb.append("hc");
            sb.append(SkanwordGameActivity.access700(this.this0).getId());
            instance.showVideoOffer((Activity)this0, (AdsManager.AdsEventsListener)skanwordGameActivity141, sb.toString(), true);
        }

        class SkanwordGameActivity141 extends AdsManager.AdsEventsListener {
            final /* synthetic */ SkanwordGameActivity14 this1;

            SkanwordGameActivity141(final SkanwordGameActivity14 this1) {
                super();
                this.this1 = this1;
            }

            public void onComplete() {
                super.onComplete();
                final StringBuilder sb = new StringBuilder();
                sb.append("onComplete video offer  mIsRunning ");
                sb.append(SkanwordGameActivity.access400(this.this1.this0));
                Log.v("", sb.toString());
                SkanwordGameActivity.access502(false);
                MainDataManager.getInstance().addUserHints(1);
                if (SkanwordGameActivity.access400(this.this1.this0)) {
                    HintForVideoDialog.showCompleteWatchDialogInContext((Context)this.this1.this0);
                    return;
                }
                SkanwordGameActivity.access602(true);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("onComplete video offer  mHasCompleteWatchDialog ");
                sb2.append(SkanwordGameActivity.access600());
                Log.v("", sb2.toString());
            }

            public void onNoAds() {
                SkanwordGameActivity.access502(false);
                super.onNoAds();
            }
        }
    }


    class SkanwordGameActivity19 implements DialogInterface.OnClickListener {
        final /* synthetic */ SkanwordGameActivity this0;

        SkanwordGameActivity19(final SkanwordGameActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }
}
