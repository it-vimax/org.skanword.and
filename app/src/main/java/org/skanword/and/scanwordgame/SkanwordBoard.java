package org.skanword.and.scanwordgame;

import java.util.logging.*;
import android.content.res.*;
import java.lang.reflect.*;
import java.util.*;
import org.skanword.and.etc.*;
import android.util.*;
import android.graphics.*;
import org.skanword.and.datamanager.*;

public class SkanwordBoard
{
    private static final Logger LOG;
    private static boolean isLandscape;
    private CrosswordActions action;
    private float bitmapScale;
    private Bitmap cacheBitmap;
    private Bitmap cacheEmptyBitmap;
    private Skanword.Question highlighQuestion;
    private Position highlightPosition;
    private Position highlightPositionPrev;
    private boolean isUserHighlightLetterChange;
    private final SkanwordBox[][] mBoxes;
    private boolean mProgressChanged;
    private final Skanword mSkanword;
    private Bitmap selectedLayerBitmap;
    
    static {
        LOG = Logger.getLogger("ua.smapssproject.scanword");
    }
    
    public SkanwordBoard(final Skanword mSkanword, int i, final AssetManager assetManager) {
        super();
        this.action = CrosswordActions.NONE;
        this.mProgressChanged = false;
        this.mSkanword = mSkanword;
        SkanwordBoard.isLandscape = (i == 2);
        this.mBoxes = (SkanwordBox[][])Array.newInstance(SkanwordBox.class, mSkanword.getSettings().getWidth(), mSkanword.getSettings().getHeight());
        int j;
        for (i = 0; i < mSkanword.getSettings().getWidth(); ++i) {
            for (j = 0; j < mSkanword.getSettings().getHeight(); ++j) {
                this.mBoxes[i][j] = new SkanwordBox(i, j, mSkanword.getImageDir(), assetManager);
            }
        }
        if (mSkanword.getKeyword() != null) {
            for (final Skanword.Keyword.KeywordCell keywordCell : mSkanword.getKeyword().getCells()) {
                this.mBoxes[keywordCell.getX()][keywordCell.getY()].setKeywordLetterNumber(keywordCell.getNumber());
            }
        }
        for (final Skanword.Question question : mSkanword.getQuestions()) {
            final Skanword.QuestionData questionData = question.getQuestionData();
            this.mBoxes[questionData.getX()][questionData.getY()].addQuestionBox(question, true);
            if (!questionData.getQuestionValue().isTextQuestion()) {
                int k;
                int n;
                int n2;
                SkanwordBox[][] mBoxes;
                for (i = 0; i < questionData.getQuestionValue().getWidth(); ++i) {
                    for (k = 0; k < questionData.getQuestionValue().getHeight(); ++k) {
                        if (i != 0 || k != 0) {
                            n = questionData.getX() + i;
                            n2 = questionData.getY() + k;
                            mBoxes = this.mBoxes;
                            if (mBoxes.length > n && mBoxes[n].length > n2) {
                                mBoxes[n][n2].addQuestionBox(question, false);
                            }
                        }
                    }
                }
            }
            i = question.getAnswer().getX();
            final int y = question.getAnswer().getY();
            final SkanwordBox[][] mBoxes2 = this.mBoxes;
            if (mBoxes2.length > i && mBoxes2[i].length > y) {
                mBoxes2[i][y].addArrow(ArrowType.create(question.getArrowData().getType()));
            }
            int x;
            int y2;
            for (i = 0; i < question.getAnswer().getLength(); ++i) {
                if (question.getQuestionData().isVerticalOrientation()) {
                    x = question.getAnswer().getX();
                    y2 = question.getAnswer().getY() + i;
                }
                else {
                    x = question.getAnswer().getX() + i;
                    y2 = question.getAnswer().getY();
                }
                this.mBoxes[x][y2].addQuestionRelation(question);
            }
        }
        this.highlightPosition = null;
        this.setAnswers();
        this.isUserHighlightLetterChange = false;
    }
    
    private int checkIsCompleteQuestion(final SkanwordBox skanwordBox) {
        return this.checkIsCompleteQuestion(skanwordBox, 0);
    }
    
    private int checkIsCompleteQuestion(final SkanwordBox skanwordBox, final int n) {
        int n3;
        final int n2 = n3 = 0;
        if (skanwordBox != null) {
            n3 = n2;
            if (skanwordBox.getBoxType() == SkanwordBox.BoxType.LETTER) {
                n3 = n2;
                if (skanwordBox.getQuestions() != null) {
                    n3 = n2;
                    if (skanwordBox.getQuestions().size() > 0) {
                        final Iterator<Skanword.Question> iterator = skanwordBox.getQuestions().iterator();
                        n3 = 0;
                    Label_0365_Outer:
                        while (iterator.hasNext()) {
                            final Skanword.Question question = iterator.next();
                            final StringBuilder sb = new StringBuilder();
                            int n4;
                            if (question.getQuestionData().isVerticalOrientation()) {
                                n4 = question.getAnswer().getY();
                            }
                            else {
                                n4 = question.getAnswer().getX();
                            }
                            final int length = question.getAnswer().getLength();
                            int i = n4;
                            while (true) {
                                while (i < length + n4) {
                                    SkanwordBox skanwordBox2;
                                    if (question.getQuestionData().isVerticalOrientation()) {
                                        skanwordBox2 = this.mBoxes[question.getAnswer().getX()][i];
                                    }
                                    else {
                                        skanwordBox2 = this.mBoxes[i][question.getAnswer().getY()];
                                    }
                                    if (skanwordBox2.getLatterResponse() != " ") {
                                        String latterResponse;
                                        String letterTarget = latterResponse = skanwordBox2.getLatterResponse();
                                        if (this.mSkanword.getReplaces() != null) {
                                            latterResponse = letterTarget;
                                            if (this.mSkanword.getReplaces().size() > 0) {
                                                final Iterator<Skanword.Replace> iterator2 = this.mSkanword.getReplaces().iterator();
                                                while (true) {
                                                    latterResponse = letterTarget;
                                                    if (!iterator2.hasNext()) {
                                                        break;
                                                    }
                                                    final Skanword.Replace replace = iterator2.next();
                                                    if (skanwordBox2.getX() != replace.getCellX() || skanwordBox2.getY() != replace.getCellY() || !letterTarget.toLowerCase().equals(replace.getLetterSource().toLowerCase())) {
                                                        continue Label_0365_Outer;
                                                    }
                                                    letterTarget = replace.getLetterTarget();
                                                }
                                            }
                                        }
                                        sb.append(latterResponse);
                                        ++i;
                                    }
                                    else {
                                        final boolean b = false;
                                        question.isComplete();
                                        question.setComplete(sb.length() == question.getAnswer().getLength() && SkanwordsDataManager.md5Answer(sb.toString(), this.mSkanword.getId()).equals(question.getAnswer().getHash()));
                                        if (b) {
                                            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED) {
                                                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_WORD_ENTERED);
                                            }
                                            else if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT) {
                                                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_SKANWORD_COMPLETE);
                                            }
                                        }
                                        if (question.isComplete()) {
                                            ++n3;
                                            continue Label_0365_Outer;
                                        }
                                        continue Label_0365_Outer;
                                    }
                                }
                                final boolean b = true;
                                continue;
                            }
                        }
                    }
                }
            }
        }
        if (n == 1 && n3 > 0) {
            this.mSkanword.calculateFinishedQuestions();
        }
        return n3;
    }
    
    private SkanwordBox getPrevLetterBox(int n, int n2) {
        if (this.highlighQuestion.getQuestionData().isVerticalOrientation()) {
            if (this.highlighQuestion.getAnswer().getY() < n2) {
                --n2;
            }
            else {
                n2 = this.highlighQuestion.getAnswer().getY() + this.highlighQuestion.getAnswer().getLength() - 1;
            }
        }
        else if (this.highlighQuestion.getAnswer().getX() < n) {
            --n;
        }
        else {
            n = this.highlighQuestion.getAnswer().getX() + this.highlighQuestion.getAnswer().getLength() - 1;
        }
        final SkanwordBox skanwordBox = this.mBoxes[n][n2];
        if ((skanwordBox.isComplete() || skanwordBox.isHintOpen()) && MainDataManager.getInstance().getOptions().isSkipGuessedLetters() && !this.highlighQuestion.isComplete()) {
            return this.getPrevLetterBox(n, n2);
        }
        this.highlightPosition = new Position(n, n2);
        return skanwordBox;
    }
    
    private Skanword.Question getQuestionForId(final int n) {
        final Skanword mSkanword = this.mSkanword;
        if (mSkanword != null && mSkanword.getQuestions() != null) {
            for (final Skanword.Question question : this.mSkanword.getQuestions()) {
                if (question.getId() == n) {
                    return question;
                }
            }
        }
        return null;
    }
    
    private Position getStartInputAnswer(final Skanword.Question question) {
        SkanwordBox skanwordBox = null;
        Position position;
        if (question != null) {
            final int n = 0;
            int n2;
            if (question.getQuestionData().isVerticalOrientation()) {
                n2 = question.getAnswer().getY();
            }
            else {
                n2 = question.getAnswer().getX();
            }
            final int n3 = question.getAnswer().getLength() + n2;
            final Logger log = SkanwordBoard.LOG;
            final StringBuilder sb = new StringBuilder();
            sb.append("START INDEX = ");
            sb.append(n2);
            sb.append(" STOP INDEX = ");
            sb.append(n3);
            sb.append("vertical ");
            sb.append(question.getQuestionData().isVerticalOrientation());
            log.info(sb.toString());
            int n4;
            while (true) {
                n4 = n;
                if (n2 >= n3) {
                    break;
                }
                if (question.getQuestionData().isVerticalOrientation()) {
                    skanwordBox = this.mBoxes[question.getAnswer().getX()][n2];
                }
                else {
                    skanwordBox = this.mBoxes[n2][question.getAnswer().getY()];
                }
                SkanwordBoard.LOG.info(skanwordBox.toString());
                if (skanwordBox.getBoxType() == SkanwordBox.BoxType.LETTER && skanwordBox.getLatterResponse() == " ") {
                    n4 = 1;
                    break;
                }
                ++n2;
            }
            if (n4 == 0 || !MainDataManager.getInstance().getOptions().isSkipGuessedLetters()) {
                skanwordBox = this.mBoxes[question.getAnswer().getX()][question.getAnswer().getY()];
            }
            position = new Position(skanwordBox.getX(), skanwordBox.getY());
        }
        else {
            position = null;
        }
        final Logger log2 = SkanwordBoard.LOG;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("X = ");
        sb2.append(position.across);
        sb2.append(" Y = ");
        sb2.append(position.down);
        log2.info(sb2.toString());
        return position;
    }
    
    private boolean isAcriveWord(final int n, final int n2) {
        final Skanword.Question highlighQuestion = this.highlighQuestion;
        if (highlighQuestion != null) {
            final SkanwordBox[][] mBoxes = this.mBoxes;
            if (n < mBoxes.length && n2 < mBoxes[n].length) {
                if (highlighQuestion.getQuestionData().isVerticalOrientation()) {
                    if (n != this.highlighQuestion.getAnswer().getX() || n2 < this.highlighQuestion.getAnswer().getY() || n2 >= this.highlighQuestion.getAnswer().getY() + this.highlighQuestion.getAnswer().getLength()) {
                        return false;
                    }
                }
                else if (n2 != this.highlighQuestion.getAnswer().getY() || n < this.highlighQuestion.getAnswer().getX() || n >= this.highlighQuestion.getAnswer().getX() + this.highlighQuestion.getAnswer().getLength()) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }
    
    private void nextLetter() {
        this.nextLetter(this.highlightPosition.across, this.highlightPosition.down);
    }
    
    private void nextLetter(int x, int y) {
        if (this.highlighQuestion.getQuestionData().isVerticalOrientation()) {
            final int y2 = this.highlighQuestion.getAnswer().getY();
            final int length = this.highlighQuestion.getAnswer().getLength();
            ++y;
            if (y2 + length <= y) {
                y = this.highlighQuestion.getAnswer().getY();
            }
        }
        else {
            final int x2 = this.highlighQuestion.getAnswer().getX();
            final int length2 = this.highlighQuestion.getAnswer().getLength();
            ++x;
            if (x2 + length2 <= x) {
                x = this.highlighQuestion.getAnswer().getX();
            }
        }
        final SkanwordBox skanwordBox = this.mBoxes[x][y];
        if ((skanwordBox.isComplete() || skanwordBox.isHintOpen()) && MainDataManager.getInstance().getOptions().isSkipGuessedLetters() && !this.highlighQuestion.isComplete()) {
            this.nextLetter(x, y);
            return;
        }
        this.highlightPosition = new Position(x, y);
    }
    
    private Skanword.Question selectHighlighQuestion(final Position position) {
        final Skanword.Question question = this.mBoxes[position.across][position.down].getQuestions().get(0);
        final SkanwordBox skanwordBox = this.mBoxes[position.across][position.down];
        if (skanwordBox.getQuestions().size() == 1) {
            return skanwordBox.getQuestions().get(0);
        }
        Skanword.Question question2 = question;
        if (skanwordBox.getQuestions().size() == 2) {
            if (this.highlighQuestion != null && this.highlightPosition.equals(position)) {
                if (skanwordBox.getQuestions().get(0).equals(this.highlighQuestion)) {
                    return skanwordBox.getQuestions().get(1);
                }
                return skanwordBox.getQuestions().get(0);
            }
            else {
                if (this.highlighQuestion != null && this.isAcriveWord(position.across, position.down)) {
                    return this.highlighQuestion;
                }
                if (skanwordBox.getQuestions().get(0).getAnswer().getX() == position.across && skanwordBox.getQuestions().get(0).getAnswer().getY() == position.down) {
                    return skanwordBox.getQuestions().get(0);
                }
                if (skanwordBox.getQuestions().get(1).getAnswer().getX() == position.across && skanwordBox.getQuestions().get(1).getAnswer().getY() == position.down) {
                    return skanwordBox.getQuestions().get(1);
                }
                if (!question.getQuestionData().isVerticalOrientation() || !SkanwordBoard.isLandscape) {
                    question2 = question;
                    if (question.getQuestionData().isVerticalOrientation()) {
                        return question2;
                    }
                    question2 = question;
                    if (SkanwordBoard.isLandscape) {
                        return question2;
                    }
                }
                question2 = this.mBoxes[position.across][position.down].getQuestions().get(1);
            }
        }
        return question2;
    }
    
    private void setAnswers() {
        final Skanword mSkanword = this.mSkanword;
        if (mSkanword == null) {
            return;
        }
        if (mSkanword.getAnswers() != null && this.mSkanword.getAnswers().length() > 0) {
            for (int i = 0; i < this.mSkanword.getAnswers().length(); ++i) {
                final int n = (int)Math.floor(i / this.getHeight());
                final int n2 = i - this.getHeight() * n;
                final SkanwordBox[][] mBoxes = this.mBoxes;
                if (n < mBoxes.length && n2 < mBoxes[n].length) {
                    final String answers = this.mSkanword.getAnswers();
                    final int n3 = i + 1;
                    final String substring = answers.substring(i, n3);
                    if (!substring.equals(" ")) {
                        final boolean equals = this.mSkanword.getHints().substring(i, n3).equals(".");
                        this.mBoxes[n][n2].setResponse(substring.toUpperCase());
                        this.mBoxes[n][n2].setHintOpen(equals);
                        this.checkIsCompleteQuestion(this.mBoxes[n][n2], 0);
                    }
                }
            }
        }
    }
    
    public Bitmap drawSkanword(final float bitmapScale, final boolean b) {
        Log.v("SkanwordsTime", "drawSkanword start");
        final int n = (int)(30.0f * bitmapScale);
        final boolean b2 = bitmapScale != this.bitmapScale || this.cacheEmptyBitmap == null || b;
        if (b2) {
            Log.v("SkanwordsTime", "drawSkanword render all");
            this.action = CrosswordActions.CHANGE_SCALE;
            (this.cacheEmptyBitmap = Bitmap.createBitmap(this.getWidth() * n, this.getHeight() * n, Bitmap.Config.RGB_565)).eraseColor(-16777216);
            Log.v("SkanwordsTime", "drawSkanword render all end");
        }
        this.bitmapScale = bitmapScale;
        final Canvas canvas = new Canvas(this.cacheEmptyBitmap);
        final Position highlightPositionPrev = this.highlightPositionPrev;
        final boolean b3 = highlightPositionPrev == null || !this.highlightPosition.equals(highlightPositionPrev);
        Log.v("SkanwordsTime", "drawSkanword draw boxes start");
        int n2 = 0;
        int n3 = 0;
        for (int i = 0; i < this.getWidth(); ++i) {
            for (int j = 0; j < this.getHeight(); ++j) {
                final SkanwordBox skanwordBox = this.mBoxes[i][j];
                final Position highlightPosition = this.highlightPosition;
                final boolean highlightLetter = highlightPosition != null && i == highlightPosition.across && j == this.highlightPosition.down;
                final boolean acriveWord = this.isAcriveWord(i, j);
                if (b2 || skanwordBox.isSelected() != highlightLetter || skanwordBox.isActiveWorld() != acriveWord || (skanwordBox.isActiveWorld() && b3)) {
                    n2 = i * n;
                    n3 = j * n;
                    skanwordBox.setHighlightLetter(highlightLetter);
                    skanwordBox.setHighlightdWord(acriveWord);
                    skanwordBox.drawBackground(canvas, n2, n3, bitmapScale);
                    if (skanwordBox.getKeywordLetterNumber() > 0) {
                        skanwordBox.drawKeywordFrame(canvas, n2, n3, bitmapScale, skanwordBox.getKeywordLetterNumber());
                    }
                    skanwordBox.drawResultLetters(canvas, n2, n3, bitmapScale);
                    skanwordBox.drawArrow(canvas, n2, n3, bitmapScale);
                }
            }
        }
        Log.v("SkanwordsTime", "drawSkanword draw boxes end");
        this.selectedLayerBitmap = null;
        this.cacheBitmap = this.cacheEmptyBitmap;
        if (this.selectedLayerBitmap != null) {
            Log.v("SkanwordsTime", "drawSkanword selected layer start");
            new Canvas(this.cacheBitmap).drawBitmap(this.selectedLayerBitmap, (float)n2, (float)n3, (Paint)null);
            Log.v("SkanwordsTime", "drawSkanword selected layer end");
        }
        Log.v("SkanwordsTime", "drawSkanword end");
        return this.cacheBitmap;
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
    
    public Bitmap getActiveQuestion() {
        Object tempBitmap = null;
        int n = 0;
        while (tempBitmap == null) {
            final Skanword.QuestionData questionData = ((Skanword.Question)this.mSkanword.getQuestions().get(n)).getQuestionData();
            tempBitmap = this.mBoxes[questionData.getX()][questionData.getY()].getTempBitmap();
            final StringBuilder sb = new StringBuilder();
            sb.append("bitmap =  ");
            sb.append(tempBitmap);
            Log.i("  ", sb.toString());
            ++n;
        }
        return (Bitmap)tempBitmap;
    }
    
    public SkanwordBox[][] getBoxes() {
        return this.mBoxes;
    }
    
    public Skanword getCurrentSkanword() {
        synchronized (this) {
            return this.mSkanword;
        }
    }
    
    public int getHeight() {
        return this.mSkanword.getSettings().getHeight();
    }
    
    public Position getHighlightLetter() {
        return this.highlightPosition;
    }
    
    public SkanwordBox getHighlightedBox() {
        if (this.getHighlightLetter() == null) {
            return null;
        }
        return this.mBoxes[this.highlightPosition.across][this.highlightPosition.down];
    }
    
    public Skanword.Question getHightlightQuestion() {
        return this.highlighQuestion;
    }
    
    public String getKeywordString() {
        final StringBuilder sb = new StringBuilder();
        for (final Skanword.Keyword.KeywordCell keywordCell : this.mSkanword.getKeyword().getCells()) {
            sb.append(this.mBoxes[keywordCell.getX()][keywordCell.getY()].getLatterResponse());
        }
        return sb.toString();
    }
    
    public int getWidth() {
        return this.mSkanword.getSettings().getWidth();
    }
    
    public int inputLetter(String upperCase, final boolean b) {
        if (this.highlighQuestion != null && this.highlightPosition != null) {
            SkanwordBox skanwordBox = this.getHighlightedBox();
            int n = 0;
            Label_0334: {
                if (skanwordBox != null) {
                    this.action = CrosswordActions.CHANGE_INPUT;
                    int checkIsCompleteQuestion;
                    if (!skanwordBox.isHintOpen() && !skanwordBox.isComplete() && upperCase != null) {
                        for (final Skanword.Replace replace : this.mSkanword.getReplaces()) {
                            if (skanwordBox.getX() == replace.getCellX() && skanwordBox.getY() == replace.getCellY() && upperCase.toLowerCase().equals(replace.getLetterSource().toLowerCase())) {
                                upperCase = replace.getLetterTarget().toUpperCase();
                            }
                        }
                        this.mSkanword.setLetter(this.highlightPosition, new Point(this.getWidth(), this.getHeight()), upperCase, b);
                        this.mProgressChanged = true;
                        skanwordBox.setResponse(upperCase);
                        skanwordBox.setHintOpen(b);
                        checkIsCompleteQuestion = this.checkIsCompleteQuestion(skanwordBox, 1);
                    }
                    else {
                        if (upperCase == null) {
                            if (!this.isUserHighlightLetterChange) {
                                skanwordBox = this.getPrevLetterBox(this.highlightPosition.across, this.highlightPosition.down);
                            }
                            if (!skanwordBox.isHintOpen() && !skanwordBox.isComplete()) {
                                skanwordBox.setResponse(" ");
                                skanwordBox.setHintOpen(b);
                                this.mSkanword.setLetter(this.highlightPosition, new Point(this.getWidth(), this.getHeight()), " ", b);
                                this.mProgressChanged = true;
                            }
                        }
                        checkIsCompleteQuestion = 0;
                    }
                    n = checkIsCompleteQuestion;
                    if (upperCase != null) {
                        if (checkIsCompleteQuestion >= 1) {
                            n = checkIsCompleteQuestion;
                            if (this.highlighQuestion.isComplete()) {
                                break Label_0334;
                            }
                        }
                        this.nextLetter();
                        n = checkIsCompleteQuestion;
                    }
                }
                else {
                    n = 0;
                }
            }
            this.isUserHighlightLetterChange = false;
            return n;
        }
        return 0;
    }
    
    public boolean isHighlightedBoxGuessed() {
        final SkanwordBox highlightedBox = this.getHighlightedBox();
        boolean b = false;
        if (highlightedBox != null) {
            if (this.highlighQuestion == null) {
                return false;
            }
            if (!this.getHighlightedBox().isHintOpen()) {
                b = b;
                if (!this.getHighlightedBox().isComplete()) {
                    return b;
                }
            }
            b = true;
        }
        return b;
    }
    
    public boolean isKeywordGuessed() {
        if (this.mSkanword.getKeyword() == null) {
            return false;
        }
        if (!this.mSkanword.getKeyword().isGuessed()) {
            this.mSkanword.checkKeyword();
        }
        for (final Skanword.Keyword.KeywordCell keywordCell : this.mSkanword.getKeyword().getCells()) {
            final SkanwordBox skanwordBox = this.mBoxes[keywordCell.getX()][keywordCell.getY()];
            if (keywordCell.isPersist()) {
                skanwordBox.setCompletition();
            }
        }
        return this.mSkanword.getKeyword().isGuessed();
    }
    
    public boolean isProgressChanged() {
        return this.mProgressChanged;
    }
    
    public int openAll() {
        for (int i = 0; i < this.getWidth(); ++i) {
            for (int j = 0; j < this.getHeight(); ++j) {
                final String s = this.mSkanword.getMask()[i][j];
                if (s != null && !s.equals("false")) {
                    int n;
                    for (int k = 0; k < 33; k = n) {
                        n = k + 1;
                        String s2 = "йцукенгшщзхъфывапролджэёячсмитьбю".substring(k, n);
                        if (SkanwordsDataManager.md5Answer(String.valueOf(s2), this.mSkanword.getId()).equals(s)) {
                            final SkanwordBox skanwordBox = this.mBoxes[i][j];
                            for (final Skanword.Replace replace : this.mSkanword.getReplaces()) {
                                if (skanwordBox.getX() == replace.getCellX() && skanwordBox.getY() == replace.getCellY() && s2.toLowerCase().equals(replace.getLetterSource().toLowerCase())) {
                                    s2 = replace.getLetterTarget().toUpperCase();
                                }
                            }
                            skanwordBox.setResponse(s2.toUpperCase());
                            skanwordBox.setHintOpen(true);
                            this.checkIsCompleteQuestion(skanwordBox, 0);
                            this.mSkanword.setLetter(new Position(i, j), new Point(this.getWidth(), this.getHeight()), s2, true, false);
                            this.mProgressChanged = true;
                        }
                    }
                }
            }
        }
        this.mSkanword.calculateFinishedQuestions();
        UserUpdatesManager.getInstance().updateScanwordMasks(this.mSkanword);
        return this.mSkanword.getQuestions().size();
    }
    
    public int openLetter() {
        if (this.highlightPosition != null) {
            final String s = this.mSkanword.getMask()[this.highlightPosition.across][this.highlightPosition.down];
            if (s != null && !s.equals("false")) {
                int n;
                for (int i = 0; i < 33; i = n) {
                    n = i + 1;
                    final String substring = "йцукенгшщзхъфывапролджэёячсмитьбю".substring(i, n);
                    if (SkanwordsDataManager.md5Answer(String.valueOf(substring), this.mSkanword.getId()).equals(s)) {
                        return this.inputLetter(substring.toUpperCase(), true);
                    }
                }
            }
        }
        return 0;
    }
    
    public void setHighlightLetter(final Position position) {
        if (position != null && (this.highlightPosition == null || (position.across < this.mBoxes.length && position.down < this.mBoxes[position.across].length))) {
            this.highlighQuestion = this.selectHighlighQuestion(position);
            if (!position.equals(this.highlightPosition)) {
                this.isUserHighlightLetterChange = true;
                final SkanwordBox skanwordBox = this.mBoxes[position.across][position.down];
                this.highlightPosition = position;
                if (skanwordBox.getBoxType() == SkanwordBox.BoxType.LETTER) {
                    this.highlightPosition = position;
                }
                else {
                    this.highlightPosition = this.getStartInputAnswer(skanwordBox.getQuestions().get(0));
                }
            }
            final SkanwordBox skanwordBox2 = this.mBoxes[this.highlightPosition.across][this.highlightPosition.down];
            if (!skanwordBox2.isComplete() && !skanwordBox2.isHintOpen() && TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_WORD_ENTERED) {
                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED);
            }
            this.action = CrosswordActions.CHANGE_SELECT;
        }
    }
    
    public void setProgressChanged(final boolean mProgressChanged) {
        this.mProgressChanged = mProgressChanged;
    }

    private enum CrosswordActions {
        NONE,
        CHANGE_SCALE,
        CHANGE_SELECT,
        CHANGE_INPUT
    }

    
    public static class Position
    {
        public int across;
        public int down;
        
        protected Position() {
            super();
        }
        
        public Position(final int across, final int down) {
            super();
            this.down = down;
            this.across = across;
        }
        
        public Position(final Position position) {
            super();
            this.across = position.across;
            this.down = position.down;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b2;
            final boolean b = b2 = false;
            if (o != null) {
                if (o.getClass() != this.getClass()) {
                    return false;
                }
                final Position position = (Position)o;
                b2 = b;
                if (position.down == this.down) {
                    b2 = b;
                    if (position.across == this.across) {
                        b2 = true;
                    }
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return this.across ^ this.down;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(this.across);
            sb.append(" x ");
            sb.append(this.down);
            sb.append("]");
            return sb.toString();
        }
    }
}
