package org.skanword.and.scanwordgame;

import java.util.logging.*;
import android.content.*;
import android.graphics.*;
import android.view.inputmethod.*;

import org.skanword.and.R;
import org.skanword.and.etc.*;
import android.view.*;
import android.util.*;
import org.skanword.and.datamanager.*;

public class SkanwordImageView extends TouchImageView
{
    private static final Logger LOG;
    private SkanwordBoard board;
    private int boardHeight;
    private int boardWidth;
    private ClickListener clickListener;
    private boolean created;
    private float displayScale;
    private boolean isNowRefresh;
    private float maxRenderScale;
    private float minRenderScale;
    private float overScale;
    private float renderScale;
    private RenderScaleListener renderScaleListener;
    private float tx;
    private float ty;
    private float zoomScale;
    
    static {
        LOG = Logger.getLogger("com.adamrosenfield.wordswithcrosses");
    }
    
    public SkanwordImageView(final Context context, final AttributeSet set) {
        super(context, set);
        this.boardWidth = 1;
        this.boardHeight = 1;
        this.renderScale = 1.0f;
        this.minRenderScale = 1.0f;
        this.maxRenderScale = 1.0f;
        this.displayScale = 1.0f;
        this.overScale = 0.0f;
        this.created = false;
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
    }
    
    private void onClickEvent(final ClickEvent clickEvent, final PointF pointF) {
        if (this.clickListener == null) {
            return;
        }
        final float n = this.renderScale * 30.0f;
        final int n2 = (int)(pointF.x / n);
        final int n3 = (int)(pointF.y / n);
        Object o2;
        final Object o = o2 = null;
        if (n2 >= 0) {
            o2 = o;
            if (n2 < this.boardWidth) {
                o2 = o;
                if (n3 >= 0) {
                    o2 = o;
                    if (n3 < this.boardHeight) {
                        o2 = new SkanwordBoard.Position(n2, n3);
                    }
                }
            }
        }
        final int n4 = SkanwordImageView1.clickEvent[clickEvent.ordinal()];
        if (n4 == 1) {
            this.clickListener.onClick((SkanwordBoard.Position)o2);
            return;
        }
        if (n4 == 2) {
            this.clickListener.onDoubleClick((SkanwordBoard.Position)o2);
            return;
        }
        if (n4 != 3) {
            return;
        }
        this.clickListener.onLongClick((SkanwordBoard.Position)o2);
    }
    
    private float setRenderScale(float renderScale, final float n, final float n2) {
        renderScale = Math.min(Math.max(renderScale, this.minRenderScale), this.displayScale);
        final PointF pixelToBitmapPos = this.pixelToBitmapPos(n, n2);
        final float n3 = renderScale / this.renderScale;
        final float maxRenderScale = this.maxRenderScale;
        if (renderScale > maxRenderScale) {
            this.overScale = renderScale / maxRenderScale;
            renderScale = maxRenderScale;
        }
        else {
            this.overScale = 0.0f;
        }
        if (Math.abs(renderScale - this.renderScale) < 1.0E-4f && this.created) {
            return this.renderScale;
        }
        this.tx = n - pixelToBitmapPos.x * n3;
        this.ty = n2 - n3 * pixelToBitmapPos.y;
        this.renderScale = renderScale;
        boolean b = false;
        this.render(false);
        this.created = true;
        final StringBuilder sb = new StringBuilder();
        sb.append(" setRenderScale ");
        sb.append(Math.abs(renderScale - this.renderScale));
        sb.append("   ");
        sb.append(this.minRenderScale / this.renderScale);
        sb.append("   ");
        sb.append(this.displayScale / this.renderScale);
        sb.append("   ");
        if (this.minRenderScale < this.displayScale) {
            b = true;
        }
        sb.append(b);
        Log.v("", sb.toString());
        renderScale = this.minRenderScale;
        if (renderScale < this.displayScale) {
            this.setMinScale(renderScale / this.renderScale);
            this.setMaxScale(this.displayScale / this.renderScale);
            renderScale = this.overScale;
            if (renderScale > 0.0f) {
                this.setScaleAndTranslate(renderScale, this.tx, this.ty);
            }
            else {
                this.setScaleAndTranslate(1.0f, this.tx, this.ty);
            }
        }
        else {
            this.setMinScale(renderScale / this.renderScale + 0.005f);
            this.setMaxScale(this.minRenderScale / this.renderScale + 0.005f);
            this.setScaleAndTranslate(this.minRenderScale / this.displayScale + 0.005f, this.tx, this.ty);
        }
        final RenderScaleListener renderScaleListener = this.renderScaleListener;
        if (renderScaleListener != null) {
            renderScaleListener.onRenderScaleChanged(this.renderScale);
        }
        return this.renderScale;
    }
    
    public void ensureVisible(final SkanwordBoard.Position position) {
        Log.v("SkanwordsGame", "ensure visible");
        this.getImageMatrix();
        final float overScale = this.overScale;
        float renderScale;
        if (overScale > 0.0f) {
            renderScale = overScale * this.renderScale;
        }
        else {
            renderScale = this.renderScale;
        }
        final float n = renderScale * 30.0f;
        final double n2 = position.across;
        Double.isNaN(n2);
        final double n3 = n;
        Double.isNaN(n3);
        final float n4 = (float)((n2 + 0.5) * n3);
        final double n5 = position.down;
        Double.isNaN(n5);
        Double.isNaN(n3);
        final float n6 = (float)((n5 + 0.5) * n3);
        final int boardWidth = this.boardWidth;
        final int boardHeight = this.boardHeight;
        final float[] array = new float[9];
        this.matrix.getValues(array);
        final float n7 = array[2];
        final float n8 = array[5];
        final StringBuilder sb = new StringBuilder();
        sb.append(" size  renderscale - ");
        sb.append(this.renderScale);
        sb.append("  display scale - ");
        sb.append(this.displayScale);
        sb.append("  max render scale - ");
        sb.append(this.maxRenderScale);
        sb.append("  zoom scale - ");
        sb.append(this.zoomScale);
        sb.append("  over scale - ");
        sb.append(this.overScale);
        sb.append("  x - ");
        sb.append(n7);
        sb.append("  y - ");
        sb.append(n8);
        Log.v("SkanwordsGame", sb.toString());
        final float n9 = -n4 + this.getWidth() / 2 - n7;
        final float n10 = -n6 + this.getHeight() / 2 - n8;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(n);
        sb2.append(" ensureVisible  dx - ");
        sb2.append(n9);
        sb2.append("  dy - ");
        sb2.append(n10);
        Log.v("SkanwordsGame", sb2.toString());
        this.translate(n9, n10);
    }
    
    public float fitToScreen() {
        Log.v("SkanwordsGame", "Fit to screen");
        return this.setRenderScale(Math.min(this.getWidth() / (float)(this.boardWidth * 30), this.getHeight() / (float)(this.boardHeight * 30)));
    }
    
    public float getRenderScale() {
        return this.renderScale;
    }
    
    public boolean onCheckIsTextEditor() {
        return true;
    }
    
    @Override
    protected void onClick(final PointF pointF) {
        this.onClickEvent(ClickEvent.CLICK, pointF);
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        final InputConnectionAccomodatingLatinIMETypeNullIssues inputConnectionAccomodatingLatinIMETypeNullIssues = new InputConnectionAccomodatingLatinIMETypeNullIssues((View)this, false);
        editorInfo.actionLabel = null;
        editorInfo.inputType = 0;
        editorInfo.imeOptions = 6;
        return (InputConnection)inputConnectionAccomodatingLatinIMETypeNullIssues;
    }
    
    @Override
    protected void onDoubleClick(final PointF pointF) {
        this.onClickEvent(ClickEvent.DOUBLE_CLICK, pointF);
    }
    
    @Override
    protected void onLongClick(final PointF pointF) {
        this.onClickEvent(ClickEvent.LONG_CLICK, pointF);
    }
    
    @Override
    protected void onScaleEnd(final float n) {
        final float renderScale = this.renderScale;
        Math.abs(renderScale * n - renderScale);
        this.setRenderScale(this.renderScale * n);
    }
    
    public void refresh() {
        Log.v("SkanwordsGame", "refresh Scanword Image View");
        if (!this.isNowRefresh) {
            this.isNowRefresh = true;
            this.render(false);
            final float minRenderScale = this.minRenderScale;
            final float displayScale = this.displayScale;
            if (minRenderScale < displayScale) {
                this.setScaleAndTranslate(1.0f, this.tx, this.ty);
            }
            else {
                this.setScaleAndTranslate(minRenderScale / displayScale + 0.005f, this.tx, this.ty);
            }
            final RenderScaleListener renderScaleListener = this.renderScaleListener;
            if (renderScaleListener != null) {
                renderScaleListener.onRenderScaleChanged(this.renderScale);
            }
            this.isNowRefresh = false;
        }
    }
    
    public void render(final boolean b) {
        this.setImageBitmap(this.board.drawSkanword(this.renderScale, b));
    }
    
    public void setBoard(final SkanwordBoard board) {
        this.board = board;
    }
    
    public void setBoard(final SkanwordBoard board, final DisplayMetrics displayMetrics, final Integer n) {
        if (this.board == null) {
            this.board = board;
            this.boardWidth = board.getWidth();
            this.boardHeight = board.getHeight();
            final Object o = new Object();
            float n2 = 0.0f;
            if (this.getContext().getTheme().resolveAttribute(R.attr.actionBarSize, (TypedValue)o, true)) {
                final int identifier = this.getResources().getIdentifier("status_bar_height", "dimen", "android");
                int dimensionPixelSize = 0;
                if (identifier > 0) {
                    dimensionPixelSize = this.getResources().getDimensionPixelSize(identifier);
                }
                n2 = (float)(TypedValue.complexToDimensionPixelSize(((TypedValue)o).data, this.getResources().getDisplayMetrics()) + dimensionPixelSize);
            }
            this.minRenderScale = Math.max(Math.max(0.5f, (displayMetrics.heightPixels - n2) / (this.boardHeight * 30)), Math.max(0.5f, displayMetrics.widthPixels / (float)(this.boardWidth * 30)));
            final StringBuilder sb = new StringBuilder();
            sb.append(" min render scales    ");
            sb.append(displayMetrics.widthPixels / (float)(this.boardWidth * 30));
            sb.append(" ");
            sb.append((displayMetrics.heightPixels - n2) / (this.boardHeight * 30));
            Log.v("", sb.toString());
            this.maxRenderScale = Math.min(n / (float)(Math.max(this.boardWidth, this.boardHeight) * 30), 4.0f);
            float n3;
            float n4;
            if (MainDataManager.getInstance().getOptions().isIncreasedRatio()) {
                n3 = this.maxRenderScale;
                n4 = 1.4f;
            }
            else {
                n3 = this.maxRenderScale;
                n4 = 1.2f;
            }
            this.displayScale = n3 * n4;
            this.renderScale = Math.min(Math.max(this.minRenderScale, this.renderScale), this.displayScale);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(n);
            sb2.append("  setBoard ");
            sb2.append(this.maxRenderScale);
            sb2.append("   ");
            sb2.append(this.renderScale);
            sb2.append("   ");
            sb2.append(this.boardWidth * this.renderScale * 30.0f);
            sb2.append("   ");
            sb2.append(this.boardHeight * this.renderScale * 30.0f);
            Log.v("SkanwordsGameFunc", sb2.toString());
        }
    }
    
    public void setClickListener(final ClickListener clickListener) {
        this.clickListener = clickListener;
    }
    
    public float setRenderScale(final float n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("set render scale ");
        sb.append(n);
        Log.v("SkanwordsGame", sb.toString());
        final float n2 = this.renderScale * 30.0f;
        Object highlightLetter;
        if ((highlightLetter = this.board.getHighlightLetter()) == null) {
            highlightLetter = new SkanwordBoard.Position(0, 0);
        }
        return this.setRenderScale(n, (((SkanwordBoard.Position)highlightLetter).across + 0.5f) * n2, (((SkanwordBoard.Position)highlightLetter).down + 0.5f) * n2);
    }
    
    public void setRenderScaleListener(final RenderScaleListener renderScaleListener) {
        this.renderScaleListener = renderScaleListener;
    }
    
    public float zoomIn() {
        Log.v("SkanwordsGame", "Zoom in");
        float n = (float)this.getWidth();
        if (this.getResources().getConfiguration().orientation == 2) {
            n = (float)this.getHeight();
        }
        final float n2 = n / 6.0f / 30.0f;
        final float n3 = 30.0f * n2;
        float zoomScale;
        if (n3 > 128.0f) {
            zoomScale = Math.max(4.266667f, this.minRenderScale);
        }
        else if (n3 < 60.0f) {
            zoomScale = Math.min(2.0f, this.displayScale);
        }
        else {
            zoomScale = Math.min(Math.max(this.minRenderScale, n2), this.displayScale);
        }
        if (zoomScale == this.zoomScale) {
            return 0.0f;
        }
        this.zoomScale = zoomScale;
        return this.setRenderScale(this.zoomScale);
    }
    
    public float zoomOut() {
        Log.v("SkanwordsGame", "Zoom out");
        return this.setRenderScale(this.renderScale / 1.5f);
    }

    private enum ClickEvent {
        CLICK,
        DOUBLE_CLICK,
        LONG_CLICK
    }

    
    public interface ClickListener
    {
        void onClick(final SkanwordBoard.Position p0);
        
        void onDoubleClick(final SkanwordBoard.Position p0);
        
        void onLongClick(final SkanwordBoard.Position p0);
    }
    
    public interface RenderScaleListener
    {
        void onRenderScaleChanged(final float p0);
    }

    static class SkanwordImageView1 {
    static final int[] clickEvent;

    static {
        clickEvent = new int[SkanwordImageView.ClickEvent.values().length];
        try {
            SkanwordImageView1.clickEvent[SkanwordImageView.ClickEvent.CLICK.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            SkanwordImageView1.clickEvent[SkanwordImageView.ClickEvent.DOUBLE_CLICK.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            SkanwordImageView1.clickEvent[SkanwordImageView.ClickEvent.LONG_CLICK.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError) {
        }
    }
}

}
