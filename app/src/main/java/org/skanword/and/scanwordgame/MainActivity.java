package org.skanword.and.scanwordgame;

import android.app.*;

import org.skanword.and.R;
import org.skanword.and.etc.*;
import android.os.*;
import android.view.*;

public class MainActivity extends Activity implements IAdsRewardedVideoActivity
{
    public MainActivity() {
        super();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(R.layout.activity_main);
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }
}
