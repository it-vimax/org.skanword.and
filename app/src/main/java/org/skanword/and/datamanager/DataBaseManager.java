package org.skanword.and.datamanager;

import android.annotation.*;
import java.util.logging.*;
import android.database.sqlite.*;
import android.content.*;
import com.google.gson.*;
import android.util.*;
import org.skanword.and.*;
import java.nio.channels.*;
import com.google.gson.reflect.*;
import android.database.*;
import org.skanword.and.etc.*;
import java.io.*;
import java.util.*;

@SuppressLint({ "NewApi" })
public class DataBaseManager extends SQLiteOpenHelper
{
    public static final int ADS_DATA_ID = 11;
    public static final String COLUMN_GENERAL_DATA = "general_data";
    public static final String COLUMN_GENERAL_DATA_ID = "_id";
    public static final String COLUMN_SET_COVER = "set_cover";
    public static final String COLUMN_SET_DATA = "set_data";
    public static final String COLUMN_SET_DATA_HASH = "set_data_hash";
    public static final String COLUMN_SET_DATA_ID = "_id";
    public static final String COLUMN_SET_DATA_SRC = "set_data";
    public static final String COLUMN_SET_DISPLAY_NAME = "set_display_name";
    public static final String COLUMN_SET_DOWNLOADED_TIME = "set_downloaded_time";
    public static final String COLUMN_SET_DOWNLOAD_TIME_LEFT = "set_download_time_left";
    public static final String COLUMN_SET_FREE_IDS = "set_free_ids";
    public static final String COLUMN_SET_ID = "set_id";
    public static final String COLUMN_SET_IMAGE_FOLDER_NAME = "set_file";
    public static final String COLUMN_SET_NAME = "set_name";
    public static final String COLUMN_SET_OFFER_IDS = "set_offer_ids";
    public static final String COLUMN_SET_SAVE_HASH = "set_save_hash";
    public static final String COLUMN_SET_SKANWORDS_COUNT = "set_scanwords_count";
    public static final String COLUMN_SET_SKANWORDS_FINISHED_COUNT = "set_scanwords_finished_count";
    public static final String COLUMN_SET_SKANWORDS_STARTED_COUNT = "set_scanwords_started_count";
    public static final String COLUMN_SKANWORD_ANSWERS_MASK = "answers";
    public static final String COLUMN_SKANWORD_COVER = "skanword_cover";
    public static final String COLUMN_SKANWORD_DISPLAY_ID = "display_id";
    public static final String COLUMN_SKANWORD_FILES_DIR = "files_dir";
    public static final String COLUMN_SKANWORD_FINISHED = "finished";
    public static final String COLUMN_SKANWORD_HASH = "hash";
    public static final String COLUMN_SKANWORD_HINTS_MASK = "hints";
    public static final String COLUMN_SKANWORD_ID = "_id";
    public static final String COLUMN_SKANWORD_KEYWORD = "keyword";
    public static final String COLUMN_SKANWORD_LETTERS_MASK = "mask";
    public static final String COLUMN_SKANWORD_LEVEL = "level";
    public static final String COLUMN_SKANWORD_OPENED = "opened";
    public static final String COLUMN_SKANWORD_QUESTIONS = "questions";
    public static final String COLUMN_SKANWORD_QUESTIONS_COUNT = "questions_count";
    public static final String COLUMN_SKANWORD_QUESTIONS_FINISHED = "questions_finished_count";
    public static final String COLUMN_SKANWORD_REPLACES = "replaces";
    public static final String COLUMN_SKANWORD_SETTINGS = "settings";
    public static final String COLUMN_SKANWORD_STARTED = "started";
    public static final String COLUMN_SKANWORD_UPDATE_TIME = "update_time";
    public static final int DAILY_BONUS_DATA_ID = 19;
    private static final String DATABASE_ALTER_TEAM_1 = "ALTER TABLE scanword_sets_data ADD COLUMN set_cover  VARCHAR( 255 ) DEFAULT NULL;";
    private static final String DATABASE_ALTER_TEAM_2 = "ALTER TABLE scanwords ADD COLUMN skanword_cover  VARCHAR( 255 ) DEFAULT NULL ;";
    private static final String DATABASE_ALTER_TEAM_3 = "ALTER TABLE scanwords ADD COLUMN questions_count  INTEGER DEFAULT 0;";
    private static final String DATABASE_ALTER_TEAM_4 = "ALTER TABLE scanwords ADD COLUMN opened  INTEGER DEFAULT 0;";
    private static final String DATABASE_ALTER_TEAM_5 = "ALTER TABLE scanwords ADD COLUMN keyword  TEXT;";
    public static final String DATABASE_NAME = "smappsscanword";
    public static final int DATABASE_VERSION = 6;
    public static final int FRIENDS_DATA_ID = 7;
    private static final Logger LOG;
    public static final int NEWS_DATA_ID = 4;
    public static final int NEWS_HASH_ID = 15;
    public static final int NOTIFICATIONS_DATA_ID = 10;
    public static final int OFFERS_DATA_ID = 12;
    public static final int OFFER_OPENED_TASKS_DATA_ID = 13;
    public static final int OPTIONS_DATA_ID = 8;
    public static final int PURCHASES_DATA_ID = 9;
    public static final int RATING_DATA_ID = 5;
    public static final int RATING_HASH_ID = 16;
    public static final int READ_NEWS_DATA_ID = 14;
    public static final int SHOP_DATA_ID = 3;
    public static final int SHOP_HASH_ID = 17;
    public static final String TABLE_GENERAL_DATA = "table_general_data";
    public static final String TABLE_SETS_INFO = "scanword_sets_data";
    public static final String TABLE_SET_DATA = "scanword_set_data";
    public static final String TABLE_SKANWORDS = "scanwords";
    public static final int UPDATES_DATA_ID = 1;
    public static final int UPDATE_LAST_DATA_ID = 2;
    public static final int USER_DATA_ID = 6;
    public static final int VERSIONS_DATA_ID = 18;
    private Integer skanwordsProgressMutex;
    
    static {
        LOG = Logger.getLogger("org.sknanwords.android");
    }
    
    public DataBaseManager(final Context context) {
        super(context, "smappsscanword", (SQLiteDatabase.CursorFactory)null, 6);
        this.skanwordsProgressMutex = 0;
    }
    
    private void createCrosswordsTable(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS scanwords(_id INTEGER PRIMARY KEY NOT NULL, set_id INTEGER, hash VARCHAR(32), level INTEGER, display_id INTEGER, settings TEXT, questions TEXT, replaces TEXT, files_dir TEXT, mask TEXT, questions_finished_count INTEGER DEFAULT 0, update_time BIGINT DEFAULT 0, started INTEGER DEFAULT 0, finished INTEGER DEFAULT 0, opened INTEGER DEFAULT 0, answers TEXT, hints TEXT,keyword TEXT,skanword_cover VARCHAR( 255 ) DEFAULT NULL,questions_count INTEGER DEFAULT 0 )");
    }
    
    private void createGeneralDataTable(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_general_data(_id BIGINT PRIMARY KEY, general_data BLOB NOT NULL );");
    }
    
    private void createSetsTable(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS scanword_sets_data(set_id INTEGER PRIMARY KEY NOT NULL, set_name VARCHAR(15), set_display_name VARCHAR(15), set_cover VARCHAR(255), set_data_hash VARCHAR(32), set_save_hash VARCHAR(32), set_scanwords_count INTEGER, set_scanwords_started_count INTEGER, set_scanwords_finished_count INTEGER, set_download_time_left INTEGER DEFAULT 0, set_downloaded_time BIGINT DEFAULT 0, set_free_ids TEXT, set_offer_ids TEXT );");
    }
    
    public long addSkanwordsSetInfo(final SkanwordsSetInfo skanwordsSetInfo) {
        long n;
        try {
            final ContentValues contentValues = new ContentValues();
            contentValues.put("set_id", skanwordsSetInfo.getId());
            contentValues.put("set_name", skanwordsSetInfo.getName());
            contentValues.put("set_data_hash", skanwordsSetInfo.getDataHash());
            contentValues.put("set_save_hash", skanwordsSetInfo.getSaveHash());
            contentValues.put("set_display_name", skanwordsSetInfo.getDisplayName());
            contentValues.put("set_scanwords_count", Integer.valueOf(skanwordsSetInfo.getSkanwordsCount()));
            contentValues.put("set_downloaded_time", Long.valueOf(skanwordsSetInfo.getDownloadedTime()));
            contentValues.put("set_download_time_left", Integer.valueOf(skanwordsSetInfo.getDownloadTimeLeft()));
            contentValues.put("set_offer_ids", new Gson().toJson(skanwordsSetInfo.getOfferIds()));
            contentValues.put("set_free_ids", new Gson().toJson(skanwordsSetInfo.getFreeIds()));
            contentValues.put("set_scanwords_finished_count", Integer.valueOf(skanwordsSetInfo.getSkanwordsFinishedCount()));
            contentValues.put("set_scanwords_started_count", Integer.valueOf(skanwordsSetInfo.getSkanwordsStartedCount()));
            final long insertWithOnConflict;
            n = (insertWithOnConflict = this.getWritableDatabase().insertWithOnConflict("scanword_sets_data", (String)null, contentValues, 5));
            if (n != -1L) {
                return insertWithOnConflict;
            }
            try {
                DataBaseManager.LOG.warning("Failed to insert puzzle into database: ");
                return n;
            }
            catch (Exception ex) {}
        }
        catch (Exception ex) {
            n = -1L;
            ex.printStackTrace();
        }
        final Exception ex;
        return n;
    }
    
    public void clearProgress(final List<Integer> list) {
        if (list.size() < 1) {
            return;
        }
        final SQLiteDatabase writableDatabase = this.getWritableDatabase();
        try {
            Log.v("", "clearProgress");
            final StringBuilder sb = new StringBuilder();
            sb.append("UPDATE scanwords SET started=0, finished=0, answers='', hints='', questions_finished_count=0, questions_count=0 WHERE set_id IN (");
            sb.append(Utils.join(list, ","));
            sb.append(")");
            writableDatabase.execSQL(sb.toString());
        }
        catch (Exception ex) {
            Log.v("", "clearProgress exeption");
            ex.printStackTrace();
        }
    }
    
    public void debugCopyDatabaseFileToExternalStorage() {
        try {
            final String path = this.getReadableDatabase().getPath();
            final FileInputStream fileInputStream = new FileInputStream(path);
            final File file = new File(SmappsScanwords.TEMP_DIR, "crosswords.db");
            final Logger log = DataBaseManager.LOG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Copying ");
            sb.append(path);
            sb.append(" ==> ");
            sb.append(file);
            log.info(sb.toString());
            final FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.getChannel().transferFrom(fileOutputStream.getChannel(), 0L, fileOutputStream.getChannel().size());
            fileInputStream.close();
            fileOutputStream.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void dropDatabase() {
        SQLiteDatabase.deleteDatabase(new File(this.getReadableDatabase().getPath()));
    }


    public List<Skanword> getFullSkanwordDataWithSelection(String object) {
        Integer n = this.skanwordsProgressMutex;
        // MONITORENTER : n
        ArrayList<Skanword> arrayList = new ArrayList<Skanword>();
        SQLiteDatabase sQLiteDatabase = this.getReadableDatabase();
        this.createSetsTable(sQLiteDatabase);
        Cursor sQLiteDatabase2 = sQLiteDatabase.query(TABLE_SKANWORDS, new String[]{"_id", COLUMN_SET_ID, COLUMN_SKANWORD_HASH, COLUMN_SKANWORD_LEVEL, COLUMN_SKANWORD_DISPLAY_ID, COLUMN_SKANWORD_QUESTIONS, COLUMN_SKANWORD_REPLACES, COLUMN_SKANWORD_SETTINGS, COLUMN_SKANWORD_LETTERS_MASK, COLUMN_SKANWORD_ANSWERS_MASK, COLUMN_SKANWORD_HINTS_MASK, COLUMN_SKANWORD_STARTED, COLUMN_SKANWORD_FINISHED, COLUMN_SKANWORD_QUESTIONS_FINISHED, COLUMN_SKANWORD_UPDATE_TIME, COLUMN_SKANWORD_FILES_DIR, COLUMN_SKANWORD_COVER, COLUMN_SKANWORD_OPENED, COLUMN_SKANWORD_KEYWORD}, (String)object, null, null, null, null, null);
        do {
            block18 : {
                boolean bl;
                if (!(bl = sQLiteDatabase2.moveToNext())) {
                    sQLiteDatabase.close();
                    StringBuilder logStt = new StringBuilder();
                    logStt.append(" got some skanwords  ");
                    logStt.append(arrayList.size());
                    Log.v((String)"SkanwordsFunc", logStt.toString());
                    // MONITOREXIT : n
                    return arrayList;
                }
                DataBaseManager1 dbm1 = new DataBaseManager1(this);
                List dbm1List = (List)new Gson().fromJson(sQLiteDatabase2.getString(5), dbm1.getType());

                Skanword.Settings settings = new Gson().fromJson(sQLiteDatabase2.getString(7), Skanword.Settings.class);

                DataBaseManager2 dbm2 = new DataBaseManager2(this);
                List dbm2List = (List)new Gson().fromJson(sQLiteDatabase2.getString(6), dbm2.getType());

                DataBaseManager3 dbm3 = new DataBaseManager3(this);
                String[][] dbm3Arr = (String[][])new Gson().fromJson(sQLiteDatabase2.getString(8), dbm3.getType());

                Skanword.Keyword keyword = new Gson().fromJson(sQLiteDatabase2.getString(18), Skanword.Keyword.class);
                int n2 = sQLiteDatabase2.getInt(0);
                int n3 = sQLiteDatabase2.getInt(3);
                int n4 = sQLiteDatabase2.getInt(4);
                try {
                    Skanword sk = new Skanword(n2, n3, n4, settings, dbm1List, dbm2List, dbm3Arr, sQLiteDatabase2.getString(2), keyword, null);
                    try {
                        sk.setSetId(sQLiteDatabase2.getInt(1));
                    }
                    catch (Exception exception) {
                        break block18;
                    }

                    try {
                        sk.setAnswers(sQLiteDatabase2.getString(9));
                    }
                    catch (Exception exception) {
                        break block18;
                    }

                    try {
                        sk.setHints(sQLiteDatabase2.getString(10));
                        bl = sQLiteDatabase2.getInt(11) != 0;
                        sk.setStarted(bl);
                        bl = sQLiteDatabase2.getInt(12) != 0;
                        sk.setFinished(bl);
                    }
                    catch (Exception exception) {}

                    try {
                        sk.setFinishedQuestionsCount(sQLiteDatabase2.getInt(13));
                    }
                    catch (Exception exception) {
                        break block18;
                    }

                    try {
                        sk.setUpdateTime(sQLiteDatabase2.getLong(14));
                        sk.setImageDir(sQLiteDatabase2.getString(15));
                        sk.setCover(sQLiteDatabase2.getString(16));
                        bl = sQLiteDatabase2.getInt(17) != 0;
                        sk.setOpened(bl);
                        sk.clearKeyword();
                        sk.checkKeyword();
                        arrayList.add((Skanword)sk);
                        continue;
                    }
                    catch (Exception exception) {
                        break block18;
                    }
                }
                catch (Exception exception) {
                    break block18;
                }
            }
            sQLiteDatabase.close();
        } while (true);
    }
    
    public Skanword getFullskanwordData(final Integer n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("_id=");
        sb.append(n);
        final List<Skanword> fullSkanwordDataWithSelection = this.getFullSkanwordDataWithSelection(sb.toString());
        if (fullSkanwordDataWithSelection.size() > 0) {
            return fullSkanwordDataWithSelection.get(0);
        }
        return null;
    }
    
    public Skanword getLastPlayedSkanword() {
        final List<Skanword> skanwordsWithSelection = this.getSkanwordsWithSelection("update_time>0 AND finished =0 AND started =1", "update_time DESC", "1");
        if (skanwordsWithSelection.size() > 0) {
            return skanwordsWithSelection.get(0);
        }
        return null;
    }
    
    public List<SkanwordsSetInfo> getSkanwordsSetsInfo() {
        final ArrayList<SkanwordsSetInfo> list = new ArrayList<SkanwordsSetInfo>();
        Object readableDatabase = this.getReadableDatabase();
        this.createSetsTable((SQLiteDatabase)readableDatabase);
        final Cursor query = ((SQLiteDatabase)readableDatabase).query("scanword_sets_data", new String[] { "set_id", "set_name", "set_display_name", "set_data_hash", "set_scanwords_count", "set_scanwords_finished_count", "set_scanwords_started_count", "set_downloaded_time", "set_download_time_left", "set_offer_ids", "set_free_ids", "set_save_hash", "set_cover" }, (String)null, (String[])null, (String)null, (String)null, (String)null);
        while (query.moveToNext()) {
            try {

                DataBaseManager4 dbm4 = new DataBaseManager4(this);
                List dbm4List = (List)new Gson().fromJson(query.getString(10), dbm4.getType());

                DataBaseManager5 dbm5 = new DataBaseManager5(this);
                List dbm5List = (List)new Gson().fromJson(query.getString(9), dbm5.getType());

                final int int1 = query.getInt(0);
                final String string = query.getString(1);
                final String string2 = query.getString(2);
                final int int2 = query.getInt(4);

                final String string3 = query.getString(3);

                final String string4 = query.getString(11);

                final int int3 = query.getInt(8);

                final long long1 = query.getLong(7);
                final int int4 = query.getInt(6);

                final int int5 = query.getInt(5);

                readableDatabase = new SkanwordsSetInfo(int1, string, string2, int2, string3, string4, int3, dbm4List, dbm5List, long1, int4, int5, query.getString(12));
                ((SkanwordsSetInfo)readableDatabase).setLocalData(true);
                list.add((SkanwordsSetInfo)readableDatabase);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        query.close();

        return list;
    }
    
    public List<Skanword> getSkanwordsWithSelection(final String s) {
        return this.getSkanwordsWithSelection(s, null, null);
    }

    public List<Skanword> getSkanwordsWithSelection(String paramString1, String paramString2, String paramString3) {
        synchronized (this.skanwordsProgressMutex) {
            Log.v("SkanwordsSpeed", "getSkanwordsWithSelection start");
            List<Skanword> arrayList1 = new ArrayList<>();
            ArrayList<Skanword>  arrayList2 = new ArrayList<>();
            SQLiteDatabase sQLiteDatabase = getReadableDatabase();
            createSetsTable(sQLiteDatabase);
            Cursor cursor = sQLiteDatabase.query("scanwords", new String[] {
                    "_id", "set_id", "hash", "level", "display_id", "started", "finished", "questions_finished_count", "update_time", "files_dir",
                    "skanword_cover", "questions_count", "opened", "keyword" }, paramString1, null, null, null, paramString2, paramString3);
            while (true) {
                boolean bool = cursor.moveToNext();
                if (bool) {
                    try {
                        int j = cursor.getInt(11);
                        if (j > 0) {
                            try {
                                Skanword.Keyword keyword = (Skanword.Keyword)(new Gson()).fromJson(cursor.getString(13), Skanword.Keyword.class);
                                j = cursor.getInt(0);
                                int k = cursor.getInt(3);
                                try {
                                    Skanword skanword = new Skanword(j, k, cursor.getInt(4), null, null, null, (String[][])null, cursor.getString(2), keyword, null);
                                    try {
                                        skanword.setSetId(cursor.getInt(1));
                                        try {
                                            if (cursor.getInt(5) != 0) {
                                                bool = true;
                                            } else {
                                                bool = false;
                                            }
                                            skanword.setStarted(bool);
                                            try {
                                                if (cursor.getInt(6) != 0) {
                                                    bool = true;
                                                } else {
                                                    bool = false;
                                                }
                                                skanword.setFinished(bool);
                                                try {
                                                    skanword.setFinishedQuestionsCount(cursor.getInt(7));
                                                    skanword.setUpdateTime(cursor.getLong(8));
                                                    try {
                                                        skanword.setImageDir(cursor.getString(9));
                                                        try {
                                                            skanword.setCover(cursor.getString(10));
                                                            skanword.setQuestionsCount(cursor.getInt(11));
                                                            if (cursor.getInt(12) != 0) {
                                                                bool = true;
                                                            } else {
                                                                bool = false;
                                                            }
                                                            skanword.setOpened(bool);
                                                            arrayList1.add(skanword);
                                                            continue;
                                                        } catch (Exception e) {}
                                                    } catch (Exception e) {}
                                                } catch (Exception e) {}
                                            } catch (Exception e) {}
                                        } catch (Exception e) {}
                                    } catch (Exception e) {}
                                } catch (Exception e) {}
                            } catch (Exception e) {}
                        } else {
                            Log.v("SkanwordsFunc", "getFullskanwordData");
                            try {
                                Skanword skanword = getFullskanwordData(Integer.valueOf(cursor.getInt(0)));
                                arrayList2.add(skanword);
                                arrayList1.add(skanword);
                                continue;
                            } catch (Exception e) {}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    cursor.close();
                    continue;
                }
                cursor.close();
                int i = arrayList2.size();
                if (i > 0)
                    try {
                        for (Skanword skanword : arrayList2) {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("skanword_cover", skanword.getCover());
                            contentValues.put("questions_count", Integer.valueOf(skanword.getQuestionsCount()));
                            contentValues.put("keyword", (new Gson()).toJson(skanword.getKeyword()));
                            StringBuilder stringBuilder1 = new StringBuilder();
                            stringBuilder1.append("_id=");
                            stringBuilder1.append(skanword.getId());
                            sQLiteDatabase.update("scanwords", contentValues, stringBuilder1.toString(), null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("getSkanwordsWithSelection end ");
                stringBuilder.append(arrayList1.size());
                Log.v("SkanwordsSpeed", stringBuilder.toString());
                return arrayList1;
            }
        }
    }

    public void importSkanwordData(List<Skanword> object) {
        Integer n = this.skanwordsProgressMutex;
        synchronized (n) {
            try {
                try {
                    SQLiteDatabase sQLiteDatabase = this.getWritableDatabase();
                    Iterator skanwordIterator = object.iterator();
                    while (skanwordIterator.hasNext()) {
                        Skanword skanword = (Skanword)skanwordIterator.next();
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(COLUMN_SET_ID, Integer.valueOf(skanword.getSetId()));
                        contentValues.put("_id", Integer.valueOf(skanword.getId()));
                        contentValues.put(COLUMN_SKANWORD_HASH, skanword.getHash());
                        contentValues.put(COLUMN_SKANWORD_LEVEL, Integer.valueOf(skanword.getLevel()));
                        contentValues.put(COLUMN_SKANWORD_DISPLAY_ID, Integer.valueOf(skanword.getViewId()));
                        contentValues.put(COLUMN_SKANWORD_FILES_DIR, skanword.getImageDir());
                        contentValues.put(COLUMN_SKANWORD_COVER, skanword.getCover());
                        contentValues.put(COLUMN_SKANWORD_QUESTIONS_COUNT, Integer.valueOf(skanword.getQuestionsCount()));
                        contentValues.put(COLUMN_SKANWORD_QUESTIONS, new Gson().toJson(skanword.getQuestions()));
                        contentValues.put(COLUMN_SKANWORD_REPLACES, new Gson().toJson(skanword.getReplaces()));
                        contentValues.put(COLUMN_SKANWORD_SETTINGS, new Gson().toJson((Object)skanword.getSettings()));
                        contentValues.put(COLUMN_SKANWORD_LETTERS_MASK, new Gson().toJson(skanword.getMask()));
                        contentValues.put(COLUMN_SKANWORD_KEYWORD, new Gson().toJson((Object)skanword.getKeyword()));
                        sQLiteDatabase.insertWithOnConflict(TABLE_SKANWORDS, null, contentValues, 4);
                    }
                    return;
                }
                catch (Exception exception) {
                    exception.printStackTrace();
                }
                return;
            }
            catch (Throwable e) {
                throw e;
            }
        }
    }

    public void insertSkanwordProgress(List<List<String>> paramList, boolean paramBoolean) {
        try {
            SQLiteDatabase sQLiteDatabase = getWritableDatabase();
            Iterator iterator = paramList.iterator();
            while (true) {
                ContentValues contentValues;
                List list;
                boolean bool;
                if (iterator.hasNext()) {
                    list = (List)iterator.next();
                    contentValues = new ContentValues();
                    bool = false;
                    contentValues.put("set_id", Integer.valueOf((String)list.get(0)));
                    contentValues.put("_id", Integer.valueOf((String)list.get(1)));
                    contentValues.put("answers", (String)list.get(2));
                    contentValues.put("hints", (String)list.get(3));
                    if (!paramBoolean && list.size() > 4)
                        contentValues.put("questions_finished_count", (String)list.get(4));
                } else {
                    return;
                }
                if (!paramBoolean)
                    bool = true;
                contentValues.put("started", Boolean.valueOf(bool));
                contentValues.put("finished", Boolean.valueOf(paramBoolean));
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append("_id=");
                stringBuilder2.append((String)list.get(1));
                sQLiteDatabase.update("scanwords", contentValues, stringBuilder2.toString(), null);
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append(list);
                stringBuilder1.append("   progress");
                Log.v("SkanwordsFunc", stringBuilder1.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        this.createCrosswordsTable(sqLiteDatabase);
        this.createSetsTable(sqLiteDatabase);
        this.createGeneralDataTable(sqLiteDatabase);
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        final Logger log = DataBaseManager.LOG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Upgrading SQLite database from version ");
        sb.append(n);
        sb.append(" to version ");
        sb.append(n2);
        log.info(sb.toString());
        if (n < 2) {
            sqLiteDatabase.execSQL("ALTER TABLE scanword_sets_data ADD COLUMN set_cover  VARCHAR( 255 ) DEFAULT NULL;");
        }
        if (n < 3) {
            sqLiteDatabase.execSQL("ALTER TABLE scanwords ADD COLUMN skanword_cover  VARCHAR( 255 ) DEFAULT NULL ;");
        }
        if (n < 4) {
            sqLiteDatabase.execSQL("ALTER TABLE scanwords ADD COLUMN questions_count  INTEGER DEFAULT 0;");
        }
        if (n < 5) {
            sqLiteDatabase.execSQL("ALTER TABLE scanwords ADD COLUMN opened  INTEGER DEFAULT 0;");
        }
        if (n < 6) {
            sqLiteDatabase.execSQL("ALTER TABLE scanwords ADD COLUMN keyword  TEXT;");
        }
    }
    
    public void removeSkanwordSetInfoAndDataWithId(final Integer n) {
        final SQLiteDatabase writableDatabase = this.getWritableDatabase();
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM scanword_sets_data WHERE set_id=");
        sb.append(n);
        writableDatabase.execSQL(sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("DELETE FROM scanwords WHERE set_id=");
        sb2.append(n);
        writableDatabase.execSQL(sb2.toString());
    }
    
    public <T> T retrieveGeneralData(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("_id=");
        sb.append(n);
        final String string = sb.toString();
        final SQLiteDatabase readableDatabase = this.getReadableDatabase();
        this.createSetsTable(readableDatabase);
        final Cursor query = readableDatabase.query("table_general_data", new String[] { "general_data" }, string, (String[])null, (String)null, (String)null, (String)null);
        final boolean moveToNext = query.moveToNext();
        Object fromBlob;
        final Object o = fromBlob = null;
        if (moveToNext) {
            try {
                fromBlob = SarializableManager.fromBlob(query.getBlob(0), null);
            }
            catch (IOException ex) {
                ex.printStackTrace();
                query.close();
                fromBlob = o;
            }
            catch (ClassNotFoundException ex2) {
                ex2.printStackTrace();
                query.close();
                fromBlob = o;
            }
        }
        query.close();
        return (T)fromBlob;
    }
    
    public void storeGeneralData(final Serializable s, final int n) {
        try {
            final byte[] blobe = SarializableManager.toBlobe(s);
            final ContentValues contentValues = new ContentValues();
            contentValues.put("_id", Integer.valueOf(n));
            contentValues.put("general_data", blobe);
            if (this.getWritableDatabase().insertWithOnConflict("table_general_data", (String)null, contentValues, 5) == -1L) {
                DataBaseManager.LOG.warning("Failed to insert puzzle into database: ");
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void storeShopData(final ShopDataManager.Shop shop) {
        this.storeGeneralData(shop, 3);
    }
    
    public void storeUserUpdateData(final HashMap<String, UserUpdatesManager.UserUpdateData> hashMap) {
        this.storeGeneralData(hashMap, 1);
    }
    
    public void storeUserUpdateData(final UserUpdatesManager.UserUpdateData userUpdateData) {
        this.storeGeneralData(userUpdateData, 2);
    }
    
    public void updateSetInfoDynamicData(final SkanwordsSetInfo skanwordsSetInfo) {
        while (true) {
            try {
                final ContentValues contentValues = new ContentValues();
                contentValues.put("set_display_name", skanwordsSetInfo.getDisplayName());
                contentValues.put("set_offer_ids", new Gson().toJson(skanwordsSetInfo.getOfferIds()));
                contentValues.put("set_free_ids", new Gson().toJson(skanwordsSetInfo.getFreeIds()));
                contentValues.put("set_scanwords_started_count", Integer.valueOf(skanwordsSetInfo.getSkanwordsStartedCount()));
                contentValues.put("set_scanwords_finished_count", Integer.valueOf(skanwordsSetInfo.getSkanwordsStartedCount()));
                final StringBuilder sb = new StringBuilder();
                sb.append("updateSetProgress set info  started count - ");
                sb.append(skanwordsSetInfo.getSkanwordsStartedCount());
                sb.append("  finished count - ");
                sb.append(skanwordsSetInfo.getSkanwordsFinishedCount());
                Log.v("", sb.toString());
                final SQLiteDatabase writableDatabase = this.getWritableDatabase();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("set_id=");
                sb2.append(skanwordsSetInfo.getId());
                writableDatabase.update("scanword_sets_data", contentValues, sb2.toString(), (String[])null);
                Log.v("", "failed to update set info");
                return;
            }
            catch (Exception ex) {
                continue;
            }
        }
    }
    
    public void updateSetInfoProgress(final SkanwordsSetInfo skanwordsSetInfo) {
        while (true) {
            try {
                final ContentValues contentValues = new ContentValues();
                contentValues.put("set_scanwords_finished_count", new Gson().toJson(skanwordsSetInfo.getSkanwordsFinishedCount()));
                contentValues.put("set_scanwords_started_count", new Gson().toJson(skanwordsSetInfo.getSkanwordsStartedCount()));
                final StringBuilder sb = new StringBuilder();
                sb.append("updateSetProgress set info  started count - ");
                sb.append(skanwordsSetInfo.getSkanwordsStartedCount());
                sb.append("  finished count - ");
                sb.append(skanwordsSetInfo.getSkanwordsFinishedCount());
                Log.v("", sb.toString());
                final SQLiteDatabase writableDatabase = this.getWritableDatabase();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("set_id=");
                sb2.append(skanwordsSetInfo.getId());
                writableDatabase.update("scanword_sets_data", contentValues, sb2.toString(), (String[])null);
                Log.v("", "failed to update set info");
                return;
            }
            catch (Exception ex) {
                continue;
            }
        }
    }
    
    public void updateSetInfosProgress(final List<Integer> list) {
        for (final Integer n : list) {
            final StringBuilder sb = new StringBuilder();
            sb.append("set_id=");
            sb.append(n);
            final List<Skanword> fullSkanwordDataWithSelection = this.getFullSkanwordDataWithSelection(sb.toString());
            Integer n2 = 0;
            Integer n3 = 0;
            for (final Skanword skanword : fullSkanwordDataWithSelection) {
                if (skanword.isFinished()) {
                    ++n2;
                }
                else {
                    if (!skanword.isStarted()) {
                        continue;
                    }
                    ++n3;
                }
            }
            try {
                final String skanwordSyncHash = SkanwordsDataManager.getSkanwordSyncHash(fullSkanwordDataWithSelection);
                final SQLiteDatabase writableDatabase = this.getWritableDatabase();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("UPDATE scanword_sets_data SET set_scanwords_started_count=");
                sb2.append(n3);
                sb2.append(", ");
                sb2.append("set_scanwords_finished_count");
                sb2.append("=");
                sb2.append(n2);
                sb2.append(", ");
                sb2.append("set_save_hash");
                sb2.append("='");
                sb2.append(skanwordSyncHash);
                sb2.append("' WHERE ");
                sb2.append("set_id");
                sb2.append("=");
                sb2.append(n);
                writableDatabase.execSQL(sb2.toString());
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void updateSkanwordProgress(Skanword skanword, boolean bl) {
        Integer n = this.skanwordsProgressMutex;
        synchronized (n) {
            try {
                try {
                    ContentValues contentValue = new ContentValues();
                    contentValue.put("_id", (int)skanword.getId());
                    contentValue.put(COLUMN_SKANWORD_ANSWERS_MASK, skanword.getAnswers());
                    contentValue.put(COLUMN_SKANWORD_HINTS_MASK, skanword.getHints());
                    contentValue.put(COLUMN_SKANWORD_STARTED, Boolean.valueOf(skanword.isStarted()));
                    contentValue.put(COLUMN_SKANWORD_FINISHED, Boolean.valueOf(skanword.isFinished()));
                    contentValue.put(COLUMN_SKANWORD_OPENED, Boolean.valueOf(skanword.isOpened()));
                    contentValue.put(COLUMN_SKANWORD_KEYWORD, new Gson().toJson((Object)skanword.getKeyword()));
                    contentValue.put(COLUMN_SKANWORD_QUESTIONS_FINISHED, Integer.valueOf(skanword.getFinishedQuestionsCount()));
                    if (bl) {
                        contentValue.put("update_time", Long.valueOf(new Date().getTime() / 1000L));
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("updateSkanwordProgress  ");
                    sb.append(skanword.isStarted());
                    Log.v("", sb.toString());
                    final SQLiteDatabase writableDatabase = this.getWritableDatabase();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("_id=");
                    sb2.append(skanword.getId());
                    writableDatabase.update("scanwords", contentValue, sb2.toString(), (String[])null);
                    final ArrayList<Integer> list = new ArrayList<Integer>();
                    list.add(skanword.getSetId());
                    this.updateSetInfosProgress(list);
                }
                catch (Exception exception) {
                    Log.v((String)"", (String)"updateSkanwordProgress exeption");
                    exception.printStackTrace();
                }
                return;
            }
            catch (Throwable throwable) {
                throw throwable;}
        }
    }

    // ---- Classes

    public static class IDAndFilename
    {
        public String filename;
        public long id;
        
        public IDAndFilename(final long id, final String filename) {
            super();
            this.id = id;
            this.filename = filename;
        }
    }

    class DataBaseManager1 extends TypeToken<List<Skanword.Question>> {
        final /* synthetic */ DataBaseManager this0;

        DataBaseManager1(final DataBaseManager this0) {
            super();
            this.this0 = this0;
        }
    }

    class DataBaseManager2 extends TypeToken<List<Skanword.Replace>> {
        final /* synthetic */ DataBaseManager this0;

        DataBaseManager2(final DataBaseManager this0) {
            super();
            this.this0 = this0;
        }
    }

    class DataBaseManager3 extends TypeToken<String[][]> {
        final /* synthetic */ DataBaseManager this0;

        DataBaseManager3(final DataBaseManager this0) {
            super();
            this.this0 = this0;
        }
    }

    class DataBaseManager4 extends TypeToken<List<Integer>> {
        final /* synthetic */ DataBaseManager this0;

        DataBaseManager4(final DataBaseManager this0) {
            super();
            this.this0 = this0;
        }
    }

    class DataBaseManager5 extends TypeToken<List<Integer>> {
        final /* synthetic */ DataBaseManager this0;

        DataBaseManager5(final DataBaseManager this0) {
            super();
            this.this0 = this0;
        }
    }
}
