package org.skanword.and.datamanager;

import java.io.*;

public class OptionsData implements Serializable
{
    private static final long serialVersionUID = -9096395339197790134L;
    private boolean autoDownload;
    private int customNotificationTimeHour;
    private int customNotificationTimeMinute;
    private boolean defaultNotificationsTime;
    private boolean disabledNotificationsSounds;
    private boolean increasedRatio;
    private boolean informNewIssues;
    private boolean onlyWifi;
    private boolean skipGuessedLetters;
    private boolean sounds;
    
    public OptionsData() {
        super();
        this.sounds = true;
        this.skipGuessedLetters = false;
        this.increasedRatio = false;
        this.autoDownload = false;
        this.onlyWifi = false;
        this.informNewIssues = true;
        this.defaultNotificationsTime = true;
        this.disabledNotificationsSounds = false;
        this.customNotificationTimeHour = 0;
        this.customNotificationTimeMinute = 0;
    }
    
    public OptionsData(final boolean sounds, final boolean skipGuessedLetters, final boolean increasedRatio, final boolean autoDownload, final boolean onlyWifi, final boolean informNewIssues, final boolean defaultNotificationsTime, final boolean disabledNotificationsSounds, final int customNotificationTimeHour, final int customNotificationTimeMinute) {
        super();
        this.sounds = true;
        this.skipGuessedLetters = false;
        this.increasedRatio = false;
        this.autoDownload = false;
        this.onlyWifi = false;
        this.informNewIssues = true;
        this.defaultNotificationsTime = true;
        this.disabledNotificationsSounds = false;
        this.customNotificationTimeHour = 0;
        this.customNotificationTimeMinute = 0;
        this.sounds = sounds;
        this.skipGuessedLetters = skipGuessedLetters;
        this.increasedRatio = increasedRatio;
        this.autoDownload = autoDownload;
        this.onlyWifi = onlyWifi;
        this.informNewIssues = informNewIssues;
        this.defaultNotificationsTime = defaultNotificationsTime;
        this.disabledNotificationsSounds = disabledNotificationsSounds;
        this.customNotificationTimeHour = customNotificationTimeHour;
        this.customNotificationTimeMinute = customNotificationTimeMinute;
    }
    
    public static long getSerialversionuid() {
        return -9096395339197790134L;
    }
    
    public int getCustomNotificationTimeHour() {
        return this.customNotificationTimeHour;
    }
    
    public int getCustomNotificationTimeMinute() {
        return this.customNotificationTimeMinute;
    }
    
    public boolean isAutoDownload() {
        return this.autoDownload;
    }
    
    public boolean isDefaultNotificationsTime() {
        return this.defaultNotificationsTime;
    }
    
    public boolean isDisabledNotificationsSounds() {
        return this.disabledNotificationsSounds;
    }
    
    public boolean isIncreasedRatio() {
        return this.increasedRatio;
    }
    
    public boolean isInformNewIssues() {
        return this.informNewIssues;
    }
    
    public boolean isOnlyWifi() {
        return this.onlyWifi;
    }
    
    public boolean isSkipGuessedLetters() {
        return this.skipGuessedLetters;
    }
    
    public boolean isSounds() {
        return this.sounds;
    }
    
    public void setAutoDownload(final boolean autoDownload) {
        this.autoDownload = autoDownload;
    }
    
    public void setCustomNotificationTimeHour(final int customNotificationTimeHour) {
        this.customNotificationTimeHour = customNotificationTimeHour;
    }
    
    public void setCustomNotificationTimeMinute(final int customNotificationTimeMinute) {
        this.customNotificationTimeMinute = customNotificationTimeMinute;
    }
    
    public void setDefaultNotificationsTime(final boolean defaultNotificationsTime) {
        this.defaultNotificationsTime = defaultNotificationsTime;
    }
    
    public void setDisabledNotificationsSounds(final boolean disabledNotificationsSounds) {
        this.disabledNotificationsSounds = disabledNotificationsSounds;
    }
    
    public void setIncreasedRatio(final boolean increasedRatio) {
        this.increasedRatio = increasedRatio;
    }
    
    public void setInformNewIssues(final boolean informNewIssues) {
        this.informNewIssues = informNewIssues;
    }
    
    public void setOnlyWifi(final boolean onlyWifi) {
        this.onlyWifi = onlyWifi;
    }
    
    public void setSkipGuessedLetters(final boolean skipGuessedLetters) {
        this.skipGuessedLetters = skipGuessedLetters;
    }
    
    public void setSounds(final boolean sounds) {
        this.sounds = sounds;
    }
}
