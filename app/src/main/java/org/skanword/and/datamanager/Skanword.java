package org.skanword.and.datamanager;

import com.google.gson.annotations.*;
import org.skanword.and.network.*;
import org.skanword.and.etc.*;
import java.util.*;
import android.util.*;
import java.io.*;
import org.skanword.and.scanwordgame.*;
import android.graphics.*;

public class Skanword implements Serializable
{
    private static final long serialVersionUID = 346611261613055968L;
    private String answers;
    private boolean changed;
    private String cover;
    private boolean finished;
    private int finishedQuestionsCount;
    private final String hash;
    private String hints;
    private final int id;
    private String imageDir;
    private final Keyword keyword;
    private final int level;
    private final String[][] mask;
    private boolean opened;
    private final List<Question> questions;
    private int questionsCount;
    @SerializedName("replace")
    private final List<Replace> replaces;
    private int setId;
    private final Settings settings;
    private boolean started;
    private long updateTime;
    @SerializedName("view_id")
    private final int viewId;
    
    public Skanword(final int id, final int level, final int viewId, final Settings settings, final List<Question> questions, final List<Replace> replaces, final String[][] mask, final String hash, final Keyword keyword, final String imageDir) {
        super();
        this.id = id;
        this.level = level;
        this.viewId = viewId;
        this.settings = settings;
        this.questions = questions;
        this.replaces = replaces;
        this.mask = mask;
        this.hash = hash;
        this.keyword = keyword;
        this.imageDir = imageDir;
    }
    
    private void checkAnswerForStarted() {
        final char[] array = new char[this.getSettings().getWidth() * this.getSettings().getHeight()];
        Arrays.fill(array, ' ');
        this.started = (new String(array).equals(this.answers) ^ true);
    }
    
    private String getLetter(int n, final int n2) {
        if (this.getSettings() == null) {
            return " ";
        }
        n = n * this.getSettings().getHeight() + n2;
        return this.getAnswers().substring(n, n + 1).toLowerCase();
    }
    
    private boolean openedByHint(int n, final int n2) {
        if (this.getSettings() == null) {
            return false;
        }
        n = n * this.getSettings().getHeight() + n2;
        return this.getHints().substring(n, n + 1).toLowerCase().equals(".");
    }
    
    private boolean wordGuessedForCell(final int n, final int n2) {
        for (final Question question : this.getQuestions()) {
            if (question.getAnswer().getX() != n && question.getAnswer().getY() != n2) {
                continue;
            }
            final boolean verticalOrientation = question.getQuestionData().isVerticalOrientation();
            new StringBuilder();
            final StringBuilder sb = new StringBuilder();
            int i = 0;
            int n3 = 0;
            while (i < question.getAnswer().getLength()) {
                final int x = question.getAnswer().getX();
                int n4;
                if (verticalOrientation) {
                    n4 = 0;
                }
                else {
                    n4 = i;
                }
                final int n5 = x + n4;
                final int y = question.getAnswer().getY();
                int n6;
                if (verticalOrientation) {
                    n6 = i;
                }
                else {
                    n6 = 0;
                }
                final int n7 = y + n6;
                int n8 = n3;
                if (n5 == n) {
                    n8 = n3;
                    if (n7 == n2) {
                        question.setHasKeywordCell(true);
                        n8 = 1;
                    }
                }
                final int n9 = this.getSettings().getHeight() * n5 + n7;
                String s = this.getAnswers().substring(n9, n9 + 1).toLowerCase();
                if (!s.equals(" ")) {
                    String s2 = s;
                    if (this.getReplaces() != null) {
                        s2 = s;
                        if (this.getReplaces().size() > 0) {
                            final Iterator<Replace> iterator2 = this.getReplaces().iterator();
                            while (true) {
                                s2 = s;
                                if (!iterator2.hasNext()) {
                                    break;
                                }
                                final Replace replace = iterator2.next();
                                if (n5 != replace.getCellX() || n7 != replace.getCellY() || !s.toLowerCase().equals(replace.getLetterSource().toLowerCase())) {
                                    continue;
                                }
                                s = replace.getLetterTarget();
                            }
                        }
                    }
                    sb.append(s2);
                }
                ++i;
                n3 = n8;
            }
            if (n3 == 0) {
                continue;
            }
            final String string = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            final String hash = question.getAnswer().getHash();
            sb2.append(MainNetworkManager.getInstance().getNetId());
            sb2.append('_');
            sb2.append(this.getId());
            sb2.append('_');
            sb2.append(string.toLowerCase());
            sb2.append('_');
            sb2.append("ce9d660ffb810b5e59b444b84a712ca1");
            if (hash.equals(Utils.md5(sb2.toString()))) {
                return true;
            }
        }
        return false;
    }
    
    public void calculateFinishedQuestions() {
        boolean finished = false;
        this.finishedQuestionsCount = 0;
    Label_0329_Outer:
        for (final Question question : this.getQuestions()) {
            if (!question.isComplete()) {
                final boolean verticalOrientation = question.getQuestionData().isVerticalOrientation();
                new StringBuilder();
                final StringBuilder sb = new StringBuilder();
                int i = 0;
                while (true) {
                    while (i < question.getAnswer().getLength()) {
                        final int x = question.getAnswer().getX();
                        int n;
                        if (verticalOrientation) {
                            n = 0;
                        }
                        else {
                            n = i;
                        }
                        final int n2 = x + n;
                        final int y = question.getAnswer().getY();
                        int n3;
                        if (verticalOrientation) {
                            n3 = i;
                        }
                        else {
                            n3 = 0;
                        }
                        final int n4 = y + n3;
                        final int n5 = this.getSettings().getHeight() * n2 + n4;
                        String s = this.answers.substring(n5, n5 + 1).toLowerCase();
                        if (s.equals(" ")) {
                            final boolean b = true;
                            if (b) {
                                continue Label_0329_Outer;
                            }
                            final String string = sb.toString();
                            final StringBuilder sb2 = new StringBuilder();
                            final String hash = question.getAnswer().getHash();
                            sb2.append(MainNetworkManager.getInstance().getNetId());
                            sb2.append('_');
                            sb2.append(this.getId());
                            sb2.append('_');
                            sb2.append(string.toLowerCase());
                            sb2.append('_');
                            sb2.append("ce9d660ffb810b5e59b444b84a712ca1");
                            if (hash.equals(Utils.md5(sb2.toString()))) {
                                question.setComplete(true);
                                ++this.finishedQuestionsCount;
                                continue Label_0329_Outer;
                            }
                            continue Label_0329_Outer;
                        }
                        else {
                            String s2 = s;
                            if (this.getReplaces() != null) {
                                s2 = s;
                                if (this.getReplaces().size() > 0) {
                                    final Iterator<Replace> iterator2 = this.getReplaces().iterator();
                                    while (true) {
                                        s2 = s;
                                        if (!iterator2.hasNext()) {
                                            break;
                                        }
                                        final Replace replace = iterator2.next();
                                        if (n2 != replace.getCellX() || n4 != replace.getCellY() || !s.toLowerCase().equals(replace.getLetterSource().toLowerCase())) {
                                            continue Label_0329_Outer;
                                        }
                                        s = replace.getLetterTarget();
                                    }
                                }
                            }
                            sb.append(s2);
                            ++i;
                        }
                    }
                    final boolean b = false;
                    continue;
                }
            }
            ++this.finishedQuestionsCount;
        }
        if (!this.isFinished()) {
            if (this.finishedQuestionsCount == this.getQuestions().size()) {
                finished = true;
            }
            this.setFinished(finished);
        }
    }
    
    public void checkKeyword() {
        if (this.getQuestions() != null) {
            if (this.getKeyword() == null) {
                return;
            }
            final Iterator<Keyword.KeywordCell> iterator = this.getKeyword().getCells().iterator();
            boolean b = true;
            while (iterator.hasNext()) {
                final Keyword.KeywordCell keywordCell = iterator.next();
                if (keywordCell.isPersist()) {
                    continue;
                }
                final String letter = this.getLetter(keywordCell.getX(), keywordCell.getY());
                final boolean openedByHint = this.openedByHint(keywordCell.getX(), keywordCell.getY());
                final boolean b2 = this.wordGuessedForCell(keywordCell.getX(), keywordCell.getY()) || openedByHint;
                final String s = this.getMask()[keywordCell.getX()][keywordCell.getY()];
                final boolean b3 = s != null && !s.equals("false") && SkanwordsDataManager.md5Answer(String.valueOf(letter), this.getId()).equals(s);
                keywordCell.setCharacter(letter, b3 && b2);
                if (b3) {
                    continue;
                }
                b = false;
            }
            if (b) {
                for (final Keyword.KeywordCell keywordCell2 : this.getKeyword().getCells()) {
                    keywordCell2.setCharacter(this.getLetter(keywordCell2.getX(), keywordCell2.getY()), true);
                }
            }
        }
    }
    
    public void clear() {
        final char[] array = new char[this.getSettings().getWidth() * this.getSettings().getHeight()];
        Arrays.fill(array, ' ');
        this.answers = new String(array);
        this.hints = new String(array);
    }
    
    public void clearKeyword() {
        if (this.getKeyword() == null) {
            return;
        }
        final Iterator<Keyword.KeywordCell> iterator = this.getKeyword().getCells().iterator();
        while (iterator.hasNext()) {
            ((Keyword.KeywordCell)iterator.next()).setCharacter(" ", false);
        }
    }
    
    public String getAnswers() {
        final String answers = this.answers;
        if (answers == null || answers.equals("")) {
            final char[] array = new char[this.getSettings().getWidth() * this.getSettings().getHeight()];
            Arrays.fill(array, ' ');
            this.answers = new String(array);
        }
        return this.answers;
    }
    
    public String getCover() {
        if (this.cover == null && this.getQuestions() != null) {
            for (final Question question : this.getQuestions()) {
                if (!question.getQuestionData().getQuestionValue().isTextQuestion()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("/");
                    sb.append(question.getQuestionData().getQuestionValue().getUrl());
                    this.cover = sb.toString();
                    break;
                }
            }
        }
        return this.cover;
    }
    
    public int getFinishedQuestionsCount() {
        return this.finishedQuestionsCount;
    }
    
    public String getHash() {
        return this.hash;
    }
    
    public String getHints() {
        final String hints = this.hints;
        if (hints == null || hints.equals("")) {
            final char[] array = new char[this.getSettings().getWidth() * this.getSettings().getHeight()];
            Arrays.fill(array, ' ');
            this.hints = new String(array);
        }
        return this.hints;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getImageDir() {
        return this.imageDir;
    }
    
    public Keyword getKeyword() {
        return this.keyword;
    }
    
    public int getLevel() {
        return this.level;
    }
    
    public String[][] getMask() {
        return this.mask;
    }
    
    public Question getNextToQuestion(final Question question) {
        if (this.getQuestionsCount() < 1) {
            return null;
        }
        final int index = this.questions.indexOf(question);
        if (index == 0) {
            return this.questions.get(this.questionsCount - 1);
        }
        return this.questions.get(index - 1);
    }
    
    public Question getPreviousToQuestion(final Question question) {
        if (this.getQuestionsCount() < 1) {
            return null;
        }
        final int index = this.questions.indexOf(question);
        if (index == this.questionsCount - 1) {
            return this.questions.get(0);
        }
        return this.questions.get(index + 1);
    }
    
    public List<Question> getQuestions() {
        return this.questions;
    }
    
    public int getQuestionsCount() {
        if (this.questionsCount < 1 && this.getQuestions() != null) {
            this.questionsCount = this.getQuestions().size();
        }
        return this.questionsCount;
    }
    
    public List<Replace> getReplaces() {
        return this.replaces;
    }
    
    public int getSetId() {
        return this.setId;
    }
    
    public Settings getSettings() {
        return this.settings;
    }
    
    public long getUpdateTime() {
        return this.updateTime;
    }
    
    public int getViewId() {
        return this.viewId;
    }
    
    public boolean hasFiles() {
        final String imageDir = this.getImageDir();
        final StringBuilder sb = new StringBuilder();
        sb.append("file name ");
        sb.append(imageDir);
        Log.v("", sb.toString());
        final String[] list = new File(imageDir).list();
        return list != null && list.length >= 1;
    }
    
    public boolean isChanged() {
        return this.changed;
    }
    
    public boolean isFinished() {
        return this.finished;
    }
    
    public boolean isOpened() {
        return this.opened;
    }
    
    public boolean isStarted() {
        return this.started;
    }
    
    public void reset() {
        final Iterator<Question> iterator = this.questions.iterator();
        while (iterator.hasNext()) {
            iterator.next().setComplete(false);
        }
    }
    
    public void setAnswers(final String answers) {
        this.answers = answers;
    }
    
    public void setChanged(final boolean changed) {
        this.changed = changed;
    }
    
    public void setCover(final String cover) {
        this.cover = cover;
    }
    
    public void setFinished(final boolean finished) {
        this.finished = finished;
    }
    
    public void setFinishedQuestionsCount(final int finishedQuestionsCount) {
        this.finishedQuestionsCount = finishedQuestionsCount;
    }
    
    public void setHints(final String hints) {
        this.hints = hints;
    }
    
    public void setImageDir(final String imageDir) {
        this.imageDir = imageDir;
    }
    
    public void setLetter(final SkanwordBoard.Position position, final Point point, final String s, final boolean b) {
        Log.v("SkanwordsTime", "setLetter  1");
        this.setLetter(position, point, s, b, true);
        Log.v("SkanwordsTime", "setLetter  2");
    }
    
    public void setLetter(final SkanwordBoard.Position position, final Point point, String answers, final boolean b, final boolean b2) {
        this.getAnswers();
        this.getHints();
        final int n = position.down + point.y * position.across;
        String lowerCase = answers;
        if (!answers.equals(" ")) {
            lowerCase = answers.toLowerCase();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.answers.substring(0, n));
        sb.append(lowerCase);
        answers = this.answers;
        final int n2 = n + 1;
        sb.append(answers.substring(n2));
        this.answers = sb.toString();
        if (b) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.hints.substring(0, n));
            sb2.append(".");
            sb2.append(this.hints.substring(n2));
            this.hints = sb2.toString();
        }
        if (lowerCase.equals(" ") && this.finishedQuestionsCount == 0) {
            this.checkAnswerForStarted();
        }
        else {
            this.started = true;
        }
        if (b2) {
            UserUpdatesManager.getInstance().updateScanwordMasks(this);
        }
        this.setChanged(true);
    }
    
    public void setOpened(final boolean opened) {
        this.opened = opened;
    }
    
    public void setQuestionsCount(final int questionsCount) {
        this.questionsCount = questionsCount;
    }
    
    public void setSetId(final int setId) {
        this.setId = setId;
    }
    
    public void setStarted(final boolean started) {
        this.started = started;
    }
    
    public void setUpdateTime(final long updateTime) {
        this.updateTime = updateTime;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("\u2116");
        sb.append(this.id);
        return sb.toString();
    }
    
    public static class Answer implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private final String hash;
        private final int length;
        private final int x;
        private final int y;
        
        public Answer(final int x, final int y, final int length, final String hash) {
            super();
            this.x = x;
            this.y = y;
            this.length = length;
            this.hash = hash;
        }
        
        static /* synthetic */ int access000(final Answer answer) {
            return answer.length;
        }
        
        public String getHash() {
            return this.hash;
        }
        
        public int getLength() {
            return this.length;
        }
        
        public int getX() {
            return this.x;
        }
        
        public int getY() {
            return this.y;
        }
    }
    
    public static class ArrowData implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private final int type;
        
        public ArrowData(final int type) {
            super();
            this.type = type;
        }
        
        public int getType() {
            return this.type;
        }
    }
    
    public static class Keyword implements Serializable
    {
        private static final long serialVersionUID = 1003502434278371538L;
        private final List<KeywordCell> cells;
        
        public Keyword(final List<KeywordCell> cells) {
            super();
            this.cells = cells;
        }
        
        public List<KeywordCell> getCells() {
            return this.cells;
        }
        
        public boolean isGuessed() {
            final Iterator<KeywordCell> iterator = this.cells.iterator();
            while (iterator.hasNext()) {
                if (!iterator.next().isPersist()) {
                    return false;
                }
            }
            Log.v("SkanwordsFunc", "isGuessed true");
            return true;
        }
        
        public static class KeywordCell implements Serializable
        {
            private static final long serialVersionUID = -3246729032383784033L;
            private String character;
            private final int number;
            private boolean persist;
            private final int x;
            private final int y;
            
            public KeywordCell(final int x, final int y, final int number, final String character, final boolean persist) {
                super();
                this.character = " ";
                this.persist = false;
                this.x = x;
                this.y = y;
                this.number = number;
                this.character = character;
                this.persist = persist;
            }
            
            public String getCharacter() {
                return this.character;
            }
            
            public int getNumber() {
                return this.number;
            }
            
            public int getX() {
                return this.x;
            }
            
            public int getY() {
                return this.y;
            }
            
            public boolean isPersist() {
                final String character = this.character;
                return character != null && !character.equals(" ") && this.persist;
            }
            
            public void setCharacter(final String character, final boolean persist) {
                this.character = character;
                this.persist = persist;
            }
        }
    }
    
    public static class Question implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private final Answer answer;
        @SerializedName("arrow")
        private final ArrowData arrowData;
        private boolean hasKeywordCell;
        private final int id;
        private boolean isComplete;
        @SerializedName("question")
        private final QuestionData questionData;
        private int unguessedCellsCount;
        
        public Question(final int id, final QuestionData questionData, final ArrowData arrowData, final Answer answer) {
            super();
            this.id = id;
            this.questionData = questionData;
            this.arrowData = arrowData;
            this.answer = answer;
            this.setComplete(false);
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b;
            if (b = (o != null && o.getClass() == this.getClass())) {
                if (((Question)o).id == this.id) {
                    return true;
                }
                b = false;
            }
            return b;
        }
        
        public Answer getAnswer() {
            return this.answer;
        }
        
        public ArrowData getArrowData() {
            return this.arrowData;
        }
        
        public int getGuessedCellsCount() {
            return this.getAnswer().length - this.getUnguessedCellsCount();
        }
        
        public int getId() {
            return this.id;
        }
        
        public QuestionData getQuestionData() {
            return this.questionData;
        }
        
        public int getUnguessedCellsCount() {
            return this.unguessedCellsCount;
        }
        
        public boolean hasKeywordCell() {
            return this.hasKeywordCell;
        }
        
        @Override
        public int hashCode() {
            return this.id;
        }
        
        public boolean isComplete() {
            return this.isComplete;
        }
        
        public void setComplete(final boolean isComplete) {
            this.isComplete = isComplete;
        }
        
        public void setHasKeywordCell(final boolean hasKeywordCell) {
            this.hasKeywordCell = hasKeywordCell;
        }
        
        public void setUnguessedCellsCount(final int unguessedCellsCount) {
            this.unguessedCellsCount = unguessedCellsCount;
        }
    }
    
    public static class QuestionData implements Serializable
    {
        private static final String VERTICAL_ORIENTATION = "vertical";
        private static final long serialVersionUID = 1L;
        private final String orientation;
        @SerializedName("value")
        private final QuestionValue questionValue;
        private final int x;
        private final int y;
        
        public QuestionData(final int x, final int y, final String orientation, final QuestionValue questionValue) {
            super();
            this.x = x;
            this.y = y;
            this.orientation = orientation;
            this.questionValue = questionValue;
        }
        
        public QuestionValue getQuestionValue() {
            return this.questionValue;
        }
        
        public int getX() {
            return this.x;
        }
        
        public int getY() {
            return this.y;
        }
        
        public boolean isVerticalOrientation() {
            return "vertical".equals(this.orientation);
        }
    }
    
    public static class QuestionValue implements Serializable
    {
        private static String STRING_TEXT_TYPE = "text";
        private static final long serialVersionUID = 1L;
        private final String formatted;
        private final int height;
        private final String original;
        @SerializedName("type")
        private final String questionType;
        private final String url;
        private final int width;
        
        public QuestionValue(final String questionType, final String formatted, final String original, final String url, final int width, final int height) {
            super();
            this.questionType = questionType;
            this.formatted = formatted;
            this.original = original;
            this.url = url;
            this.width = width;
            this.height = height;
        }
        
        public String getFormatted() {
            return this.formatted;
        }
        
        public int getHeight() {
            int height;
            if ((height = this.height) <= 0) {
                height = 1;
            }
            return height;
        }
        
        public String getOriginal() {
            return this.original;
        }
        
        public String getQuestionType() {
            return this.questionType;
        }
        
        public String getUrl() {
            return this.url;
        }
        
        public int getWidth() {
            int width;
            if ((width = this.width) <= 0) {
                width = 1;
            }
            return width;
        }
        
        public boolean isTextQuestion() {
            return QuestionValue.STRING_TEXT_TYPE.equals(this.questionType);
        }
    }
    
    public static class Replace implements Serializable
    {
        private static final long serialVersionUID = 2864107489648774608L;
        @SerializedName("cell_x")
        private final int cellX;
        @SerializedName("cell_y")
        private final int cellY;
        @SerializedName("letter_source")
        private final String letterSource;
        @SerializedName("letter_target")
        private final String letterTarget;
        
        public Replace(final int cellX, final int cellY, final String letterSource, final String letterTarget) {
            super();
            this.cellX = cellX;
            this.cellY = cellY;
            this.letterSource = letterSource;
            this.letterTarget = letterTarget;
        }
        
        public int getCellX() {
            return this.cellX;
        }
        
        public int getCellY() {
            return this.cellY;
        }
        
        public String getLetterSource() {
            return this.letterSource;
        }
        
        public String getLetterTarget() {
            return this.letterTarget;
        }
    }
    
    public static class Settings implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private final int height;
        private final int width;
        
        public Settings(final int width, final int height) {
            super();
            this.width = width;
            this.height = height;
        }
        
        public int getHeight() {
            return this.height;
        }
        
        public int getWidth() {
            return this.width;
        }
    }
}
