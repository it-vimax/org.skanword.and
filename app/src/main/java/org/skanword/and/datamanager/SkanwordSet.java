package org.skanword.and.datamanager;

import com.google.gson.annotations.*;
import java.util.*;
import org.skanword.and.network.*;
import java.io.*;

public class SkanwordSet implements Serializable
{
    private static final long serialVersionUID = 1L;
    @SerializedName("media_url")
    private final String mediaUrl;
    @SerializedName("list")
    private final List<Skanword> scanwords;
    @SerializedName("set_id")
    private final Integer setId;
    
    public SkanwordSet(final Integer setId, final String mediaUrl, final List<Skanword> scanwords) {
        super();
        this.setId = setId;
        this.mediaUrl = mediaUrl;
        this.scanwords = scanwords;
    }
    
    public Integer getId() {
        return this.setId;
    }
    
    public String getMediaUrl() {
        return this.mediaUrl;
    }
    
    public List<Skanword> getSkanwords() {
        return this.scanwords;
    }
    
    public boolean hasUnzipedFiles() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.setId);
        sb.append(".zip");
        final String[] list = new File(DownloaderArhiveFile.getUnzipPath(sb.toString())).list();
        return list != null && list.length >= 1;
    }
}
