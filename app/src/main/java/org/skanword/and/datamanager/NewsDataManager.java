package org.skanword.and.datamanager;

import org.skanword.and.*;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;

import android.util.*;
import java.io.*;
import java.util.*;

public class NewsDataManager
{
    private List<News> mNews;
    private String mNewsHash;
    private Set<Integer> mReadNews;
    
    public NewsDataManager() {
        super();
        this.mNews = null;
        this.mReadNews = null;
        this.mNewsHash = "";
    }
    
    public List<News> getNews() {
        return this.mNews;
    }
    
    public String getNewsHash() {
        return this.mNewsHash;
    }
    
    public int getNotReadNewsCount() {
        final List<News> mNews = this.mNews;
        int n = 0;
        int n2 = 0;
        if (mNews != null) {
            final Iterator<News> iterator = mNews.iterator();
            while (true) {
                n = n2;
                if (!iterator.hasNext()) {
                    break;
                }
                if (this.mReadNews.contains(iterator.next().getId())) {
                    continue;
                }
                ++n2;
            }
        }
        return n;
    }
    
    public void retrieveData() {
        this.mNewsHash = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(15);
        if (this.mNewsHash == null) {
            this.mNewsHash = "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("mNewsHash retrieve ");
        sb.append(this.mNewsHash);
        Log.v("", sb.toString());
        List<NewsDataManager.News> resUpdateNews = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(4);
        this.updateNews(resUpdateNews, false);
        final Set<Integer> set = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(14);
        if (set == null) {
            this.mReadNews = Collections.synchronizedSet(new HashSet<Integer>());
            return;
        }
        this.mReadNews = Collections.synchronizedSet(set);
    }
    
    public void setNewsHash(final String mNewsHash) {
        this.mNewsHash = mNewsHash;
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mNewsHash, 15);
        final StringBuilder sb = new StringBuilder();
        sb.append("mNewsHash update ");
        sb.append(this.mNewsHash);
        Log.v("", sb.toString());
    }
    
    public void updateNews(final List<News> list, final boolean b) {
        if (list == null) {
            return;
        }
        this.mNews = Collections.synchronizedList(list);
        if (b) {
            SmappsScanwords.getDatabaseHelper().storeGeneralData(new ArrayList<Object>(list), 4);
        }
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_RATING_UPDATE"));
    }
    
    public void updateReadedNews() {
        final List<News> mNews = this.mNews;
        if (mNews != null) {
            if (this.mReadNews == null) {
                return;
            }
            final Iterator<News> iterator = mNews.iterator();
            while (iterator.hasNext()) {
                this.mReadNews.add(iterator.next().getId());
            }
            SmappsScanwords.getDatabaseHelper().storeGeneralData(new HashSet<Object>(this.mReadNews), 14);
        }
    }
    
    public static class News implements Serializable
    {
        private static final long serialVersionUID = 4256668074486644081L;
        private final String action;
        private final String date;
        private final int id;
        private final String image;
        private final String link;
        private String newsHash;
        private final String text;
        private final String title;
        
        public News(final int id, final String title, final String text, final String image, final String date, final String action, final String link, final String newsHash) {
            super();
            this.id = id;
            this.title = title;
            this.text = text;
            this.image = image;
            this.date = date;
            this.action = action;
            this.link = link;
            this.newsHash = newsHash;
        }
        
        public String getAction() {
            return this.action;
        }
        
        public String getDate() {
            return this.date;
        }
        
        public int getId() {
            return this.id;
        }
        
        public String getImage() {
            return this.image;
        }
        
        public String getLink() {
            return this.link;
        }
        
        public String getNewsHash() {
            return this.newsHash;
        }
        
        public String getText() {
            return this.text;
        }
        
        public String getTitle() {
            return this.title;
        }
        
        public void setNewsHash(final String newsHash) {
            this.newsHash = newsHash;
        }
    }
}
