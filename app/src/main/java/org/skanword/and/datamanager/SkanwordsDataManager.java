package org.skanword.and.datamanager;

import org.skanword.and.*;
import org.skanword.and.etc.*;

import android.app.Activity;
import android.content.*;
import android.util.*;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventDispatcher;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;
import org.skanword.and.network.*;
import org.skanword.and.menu.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.annotation.*;

public class SkanwordsDataManager
{
    public static final String UPDATE_ISSUES_LIST = "UPDATE_ISSUES_LIST";
    private List<Integer> mFreeSkanwords;
    private Skanword mLastPlayedSkanword;
    private SetsListObject mLastReceivedObject;
    private Integer mLastRecievedListMutex;
    private List<Integer> mOfferSkanwords;
    private Integer mSetInfosMutex;
    private List<SkanwordsSetInfo> mSetsInfos;
    private EventListener mTimerEventListener;
    private SkanwordsSetInfo mTomorrowSet;
    private boolean mTomorrowSetTimeUp;
    
    public SkanwordsDataManager() {
        super();
        this.mTomorrowSet = null;
        this.mTomorrowSetTimeUp = false;
        final Integer value = 1;
        this.mSetInfosMutex = value;
        this.mLastRecievedListMutex = value;
        this.mSetsInfos = Collections.synchronizedList(new ArrayList<SkanwordsSetInfo>());
        this.mTimerEventListener = new SkanwordsDataManager1(this);
        SmappsScanwords.getEventsDispatcher().addListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
    }
    
    static /* synthetic */ void access000(final SkanwordsDataManager skanwordsDataManager) {
        skanwordsDataManager.getSkanwordsSetsFromDB();
    }
    
    public static String dirrectionStringRepresentation(final TaskListDirrectionType taskListDirrectionType) {
        final int n = SkanwordsDataManager7.taskDirectoryType[taskListDirrectionType.ordinal()];
        if (n == 1) {
            return "top";
        }
        if (n == 2) {
            return "current";
        }
        if (n == 3) {
            return "previous";
        }
        if (n != 4) {
            return "top";
        }
        return "next";
    }
    
    private SetsListObject getLastReceivedListObject() {
        synchronized (this.mLastRecievedListMutex) {
            return this.mLastReceivedObject;
        }
    }
    
    private SkanwordsSetInfo getSkanwordSetInfoWithId(final Integer n) {
        synchronized (this.mSetInfosMutex) {
            for (final SkanwordsSetInfo skanwordsSetInfo : this.mSetsInfos) {
                if (skanwordsSetInfo.getId().equals(n)) {
                    return skanwordsSetInfo;
                }
            }
            return null;
        }
    }
    
    public static String getSkanwordSyncHash(final List<Skanword> list) {
        if (list.size() < 1) {
            return "";
        }
        final ArrayList<Skanword> list2 = new ArrayList<Skanword>();
        final ArrayList<Skanword> list3 = new ArrayList<Skanword>();
        for (final Skanword skanword : list) {
            if (skanword.isFinished()) {
                list2.add(skanword);
            }
            else {
                if (!skanword.isStarted()) {
                    continue;
                }
                list3.add(skanword);
            }
        }
        if (list2.size() + list3.size() < 1) {
            return "";
        }
        Comparator<Skanword> sortList1 = new SkanwordsDataManager.SkanwordsDataManager5();
        Comparator<Skanword> sortList2 = new SkanwordsDataManager.SkanwordsDataManager6();
        Collections.sort(list2, sortList1);
        Collections.sort(list3, sortList2);

        final StringBuilder sb = new StringBuilder();
        for (final Skanword skanword2 : list2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(skanword2.getId());
            sb2.append("_");
            sb.append(sb2.toString());
        }
        for (final Skanword skanword3 : list3) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("c");
            sb3.append(skanword3.getId());
            sb3.append("_");
            sb3.append(skanword3.getAnswers());
            sb3.append("_");
            sb3.append(skanword3.getHints());
            sb3.append("_");
            sb.append(sb3.toString());
        }
        if (sb.length() > 0) {
            sb.append("c4303aa5d29797f08459d60fa9238da7");
        }
        else {
            sb.append("_c4303aa5d29797f08459d60fa9238da7");
        }
        return Utils.md5(sb.toString());
    }
    
    private void getSkanwordsSetsFromDB() {
        this.getSkanwordsSetsFromDB(null);
    }

    private void getSkanwordsSetsFromDB(final Context context) {
        Log.v("SkanwordsFunc", "getSkanwordsSetsFromDB");
        synchronized (this.mSetInfosMutex) {
            Collections.sort(this.mSetsInfos = Collections.synchronizedList((List<SkanwordsSetInfo>)SmappsScanwords.getDatabaseHelper(context).getSkanwordsSetsInfo()), (Comparator<? super SkanwordsSetInfo>)new SkanwordsDataManager.SkanwordsDataManager2(this));
            // monitorexit(this.mSetInfosMutex)
            this.updateFreeAndOfferSkanwords();
            this.mLastPlayedSkanword = SmappsScanwords.getDatabaseHelper().getLastPlayedSkanword();
        }
    }
    
    public static String listTypeStringRepresentation(final TaskListType taskListType) {
        final int n = SkanwordsDataManager7.taskListType[taskListType.ordinal()];
        if (n == 1) {
            return "new";
        }
        if (n == 2) {
            return "started";
        }
        if (n != 3) {
            return "top";
        }
        return "finished";
    }
    
    public static String md5Answer(final String s, final Integer n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(MainNetworkManager.getInstance().getNetId());
        sb.append('_');
        sb.append(n);
        sb.append('_');
        sb.append(s.toLowerCase());
        sb.append('_');
        sb.append("ce9d660ffb810b5e59b444b84a712ca1");
        return Utils.md5(sb.toString());
    }
    
    private void placeProgressToSkanwords(final SkanwordsSetDataObject skanwordsSetDataObject, final List<Skanword> list) {
        for (final Skanword skanword : list) {
            for (final List<String> list2 : skanwordsSetDataObject.getSaveSkanwords()) {
                if (skanword.getId() == Integer.valueOf(list2.get(1))) {
                    skanword.setAnswers(list2.get(2));
                    skanword.setHints(list2.get(3));
                    break;
                }
            }
            for (final List<String> list3 : skanwordsSetDataObject.getSolvedSkanwords()) {
                if (skanword.getId() == Integer.valueOf(list3.get(1))) {
                    skanword.setAnswers(list3.get(2));
                    skanword.setHints(list3.get(3));
                    break;
                }
            }
        }
    }
    
    private void replaceSkanwordSetInfo(final SkanwordsSetInfo skanwordsSetInfo) {
        synchronized (this.mSetInfosMutex) {
            final SkanwordsSetInfo skanwordSetInfoWithId = this.getSkanwordSetInfoWithId(skanwordsSetInfo.getId());
            if (skanwordSetInfoWithId != null) {
                this.mSetsInfos.set(this.mSetsInfos.indexOf(skanwordSetInfoWithId), skanwordsSetInfo);
                SmappsScanwords.getDatabaseHelper().addSkanwordsSetInfo(skanwordsSetInfo);
            }
        }
    }
    
    private void updateFreeAndOfferSkanwords() {
        this.mFreeSkanwords = new ArrayList<Integer>();
        this.mOfferSkanwords = new ArrayList<Integer>();
        synchronized (this.mSetInfosMutex) {
            for (final SkanwordsSetInfo skanwordsSetInfo : this.mSetsInfos) {
                if (skanwordsSetInfo.getFreeIds().size() > 0) {
                    this.mFreeSkanwords.addAll(skanwordsSetInfo.getFreeIds());
                }
                if (skanwordsSetInfo.getOfferIds().size() > 0) {
                    this.mOfferSkanwords.addAll(skanwordsSetInfo.getOfferIds());
                }
            }
        }
    }
    
    protected void appTimerTicked() {
        if (this.timeTillTomorrowIssue() < 0L && !this.mTomorrowSetTimeUp) {
            SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("UPDATE_ISSUES_LIST"));
            this.mTomorrowSetTimeUp = true;
        }
    }
    
    public void deleteIssuesWithId(final int n) {
        this.deleteIssuesWithId(n, true);
    }
    
    public void deleteIssuesWithId(final int n, final boolean b) {
        if (n < 1) {
            return;
        }
        SmappsScanwords.deleteRecursive(new File(DownloaderArhiveFile.getUnzipPath(this.getSkanwordSetInfoWithId(n).getMediaUrl())), false);
        SmappsScanwords.getDatabaseHelper().removeSkanwordSetInfoAndDataWithId(n);
        this.getSkanwordsSetsFromDB();
    }
    
    public Skanword getLastPlayedSkanword() {
        return this.mLastPlayedSkanword;
    }
    
    public int getLatesIssueIdOfType(final TaskListType taskListType) {
        if (this.getLastReceivedListObject() != null && this.getLastReceivedListObject().getListType() == taskListType) {
            return this.getLastReceivedListObject().getLatestSetId();
        }
        return 0;
    }
    
    public int getMaxSkanwordsOnPageCount() {
        return 100;
    }
    
    public int getOldestIssueIdOfType(final TaskListType taskListType) {
        if (this.getLastReceivedListObject() != null && this.getLastReceivedListObject().getListType() == taskListType) {
            return this.getLastReceivedListObject().getOldestSetId();
        }
        return 0;
    }
    
    public String getProgressOfSetInfo(final SkanwordsSetInfo skanwordsSetInfo, final TaskListType taskListType) {
        final int n = SkanwordsDataManager7.taskListType[taskListType.ordinal()];
        String s;
        int n2;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    s = "";
                    n2 = 0;
                }
                else {
                    n2 = skanwordsSetInfo.getSkanwordsFinishedCount();
                    s = "Решено ";
                }
            }
            else {
                n2 = skanwordsSetInfo.getSkanwordsStartedCount();
                s = "Начато ";
            }
        }
        else {
            n2 = skanwordsSetInfo.getSkanwordsCount() - skanwordsSetInfo.getSkanwordsFinishedCount() - skanwordsSetInfo.getSkanwordsStartedCount();
            s = "Новых ";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(n2);
        sb.append(" из ");
        sb.append(skanwordsSetInfo.getSkanwordsCount());
        return sb.toString();
    }
    
    public List<SkanwordsSetInfo> getSetsInfos() {
        synchronized (this.mSetInfosMutex) {
            return this.mSetsInfos;
        }
    }
    
    public List<Skanword> getSkanwordsForListType(final TaskListType taskListType) {
        if (taskListType == TaskListType.STARTED_TASKS) {
            final DataBaseManager databaseHelper = SmappsScanwords.getDatabaseHelper();
            final StringBuilder sb = new StringBuilder();
            sb.append("started=1 AND finished=0 AND questions!=0 ORDER BY _id DESC  LIMIT ");
            sb.append(this.getMaxSkanwordsOnPageCount());
            return databaseHelper.getSkanwordsWithSelection(sb.toString());
        }
        if (taskListType == TaskListType.FINISHED_TASKS) {
            final DataBaseManager databaseHelper2 = SmappsScanwords.getDatabaseHelper();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("finished=1 AND questions!=0 ORDER BY _id DESC  LIMIT ");
            sb2.append(this.getMaxSkanwordsOnPageCount());
            return databaseHelper2.getSkanwordsWithSelection(sb2.toString());
        }
        return SmappsScanwords.getDatabaseHelper().getSkanwordsWithSelection(null);
    }
    
    public List<Skanword> getSkanwordsForListType(final TaskListType taskListType, final Integer n) {
        final DataBaseManager databaseHelper = SmappsScanwords.getDatabaseHelper();
        final StringBuilder sb = new StringBuilder();
        sb.append("set_id=");
        sb.append(n);
        return databaseHelper.getSkanwordsWithSelection(sb.toString());
    }
    
    public List<SkanwordsSetInfo> getSortedSetsInfosList(final TaskListType taskListType) {
        return this.getSortedSetsInfosList(taskListType, VisibilityListType.ALL_ISSUES);
    }
    
    public List<SkanwordsSetInfo> getSortedSetsInfosList(final TaskListType taskListType, final VisibilityListType visibilityListType) {
        synchronized (this.mSetInfosMutex) {
            final ArrayList<SkanwordsSetInfo> list = new ArrayList<SkanwordsSetInfo>();
            ArrayList<SkanwordsSetInfo> list2;
            if (visibilityListType != VisibilityListType.DOWNLOADED_ISSUES && MainNetworkManager.getInstance().hasNetworkConnection()) {
                list2 = list;
                if (this.getLastReceivedListObject() != null) {
                    list2 = list;
                    if (taskListType == this.getLastReceivedListObject().getListType()) {
                        list2 = new ArrayList<SkanwordsSetInfo>(this.getLastReceivedListObject().getSetsList());
                    }
                }
            }
            else {
                list2 = new ArrayList<SkanwordsSetInfo>(this.mSetsInfos);
            }
            if (this.mTomorrowSet != null && list2.size() > 0) {
                list2.add(0, this.mTomorrowSet);
            }
            final ArrayList<SkanwordsSetInfo> list3 = new ArrayList<SkanwordsSetInfo>();
            for (final SkanwordsSetInfo skanwordsSetInfo : list2) {
                if (taskListType == TaskListType.FINISHED_TASKS && skanwordsSetInfo.isFinished()) {
                    list3.add(skanwordsSetInfo);
                }
                else if (taskListType == TaskListType.STARTED_TASKS && skanwordsSetInfo.isStarted()) {
                    list3.add(skanwordsSetInfo);
                }
                else {
                    if (taskListType != TaskListType.ISSUE_TASKS || !skanwordsSetInfo.hasNew()) {
                        continue;
                    }
                    list3.add(skanwordsSetInfo);
                }
            }
            return list3;
        }
    }
    
    public SkanwordsSetInfo getTomorrowSet() {
        return this.mTomorrowSet;
    }
    
    public int getTomorrowSetId() {
        if (this.getLastReceivedListObject() != null && this.getLastReceivedListObject().getTomorrowSet() != null) {
            return this.getLastReceivedListObject().getTomorrowSet().getId();
        }
        return 0;
    }
    
    public void importProgress(final SkanwordsSetDataObject skanwordsSetDataObject) {
        final ArrayList<Integer> list = new ArrayList<Integer>();
        for (final Map<String, Integer> map : skanwordsSetDataObject.getRequestdSetIds()) {
            if ((0x1 & map.get("return")) != 0x0) {
                list.add(map.get("set_id"));
            }
        }
        if (list.size() < 1) {
            return;
        }
        skanwordsSetDataObject.getSaveSkanwords().size();
        skanwordsSetDataObject.getSolvedSkanwords().size();
        SmappsScanwords.getDatabaseHelper().clearProgress(list);
        SmappsScanwords.getDatabaseHelper().insertSkanwordProgress(skanwordsSetDataObject.getSaveSkanwords(), false);
        SmappsScanwords.getDatabaseHelper().insertSkanwordProgress(skanwordsSetDataObject.getSolvedSkanwords(), true);
        SmappsScanwords.getDatabaseHelper().updateSetInfosProgress(list);
    }
    
    public boolean isPageAvailable(final boolean b) {
        if (this.getLastReceivedListObject() == null || !MainNetworkManager.getInstance().hasNetworkConnection()) {
            return false;
        }
        if (b) {
            return this.getLastReceivedListObject().isPageNextAvailable();
        }
        return this.getLastReceivedListObject().isPagePreviousAvailable();
    }
    
    public boolean isSkanwordFree(final Integer n) {
        final List<Integer> mFreeSkanwords = this.mFreeSkanwords;
        return mFreeSkanwords != null && mFreeSkanwords.contains(n);
    }
    
    public boolean isSkanwordOffered(final Integer n) {
        final List<Integer> mOfferSkanwords = this.mOfferSkanwords;
        return mOfferSkanwords != null && mOfferSkanwords.contains(n);
    }
    
    public void notifyLocalDataUsegae(final TaskListType taskListType, final TaskListDirrectionType taskListDirrectionType) {
        final StringBuilder sb = new StringBuilder();
        sb.append("notifyLocalDataUsegae ");
        sb.append(taskListType);
        Log.v("", sb.toString());
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SkanwordsSetsUpdateEvent("EVENT_SCANWORDS_SETS_INFOS_UPDATE", taskListType, taskListDirrectionType != TaskListDirrectionType.CURRECT_DIRRECTION));
    }
    
    public void openScanwordForOffer(final Integer n) {
        synchronized (this.mSetInfosMutex) {
            for (final SkanwordsSetInfo skanwordsSetInfo : this.mSetsInfos) {
                if (skanwordsSetInfo.getOfferIds().contains(n)) {
                    skanwordsSetInfo.getFreeIds().add(n);
                    this.updateFreeAndOfferSkanwords();
                    SmappsScanwords.getDatabaseHelper().updateSetInfoDynamicData(skanwordsSetInfo);
                    break;
                }
            }
            // monitorexit(this.mSetInfosMutex)
            SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_SCANWORDS_TASKS_UPDATE"));
        }
    }
    
    public boolean readyToRateTheApp() {
        while (true) {
            while (true) {
                int n = 0;
                Label_0085: {
                    synchronized (this.mSetInfosMutex) {
                        final Iterator<SkanwordsSetInfo> iterator = this.mSetsInfos.iterator();
                        n = 0;
                        while (iterator.hasNext()) {
                            final SkanwordsSetInfo skanwordsSetInfo = iterator.next();
                            if (skanwordsSetInfo.isFinished()) {
                                break Label_0085;
                            }
                            final int n2 = n;
                            if (skanwordsSetInfo.isStarted()) {
                                break Label_0085;
                            }
                            if ((n = n2) >= 2) {
                                return true;
                            }
                        }
                        return false;
                    }
                }
                final int n2 = n + 1;
                continue;
            }
        }
    }
    
    public void retrieveData(final Context context) {
        this.getSkanwordsSetsFromDB(context);
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SkanwordsSetsUpdateEvent("EVENT_SCANWORDS_SETS_INFOS_UPDATE", TaskListType.ALL_TASKS));
    }

    public boolean saveSkanwordState(final Skanword skanword, final boolean b) {
        if (skanword != null) {
            if (skanword.isChanged()) {
                skanword.setChanged(false);
                SmappsScanwords.getExecutorService().execute((Runnable)new SkanwordsDataManager.SkanwordsDataManager4(this, skanword, b));
            }
            else if (b) {
                SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SkanwordsDataManager.SkanwordsSetsUpdateEvent("EVENT_SCANWORDS_SETS_INFOS_UPDATE"));
                SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_SCANWORDS_TASKS_UPDATE"));
            }
        }
        return true;
    }
    
    public String setMediaFileUrlForSetInfo(final String s, final Integer n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(n);
        sb.append(".zip");
        return sb.toString();
    }

    public void setsListReceived(SetsListObject setsListObject) {
        synchronized (this) {
            if (setsListObject == null) {
                return;
            }
            Integer n = this.mLastRecievedListMutex;
            synchronized (n) {
                this.mLastReceivedObject = setsListObject;
                Object o = new StringBuilder();
                ((StringBuilder)o).append("setsListReceived ");
                ((StringBuilder)o).append(mLastReceivedObject.getListType());
                Log.v("SkanwordsFunc", ((StringBuilder)o).toString());
                this.updateSkanwordsList(mLastReceivedObject.getSetsList());
                o = this.getLastReceivedListObject().getTomorrowSet();
                final boolean b = true;
                this.updateTomorrowSetInfo((SkanwordsSetInfo)o, true);
                o = SmappsScanwords.getEventsDispatcher();
                final TaskListType listType = mLastReceivedObject.getListType();
                if (mLastReceivedObject.getDirrectionType() != TaskListDirrectionType.CURRECT_DIRRECTION) {
                    ((EventDispatcher)o).dispatchEvent((Event)new SkanwordsSetsUpdateEvent("EVENT_SCANWORDS_SETS_INFOS_UPDATE", listType, b));
                    return;
                }
            }
        }
    }
    
    public void storeSkanwordsSet(final SkanwordsSetDataObject skanwordsSetDataObject) {
        if (skanwordsSetDataObject == null) {
            return;
        }
        this.importProgress(skanwordsSetDataObject);
        this.getSkanwordsSetsFromDB();
    }
    
    public void storeSkanwordsSet(final SkanwordsSetDataObject skanwordsSetDataObject, final SkanwordsSetInfo skanwordsSetInfo) {
        final SkanwordSet fristScanwordSet = skanwordsSetDataObject.getFristScanwordSet();
        SmappsScanwords.getDatabaseHelper().addSkanwordsSetInfo(skanwordsSetInfo);
        this.placeProgressToSkanwords(skanwordsSetDataObject, fristScanwordSet.getSkanwords());
        for (final Skanword skanword : fristScanwordSet.getSkanwords()) {
            skanword.setImageDir(DownloaderArhiveFile.getUnzipPath(skanwordsSetInfo.getMediaUrl()));
            skanword.setSetId(fristScanwordSet.getId());
            skanword.clearKeyword();
            skanword.checkKeyword();
        }
        SmappsScanwords.getDatabaseHelper().importSkanwordData(fristScanwordSet.getSkanwords());
        this.importProgress(skanwordsSetDataObject);
        this.getSkanwordsSetsFromDB();
    }
    
    public long timeTillTomorrowIssue() {
        if (this.getTomorrowSet() == null) {
            return 0L;
        }
        return this.getTomorrowSet().getDownloadTimeLeft() - (new Date().getTime() - this.getTomorrowSet().getDownloadedTime()) / 1000L;
    }

    @SuppressLint(value={"UseSparseArrays"})
    public void updateSkanwordsList(List var1) {
        Log.v("SkanwordsFunc", "updateSkanwordsList");
        if (var1 != null) {
            Integer var4 = this.mSetInfosMutex;
            synchronized(var4){}

            Throwable var10000;
            boolean var10001;
            label2898: {
                HashMap var5;
                SkanwordsSetInfo var7;
                label2891: {
                    Iterator var6;
                    try {
                        var5 = new HashMap();
                        if (this.mSetsInfos == null || this.mSetsInfos.size() <= 0) {
                            break label2891;
                        }

                        var6 = this.mSetsInfos.iterator();
                    } catch (Throwable var315) {
                        var10000 = var315;
                        var10001 = false;
                        break label2898;
                    }

                    while(true) {
                        try {
                            if (!var6.hasNext()) {
                                break;
                            }

                            var7 = (SkanwordsSetInfo)var6.next();
                            var5.put(var7.getId(), var7.getDataHash());
                        } catch (Throwable var314) {
                            var10000 = var314;
                            var10001 = false;
                            break label2898;
                        }
                    }
                }

                Iterator var316;
                ArrayList var318;
                try {
                    var318 = new ArrayList();
                    var316 = var1.iterator();
                } catch (Throwable var307) {
                    var10000 = var307;
                    var10001 = false;
                    break label2898;
                }

                while(true) {
                    String var8;
                    try {
                        if (!var316.hasNext()) {
                            break;
                        }

                        var7 = (SkanwordsSetInfo)var316.next();
                        var8 = (String)var5.remove(var7.getId());
                    } catch (Throwable var308) {
                        var10000 = var308;
                        var10001 = false;
                        break label2898;
                    }

                    boolean var2;
                    boolean var3;
                    label2873: {
                        label2872: {
                            var3 = false;
                            if (var8 != null) {
                                try {
                                    if (var8.equals(var7.getDataHash())) {
                                        break label2872;
                                    }
                                } catch (Throwable var313) {
                                    var10000 = var313;
                                    var10001 = false;
                                    break label2898;
                                }
                            }

                            var2 = true;
                            break label2873;
                        }

                        var2 = false;
                    }

                    if (var2) {
                        if (var8 != null) {
                            try {
                                this.deleteIssuesWithId(var7.getId(), false);
                            } catch (Throwable var306) {
                                var10000 = var306;
                                var10001 = false;
                                break label2898;
                            }
                        }
                    } else {
                        SkanwordsSetInfo var319;
                        try {
                            var319 = this.getSkanwordSetInfoWithId(var7.getId());
                            var319.setToday(var7.isToday());
                        } catch (Throwable var305) {
                            var10000 = var305;
                            var10001 = false;
                            break label2898;
                        }

                        var2 = var3;

                        label2862: {
                            try {
                                if (var319.getDisplayName().equals(var7.getDisplayName())) {
                                    break label2862;
                                }

                                var319.setDisplayName(var7.getDisplayName());
                            } catch (Throwable var312) {
                                var10000 = var312;
                                var10001 = false;
                                break label2898;
                            }

                            var2 = true;
                        }

                        label2856: {
                            try {
                                if (Utils.compareLists(var319.getFreeIds(), var7.getFreeIds())) {
                                    break label2856;
                                }

                                var319.setFreeIds(var7.getFreeIds());
                            } catch (Throwable var311) {
                                var10000 = var311;
                                var10001 = false;
                                break label2898;
                            }

                            var2 = true;
                        }

                        label2850: {
                            try {
                                if (Utils.compareLists(var319.getOfferIds(), var7.getOfferIds())) {
                                    break label2850;
                                }

                                var319.setOfferIds(var7.getOfferIds());
                            } catch (Throwable var310) {
                                var10000 = var310;
                                var10001 = false;
                                break label2898;
                            }

                            var2 = true;
                        }

                        StringBuilder var9;
                        label2844: {
                            try {
                                var9 = new StringBuilder();
                                var9.append(var7.getId());
                                var9.append("  started count  ");
                                var9.append(var7.getSkanwordsStartedCount());
                                var9.append("  finished count - ");
                                var9.append(var7.getSkanwordsFinishedCount());
                                Log.v("", var9.toString());
                                if (var319.getSkanwordsStartedCount() == var7.getSkanwordsStartedCount() && var319.getSkanwordsFinishedCount() == var7.getSkanwordsFinishedCount()) {
                                    break label2844;
                                }
                            } catch (Throwable var309) {
                                var10000 = var309;
                                var10001 = false;
                                break label2898;
                            }

                            try {
                                var319.setStartedCount(var7.getSkanwordsStartedCount());
                                var319.setFinishedCount(var7.getSkanwordsFinishedCount());
                            } catch (Throwable var304) {
                                var10000 = var304;
                                var10001 = false;
                                break label2898;
                            }
                        }

                        try {
                            if (!var319.getSaveHash().equals(var7.getSaveHash())) {
                                var9 = new StringBuilder();
                                var9.append(var319.getId());
                                var9.append(" - setId not same save hash  ");
                                var9.append(var319.getSaveHash());
                                var9.append("  new save hash - ");
                                var9.append(var7.getSaveHash());
                                Log.v("SkanwordsFunc", var9.toString());
                                HashMap var320 = new HashMap();
                                var320.put("set_id", var7.getId());
                                var320.put("return", 1);
                                var318.add(var320);
                            }
                        } catch (Throwable var303) {
                            var10000 = var303;
                            var10001 = false;
                            break label2898;
                        }

                        if (var2) {
                            try {
                                SmappsScanwords.getDatabaseHelper().updateSetInfoDynamicData(var319);
                            } catch (Throwable var302) {
                                var10000 = var302;
                                var10001 = false;
                                break label2898;
                            }
                        }
                    }
                }

                try {
                    this.updateFreeAndOfferSkanwords();
                    Collections.sort(this.mSetsInfos, new SkanwordsDataManager3(this));
                    if (var318.size() > 0) {
                        this.storeSkanwordsSet(MainNetworkManager.getInstance().skanwordSetGet(MainMenuActivity.instance, var318));
                    }
                } catch (Throwable var301) {
                    var10000 = var301;
                    var10001 = false;
                    break label2898;
                }

                label2817:
                try {
                    return;
                } catch (Throwable var300) {
                    var10000 = var300;
                    var10001 = false;
                    break label2817;
                }
            }

            while(true) {
                Throwable var317 = var10000;

                try {
                    throw var317;
                } catch (Throwable var299) {
                    var10000 = var299;
                    var10001 = false;
                    continue;
                }
            }
        }
    }
    
    public void updateTomorrowSetInfo(final SkanwordsSetInfo mTomorrowSet, final boolean b) {
        if (mTomorrowSet == null) {
            if (b) {
                this.mTomorrowSet = null;
            }
            return;
        }
        this.mTomorrowSetTimeUp = false;
        (this.mTomorrowSet = mTomorrowSet).setDownloadedTime(new Date().getTime());
    }

    public enum ScanwordsSetsInfosSortType {
        DEFAULT,
        STARTED,
        FINISHED

    }

    
    public static class SkanwordsSetsUpdateEvent extends SimpleEvent
    {
        private boolean mScrollToTop;
        private final TaskListType mTaskListType;
        
        public SkanwordsSetsUpdateEvent(final String s) {
            super(s);
            this.mScrollToTop = true;
            this.mTaskListType = TaskListType.ALL_TASKS;
        }
        
        public SkanwordsSetsUpdateEvent(final String s, final TaskListType mTaskListType) {
            super(s);
            this.mScrollToTop = true;
            this.mTaskListType = mTaskListType;
        }
        
        public SkanwordsSetsUpdateEvent(final String s, final TaskListType mTaskListType, final boolean mScrollToTop) {
            super(s);
            this.mScrollToTop = true;
            this.mTaskListType = mTaskListType;
            this.mScrollToTop = mScrollToTop;
        }
        
        public SkanwordsSetsUpdateEvent(final String s, final boolean mScrollToTop) {
            super(s);
            this.mScrollToTop = true;
            this.mTaskListType = TaskListType.ALL_TASKS;
            this.mScrollToTop = mScrollToTop;
        }
        
        public TaskListType getTaskListType() {
            return this.mTaskListType;
        }
        
        public boolean scrollToTop() {
            return this.mScrollToTop;
        }
    }
    
    public static class SolveState implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private String answers;
        private boolean changed;
        private boolean finished;
        private int finishedQuestions;
        private String hints;
        private final int setId;
        private Skanword skanword;
        private final Long skanwordId;
        private boolean started;
        private Long updateTime;
        
        public SolveState(final String answers, final String hints, final boolean finished, final boolean started, final Long updateTime, final int finishedQuestions, final Long skanwordId, final int setId) {
            super();
            this.answers = answers;
            this.hints = hints;
            this.finished = finished;
            this.started = started;
            this.updateTime = updateTime;
            this.finishedQuestions = finishedQuestions;
            this.skanwordId = skanwordId;
            this.setId = setId;
        }
        
        public boolean checkAnswersForScaword(final Skanword skanword) {
            this.finishedQuestions = 0;
        Label_0325_Outer:
            for (final Skanword.Question question : skanword.getQuestions()) {
                if (!question.isComplete()) {
                    final boolean verticalOrientation = question.getQuestionData().isVerticalOrientation();
                    new StringBuilder();
                    final StringBuilder sb = new StringBuilder();
                    int i = 0;
                    while (true) {
                        while (i < question.getAnswer().getLength()) {
                            final int x = question.getAnswer().getX();
                            int n;
                            if (verticalOrientation) {
                                n = 0;
                            }
                            else {
                                n = i;
                            }
                            final int n2 = x + n;
                            final int y = question.getAnswer().getY();
                            int n3;
                            if (verticalOrientation) {
                                n3 = i;
                            }
                            else {
                                n3 = 0;
                            }
                            final int n4 = y + n3;
                            final int n5 = skanword.getSettings().getHeight() * n2 + n4;
                            String s = this.answers.substring(n5, n5 + 1).toLowerCase();
                            if (s.equals(" ")) {
                                final boolean b = true;
                                if (b) {
                                    continue Label_0325_Outer;
                                }
                                final String string = sb.toString();
                                final StringBuilder sb2 = new StringBuilder();
                                final String hash = question.getAnswer().getHash();
                                sb2.append(MainNetworkManager.getInstance().getNetId());
                                sb2.append('_');
                                sb2.append(skanword.getId());
                                sb2.append('_');
                                sb2.append(string.toLowerCase());
                                sb2.append('_');
                                sb2.append("ce9d660ffb810b5e59b444b84a712ca1");
                                if (hash.equals(Utils.md5(sb2.toString()))) {
                                    question.setComplete(true);
                                    this.increaseFinishedQuestions();
                                    continue Label_0325_Outer;
                                }
                                continue Label_0325_Outer;
                            }
                            else {
                                String s2 = s;
                                if (skanword.getReplaces() != null) {
                                    s2 = s;
                                    if (skanword.getReplaces().size() > 0) {
                                        final Iterator<Skanword.Replace> iterator2 = skanword.getReplaces().iterator();
                                        while (true) {
                                            s2 = s;
                                            if (!iterator2.hasNext()) {
                                                break;
                                            }
                                            final Skanword.Replace replace = iterator2.next();
                                            if (n2 != replace.getCellX() || n4 != replace.getCellY() || !s.toLowerCase().equals(replace.getLetterSource().toLowerCase())) {
                                                continue Label_0325_Outer;
                                            }
                                            s = replace.getLetterTarget();
                                        }
                                    }
                                }
                                sb.append(s2);
                                ++i;
                            }
                        }
                        final boolean b = false;
                        continue;
                    }
                }
                this.increaseFinishedQuestions();
            }
            return false;
        }
        
        public String getAnswers() {
            return this.answers;
        }
        
        public int getFinishedQuestions() {
            return this.finishedQuestions;
        }
        
        public String getHints() {
            return this.hints;
        }
        
        public Skanword getScanword() {
            return this.skanword;
        }
        
        public Long getScanwordId() {
            return this.skanwordId;
        }
        
        public int getSetId() {
            return this.setId;
        }
        
        public Long getUpdateTime() {
            return this.updateTime;
        }
        
        public void increaseFinishedQuestions() {
            final int finishedQuestions = this.finishedQuestions;
            boolean finished = true;
            this.finishedQuestions = finishedQuestions + 1;
            this.started = true;
            if (this.finishedQuestions < this.skanword.getQuestions().size()) {
                finished = false;
            }
            this.finished = finished;
        }
        
        public boolean isChanged() {
            return this.changed;
        }
        
        public boolean isFinished() {
            return this.finished;
        }
        
        public boolean isStarted() {
            return this.started;
        }
        
        public void setChanged(final boolean changed) {
            this.changed = changed;
        }
        
        public void setScanword(final Skanword skanword) {
            this.skanword = skanword;
        }
        
        public void updated() {
            this.updateTime = new Date().getTime();
        }
    }

    public enum TaskListDirrectionType {
        TOP_DIRRECTION,
        CURRECT_DIRRECTION,
        PREVIOUS_DIRRECTION,
        NEXT_DIRRECTION

    }

    public enum TaskListType {
        ISSUE_TASKS,
        STARTED_TASKS,
        FINISHED_TASKS,
        ALL_TASKS

    }

    public enum VisibilityListType {
        ALL_ISSUES,
        DOWNLOADED_ISSUES,
        NOT_DOWNLOADED_ISSUES;

    }

    class SkanwordsDataManager1 implements EventListener {
        final /* synthetic */ SkanwordsDataManager this0;

        SkanwordsDataManager1(final SkanwordsDataManager this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.appTimerTicked();
        }
    }

    static class SkanwordsDataManager7 {
        // FF: synthetic field
        static final int[] taskDirectoryType = new int[TaskListDirrectionType.values().length];
        // FF: synthetic field
        static final int[] taskListType;

        static {
            try {
                taskDirectoryType[TaskListDirrectionType.TOP_DIRRECTION.ordinal()] = 1;
            } catch (NoSuchFieldError var7) {
            }

            try {
                taskDirectoryType[TaskListDirrectionType.CURRECT_DIRRECTION.ordinal()] = 2;
            } catch (NoSuchFieldError var6) {
            }

            try {
                taskDirectoryType[TaskListDirrectionType.PREVIOUS_DIRRECTION.ordinal()] = 3;
            } catch (NoSuchFieldError var5) {
            }

            try {
                taskDirectoryType[TaskListDirrectionType.NEXT_DIRRECTION.ordinal()] = 4;
            } catch (NoSuchFieldError var4) {
            }

            taskListType = new int[TaskListType.values().length];

            try {
                taskListType[TaskListType.ISSUE_TASKS.ordinal()] = 1;
            } catch (NoSuchFieldError var3) {
            }

            try {
                taskListType[TaskListType.STARTED_TASKS.ordinal()] = 2;
            } catch (NoSuchFieldError var2) {
            }

            try {
                taskListType[TaskListType.FINISHED_TASKS.ordinal()] = 3;
            } catch (NoSuchFieldError var1) {
            }
        }
    }

    static final class SkanwordsDataManager5 implements Comparator<Skanword> {
        SkanwordsDataManager5() {
            super();
        }

        @Override
        public int compare(final Skanword skanword, final Skanword skanword2) {
            if (skanword2.isFinished() && skanword.isFinished()) {
                return skanword.getId() - skanword2.getId();
            }
            if (skanword2.isFinished()) {
                return 1;
            }
            if (skanword.isFinished()) {
                return -1;
            }
            return 0;
        }
    }

    static final class SkanwordsDataManager6 implements Comparator<Skanword> {
        SkanwordsDataManager6() {
            super();
        }

        @Override
        public int compare(final Skanword skanword, final Skanword skanword2) {
            if (skanword2.isStarted() && skanword.isStarted()) {
                return skanword.getId() - skanword2.getId();
            }
            if (skanword2.isStarted()) {
                return 1;
            }
            if (skanword.isStarted()) {
                return -1;
            }
            return 0;
        }
    }

    class SkanwordsDataManager2 implements Comparator<SkanwordsSetInfo> {
        final SkanwordsDataManager this0;

        SkanwordsDataManager2(final SkanwordsDataManager this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public int compare(final SkanwordsSetInfo skanwordsSetInfo, final SkanwordsSetInfo skanwordsSetInfo2) {
            return skanwordsSetInfo2.getId() - skanwordsSetInfo.getId();
        }
    }

    class SkanwordsDataManager4 implements Runnable {
        final /* synthetic */ SkanwordsDataManager this0;
        final /* synthetic */ boolean valnotify;
        final /* synthetic */ Skanword valskanword;

        SkanwordsDataManager4(final SkanwordsDataManager this0, final Skanword valskanword, final boolean valnotify) {
            super();
            this.this0 = this0;
            this.valskanword = valskanword;
            this.valnotify = valnotify;
        }

        @Override
        public void run() {
            this.valskanword.setChanged(false);
            SmappsScanwords.getDatabaseHelper().updateSkanwordProgress(this.valskanword, true);
            SkanwordsDataManager.access000(this.this0);
            if (this.valnotify) {
                SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SkanwordsSetsUpdateEvent("EVENT_SCANWORDS_SETS_INFOS_UPDATE"));
                SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_SCANWORDS_TASKS_UPDATE"));
            }
        }
    }

    class SkanwordsDataManager3
            implements Comparator<SkanwordsSetInfo>
    {
        SkanwordsDataManager3(SkanwordsDataManager paramSkanwordsDataManager) {}

        public int compare(SkanwordsSetInfo paramSkanwordsSetInfo1, SkanwordsSetInfo paramSkanwordsSetInfo2)
        {
            return paramSkanwordsSetInfo2.getId().intValue() - paramSkanwordsSetInfo1.getId().intValue();
        }
    }

}
