package org.skanword.and.datamanager;

import java.util.*;
import org.skanword.and.*;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;

import android.util.*;
import java.io.*;
import com.google.gson.annotations.*;

public class RatingDataManager
{
    private String mRatingHash;
    private HashMap<String, List<UsersRating>> ratings;
    
    public RatingDataManager() {
        super();
        this.ratings = null;
        this.mRatingHash = "";
    }
    
    public List<UsersRating> getCommonRating() {
        final HashMap<String, List<UsersRating>> ratings = this.ratings;
        if (ratings != null) {
            return ratings.get("common_skanword");
        }
        return null;
    }
    
    public List<UsersRating> getFriendsRating() {
        final HashMap<String, List<UsersRating>> ratings = this.ratings;
        if (ratings != null) {
            return ratings.get("friends_skanword");
        }
        return null;
    }
    
    public String getRatingHash() {
        return this.mRatingHash;
    }
    
    public void retrieveData() {
        this.mRatingHash = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(16);
        if (this.mRatingHash == null) {
            this.mRatingHash = "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("mRatingHash retrieve ");
        sb.append(this.mRatingHash);
        Log.v("", sb.toString());
        HashMap<String, List<RatingDataManager.UsersRating>> retGenDate = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(5);
        this.upadateUsersRating(retGenDate, false);
    }
    
    public void setRatingHash(final String mRatingHash) {
        this.mRatingHash = mRatingHash;
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mRatingHash, 16);
        final StringBuilder sb = new StringBuilder();
        sb.append("mRatingHash update ");
        sb.append(this.mRatingHash);
        Log.v("", sb.toString());
    }
    
    public void upadateUsersRating(final HashMap<String, List<UsersRating>> ratings, final boolean b) {
        if (ratings == null) {
            return;
        }
        this.ratings = ratings;
        if (b) {
            SmappsScanwords.getDatabaseHelper().storeGeneralData(this.ratings, 5);
        }
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_RATING_UPDATE"));
    }
    
    public static class RatingInfo implements Serializable
    {
        private static final long serialVersionUID = 9098701843974191552L;
        private final HashMap<String, String> leaderboards;
        @SerializedName("total")
        private final HashMap<String, List<UsersRating>> totalRatings;
        
        public RatingInfo(final HashMap<String, String> leaderboards, final HashMap<String, List<UsersRating>> totalRatings) {
            super();
            this.leaderboards = leaderboards;
            this.totalRatings = totalRatings;
        }
        
        public HashMap<String, String> getLeaderboards() {
            return this.leaderboards;
        }
        
        public HashMap<String, List<UsersRating>> getTotalRatings() {
            return this.totalRatings;
        }
    }

    public enum RatingType {
        SCANWORD_COMMON,
        SCANWORD_FRIENDS

    }
    
    public static class SocialUser implements Serializable
    {
        private static final long serialVersionUID = 2348805317929820015L;
        @SerializedName("social_id")
        private final String socialId;
        @SerializedName("user_id")
        private final String userId;
        @SerializedName("user_name")
        private final String userName;
        @SerializedName("user_photo")
        private final String userPhoto;
        
        public SocialUser(final String userId, final String socialId, final String userName, final String userPhoto) {
            super();
            this.userId = userId;
            this.socialId = socialId;
            this.userName = userName;
            this.userPhoto = userPhoto;
        }
        
        public String getSocialId() {
            return this.socialId;
        }
        
        public String getUserId() {
            return this.userId;
        }
        
        public String getUserName() {
            return this.userName;
        }
        
        public String getUserPhoto() {
            return this.userPhoto;
        }
    }
    
    public static class UsersRating implements Serializable
    {
        private static final long serialVersionUID = -8243320369198349000L;
        @SerializedName("user_social")
        private final SocialUser socialUser;
        @SerializedName("user_id")
        private final String userId;
        @SerializedName("user_place")
        private final int userPlace;
        @SerializedName("user_score")
        private final String userScore;
        
        public UsersRating(final int userPlace, final String userId, final String userScore, final SocialUser socialUser) {
            super();
            this.userPlace = userPlace;
            this.userId = userId;
            this.userScore = userScore;
            this.socialUser = socialUser;
        }
        
        public SocialUser getSocialUser() {
            return this.socialUser;
        }
        
        public String getUserId() {
            return this.userId;
        }
        
        public int getUserPlace() {
            return this.userPlace;
        }
        
        public String getUserScore() {
            return this.userScore;
        }
    }
}
