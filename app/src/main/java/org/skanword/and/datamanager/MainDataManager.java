package org.skanword.and.datamanager;

import android.annotation.*;
import org.skanword.and.*;
import android.content.*;
import java.util.logging.*;
import android.util.*;
import java.io.*;
import android.app.*;
import org.skanword.and.etc.*;
import java.util.*;
import java.net.*;

import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;
import org.skanword.and.network.*;

@SuppressLint({ "UseSparseArrays" })
public class MainDataManager
{
    public static final String DAILY_BONUS_READY = "DAILY_BONUS_READY";
    public static final String EVENT_DOWNLOAD_TODAY_ISSUE = "EVENT_DOWNLOAD_TODAY_ISSUE";
    public static final String EVENT_NEWS_UPDATE = "EVENT_NEWS_UPDATE";
    public static final String EVENT_NOTIFICATIONS_UPDATE = "EVENT_NOTIFICATIONS_UPDATE";
    public static final String EVENT_NO_TOKEN = "EVENT_NO_TOKEN";
    public static final String EVENT_OPTIONS_UPDATE = "EVENT_OPTIONS_UPDATE";
    public static final String EVENT_PROGRESS_UPDATE = "EVENT_PROGRESS_UPDATE";
    public static final String EVENT_RATING_UPDATE = "EVENT_RATING_UPDATE";
    public static final String EVENT_SHOP_DATA_UPDATE = "EVENT_SHOP_DATA_UPDATE";
    public static final String EVENT_SKANWORDS_SETS_INFOS_UPDATE = "EVENT_SCANWORDS_SETS_INFOS_UPDATE";
    public static final String EVENT_SKANWORDS_TASKS_UPDATE = "EVENT_SCANWORDS_TASKS_UPDATE";
    public static final String EVENT_USER_INFO_UPDATE = "EVENT_USER_INFO_UPDATE";
    public static final String GENERAL_PREFERENCES_NAME = "GENERAL_PREFERENCES_NAME";
    public static final String TUTORIAL_PREFERENCES_NAME = "tutorial";
    public static final String USER_DATA_PREFS_NAME = "UserDataPrefs";
    private static MainDataManager instance;
    private ConfigObject.AppAds mAppAds;
    private ConfigObject.AppOffers mAppOffers;
    private DailyBonusObject mDailyBonus;
    private ConfigObject.LocalNotification mLocalNotifications;
    private final NewsDataManager mNewsDataManager;
    private OptionsData mOptionsData;
    private PurchasesData mPurchasesData;
    private final RatingDataManager mRatingDataManager;
    private final ShopDataManager mShopDataManager;
    private final SkanwordsDataManager mSkanwordsDataManager;
    private UserObject mUserData;
    private List<String> mUserFriends;
    private ConfigObject.Versions mVersions;
    private volatile Map<Integer, Integer> setsProgresses;
    
    protected MainDataManager() {
        super();
        this.mUserFriends = null;
        this.setsProgresses = new HashMap<Integer, Integer>();
        this.mSkanwordsDataManager = new SkanwordsDataManager();
        this.mShopDataManager = new ShopDataManager();
        this.mRatingDataManager = new RatingDataManager();
        this.mNewsDataManager = new NewsDataManager();
    }
    
    static /* synthetic */ SkanwordsDataManager access000(final MainDataManager mainDataManager) {
        return mainDataManager.mSkanwordsDataManager;
    }
    
    public static MainDataManager getInstance() {
        if (MainDataManager.instance == null) {
            (MainDataManager.instance = new MainDataManager()).retrieveLocalData(SmappsScanwords.getContext());
        }
        return MainDataManager.instance;
    }
    
    public static MainDataManager getInstance(final Context context) {
        if (MainDataManager.instance == null) {
            (MainDataManager.instance = new MainDataManager()).retrieveLocalData(context);
        }
        return MainDataManager.instance;
    }
    
    private void retrieveLocalData(final Context context) {
        this.mSkanwordsDataManager.retrieveData(context);
        this.mShopDataManager.retrieveData();
        this.mRatingDataManager.retrieveData();
        this.mNewsDataManager.retrieveData();
        this.mVersions = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(18);
        this.mUserData = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(6);
        if (this.mUserData == null) {
            this.mUserData = new UserObject();
        }
        this.mOptionsData = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(8);
        if (this.mOptionsData == null) {
            this.mOptionsData = new OptionsData();
        }
        this.mPurchasesData = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(9);
        if (this.mPurchasesData == null) {
            this.mPurchasesData = new PurchasesData();
        }
        this.mLocalNotifications = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(10);
        if (this.mLocalNotifications == null) {
            this.mLocalNotifications = new ConfigObject.LocalNotification();
        }
        this.mAppAds = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(11);
        if (this.mAppAds == null) {
            this.mAppAds = new ConfigObject.AppAds();
        }
        this.mAppOffers = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(12);
        if (this.mAppOffers == null) {
            this.mAppOffers = new ConfigObject.AppOffers();
        }
        this.mLocalNotifications = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(10);
        if (this.mLocalNotifications == null) {
            this.mLocalNotifications = new ConfigObject.LocalNotification();
        }
        this.mDailyBonus = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(19);
        this.mUserFriends = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(7);
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_USER_INFO_UPDATE"));
    }
    
    public void addDownloadingProgress(final SkanwordsSetInfo isDownloading) {
        this.setsProgresses.put(isDownloading.getId(), 1);
        final Logger log = SmappsScanwords.LOG;
        final StringBuilder sb = new StringBuilder();
        sb.append("addDownloadingProgress - ");
        sb.append(this.setIsDownloading(isDownloading));
        log.warning(sb.toString());
    }
    
    public void addUserHints(final int n) {
        getInstance().getUserData().setHints(getInstance().getUserData().getHints() + n);
        getInstance().storeUserData();
    }
    
    public void configDataReceived(final ConfigObject configObject) {
        if (configObject == null) {
            return;
        }
        final ConfigObject.Versions versions = configObject.getVersions();
        final int n = 0;
        boolean b = false;
        Label_0068: {
            if (versions != null) {
                Log.v("SkanwordsFunc", "config recieved versions");
                final ConfigObject.Versions mVersions = this.mVersions;
                if (mVersions == null || !mVersions.equals(configObject.getVersions())) {
                    Log.v("SkanwordsFunc", "versions not the same");
                    b = true;
                    break Label_0068;
                }
            }
            b = false;
        }
        this.mVersions = configObject.getVersions();
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mVersions, 18);
        if (b) {
            Log.v("SkanwordsFunc", "check versions after config received");
            SmappsScanwords.instance.checkVersions();
        }
        if (configObject.getLocalNotifications() != null) {
            this.mLocalNotifications = configObject.getLocalNotifications();
            SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mLocalNotifications, 10);
            SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_NOTIFICATIONS_UPDATE"));
        }
        if (configObject.getAppAds() != null) {
            this.mAppAds = configObject.getAppAds();
            SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mAppAds, 11);
        }
        if (configObject.getAppOffers() != null) {
            this.mAppOffers = configObject.getAppOffers();
            SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mAppOffers, 12);
        }
        if (!configObject.isPushTokenExist()) {
            SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_NO_TOKEN"));
        }
        int n2 = n;
        if (!configObject.getShopHash().equals(this.mShopDataManager.getShopHash())) {
            final StringBuilder sb = new StringBuilder();
            sb.append(configObject.getShopHash());
            sb.append(" - config shop hash | stored shop hash - ");
            sb.append(this.mShopDataManager.getShopHash());
            Log.v("", sb.toString());
            n2 = 2;
        }
        int n3 = n2;
        if (!configObject.getNewsHash().equals(this.mNewsDataManager.getNewsHash())) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(configObject.getNewsHash());
            sb2.append(" - config news hash | stored news hash - ");
            sb2.append(this.mNewsDataManager.getNewsHash());
            Log.v("", sb2.toString());
            n3 = (n2 | 0x1);
        }
        if (n3 > 0) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("request info after config got for mask ");
            sb3.append(n3);
            Log.v("", sb3.toString());
            MainNetworkManager.getInstance().requestInfo(n3, null);
        }
    }
    
    public void dailyBonusObjectRecieved(DailyBonusObject mDailyBonus) {
        final StringBuilder sb = new StringBuilder();
        sb.append("1dailyBonusObjectRecieved ");
        sb.append(mDailyBonus);
        Log.v("SkanwordsFunc", sb.toString());
        LocalNotificationsManager.createDailyBonusNotifications(mDailyBonus, SmappsScanwords.getContext());
        final DailyBonusObject mDailyBonus2 = this.mDailyBonus;
        if (mDailyBonus2 == null) {
            this.mDailyBonus = mDailyBonus;
        }
        else {
            if (mDailyBonus2.getTime() != mDailyBonus.getTime()) {
                mDailyBonus2.setTime(mDailyBonus.getTime());
                mDailyBonus2.setScratchedPosition(0);
            }
            mDailyBonus2.setTimeLeft(mDailyBonus.getTimeLeft());
            mDailyBonus2.setVariants(mDailyBonus.getVariants());
            mDailyBonus2.setWin(mDailyBonus.getWin());
            mDailyBonus = mDailyBonus2;
        }
        if (mDailyBonus != null) {
            mDailyBonus.setDownloadedTime(new Date().getTime());
            final DataBaseManager databaseHelper = SmappsScanwords.getDatabaseHelper();
            if (databaseHelper != null) {
                databaseHelper.storeGeneralData(mDailyBonus, 19);
            }
            if (mDailyBonus.getTimeLeft() < 1) {
                SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("DAILY_BONUS_READY"));
            }
        }
    }
    
    public void downloadSetWithId(final Activity activity, final SkanwordsSetInfo skanwordsSetInfo, final IDowloadSetProgress dowloadSetProgress) {
        DownloadSetDataRunnable downSetDataRun = new DownloadSetDataRunnable(this, activity, SetType.SKANWORD_SET_TYPE, skanwordsSetInfo, dowloadSetProgress);
        SmappsScanwords.getExecutorService().execute(downSetDataRun);
    }
    
    public ConfigObject.AppAds getAppAds() {
        return this.mAppAds;
    }
    
    public ConfigObject.AppOffers getAppOffers() {
        return this.mAppOffers;
    }
    
    public DailyBonusObject getDailyBonus() {
        return this.mDailyBonus;
    }
    
    public ConfigObject.LocalNotification getLocalNotifications() {
        return this.mLocalNotifications;
    }
    
    public NewsDataManager getNewsDataManager() {
        return this.mNewsDataManager;
    }
    
    public OptionsData getOptions() {
        return this.mOptionsData;
    }
    
    public PurchasesData getPurchasesData() {
        return this.mPurchasesData;
    }
    
    public RatingDataManager getRatingDataManager() {
        return this.mRatingDataManager;
    }
    
    public int getSetDownloadingProgress(final Integer n) {
        if (this.setsProgresses != null && n != null) {
            return this.setsProgresses.get(n);
        }
        return 0;
    }
    
    public ShopDataManager getShopDataManager() {
        return this.mShopDataManager;
    }
    
    public SkanwordsDataManager getSkanwordsDataManager() {
        return this.mSkanwordsDataManager;
    }
    
    public UserObject getUserData() {
        return this.mUserData;
    }
    
    public List<String> getUserFriends() {
        return this.mUserFriends;
    }
    
    public ConfigObject.Versions getVersions() {
        return this.mVersions;
    }
    
    public void removeDownloadingProgress(final SkanwordsSetInfo skanwordsSetInfo) {
        this.setsProgresses.remove(skanwordsSetInfo.getId());
    }
    
    public void rewriteDailyBonusObjectRecieved(final DailyBonusObject mDailyBonus) {
        final StringBuilder sb = new StringBuilder();
        sb.append("rewriteDailyBonusObjectRecieved ");
        sb.append(mDailyBonus);
        Log.v("SkanwordsFunc", sb.toString());
        if (mDailyBonus == null) {
            LocalNotificationsManager.clearBonusNotifs();
        }
        this.mDailyBonus = mDailyBonus;
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mDailyBonus, 19);
    }
    
    public boolean setIsDownloading(final SkanwordsSetInfo skanwordsSetInfo) {
        return this.setsProgresses.get(skanwordsSetInfo.getId()) != null;
    }
    
    public void setLocalNotifications(final ConfigObject.LocalNotification mLocalNotifications) {
        this.mLocalNotifications = mLocalNotifications;
    }
    
    public void setUserFriends(final List<String> mUserFriends) {
        this.mUserFriends = mUserFriends;
        SmappsScanwords.getDatabaseHelper().storeGeneralData(new ArrayList<Object>(mUserFriends), 7);
    }
    
    public void storeOptions() {
        if (this.mOptionsData == null) {
            return;
        }
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_OPTIONS_UPDATE"));
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mOptionsData, 8);
    }
    
    public void storePurchases() {
        if (this.mPurchasesData == null) {
            return;
        }
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mPurchasesData, 9);
    }
    
    public void storeUserData() {
        if (this.mUserData == null) {
            return;
        }
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_USER_INFO_UPDATE"));
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mUserData, 6);
    }
    
    public void updateNewsData(final List<NewsDataManager.News> list, final String newsHash) {
        if (list == null) {
            return;
        }
        this.mNewsDataManager.updateNews(list, true);
        this.mNewsDataManager.setNewsHash(newsHash);
    }
    
    public void updateProgress(final SkanwordsSetInfo skanwordsSetInfo, final int n) {
        this.setsProgresses.put(skanwordsSetInfo.getId(), n);
    }
    
    public void updateRatingData(final RatingDataManager.RatingInfo ratingInfo, final String ratingHash) {
        if (ratingInfo == null) {
            return;
        }
        this.mRatingDataManager.upadateUsersRating(ratingInfo.getTotalRatings(), true);
        this.mRatingDataManager.setRatingHash(ratingHash);
    }
    
    public void updateShopData(final ShopDataManager.Shop shop, final String shopHash) {
        if (shop == null) {
            return;
        }
        this.mShopDataManager.updateShopData(shop, true);
        this.mShopDataManager.setShopHash(shopHash);
    }
    
    public void userDataReceived(final UserObject mUserData) {
        if (mUserData != null) {
            this.mUserData = mUserData;
        }
        this.mUserData.setUpdatedTime(new Date().getTime() / 1000L);
        this.storeUserData();
        SmappsScanwords.getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putBoolean("show_hints_offers", mUserData.isShowHintsVideoOffer()).commit();
    }
    
    private class DownloadSetDataRunnable implements Runnable
    {
        private final IDowloadSetProgress dowloadSetProgress;
        private Activity mCallerActivity;
        private final SkanwordsSetInfo setInfo;
        private final SetType setType;
        final /* synthetic */ MainDataManager this0;
        
        public DownloadSetDataRunnable(final MainDataManager this0, final Activity mCallerActivity, final SetType setType, final SkanwordsSetInfo setInfo, final IDowloadSetProgress dowloadSetProgress) {
            super();
            this.this0 = this0;
            this.mCallerActivity = mCallerActivity;
            this.setType = setType;
            this.setInfo = setInfo;
            this.dowloadSetProgress = dowloadSetProgress;
        }
        
        @Override
        public void run() {
            final SetType setType = this.setType;
            final SetType skanword_SET_TYPE = SetType.SKANWORD_SET_TYPE;
            String mediaUrl = null;
            String setMediaFileUrlForSetInfo = null;
            Label_0150: {
                if (setType == skanword_SET_TYPE) {
                    final HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
                    hashMap.put("set_id", this.setInfo.getId());
                    hashMap.put("return", 3);
                    final List<HashMap<String, Integer>> list = new ArrayList<HashMap<String, Integer>>();
                    list.add(hashMap);
                    final SkanwordsSetDataObject skanwordSetGet = MainNetworkManager.getInstance().skanwordSetGet(this.mCallerActivity, list);
                    if (skanwordSetGet == null) {
                        this.dowloadSetProgress.onProgress(-1);
                        return;
                    }
                    if (skanwordSetGet.getFristScanwordSet() != null) {
                        mediaUrl = skanwordSetGet.getFristScanwordSet().getMediaUrl();
                        this.this0.mSkanwordsDataManager.storeSkanwordsSet(skanwordSetGet, this.setInfo);
                        setMediaFileUrlForSetInfo = this.this0.mSkanwordsDataManager.setMediaFileUrlForSetInfo(mediaUrl, this.setInfo.getId());
                        break Label_0150;
                    }
                }
                setMediaFileUrlForSetInfo = null;
            }
            if (mediaUrl == null) {
                MainNetworkManager.getInstance().serverErrorLog("scanword_set_get", "no url");
                this.dowloadSetProgress.onProgress(-1);
                return;
            }
            final DownloaderArhiveFile downloaderArhiveFile = new DownloaderArhiveFile();
            downloaderArhiveFile.setProgress(this.dowloadSetProgress, 10);
            try {
                downloaderArhiveFile.dowload(new URL(mediaUrl), setMediaFileUrlForSetInfo);
            }
            catch (Exception ex) {
                final MainNetworkManager instance = MainNetworkManager.getInstance();
                final StringBuilder sb = new StringBuilder();
                sb.append("exeption with message : ");
                sb.append(ex.getMessage());
                instance.serverErrorLog("downloading_archive", sb.toString());
                SmappsScanwords.sendCaughtException(ex);
                this.dowloadSetProgress.onProgress(-1);
            }
        }
    }
    
    public interface IDowloadSetProgress
    {
        public static final int ERROR_PROGRESS = -1;
        void onProgress(final int p0);
    }

    public static enum SetType {
        SKANWORD_SET_TYPE;
    }

    public static enum UpdateLayout {
        LAYOUT_MAIN,
        LAYOUT_RATING,
        LAYOUT_AUTO

    }
}
