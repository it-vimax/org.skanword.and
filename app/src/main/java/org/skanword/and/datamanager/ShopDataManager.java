package org.skanword.and.datamanager;

import java.util.*;
import org.skanword.and.*;
import java.io.*;
import android.util.*;
import org.skanword.and.inappbilling.*;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;

import com.google.gson.annotations.*;

public class ShopDataManager
{
    private Shop mShop;
    private String mShopHash;
    
    public ShopDataManager() {
        super();
        this.mShop = null;
        this.mShopHash = null;
    }
    
    public ShopItem getDefaultShopItemOfType(final ShopProductType shopProductType) {
        if (this.mShop == null) {
            return null;
        }
        final int n = ShopDataManager1.shopProductType[shopProductType.ordinal()];
        List<ShopItem>  list;
        if (n != 1) {
            if (n != 2) {
                list = null;
            }
            else {
                list = this.mShop.getItemsCommon().getHints();
            }
        }
        else {
            list = this.mShop.getItemsCommon().getVip();
        }
        for (final ShopItem shopItem : list) {
            if (shopItem.isDefaultItem()) {
                return shopItem;
            }
        }
        return null;
    }
    
    public long getSaleTimeLeft() {
        final Shop mShop = this.mShop;
        if (mShop != null && mShop.getSale() != null) {
            return this.mShop.getSale().getSaleTimeLeft() - (new Date().getTime() / 1000L - this.mShop.getUpdatedTime());
        }
        return 0L;
    }
    
    public String getShopHash() {
        return this.mShopHash;
    }
    
    public List<ShopItem> getShopItemsOfType(final ShopProductType shopProductType) {
        if (this.mShop == null) {
            return null;
        }
        final int n = ShopDataManager1.shopProductType[shopProductType.ordinal()];
        List<ShopItem> vip;
        if (n != 1) {
            if (n == 2) {
                return this.mShop.getItemsCommon().getHints();
            }
            if (n == 3) {
                return this.mShop.getItemsCommon().getAdsOff();
            }
            if (n != 4) {
                return null;
            }
            final ArrayList<ShopItem> list = new ArrayList<ShopItem>();
            final Iterator<ShopItem> iterator = this.mShop.getItemsCommon().getHints().iterator();
            while (true) {
                vip = list;
                if (!iterator.hasNext()) {
                    break;
                }
                final ShopItem shopItem = iterator.next();
                if (!shopItem.isMiniShop()) {
                    continue;
                }
                list.add(shopItem);
            }
        }
        else {
            vip = this.mShop.getItemsCommon().getVip();
        }
        return vip;
    }
    
    public void retrieveData() {
        this.mShopHash = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(17);
        if (this.mShopHash == null) {
            this.mShopHash = "";
        }
        ShopDataManager.Shop retGenData = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(3);
        this.updateShopData(retGenData, false);
    }
    
    public boolean saleEnabled() {
        return this.getSaleTimeLeft() > 0L;
    }
    
    public void setShopHash(final String mShopHash) {
        this.mShopHash = mShopHash;
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mShopHash, 17);
        final StringBuilder sb = new StringBuilder();
        sb.append("mShopHash update ");
        sb.append(this.mShopHash);
        Log.v("", sb.toString());
    }
    
    public boolean shopItemsTypeWithSale(final ShopProductType shopProductType) {
        if (!this.saleEnabled()) {
            return false;
        }
        final List<ShopItem> list = null;
        if (this.mShop == null) {
            return false;
        }
            final int n = ShopDataManager1.shopProductType[shopProductType.ordinal()];
        List<ShopItem> list2;
        if (n != 1) {
            if (n != 2) {
                list2 = list;
            }
            else {
                list2 = this.mShop.getItemsCommon().getHints();
            }
        }
        else {
            list2 = this.mShop.getItemsCommon().getVip();
        }
        final Iterator<ShopItem> iterator = list2.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().isSale()) {
                return true;
            }
        }
        return false;
    }
    
    public void updateShopData(final Shop mShop, final boolean b) {
        if (mShop == null) {
            return;
        }
        if (this.mShop != null) {
            mShop.setUpdatedTime(new Date().getTime() / 1000L);
        }
        this.mShop = mShop;
        final ArrayList<String> list = new ArrayList<String>();
        final Iterator<ShopItem> iterator = this.mShop.getItemsCommon().getHints().iterator();
        while (iterator.hasNext()) {
            list.add(((ShopItem)iterator.next()).getId());
        }
        final Iterator<ShopItem> iterator2 = this.mShop.getItemsCommon().getVip().iterator();
        while (iterator2.hasNext()) {
            list.add(iterator2.next().getId());
        }
        InAppManager.getInstance().getProductsPrices(list);
        if (b) {
            SmappsScanwords.getDatabaseHelper().storeShopData(mShop);
        }
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_SHOP_DATA_UPDATE"));
    }
    
    public static class Shop implements Serializable
    {
        private static final long serialVersionUID = -1176298582371777763L;
        @SerializedName("items_common")
        private final ShopItemsSet itemsCommon;
        @SerializedName("items_sale")
        private final ShopItemsSet itemsSale;
        private final SaleData sale;
        @SerializedName("updated_time")
        private long updatedTime;
        
        public Shop(final ShopItemsSet itemsCommon, final ShopItemsSet itemsSale, final SaleData sale) {
            super();
            this.updatedTime = 0L;
            this.itemsCommon = itemsCommon;
            this.itemsSale = itemsSale;
            this.sale = sale;
        }
        
        public ShopItemsSet getItemsCommon() {
            return this.itemsCommon;
        }
        
        public ShopItemsSet getItemsSale() {
            return this.itemsSale;
        }
        
        public SaleData getSale() {
            return this.sale;
        }
        
        public long getUpdatedTime() {
            return this.updatedTime;
        }
        
        public void setUpdatedTime(final long updatedTime) {
            this.updatedTime = updatedTime;
        }
        
        public static class SaleData implements Serializable
        {
            private static final long serialVersionUID = 5925859866820118892L;
            @SerializedName("time_left")
            private final long saleTimeLeft;
            
            public SaleData(final Long n) {
                super();
                this.saleTimeLeft = n;
            }
            
            public long getSaleTimeLeft() {
                return this.saleTimeLeft;
            }
        }
        
        public static class ShopItemsSet implements Serializable
        {
            private static final long serialVersionUID = -2770942828674933010L;
            @SerializedName("ads_off")
            private final List<ShopItem> adsOff;
            private final List<ShopItem> hints;
            private final List<ShopItem> vip;
            
            public ShopItemsSet(final List<ShopItem> hints, final List<ShopItem> vip, final List<ShopItem> adsOff) {
                super();
                this.hints = hints;
                this.vip = vip;
                this.adsOff = adsOff;
            }
            
            public List<ShopItem> getAdsOff() {
                return this.adsOff;
            }
            
            public List<ShopDataManager.ShopItem> getHints() {
                return this.hints;
            }
            
            public List<ShopItem> getVip() {
                return this.vip;
            }
        }
    }

    static class ShopDataManager1 {
        // FF: synthetic field
        static final int[] shopProductType = new int[ShopProductType.values().length];

        static {
            try {
                shopProductType[ShopProductType.SUBSCRIPTION.ordinal()] = 1;
            } catch (NoSuchFieldError var4) {
            }

            try {
                shopProductType[ShopProductType.HINTS.ordinal()] = 2;
            } catch (NoSuchFieldError var3) {
            }

            try {
                shopProductType[ShopProductType.ADS_OFF.ordinal()] = 3;
            } catch (NoSuchFieldError var2) {
            }

            try {
                shopProductType[ShopProductType.MINI_SHOP.ordinal()] = 4;
            } catch (NoSuchFieldError var1) {
            }
        }
    }

    public enum ShopProductType {
        SUBSCRIPTION,
        HINTS,
        ADS_OFF,
        MINI_SHOP
    }

    public static class ShopItem implements Serializable
    {
        private static final long serialVersionUID = -1697317095487755238L;
        @SerializedName("default")
        private final boolean defaultItem;
        private final String description;
        @SerializedName("icon_name")
        private final String iconName;
        private final String id;
        @SerializedName("mini_shop")
        private final boolean miniShop;
        private final String name;
        private final boolean sale;
        private final int sort;
        @SerializedName("success_message")
        private final String successMessage;
        @SerializedName("success_message_restored")
        private final String successMessageRestored;
        @SerializedName("vip_cross_archive")
        private final int vipScanwordsArchive;
        @SerializedName("vip_cross_new")
        private final int vipScanwordsNew;
        private final boolean visible;

        public ShopItem(final String id, final String name, final String description, final String successMessage, final String successMessageRestored, final boolean defaultItem, final boolean visible, final boolean sale, final boolean miniShop, final int sort, final int vipScanwordsNew, final int vipScanwordsArchive, final String iconName) {
            super();
            this.id = id;
            this.name = name;
            this.description = description;
            this.successMessage = successMessage;
            this.successMessageRestored = successMessageRestored;
            this.defaultItem = defaultItem;
            this.visible = visible;
            this.sale = sale;
            this.miniShop = miniShop;
            this.sort = sort;
            this.iconName = iconName;
            this.vipScanwordsNew = vipScanwordsNew;
            this.vipScanwordsArchive = vipScanwordsArchive;
        }

        public String getDescription() {
            return this.description;
        }

        public String getIconName() {
            return this.iconName;
        }

        public String getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public int getSort() {
            return this.sort;
        }

        public String getSuccessMessage() {
            return this.successMessage;
        }

        public String getSuccessMessageRestored() {
            return this.successMessageRestored;
        }

        public int getVipScanwordsArchive() {
            return this.vipScanwordsArchive;
        }

        public int getVipScanwordsNew() {
            return this.vipScanwordsNew;
        }

        public boolean isDefaultItem() {
            return this.defaultItem;
        }

        public boolean isMiniShop() {
            return this.miniShop;
        }

        public boolean isSale() {
            return this.sale;
        }

        public boolean isVisible() {
            return this.visible;
        }
    }
}
