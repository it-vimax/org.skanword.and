package org.skanword.and.datamanager;

import com.google.gson.annotations.*;
import org.skanword.and.network.*;
import java.io.*;
import java.util.*;

public class SkanwordsSetInfo implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String cover;
    @SerializedName("data_hash")
    private final String dataHash;
    @SerializedName("display_name")
    private String displayName;
    @SerializedName("download_time_left")
    private int downloadTimeLeft;
    @SerializedName("downloaded_time")
    private long downloadedTime;
    @SerializedName("solved_count")
    private int finishedCount;
    @SerializedName("free_ids")
    private List<Integer> freeIds;
    private final Integer id;
    private boolean isLocalData;
    private final String name;
    @SerializedName("offer_ids")
    private List<Integer> offerIds;
    private boolean progressChanged;
    @SerializedName("save_hash")
    private String saveHash;
    @SerializedName("cross_count")
    private final int skanwordsCount;
    @SerializedName("save_count")
    private int startedCount;
    private boolean today;
    
    public SkanwordsSetInfo(final Integer id, final String name, final String displayName, final int skanwordsCount, final String dataHash, final String saveHash, final int downloadTimeLeft, final List<Integer> freeIds, final List<Integer> offerIds, final long downloadedTime, final int startedCount, final int finishedCount, final String cover) {
        super();
        this.downloadedTime = 0L;
        this.today = false;
        this.progressChanged = false;
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.skanwordsCount = skanwordsCount;
        this.dataHash = dataHash;
        this.saveHash = saveHash;
        this.downloadTimeLeft = downloadTimeLeft;
        this.freeIds = freeIds;
        this.offerIds = offerIds;
        this.startedCount = startedCount;
        this.finishedCount = finishedCount;
        this.cover = cover;
        this.downloadedTime = downloadedTime;
    }
    
    public static long getSerialversionuid() {
        return 1L;
    }
    
    public String getCover() {
        return this.cover;
    }
    
    public String getDataHash() {
        return this.dataHash;
    }
    
    public String getDisplayName() {
        return this.displayName;
    }
    
    public int getDownloadTimeLeft() {
        return this.downloadTimeLeft;
    }
    
    public long getDownloadedTime() {
        return this.downloadedTime;
    }
    
    public List<Integer> getFreeIds() {
        return this.freeIds;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public String getMediaUrl() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.id);
        sb.append(".zip");
        return sb.toString();
    }
    
    public String getName() {
        return this.name;
    }
    
    public List<Integer> getOfferIds() {
        return this.offerIds;
    }
    
    public String getSaveHash() {
        return this.saveHash;
    }
    
    public int getSkanwordsCount() {
        return this.skanwordsCount;
    }
    
    public int getSkanwordsFinishedCount() {
        return this.finishedCount;
    }
    
    public int getSkanwordsStartedCount() {
        return this.startedCount;
    }
    
    public boolean hasDownloadedFiles() {
        if (this.getMediaUrl() == null) {
            return false;
        }
        final String[] list = new File(DownloaderArhiveFile.getUnzipPath(this.getMediaUrl())).list();
        return list != null && list.length >= 1;
    }
    
    public boolean hasNew() {
        return this.skanwordsCount - this.getSkanwordsStartedCount() - this.getSkanwordsFinishedCount() > 0;
    }
    
    public boolean isFinished() {
        return this.getSkanwordsFinishedCount() > 0;
    }
    
    public boolean isLocalData() {
        return this.isLocalData;
    }
    
    public boolean isProgressChanged() {
        return this.progressChanged;
    }
    
    public boolean isStarted() {
        return this.getSkanwordsStartedCount() > 0;
    }
    
    public boolean isToday() {
        return this.today;
    }
    
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }
    
    public void setDownloadTimeLeft(final int downloadTimeLeft) {
        this.downloadTimeLeft = downloadTimeLeft;
    }
    
    public void setDownloadedTime(final long downloadedTime) {
        this.downloadedTime = downloadedTime;
    }
    
    public void setFinishedCount(final Integer n) {
        this.finishedCount = n;
    }
    
    public void setFreeIds(final List<Integer> freeIds) {
        this.freeIds = freeIds;
    }
    
    public void setLocalData(final boolean isLocalData) {
        this.isLocalData = isLocalData;
    }
    
    public void setOfferIds(final List<Integer> offerIds) {
        this.offerIds = offerIds;
    }
    
    public void setProgressChanged(final boolean progressChanged) {
        this.progressChanged = progressChanged;
    }
    
    public void setSaveHash(final String saveHash) {
        this.saveHash = saveHash;
    }
    
    public void setStartedCount(final Integer n) {
        this.startedCount = n;
    }
    
    public void setToday(final boolean today) {
        this.today = today;
    }
    
    @Override
    public String toString() {
        return this.getDisplayName();
    }
    
    public static class SkanwordSetDate implements Serializable
    {
        private static final long serialVersionUID = 1L;
        private final int day;
        @SerializedName("today")
        private boolean isTooday;
        private final int mounth;
        @SerializedName("visible_from")
        private final int visibleFrom;
        private final int year;
        
        public SkanwordSetDate(final int visibleFrom, final int year, final int mounth, final int day, final boolean isTooday) {
            super();
            this.visibleFrom = visibleFrom;
            this.year = year;
            this.mounth = mounth;
            this.day = day;
            this.isTooday = isTooday;
        }
        
        public int getDay() {
            return this.day;
        }
        
        public int getMounth() {
            return this.mounth;
        }
        
        public int getVisibleFrom() {
            return this.visibleFrom;
        }
        
        public int getYear() {
            return this.year;
        }
        
        public boolean isTooday() {
            return this.isTooday;
        }
        
        public void setToday(final boolean isTooday) {
            this.isTooday = isTooday;
        }
    }
}
