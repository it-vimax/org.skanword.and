package org.skanword.and.datamanager;

import org.skanword.and.*;
import java.io.*;
import android.util.*;
import java.util.*;
import org.skanword.and.etc.*;
import org.skanword.and.scanwordgame.*;
import com.google.gson.*;
import com.google.gson.annotations.*;

public class UserUpdatesManager
{
    public static final String OFFER_NAME_ADCOLONY_VIDEO = "acv";
    public static final String OFFER_NAME_ADCOLONY_VIDEO_HASH = "acvh";
    public static final String OFFER_NAME_CHARTBOOST_VIDEO = "cbv";
    public static final String OFFER_NAME_CHARTBOOST_VIDEO_HASH = "cbvh";
    public static final String OFFER_NAME_FLURRY_VIDEO = "flv";
    public static final String OFFER_NAME_FLURRY_VIDEO_HASH = "flvh";
    public static final String OFFER_NAME_HEYZAP_VIDEO = "hzv";
    public static final String OFFER_NAME_HEYZAP_VIDEO_HASH = "hzvh";
    public static final String OFFER_NAME_UNITY_VIDEO = "unv";
    public static final String OFFER_NAME_UNITY_VIDEO_HASH = "unvh";
    public static final String OFFER_NAME_VUNGLE_VIDEO = "vgv";
    public static final String OFFER_NAME_VUNGLE_VIDEO_HASH = "vgvh";
    public static final String OFFER_SECRET_KEY = "cbc69bba7dcf617944325eaa0538d3a3";
    public static final String UPDATES_OBJECT_TIME = "_uot";
    public static final String UPDATES_SKAN_MASK = "ucm";
    public static final String UPDATES_USER_HINTS_BONUS = "uhb";
    public static final String UPDATES_USER_KEYWORD_BONUS = "uhkb";
    public static final String UPDATES_USER_OFFERS_USED = "uou";
    public static final String UPDATES_USER_STAT = "uus";
    public static final String USER_STATS_FIELD_DOUBLE_BONUS = "x2use";
    private static UserUpdatesManager instance;
    private UserUpdateData currentData;
    private Map<String, UserUpdateData> userDatas;
    
    protected UserUpdatesManager() {
        super();
    }
    
    public static UserUpdatesManager getInstance() {
        if (UserUpdatesManager.instance == null) {
            (UserUpdatesManager.instance = new UserUpdatesManager()).retrieveLocalData();
        }
        return UserUpdatesManager.instance;
    }
    
    private void retrieveLocalData() {
        final DataBaseManager databaseHelper = SmappsScanwords.getDatabaseHelper();
        if (databaseHelper != null) {
            final HashMap<String, UserUpdateData> hashMap = databaseHelper.retrieveGeneralData(1);
            if (hashMap == null) {
                this.userDatas = Collections.synchronizedMap(new HashMap<String, UserUpdateData>());
            }
            else {
                this.userDatas = Collections.synchronizedMap(hashMap);
            }
            this.currentData = SmappsScanwords.getDatabaseHelper().retrieveGeneralData(2);
            if (this.currentData == null) {
                this.currentData = new UserUpdateData();
            }
        }
    }
    
    private void saveData() {
        SmappsScanwords.getDatabaseHelper().storeGeneralData(this.currentData, 2);
    }
    
    public void bonusRecieved(final boolean b, final boolean b2, final boolean b3) {
        synchronized (this.currentData) {
            final StringBuilder sb = new StringBuilder();
            sb.append("  bonusRecieved   ");
            sb.append(b);
            Log.v("SkanwordsFunc", sb.toString());
            this.currentData.bonusRecieved(b, b2, b3);
            // monitorexit(this.currentData)
            this.saveData();
        }
    }
    
    public void clearUpdateDataForHashes(final List<String> list) {
        if (list != null) {
            if (list.size() < 1) {
                return;
            }
            synchronized (this.userDatas) {
                for (final String s : list) {
                    final Iterator<Map.Entry<String, UserUpdateData>> iterator2 = this.userDatas.entrySet().iterator();
                    while (iterator2.hasNext()) {
                        if (s.equals(iterator2.next().getKey())) {
                            iterator2.remove();
                        }
                    }
                }
                SmappsScanwords.getDatabaseHelper().storeUserUpdateData(new HashMap<String, UserUpdateData>((Map<? extends String, ? extends UserUpdateData>)this.userDatas));
            }
        }
    }
    
    public Map<String, UserUpdateData> getUserDatas() {
        return this.userDatas;
    }
    
    public void loggedIn(final LoginType loginType, final String s) {
        if (this.currentData != null) {
            final int n = UserUpdatesManager1.loginType[loginType.ordinal()];
            boolean b = true;
            String s2;
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n != 4) {
                            s2 = "";
                        }
                        else {
                            s2 = "notification_remote";
                        }
                    }
                    else {
                        s2 = "desktop";
                    }
                }
                else {
                    s2 = "nl";
                }
            }
            else {
                s2 = "background";
            }
            String format = s2;
            if (s != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("loggedIn notif suffix ");
                sb.append(s);
                Log.v("SkanwordsNotif", sb.toString());
                format = String.format("%s%s", s2, s);
            }
            while (true) {
                final int int1 = SmappsScanwords.getAppSharedPreferences("GENERAL_PREFERENCES_NAME").getInt("login_day", 0);
                final int value = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
                SmappsScanwords.getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putInt("login_day", value).commit();
                while (true) {
                    Label_0213: {
                        synchronized (this.currentData) {
                            final UserUpdateData currentData = this.currentData;
                            if (int1 != value) {
                                currentData.logginUser(format, b);
                                // monitorexit(this.currentData)
                                this.saveData();
                                return;
                            }
                            break Label_0213;
                        }
                    }
                    b = false;
                    continue;
                }
            }
        }
    }

    public void offerUsed(String string, String string2) {
        UserUpdateData userUpdateData = this.currentData;
        synchronized (userUpdateData) {
            this.currentData.userOfferUsed(string, string2);
        }
        this.saveData();
    }
    
    public Map<String, UserUpdateData> saveCurrentData() {
        final StringBuilder sb = new StringBuilder();
        sb.append("  saveTime");
        final UserUpdateData currentData = this.currentData;
        Serializable value;
        if (currentData != null) {
            value = currentData.hasData();
        }
        else {
            value = " no data";
        }
        sb.append(value);
        Log.v("SkanwordsFunc", sb.toString());
        final UserUpdateData currentData2 = this.currentData;
        if (currentData2 != null && (currentData2.hasData() || SmappsScanwords.getTime(false, false) + SmappsScanwords.getTime(true, false) >= 5)) {
            final long time = new Date().getTime();
            synchronized (this.currentData) {
                this.saveTime();
                this.currentData.saveTimeHash(time / 1000L);
                final Map<String, UserUpdateData> userDatas = this.userDatas;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("");
                sb2.append(time);
                userDatas.put(Utils.md5(sb2.toString()), this.currentData);
                SmappsScanwords.getDatabaseHelper().storeUserUpdateData(new HashMap<String, UserUpdateData>((Map<? extends String, ? extends UserUpdateData>)this.userDatas));
                this.currentData = new UserUpdateData();
                this.saveData();
            }
        }
        return new HashMap<String, UserUpdateData>(this.userDatas);
    }
    
    public void saveTime() {
        synchronized (this.currentData) {
            if (SkanwordGameActivity.isGameActive()) {
                this.currentData.getUserStats().setTimeCrossOffline(this.currentData.getUserStats().getTimeCrossOffline() + SmappsScanwords.getTime(false, true));
                this.currentData.getUserStats().setTimeCrossOnline(this.currentData.getUserStats().getTimeCrossOnline() + SmappsScanwords.getTime(true, true));
            }
            else {
                this.currentData.getUserStats().setTimeMainOffline(this.currentData.getUserStats().getTimeMainOffline() + SmappsScanwords.getTime(false, true));
                this.currentData.getUserStats().setTimeMainOnline(this.currentData.getUserStats().getTimeMainOnline() + SmappsScanwords.getTime(true, true));
            }
            final Gson gson = new Gson();
            final StringBuilder sb = new StringBuilder();
            sb.append(gson.toJson(this.currentData));
            sb.append("  saveTime");
            Log.v("SkanwordsFunc", sb.toString());
        }
    }
    
    public void updateScanwordMasks(final Skanword skanword) {
        synchronized (this.currentData) {
            final UserUpdateData currentData = this.currentData;
            final StringBuilder sb = new StringBuilder();
            sb.append("c");
            sb.append(skanword.getId());
            currentData.placeMask(sb.toString(), skanword.getAnswers(), skanword.getHints());
            // monitorexit(this.currentData)
            this.saveData();
        }
    }
    
    public void usedHint(final int n) {
        synchronized (this.currentData) {
            this.currentData.useHints(n);
            // monitorexit(this.currentData)
            this.saveData();
        }
    }
    
    public static class UserUpdateData implements Serializable
    {
        private static final long serialVersionUID = 346611263252355968L;
        private boolean keyword;
        @SerializedName("ucm")
        private HashMap<String, SkanwordMask> skanwordsMasks;
        @SerializedName("_uot")
        private Long time;
        @SerializedName("uhb")
        private HashMap<String, Object> userHintsBonus;
        @SerializedName("uhkb")
        private HashMap<String, Object> userKeywordBonus;
        @SerializedName("uou")
        private HashMap<String, String> userOffersUsed;
        @SerializedName("uus")
        private UserStats userStats;
        
        public UserUpdateData() {
            super();
            this.keyword = true;
        }
        
        public UserUpdateData(final HashMap<String, SkanwordMask> skanwordsMasks, final HashMap<String, String> userOffersUsed, final UserStats userStats, final HashMap<String, Object> userHintsBonus) {
            super();
            this.keyword = true;
            this.skanwordsMasks = skanwordsMasks;
            this.userOffersUsed = userOffersUsed;
            this.userStats = userStats;
            this.userHintsBonus = userHintsBonus;
        }
        
        public void bonusRecieved(final boolean b, final boolean b2, final boolean b3) {
            HashMap<String, Object> hashMap;
            if (b3) {
                hashMap = this.getUserHintsBonus();
            }
            else {
                hashMap = this.getUserKeywordBonus();
            }
            if (!b2) {
                hashMap.put("x2use", 0);
                return;
            }
            if (b) {
                hashMap.put("x2use", 1);
                return;
            }
            hashMap.put("x2use", 2);
        }
        
        public HashMap<String, SkanwordMask> getSkanwordsMasks() {
            if (this.skanwordsMasks == null) {
                this.skanwordsMasks = new HashMap<String, SkanwordMask>();
            }
            return this.skanwordsMasks;
        }
        
        public HashMap<String, Object> getUserHintsBonus() {
            if (this.userHintsBonus == null) {
                this.userHintsBonus = new HashMap<String, Object>();
            }
            return this.userHintsBonus;
        }
        
        public HashMap<String, Object> getUserKeywordBonus() {
            if (this.userKeywordBonus == null) {
                this.userKeywordBonus = new HashMap<String, Object>();
            }
            return this.userKeywordBonus;
        }
        
        public HashMap<String, String> getUserOffersUsed() {
            if (this.userOffersUsed == null) {
                this.userOffersUsed = new HashMap<String, String>();
            }
            return this.userOffersUsed;
        }
        
        public UserStats getUserStats() {
            if (this.userStats == null) {
                this.userStats = new UserStats();
            }
            return this.userStats;
        }
        
        public boolean hasData() {
            return this.getSkanwordsMasks().size() > 0 || this.getUserOffersUsed().size() > 0 || this.getUserStats().hasData() || this.getUserHintsBonus().size() > 0;
        }
        
        public void logginUser(final String s, final boolean b) {
            final UserStats userStats = this.getUserStats();
            userStats.setLoginsCount(userStats.getLoginsCount() + 1);
            if (b) {
                userStats.setLoginsCountUniq(userStats.getLoginsCountUniq() + 1);
            }
            Integer value;
            if ((value = userStats.getLoginsFrom().get(s)) == null) {
                value = 0;
            }
            userStats.getLoginsFrom().put(s, value + 1);
        }
        
        public void placeMask(final String s, final String s2, final String s3) {
            this.getSkanwordsMasks().put(s, new SkanwordMask(s2, s3));
        }
        
        public void saveTimeHash(final Long time) {
            this.time = time;
        }
        
        public void useHints(final int n) {
            final UserStats userStats = this.getUserStats();
            userStats.hintsCellsOpened++;
            userStats.hintsLettersUsed++;
        }
        
        public void userOfferUsed(String s, final String s2) {
            if (s == null) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("user offer used withtarget - ");
            sb.append(s);
            sb.append(" for service ");
            sb.append(s2);
            Log.v("", sb.toString());
            final String s3 = this.getUserOffersUsed().get(s2);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("offers of service  ");
            sb2.append(s3);
            Log.v("", sb2.toString());
            final String s4 = null;
            String string;
            if (s3 == null) {
                string = s;
            }
            else {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(s3);
                sb3.append(",");
                sb3.append(s);
                string = sb3.toString();
            }
            if (s2.equals("vgv")) {
                s = "vgvh";
            }
            else if (s2.equals("acv")) {
                s = "acvh";
            }
            else if (s2.equals("cbv")) {
                s = "cbvh";
            }
            else if (s2.equals("unv")) {
                s = "unvh";
            }
            else if (s2.equals("flv")) {
                s = "flvh";
            }
            else {
                s = s4;
                if (s2.equals("hzv")) {
                    s = "hzvh";
                }
            }
            if (s == null) {
                return;
            }
            this.userOffersUsed.put(s, Utils.md5(String.format("%s_%s", string, "cbc69bba7dcf617944325eaa0538d3a3")));
            this.userOffersUsed.put(s2, string);
        }
        
        public static class SkanwordMask implements Serializable
        {
            private static final long serialVersionUID = 1L;
            @SerializedName("a")
            private final String answerMask;
            @SerializedName("h")
            private final String hintsMask;
            
            public SkanwordMask(final String answerMask, final String hintsMask) {
                super();
                this.answerMask = answerMask;
                this.hintsMask = hintsMask;
            }
        }
        
        public static class UserStats implements Serializable
        {
            private static final long serialVersionUID = 8870257552488368218L;
            @SerializedName("lu")
            private int LoginsCountUniq;
            @SerializedName("hco")
            private int hintsCellsOpened;
            @SerializedName("hlu")
            private int hintsLettersUsed;
            @SerializedName("lc")
            private int loginsCount;
            @SerializedName("lf")
            private HashMap<String, Integer> loginsFrom;
            @SerializedName("tc_off")
            private int timeCrossOffline;
            @SerializedName("tc_on")
            private int timeCrossOnline;
            @SerializedName("tm_off")
            private int timeMainOffline;
            @SerializedName("tm_on")
            private int timeMainOnline;
            
            public UserStats() {
                super();
                this.loginsCount = 0;
                this.LoginsCountUniq = 0;
                this.hintsCellsOpened = 0;
                this.hintsLettersUsed = 0;
                this.timeCrossOnline = 0;
                this.timeCrossOffline = 0;
                this.timeMainOnline = 0;
                this.timeMainOffline = 0;
                this.loginsFrom = new HashMap<String, Integer>();
            }
            
            public UserStats(final int loginsCount, final int loginsCountUniq, final HashMap<String, Integer> loginsFrom, final int hintsCellsOpened, final int hintsLettersUsed, final int timeCrossOnline, final int timeCrossOffline, final int timeMainOnline, final int timeMainOffline) {
                super();
                this.loginsCount = 0;
                this.LoginsCountUniq = 0;
                this.hintsCellsOpened = 0;
                this.hintsLettersUsed = 0;
                this.timeCrossOnline = 0;
                this.timeCrossOffline = 0;
                this.timeMainOnline = 0;
                this.timeMainOffline = 0;
                this.loginsCount = loginsCount;
                this.LoginsCountUniq = loginsCountUniq;
                this.loginsFrom = loginsFrom;
                this.hintsCellsOpened = hintsCellsOpened;
                this.hintsLettersUsed = hintsLettersUsed;
                this.timeCrossOffline = timeCrossOffline;
                this.timeCrossOnline = timeCrossOnline;
                this.timeMainOffline = timeMainOffline;
                this.timeMainOnline = timeMainOnline;
            }
            
            static /* synthetic */ int access008(final UserStats userStats) {
                return userStats.hintsCellsOpened++;
            }
            
            static /* synthetic */ int access108(final UserStats userStats) {
                return userStats.hintsLettersUsed++;
            }
            
            public int getHintsCellsOpened() {
                return this.hintsCellsOpened;
            }
            
            public int getHintsLettersUsed() {
                return this.hintsLettersUsed;
            }
            
            public int getLoginsCount() {
                return this.loginsCount;
            }
            
            public int getLoginsCountUniq() {
                return this.LoginsCountUniq;
            }
            
            public HashMap<String, Integer> getLoginsFrom() {
                if (this.loginsFrom == null) {
                    this.loginsFrom = new HashMap<String, Integer>();
                }
                return this.loginsFrom;
            }
            
            public int getTimeCrossOffline() {
                return this.timeCrossOffline;
            }
            
            public int getTimeCrossOnline() {
                return this.timeCrossOnline;
            }
            
            public int getTimeMainOffline() {
                return this.timeMainOffline;
            }
            
            public int getTimeMainOnline() {
                return this.timeMainOnline;
            }
            
            public boolean hasData() {
                return this.loginsCount + this.hintsCellsOpened > 0 || this.timeCrossOffline + this.timeCrossOnline + this.timeMainOffline + this.timeMainOnline >= 5;
            }
            
            public void setHintsCellsOpened(final int hintsCellsOpened) {
                this.hintsCellsOpened = hintsCellsOpened;
            }
            
            public void setHintsLettersUsed(final int hintsLettersUsed) {
                this.hintsLettersUsed = hintsLettersUsed;
            }
            
            public void setLoginsCount(final int loginsCount) {
                this.loginsCount = loginsCount;
            }
            
            public void setLoginsCountUniq(final int loginsCountUniq) {
                this.LoginsCountUniq = loginsCountUniq;
            }
            
            public void setTimeCrossOffline(final int timeCrossOffline) {
                this.timeCrossOffline = timeCrossOffline;
            }
            
            public void setTimeCrossOnline(final int timeCrossOnline) {
                this.timeCrossOnline = timeCrossOnline;
            }
            
            public void setTimeMainOffline(final int timeMainOffline) {
                this.timeMainOffline = timeMainOffline;
            }
            
            public void setTimeMainOnline(final int timeMainOnline) {
                this.timeMainOnline = timeMainOnline;
            }
        }
    }

    static class UserUpdatesManager1 {
        static final int[] loginType;

        static {
            loginType = new int[UserUpdatesManager.LoginType.values().length];
            try {
                UserUpdatesManager1.loginType[UserUpdatesManager.LoginType.LOGGIN_APP_FOREGROUND.ordinal()] = 1;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                UserUpdatesManager1.loginType[UserUpdatesManager.LoginType.LOGGIN_LOCAL_NOTIFICATION.ordinal()] = 2;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                UserUpdatesManager1.loginType[UserUpdatesManager.LoginType.LOGGIN_APP_START.ordinal()] = 3;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                UserUpdatesManager1.loginType[UserUpdatesManager.LoginType.LOGGIN_PUSH_NOTIFICATION.ordinal()] = 4;
            }
            catch (NoSuchFieldError noSuchFieldError) {
            }
        }
    }

    public enum LoginType {
        LOGGIN_LOCAL_NOTIFICATION,
        LOGGIN_PUSH_NOTIFICATION,
        LOGGIN_APP_START,
        LOGGIN_APP_FOREGROUND

    }

}
