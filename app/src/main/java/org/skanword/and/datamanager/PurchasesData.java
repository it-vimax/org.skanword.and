package org.skanword.and.datamanager;

import java.io.*;
import android.util.*;
import java.util.*;

public class PurchasesData implements Serializable
{
    private static final long serialVersionUID = 1457480924490174550L;
    private List<String> purchasesMade;
    private List<String> purchasesRestored;
    
    public PurchasesData() {
        super();
        this.purchasesMade = new ArrayList<String>();
        this.purchasesRestored = new ArrayList<String>();
    }
    
    public PurchasesData(final List<String> list, final List<String> list2) {
        super();
        this.purchasesMade = list;
        this.purchasesRestored = list;
        final StringBuilder sb = new StringBuilder();
        sb.append(" purchases  restored ");
        sb.append(list);
        Log.v("", sb.toString());
    }
    
    public void addPurchase(final String s, final Long n, final boolean b) {
        if (b) {
            this.purchasesRestored.add(s);
            return;
        }
        this.purchasesMade.add(s);
    }
    
    public void clearPurcheses(final List<String> list) {
        for (final String s : list) {
            this.purchasesMade.remove(s);
            this.purchasesRestored.remove(s);
        }
    }
    
    public List<String> getPurchases() {
        return this.purchasesMade;
    }
    
    public List<String> getPurchasesRestored() {
        return this.purchasesRestored;
    }
}
