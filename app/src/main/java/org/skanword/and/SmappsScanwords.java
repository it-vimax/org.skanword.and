package org.skanword.and;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventDispatcher;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;

import com.nostra13.universalimageloader.cache.disc.naming.FileNameGenerator;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.MemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.skanword.and.datamanager.DataBaseManager;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.Foreground;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.DailyBonusObject;
import org.skanword.and.network.MainNetworkManager;

public class SmappsScanwords extends Application {
    public static final String APPLICATION_TIMER_EVENT = "APPLICATION_TIMER_EVENT";
    public static File CACHE_DIR;
    public static File CROSSWORDS_DIR;
    public static File DEBUG_DIR;
    public static final String DEVELOPER_EMAIL = "support@socialgames.bz";
    public static final Logger LOG;
    public static final String NET_ID = "android";
    private static final int PREFERENCES_VERSION = 4;
    private static final String PREFERENCES_VERSION_PREF = "preferencesVersion";
    public static File QUARANTINE_DIR;
    public static File TEMP_DIR;
    private static String advertisingId;
    private static DataBaseManager dbHelper;
    private static ExecutorService executorService;
    public static SmappsScanwords instance;
    private static Context mContext;
    private static EventDispatcher mEventsDispatcher;
    private static int mTimeOffline;
    private static int mTimeOnline;
    private static ScheduledExecutorService scheduleExecutorService;
    private static String vendorId;
    private TimerTask mTimerTask;
    private volatile SmappsScanwords.TrackerSendHelper mTracker;
    private AlertDialog mVersionInformDialog;
    private AlertDialog mVersionLockDialog;
    private Timer timerExecutor;

    static {
        LOG = Logger.getLogger("org.sknanwords.android");
        SmappsScanwords.vendorId = "";
        SmappsScanwords.advertisingId = "";
        SmappsScanwords.scheduleExecutorService = Executors.newSingleThreadScheduledExecutor();
        SmappsScanwords.executorService = Executors.newFixedThreadPool(3);
        SmappsScanwords.instance = null;
    }

    public SmappsScanwords() {
        super();
        this.mVersionInformDialog = null;
        this.mVersionLockDialog = null;
    }

    static /* synthetic */ String access002(final String advertisingId) {
        return SmappsScanwords.advertisingId = advertisingId;
    }

    static /* synthetic */ EventDispatcher access100() {
        return SmappsScanwords.mEventsDispatcher;
    }

    static /* synthetic */ int access208() {
        final int mTimeOnline = SmappsScanwords.mTimeOnline;
        SmappsScanwords.mTimeOnline = mTimeOnline + 1;
        return mTimeOnline;
    }

    static /* synthetic */ int access308() {
        final int mTimeOffline = SmappsScanwords.mTimeOffline;
        SmappsScanwords.mTimeOffline = mTimeOffline + 1;
        return mTimeOffline;
    }

    static /* synthetic */ AlertDialog access400(final SmappsScanwords smappsScanwords) {
        return smappsScanwords.mVersionInformDialog;
    }

    static /* synthetic */ AlertDialog access402(final SmappsScanwords smappsScanwords, final AlertDialog mVersionInformDialog) {
        return smappsScanwords.mVersionInformDialog = mVersionInformDialog;
    }

    static /* synthetic */ AlertDialog access500(final SmappsScanwords smappsScanwords) {
        return smappsScanwords.mVersionLockDialog;
    }

    static /* synthetic */ AlertDialog access502(final SmappsScanwords smappsScanwords, final AlertDialog mVersionLockDialog) {
        return smappsScanwords.mVersionLockDialog = mVersionLockDialog;
    }

    private void clearData() {
        this.deleteDatabase("smappsscanword");
        deleteRecursive(SmappsScanwords.CROSSWORDS_DIR, true);
    }

    public static void deleteRecursive(final File file, final boolean b) {
        if (file.isDirectory()) {
            final File[] listFiles = file.listFiles();
            for (int length = listFiles.length, i = 0; i < length; ++i) {
                deleteRecursive(listFiles[i], false);
            }
        }
        if (!b) {
            file.delete();
        }
    }

    public static SharedPreferences getAppSharedPreferences(final String s) {
        return getAppSharedPreferences(s, 0);
    }

    public static SharedPreferences getAppSharedPreferences(final String s, final int n) {
        synchronized (SmappsScanwords.class) {
            return getContext().getSharedPreferences(s, n);
        }
    }

    public static SharedPreferences.Editor getAppSharedPreferencesEditor(final String s) {
        return getAppSharedPreferences(s).edit();
    }

    public static Context getContext() {
        return SmappsScanwords.mContext;
    }

    public static DataBaseManager getDatabaseHelper() {
        return getDatabaseHelper(getContext());
    }

    public static DataBaseManager getDatabaseHelper(final Context context) {
        if (SmappsScanwords.dbHelper == null && context != null) {
            SmappsScanwords.dbHelper = new DataBaseManager(context);
        }
        return SmappsScanwords.dbHelper;
    }

    public static EventDispatcher getEventsDispatcher() {
        return SmappsScanwords.mEventsDispatcher;
    }

    public static ExecutorService getExecutorService() {
        return SmappsScanwords.executorService;
    }

    public static ScheduledExecutorService getScheduleExecutorService() {
        return SmappsScanwords.scheduleExecutorService;
    }

    public static double getScreenSizeInInches(final DisplayMetrics displayMetrics) {
        return Math.hypot(displayMetrics.widthPixels / displayMetrics.xdpi, displayMetrics.heightPixels / displayMetrics.ydpi);
    }

    public static int getTime(final boolean b, final boolean b2) {
        int n;
        if (b) {
            n = SmappsScanwords.mTimeOnline;
            if (b2) {
                SmappsScanwords.mTimeOnline = 0;
                return n;
            }
        } else {
            final int n2 = n = SmappsScanwords.mTimeOffline;
            if (b2) {
                SmappsScanwords.mTimeOffline = 0;
                n = n2;
            }
        }
        return n;
    }

    public static String getVendorId() {
        return SmappsScanwords.vendorId;
    }

    public static boolean isTabletish(final DisplayMetrics displayMetrics) {
        final int sdk_INT = Build.VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT < 11) {
            return false;
        }
        if (getScreenSizeInInches(displayMetrics) > 9.0) {
            b = true;
        }
        return b;
    }

    public static boolean makeDirs() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return false;
        }
        final File[] array = {SmappsScanwords.CROSSWORDS_DIR, SmappsScanwords.TEMP_DIR, SmappsScanwords.QUARANTINE_DIR, SmappsScanwords.DEBUG_DIR};
        for (int length = array.length, i = 0; i < length; ++i) {
            final File file = array[i];
            if (!file.isDirectory() && !file.mkdirs()) {
                final Logger log = SmappsScanwords.LOG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to create directory tree: ");
                sb.append(file);
                log.warning(sb.toString());
                return false;
            }
        }
        return true;
    }

    private void migratePreferences(final SharedPreferences sharedPreferences, final int n) {
        final Logger log = SmappsScanwords.LOG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Upgrading preferences from version ");
        sb.append(n);
        sb.append(" to to version ");
        sb.append(4);
        log.info(sb.toString());
        final SharedPreferences.Editor edit = sharedPreferences.edit();
        while (true) {
            Label_0166: {
                Label_0130: {
                    if (n != 0) {
                        if (n != 1) {
                            if (n == 2) {
                                break Label_0130;
                            }
//                            if (n != 3) {
//                                break Label_0191;
//                            }
                            break Label_0166;
                        }
                    }
                    else {
                        edit.putBoolean("enableIndividualDownloadNotifications", sharedPreferences.getBoolean("suppressMessages", false) ^ true);
                    }
                    edit.putBoolean("showRevealedLetters", sharedPreferences.getBoolean("suppressHints", false) ^ true);
                }
                String s;
                if (sharedPreferences.getBoolean("forceKeyboard", false)) {
                    s = "SHOW";
                }
                else {
                    s = "AUTO";
                }
                edit.putString("showKeyboard", s);
                try {
                    edit.putString("clueSize", Integer.toString(sharedPreferences.getInt("clueSize", 12)));
                    edit.putInt("preferencesVersion", 4);
                    edit.commit();
                }
                catch (ClassCastException ex) {
                    continue;
                }
            }
            break;
        }
    }

    private void retrieveAdvertisingId() {
        ((AsyncTask)new SmappsScanwords.SmappsScanwords2(this)).execute((Object[])new Void[0]);
    }

    private static void saveLogFile() {
    }

    public static void sendCaughtException(final Exception ex) {
        SmappsScanwords.instance.getTrackerSendHelper().send(new HitBuilders.ExceptionBuilder().setDescription(new StandardExceptionParser(getContext(), (Collection) null).getDescription(Thread.currentThread().getName(), (Throwable) ex)).setFatal(false).build());
    }

    @SuppressLint({"WorldReadableFiles"})
    public static Intent sendDebug(Context var0) {
        File var1 = new File(var0.getFilesDir(), "debug.zip");
        if (var1.exists()) {
            var1.delete();
        }

        if (!DEBUG_DIR.exists()) {
            Logger var14 = LOG;
            StringBuilder var16 = new StringBuilder();
            var16.append("Can't send debug package, ");
            var16.append(DEBUG_DIR);
            var16.append(" doesn't exist");
            var14.warning(var16.toString());
            return null;
        } else {
            saveLogFile();

            label70: {
                IOException var10000;
                label69: {
                    ZipOutputStream var11;
                    boolean var10001;
                    try {
                        var11 = new ZipOutputStream(var0.openFileOutput("debug.zip", Context.MODE_WORLD_READABLE));
                    } catch (IOException var10) {
                        var10000 = var10;
                        var10001 = false;
                        break label69;
                    }

                    try {
                        break label70;
                    } finally {
                        label63:
                        try {
                            zipDir(DEBUG_DIR.getAbsolutePath(), var11);
                            var11.close();
                        } catch (IOException var8) {
                            var10000 = var8;
                            var10001 = false;
                            break label63;
                        }
                    }
                }

                IOException var12 = var10000;
                var12.printStackTrace();
                return null;
            }

            Intent var13 = new Intent("android.intent.action.SEND");
            var13.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            var13.putExtra("android.intent.extra.EMAIL", new String[]{"support@socialgames.bz"});
            var13.putExtra("android.intent.extra.SUBJECT", "Words With Crosses Debug Package");
            Uri var15 = Uri.fromFile(var1);
            var13.putExtra("android.intent.extra.STREAM", var15);
            Logger var2 = LOG;
            StringBuilder var3 = new StringBuilder();
            var3.append("Sending debug info: ");
            var3.append(var15);
            var2.info(var3.toString());
            var13.setType("application/octet-stream");
            return var13;
        }
    }

    public static void sendGAEvent(final SmappsScanwords.ActionType actionType, final String s) {
        if (SmappsScanwords.instance != null) {
            if (getExecutorService() == null) {
                return;
            }
            getExecutorService().execute((Runnable) new SmappsScanwords.SmappsScanwords7(actionType, s));
        }
    }

    private void showVersionInformAlert() {
        Log.v("SkanwordsFunc", "showVersionInformAlert");
        if (this.mVersionLockDialog != null) {
            return;
        }
        TopActivityManager.get().getTopActivity().runOnUiThread((Runnable) new SmappsScanwords.SmappsScanwords6(this));
    }

    private void showVersionLockAlert() {
        Log.v("SkanwordsFunc", "showVersionLockAlert");
        TopActivityManager.get().getTopActivity().runOnUiThread((Runnable) new SmappsScanwords.SmappsScanwords5(this));
    }

    public static void zipDir(final String s, final ZipOutputStream zipOutputStream) throws IOException {
        final File file = new File(s);
        final String[] list = file.list();
        final byte[] array = new byte[4096];
        for (int i = 0; i < list.length; ++i) {
            final File file2 = new File(file, list[i]);
            if (file2.isDirectory()) {
                zipDir(file2.getPath(), zipOutputStream);
            } else {
                final FileInputStream fileInputStream = new FileInputStream(file2);
                zipOutputStream.putNextEntry(new ZipEntry(file2.getPath()));
                while (true) {
                    final int read = fileInputStream.read(array);
                    if (read == -1) {
                        break;
                    }
                    zipOutputStream.write(array, 0, read);
                }
                fileInputStream.close();
            }
        }
    }

    protected void attachBaseContext(final Context context) {
        super.attachBaseContext(context);
        MultiDex.install((Context) this);
    }

    public boolean checkVersions() {
        PackageInfo packageInfo;
        try {
            packageInfo = getContext().getPackageManager().getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
            packageInfo = null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(" Checkversions");
        sb.append(packageInfo != null);
        sb.append("  ");
        sb.append(MainDataManager.getInstance().getVersions() != null);
        sb.append("  ");
        sb.append(TopActivityManager.get().getTopActivity() != null);
        Log.v("SkanwordsFunc", sb.toString());
        if (packageInfo != null && MainDataManager.getInstance().getVersions() != null) {
            Log.v("SkanwordsFunc", " Checkversions can check build ");
            if (TopActivityManager.get().getTopActivity() != null) {
                if (packageInfo.versionCode <= MainDataManager.getInstance().getVersions().getLock().getBuild()) {
                    this.showVersionLockAlert();
                    return true;
                }
                if (packageInfo.versionCode <= MainDataManager.getInstance().getVersions().getInform().getBuild()) {
                    this.showVersionInformAlert();
                    return true;
                }
            } else {
                getScheduleExecutorService().schedule((Runnable) new SmappsScanwords.SmappsScanwords4(this), 2L, TimeUnit.SECONDS);
            }
        }
        return false;
    }

    public String getAdvertisingId() {
        return SmappsScanwords.advertisingId;
    }

    public SmappsScanwords.TrackerSendHelper getTrackerSendHelper() {
        synchronized (this) {
            if (this.mTracker == null) {
                final GoogleAnalytics instance = GoogleAnalytics.getInstance((Context) this);
                this.mTracker = new SmappsScanwords.TrackerSendHelper(instance.newTracker(2131951616));
                instance.enableAutoActivityReports((Application) this);
            }
            return this.mTracker;
        }
    }

    public void navigateToGooglePlay() {
        String var1 = this.getPackageName();

        StringBuilder var2;
        try {
            var2 = new StringBuilder();
            var2.append("market://details?id=");
            var2.append(var1);
            Intent var5 = new Intent("android.intent.action.VIEW", Uri.parse(var2.toString()));
            var5.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            this.startActivity(var5);
        } catch (ActivityNotFoundException var3) {
            var2 = new StringBuilder();
            var2.append("https://play.google.com/store/apps/details?id=");
            var2.append(var1);
            Intent var4 = new Intent("android.intent.action.VIEW", Uri.parse(var2.toString()));
            var4.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            this.startActivity(var4);
        }
    }

    public void onCreate() {
        super.onCreate();
        final Configuration configuration = this.getResources().getConfiguration();
        configuration.fontScale = 1.0f;
        this.getResources().updateConfiguration(configuration, this.getResources().getDisplayMetrics());
        SmappsScanwords.instance = this;
        SmappsScanwords.mContext = (Context) this;
        final boolean boolean1 = getAppSharedPreferences("GENERAL_PREFERENCES_NAME").getBoolean("first_app_start", true);
        TopActivityManager.init((Application) this);
        Foreground.init((Application) this);
        Foreground.get().addListener((Foreground.Listener) new SmappsScanwords.SmappsScanwords1(this, boolean1));
        getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putBoolean("first_app_start", false).commit();
        final SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences((Context) this);
        final int int1 = defaultSharedPreferences.getInt("preferencesVersion", 0);
        if (int1 != 4) {
            this.migratePreferences(defaultSharedPreferences, int1);
        }
        final File externalStorageDirectory = Environment.getExternalStorageDirectory();
        final StringBuilder sb = new StringBuilder();
        sb.append("Android/data/");
        sb.append(this.getPackageName());
        sb.append("/files");
        final File file = new File(externalStorageDirectory, sb.toString());
        SmappsScanwords.CROSSWORDS_DIR = new File(file, "crosswords");
        SmappsScanwords.TEMP_DIR = new File(file, "temp");
        SmappsScanwords.QUARANTINE_DIR = new File(file, "quarantine");
        SmappsScanwords.CACHE_DIR = this.getCacheDir();
        SmappsScanwords.DEBUG_DIR = new File(SmappsScanwords.CACHE_DIR, "debug");
        makeDirs();
        if (!SmappsScanwords.DEBUG_DIR.isDirectory() && !SmappsScanwords.DEBUG_DIR.mkdirs()) {
            final Logger log = SmappsScanwords.LOG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to create directory tree: ");
            sb2.append(SmappsScanwords.DEBUG_DIR);
            log.warning(sb2.toString());
        } else {
            final File file2 = new File(SmappsScanwords.DEBUG_DIR, "device.txt");
            try {
                final PrintWriter printWriter = new PrintWriter(file2);
                try {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("VERSION INT: ");
                    sb3.append(Build.VERSION.SDK_INT);
                    printWriter.println(sb3.toString());
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("VERSION RELEASE: ");
                    sb4.append(Build.VERSION.RELEASE);
                    printWriter.println(sb4.toString());
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("MODEL: ");
                    sb5.append(Build.MODEL);
                    printWriter.println(sb5.toString());
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("DEVICE: ");
                    sb6.append(Build.DEVICE);
                    printWriter.println(sb6.toString());
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append("DISPLAY: ");
                    sb7.append(Build.DISPLAY);
                    printWriter.println(sb7.toString());
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append("MANUFACTURER: ");
                    sb8.append(Build.MANUFACTURER);
                    printWriter.println(sb8.toString());
                } finally {
                    printWriter.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (!getAppSharedPreferences("GENERAL_PREFERENCES_NAME").getString("db_version", "").equals("1.0.4")) {
            this.clearData();
            getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putString("db_version", "1.0.4").commit();
        }
        SmappsScanwords.mEventsDispatcher = new EventDispatcher();
        MainNetworkManager.getInstance().onCreate(this.getApplicationContext());
        ImageLoader.getInstance().init(new ImageLoaderConfiguration.Builder(this.getApplicationContext()).threadPriority(3).denyCacheImageMultipleSizesInMemory().memoryCache((MemoryCache) new LruMemoryCache(2097152)).memoryCacheSize(2097152).memoryCacheSizePercentage(13).denyCacheImageMultipleSizesInMemory().diskCacheFileNameGenerator((FileNameGenerator) new Md5FileNameGenerator()).diskCacheSize(52428800).tasksProcessingOrder(QueueProcessingType.LIFO).writeDebugLogs().build());
        this.retrieveAdvertisingId();
    }

    public void onTerminate() {
        super.onTerminate();
        InAppManager.getInstance().clear();
        MainNetworkManager.getInstance().clear();
    }

    protected void pauseTimer() {
        final Timer timerExecutor = this.timerExecutor;
        if (timerExecutor != null) {
            timerExecutor.cancel();
        }
    }

    protected void resumeTimer() {
        this.pauseTimer();
        this.mTimerTask = (TimerTask) new SmappsScanwords.SmappsScanwords3(this);
        (this.timerExecutor = new Timer()).scheduleAtFixedRate(this.mTimerTask, 0L, 1000L);
    }

    public void startDataApp() {
        SmappsScanwords.dbHelper = new DataBaseManager((Context) this);
        SmappsScanwords.vendorId = Utils.getAndroidUniqueDeviceId();
        InAppManager.getInstance();
    }

    // ***************** Classes
    public static class TrackerSendHelper
    {
        private final Tracker mTracker;

        TrackerSendHelper(final Tracker mTracker) {
            super();
            (this.mTracker = mTracker).enableAdvertisingIdCollection(true);
        }

        public void send(final String screenName, final Map<String, String> map) {
            synchronized (this) {
                this.mTracker.setScreenName(screenName);
                this.mTracker.send((Map)map);
            }
        }

        public void send(final Map<String, String> map) {
            synchronized (this) {
                this.mTracker.send((Map)map);
            }
        }
    }

    class SmappsScanwords2 extends AsyncTask {
        // FF: synthetic field
        final SmappsScanwords this0;

        SmappsScanwords2(SmappsScanwords var1) {
            this.this0 = var1;
        }

        protected Object doInBackground(Object[] var1) {
            Info var6;
            label21: {
                try {
                    var6 = AdvertisingIdClient.getAdvertisingIdInfo(this.this0.getApplicationContext());
                    break label21;
                } catch (GooglePlayServicesNotAvailableException var3) {
                    var3.printStackTrace();
                } catch (GooglePlayServicesRepairableException var4) {
                    var4.printStackTrace();
                } catch (IOException var5) {
                    var5.printStackTrace();
                }

                var6 = null;
            }

            try {
                String var7 = var6.getId();
                return var7;
            } catch (NullPointerException var2) {
                var2.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(String var1) {
            SmappsScanwords.access002(var1);
        }
    }

    static final class SmappsScanwords7 implements Runnable {
        final /* synthetic */ String valaction;
        final /* synthetic */ SmappsScanwords.ActionType valcategory;

        SmappsScanwords7(final SmappsScanwords.ActionType valcategory, final String valaction) {
            super();
            this.valcategory = valcategory;
            this.valaction = valaction;
        }

        @Override
        public void run() {
            String category;
            if (this.valcategory == SmappsScanwords.ActionType.USER_ACTION) {
                category = "user_category";
            }
            else {
                category = "app_category";
            }
            SmappsScanwords.instance.getTrackerSendHelper().send(new HitBuilders.EventBuilder().setCategory(category).setAction(this.valaction).build());
        }
    }

    public enum ActionType {
        APP_ACTION,
        USER_ACTION;
    }

    class SmappsScanwords6 implements Runnable {
        final /* synthetic */ SmappsScanwords this0;

        SmappsScanwords6(final SmappsScanwords this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (SmappsScanwords.access400(this.this0) == null) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)TopActivityManager.get().getTopActivity(), R.attr.dialogTheme);
                alertDialogBuilder.setMessage((CharSequence)MainDataManager.getInstance().getVersions().getInform().getText()).setPositiveButton("Обновить", (DialogInterface.OnClickListener)new SmappsScanwords6.SmappsScanwords61(this)).setNegativeButton("Позже", (DialogInterface.OnClickListener)null);
                SmappsScanwords.access402(this.this0, alertDialogBuilder.create());
            }
            if (!SmappsScanwords.access400(this.this0).isShowing()) {
                SmappsScanwords.access400(this.this0).show();
            }
        }

        class SmappsScanwords61 implements DialogInterface.OnClickListener {
            final /* synthetic */ SmappsScanwords6 this1;

            SmappsScanwords61(final SmappsScanwords6 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.this1.this0.navigateToGooglePlay();
            }
        }
    }

    class SmappsScanwords1 implements Foreground.Listener {
        boolean firstRequestSync = true;
        boolean firstStart = true;
        final /* synthetic */ SmappsScanwords this0;
        final /* synthetic */ boolean valfirstAppStart;

        SmappsScanwords1(final SmappsScanwords this0, final boolean valfirstAppStart) {
            super();
            this.this0 = this0;
            this.valfirstAppStart = valfirstAppStart;
        }

        public void onBecameBackground(final Activity activity) {
            if (MainMenuActivity.hasPermissions()) {
                Log.v("SkanwordsFunc", "onBecameBackground");
                this.this0.pauseTimer();
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "app_minimize");
            }
        }

        public void onBecameForeground(final Activity activity) {
            if (MainMenuActivity.instance != null && MainMenuActivity.hasPermissions()) {
                Log.v("SkanwordsFunc", "onBecameForeground");
                final Bundle openIntentExtras = MainMenuActivity.instance.getOpenIntentExtras();
                final boolean b = openIntentExtras != null && openIntentExtras.getBoolean("local_notification", false);
                final boolean b2 = openIntentExtras != null && openIntentExtras.getBoolean("push_notification", false);
                final StringBuilder sb = new StringBuilder();
                sb.append("onBecameForeground");
                sb.append(openIntentExtras);
                Log.v("SkanwordsNotif", sb.toString());
                if (b) {
                    UserUpdatesManager.getInstance().loggedIn(UserUpdatesManager.LoginType.LOGGIN_LOCAL_NOTIFICATION, openIntentExtras.getString("i", (String)null));
                }
                else if (b2) {
                    UserUpdatesManager.getInstance().loggedIn(UserUpdatesManager.LoginType.LOGGIN_PUSH_NOTIFICATION, openIntentExtras.getString("i", (String)null));
                }
                else if (this.firstStart) {
                    UserUpdatesManager.getInstance().loggedIn(UserUpdatesManager.LoginType.LOGGIN_APP_START, (String)null);
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "app_enter");
                }
                else {
                    UserUpdatesManager.getInstance().loggedIn(UserUpdatesManager.LoginType.LOGGIN_APP_FOREGROUND, (String)null);
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "app_recover");
                }
                if ((!this.firstStart || MainNetworkManager.getInstance().getUserAuth() == null) && (this.firstStart || MainNetworkManager.getInstance().getUserAuth() != null) && MainNetworkManager.getInstance().getUserAuth() == null && this.valfirstAppStart) {
                    this.this0.checkVersions();
                }
                else {
                    MainNetworkManager.getInstance().requestConfigExecWithCompleteBlock((MainNetworkManager.RequestCompletionBlock)new SmappsScanwords1.SmappsScanwords11(this));
                }
                this.firstStart = false;
                this.this0.resumeTimer();
            }
        }

        class SmappsScanwords11 implements MainNetworkManager.RequestCompletionBlock {
            final /* synthetic */ SmappsScanwords1 this1;

            SmappsScanwords11(final SmappsScanwords1 this1) {
                super();
                this.this1 = this1;
            }

            public void complete(final boolean b) {
                if (b) {
                    MainNetworkManager.getInstance().requestSync((MainNetworkManager.RequestCompletionBlock)null);
                    if (this.this1.firstRequestSync) {
                        this.this1.firstRequestSync = false;
                        final DailyBonusObject dailyBonus = MainDataManager.getInstance(SmappsScanwords.getContext()).getDailyBonus();
                        if (dailyBonus != null) {
                            dailyBonus.setScratchedPosition(0);
                        }
                    }
                }
                else {
                    this.this1.this0.checkVersions();
                }
            }
        }
    }

    class SmappsScanwords3 extends TimerTask {
        final /* synthetic */ SmappsScanwords this0;

        SmappsScanwords3(final SmappsScanwords this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            SmappsScanwords.access100().dispatchEvent((Event)new SimpleEvent("APPLICATION_TIMER_EVENT"));
            if (Foreground.get().isForeground()) {
                if (MainNetworkManager.getInstance().hasNetworkConnection()) {
                    SmappsScanwords.access208();
                    return;
                }
                SmappsScanwords.access308();
            }
        }
    }

    class SmappsScanwords4 implements Runnable {
        // FF: synthetic field
        final SmappsScanwords this0;

        SmappsScanwords4(SmappsScanwords var1) {
            this.this0 = var1;
        }

        public void run() {
            this.this0.checkVersions();
        }
    }


    class SmappsScanwords5 implements Runnable {
        final /* synthetic */ SmappsScanwords this0;

        SmappsScanwords5(final SmappsScanwords this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (SmappsScanwords.access400(this.this0) != null && SmappsScanwords.access400(this.this0).isShowing()) {
                SmappsScanwords.access400(this.this0).dismiss();
            }
            if (SmappsScanwords.access500(this.this0) == null) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)TopActivityManager.get().getTopActivity(), R.attr.dialogTheme);
                alertDialogBuilder.setMessage((CharSequence)MainDataManager.getInstance().getVersions().getLock().getText());
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("Обновить", (DialogInterface.OnClickListener)null);
                SmappsScanwords.access502(this.this0, alertDialogBuilder.create());
            }
            if (!SmappsScanwords.access500(this.this0).isShowing()) {
                SmappsScanwords.access500(this.this0).show();
                SmappsScanwords.access500(this.this0).getButton(-1).setOnClickListener((View.OnClickListener)new SmappsScanwords5.SmappsScanwords51(this));
            }
        }

        class SmappsScanwords51 implements View.OnClickListener {
            final /* synthetic */ SmappsScanwords5 this1;

            SmappsScanwords51(final SmappsScanwords5 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final View view) {
                this.this1.this0.navigateToGooglePlay();
            }
        }
    }
}
