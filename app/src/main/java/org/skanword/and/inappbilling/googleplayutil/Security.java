package org.skanword.and.inappbilling.googleplayutil;

import android.util.*;
import java.security.spec.*;
import java.security.*;
import android.text.*;

public class Security
{
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "IABUtil/Security";
    
    public Security() {
        super();
    }
    
    public static PublicKey generatePublicKey(final String s) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(s)));
        }
        catch (Base64DecoderException ex) {
            Log.e("IABUtil/Security", "Base64 decoding failed.");
            throw new IllegalArgumentException(ex);
        }
        catch (InvalidKeySpecException ex2) {
            Log.e("IABUtil/Security", "Invalid key specification.");
            throw new IllegalArgumentException(ex2);
        }
        catch (NoSuchAlgorithmException ex3) {
            throw new RuntimeException(ex3);
        }
    }

    public static boolean verify(PublicKey publicKey, String string, String string2) {
        try {
            Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initVerify(publicKey);
            signature.update(string.getBytes());
            if (signature.verify(Base64.decode((String)string2))) return true;
            Log.e((String)"IABUtil/Security", (String)"Signature verification failed.");
            return false;
        }
        catch (NoSuchAlgorithmException noSuchAlgorithmException) {
            Log.e((String)"IABUtil/Security", (String)"NoSuchAlgorithmException.");
            return false;
        }
        catch (InvalidKeyException invalidKeyException) {
            Log.e((String)"IABUtil/Security", (String)"Invalid key specification.");
            return false;
        }
        catch (SignatureException signatureException) {
            Log.e((String)"IABUtil/Security", (String)"Signature exception.");
            return false;
        }
        catch (Base64DecoderException base64DecoderException) {
            Log.e((String)"IABUtil/Security", (String)"Base64 decoding failed.");
            return false;
        }
    }
    
    public static boolean verifyPurchase(final String s, final String s2, final String s3) {
        if (!TextUtils.isEmpty((CharSequence)s2) && !TextUtils.isEmpty((CharSequence)s) && !TextUtils.isEmpty((CharSequence)s3)) {
            return verify(generatePublicKey(s), s2, s3);
        }
        Log.e("IABUtil/Security", "Purchase verification failed: missing data.");
        return false;
    }
}
