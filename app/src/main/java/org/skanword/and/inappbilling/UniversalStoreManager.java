package org.skanword.and.inappbilling;

import android.util.*;

import org.skanword.and.R;
import org.skanword.and.network.*;
import org.onepf.oms.appstore.googleUtils.*;
import android.app.*;
import android.content.*;
import android.util.Base64;

import java.util.*;
import org.onepf.oms.*;

public class UniversalStoreManager implements StoreManagerInterface
{
    String base64EncodedPublicKey;
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener;
    private StoreManagerDelegateInterface mDelegate;
    private IabHelper.QueryInventoryFinishedListener mGotInventoryListener;
    private OpenIabHelper mHelper;
    private Inventory mInventory;
    private List<String> mPoructsIds;
    private Activity mPurchaseActivity;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;

    public UniversalStoreManager() {
        super();
        this.base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5rrzJO4B4406DREi/WvYqTUyAtohs7CFw+cjpu3Xclr0etxhSKrAdZGnbo+BHVLmU48GQWmfaopzmFsFEMlThQB941b6vOUqJHASUYlCun94qpKsN0ltWiIh/SVdANNc5PWG+Ukzl+Dyvg8pFBewwjrO2ofR18P/lE3DeSz1EI8DzcFIARgNqbKrHdKEEN0wMT4xpfzBM2mgiWq4/V0D0adh1S+Pt0u8YdmmTrysHOA5o6AQ3OiGGbnDV6oT4U2McGQ8VnvhrOBwFij40FRS2O9wR7/jpTjehSiXyvEZGzbnhMdbLQRsZ1NvsrhDMI7mSCtsawChzA61JrI/nenBrwIDAQAB";
        this.mDelegate = null;
        this.mInventory = null;
        this.mGotInventoryListener = (IabHelper.QueryInventoryFinishedListener)new UniversalStoreManager.UniversalStoreManager3(this);
        this.mPurchaseFinishedListener = (IabHelper.OnIabPurchaseFinishedListener)new UniversalStoreManager.UniversalStoreManager4(this);
        this.mConsumeFinishedListener = (IabHelper.OnConsumeFinishedListener)new UniversalStoreManager.UniversalStoreManager5(this);
    }
    
    static /* synthetic */ OpenIabHelper access000(final UniversalStoreManager universalStoreManager) {
        return universalStoreManager.mHelper;
    }
    
    static /* synthetic */ void access100(final UniversalStoreManager universalStoreManager) {
        universalStoreManager.queryProducts();
    }
    
    static /* synthetic */ Inventory access200(final UniversalStoreManager universalStoreManager) {
        return universalStoreManager.mInventory;
    }
    
    static /* synthetic */ Inventory access202(final UniversalStoreManager universalStoreManager, final Inventory mInventory) {
        return universalStoreManager.mInventory = mInventory;
    }
    
    static /* synthetic */ StoreManagerDelegateInterface access300(final UniversalStoreManager universalStoreManager) {
        return universalStoreManager.mDelegate;
    }
    
    static /* synthetic */ Activity access400(final UniversalStoreManager universalStoreManager) {
        return universalStoreManager.mPurchaseActivity;
    }
    
    private void queryProducts() {
        final OpenIabHelper mHelper = this.mHelper;
        final boolean b = true;
        if (mHelper != null && this.mPoructsIds != null && mHelper.getSetupState() == 0) {
            Log.v("", " trying to query inventory  ");
            final OpenIabHelper mHelper2 = this.mHelper;
            final List<String> mPoructsIds = this.mPoructsIds;
            mHelper2.queryInventoryAsync(true, (List)mPoructsIds, (List)mPoructsIds, this.mGotInventoryListener);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("failed  to query inventory  ");
        sb.append(this.mHelper == null);
        sb.append("  ");
        sb.append(this.mPoructsIds == null && b);
        Log.v("", sb.toString());
        if (this.mHelper != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("failed  to query inventory  ");
            sb2.append(this.mHelper.getSetupState());
            Log.v("", sb2.toString());
        }
    }
    
    @Override
    public void clear() {
        final OpenIabHelper mHelper = this.mHelper;
        if (mHelper != null) {
            mHelper.dispose();
        }
        this.mHelper = null;
    }
    
    @Override
    public void consumeProducts(final List<String> list) {
        final Inventory mInventory = this.mInventory;
    }
    
    @Override
    public String getAppStoreName() {
        final OpenIabHelper mHelper = this.mHelper;
        if (mHelper != null && mHelper.getSetupState() == 0) {
            return this.mHelper.getConnectedAppstoreName();
        }
        return "google";
    }
    
    @Override
    public String getPriceForProduct(final String s) {
        if (this.mHelper != null) {
            final Inventory mInventory = this.mInventory;
            if (mInventory != null && mInventory.getSkuDetails(s) != null) {
                return this.mInventory.getSkuDetails(s).getPrice();
            }
        }
        return "";
    }
    
    @Override
    public void handleActivityResult(final int n, final int n2, final Intent intent) {
        final OpenIabHelper mHelper = this.mHelper;
        if (mHelper != null) {
            mHelper.handleActivityResult(n, n2, intent);
        }
    }
    
    @Override
    public void initManager(final StoreManagerDelegateInterface mDelegate, final Activity activity) {
        final OpenIabHelper mHelper = this.mHelper;
        if (mHelper != null && mHelper.getSetupState() == 0) {
            return;
        }
        this.mDelegate = mDelegate;
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("com.google.play", this.base64EncodedPublicKey);
        final OpenIabHelper.Options.Builder addStoreKeys = new OpenIabHelper.Options.Builder().setVerifyMode(0).addStoreKeys((Map)hashMap);
        addStoreKeys.addPreferredStoreName(new String[] { "com.google.play" });
//        (this.mHelper = new OpenIabHelper((Context)activity, addStoreKeys.build())).startSetup((IabHelper.OnIabSetupFinishedListener)new UniversalStoreManager.UniversalStoreManager1(this));
    }
    
    public boolean isHint(final String s) {
        final String[] split = s.split("\\.");
        for (int length = split.length, i = 0; i < length; ++i) {
            if (split[i].equals("hint")) {
                return true;
            }
        }
        return false;
    }
    
    @Override

    public void purchaseProduct(String var1, Activity var2) {
        this.mPurchaseActivity = var2;
        if (this.mHelper != null && this.mInventory != null) {
            if (var1.split("\\.").length != 0) {
                MainNetworkManager var3 = MainNetworkManager.getInstance();
                StringBuilder var4 = new StringBuilder();
                var4.append("1. purchaseProduct with id ");
                var4.append(var1);
                var3.serverLog("payment_track", var4.toString());
                if (this.isHint(var1)) {
                    this.mHelper.launchPurchaseFlow(var2, var1, 10001, this.mPurchaseFinishedListener, "");
                } else {
                    this.mHelper.launchSubscriptionPurchaseFlow(var2, var1, 10001, this.mPurchaseFinishedListener, "");
                }
            }
        } else {
            android.app.AlertDialog.Builder var5 = new android.app.AlertDialog.Builder(var2, R.attr.dialogTheme);
            var5.setMessage("Не удалось подключиться к Google Play.").setPositiveButton("Ок", new UniversalStoreManager2(this));
            var5.create().show();
        }
    }
    
    public String purchaseToString(final Purchase purchase) {
        if (purchase == null) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("{\"orderId\":\"");
        sb.append(purchase.getOrderId());
        sb.append("\",\"packageName\":\"");
        sb.append(purchase.getPackageName());
        sb.append("\",\"productId\":\"");
        sb.append(purchase.getSku());
        sb.append("\",\"purchaseTime\":\"");
        sb.append(purchase.getPurchaseTime());
        sb.append("\",\"purchaseState\":\"");
        sb.append(purchase.getPurchaseState());
        sb.append("\",\"developerPayload\":\"");
        sb.append(purchase.getDeveloperPayload());
        sb.append("\",\"token\":\"");
        sb.append(purchase.getToken());
        sb.append("\"}");
        return sb.toString();
    }
    
    @Override
    public void restorePurchases(final Activity activity) {
        final Inventory mInventory = this.mInventory;
        if (mInventory == null) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)activity, R.attr.dialogTheme);
            alertDialogBuilder.setMessage("Не удалось подключиться к Google Play.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new UniversalStoreManager.UniversalStoreManager6(this));
            alertDialogBuilder.create().show();
            return;
        }
        final List allPurchases = mInventory.getAllPurchases();
        final StringBuilder sb = new StringBuilder();
        sb.append("getAllPurchases :");
        sb.append(this.mInventory.mPurchaseMap);
        Log.v("", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("getAllOwnedSkus :");
        sb2.append(this.mInventory.getAllOwnedSkus());
        Log.v("", sb2.toString());
        final ArrayList<String> list = new ArrayList<String>();
        final Iterator<Purchase> iterator = allPurchases.iterator();
        while (iterator.hasNext()) {
            list.add(Base64.encodeToString(this.purchaseToString(iterator.next()).getBytes(), 0));
        }
        final StoreManagerDelegateInterface mDelegate = this.mDelegate;
        if (mDelegate != null) {
            mDelegate.purchasesRestored((List)list);
        }
    }
    
    @Override
    public void updatePricesForProducts(List var1) {
        this.mPoructsIds = new ArrayList(var1);
        Iterator var4 = var1.iterator();

        while(var4.hasNext()) {
            String var2 = (String)var4.next();
            try {
                SkuManager.getInstance().mapSku(var2, "com.google.play", var2);
            } catch(Exception e) {

            }

        }

        this.queryProducts();
    }
    
    boolean verifyDeveloperPayload(final Purchase purchase) {
        return true;
    }

    // ----- Classes

    class UniversalStoreManager3 implements IabHelper.QueryInventoryFinishedListener {
        final /* synthetic */ UniversalStoreManager this0;

        UniversalStoreManager3(final UniversalStoreManager this0) {
            super();
            this.this0 = this0;
        }

        public void onQueryInventoryFinished(final IabResult iabResult, final Inventory inventory) {
            Log.d("", "Query inventory finished.");
            if (iabResult.isFailure()) {
                Log.d("", "Query inventory failed.");
                return;
            }
            UniversalStoreManager.access202(this.this0, inventory);
            for (final Purchase purchase : UniversalStoreManager.access200(this.this0).getAllPurchases()) {
                if (this.this0.isHint(purchase.getSku())) {
                    UniversalStoreManager.access000(this.this0).consumeAsync(purchase, this.this0.mConsumeFinishedListener);
                    UniversalStoreManager.access300(this.this0).purchaseComplete(purchase.getSku(), Base64.encodeToString(this.this0.purchaseToString(purchase).getBytes(), 0));
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Query inventory was successful. ");
            sb.append(inventory.getAllOwnedSkus());
            Log.v("", sb.toString());
            if (UniversalStoreManager.access300(this.this0) != null) {
                UniversalStoreManager.access300(this.this0).initComplete();
            }
        }
    }

    class UniversalStoreManager4 implements IabHelper.OnIabPurchaseFinishedListener {
        final /* synthetic */ UniversalStoreManager this0;

        UniversalStoreManager4(final UniversalStoreManager this0) {
            super();
            this.this0 = this0;
        }

        public void onIabPurchaseFinished(final IabResult iabResult, final Purchase purchase) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Purchase finished: ");
            sb.append(iabResult);
            sb.append(", purchase: ");
            sb.append(this.this0.purchaseToString(purchase));
            Log.v("", sb.toString());
            final MainNetworkManager instance = MainNetworkManager.getInstance();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("2. onIabPurchaseFinished with result success ");
            sb2.append(iabResult.isFailure() ^ true);
            instance.serverLog("payment_track", sb2.toString());
            if (iabResult.isFailure()) {
                final MainNetworkManager instance2 = MainNetworkManager.getInstance();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("2.1. onIabPurchaseFinished failed with response message - ");
                sb3.append(iabResult.getMessage());
                instance2.serverLog("payment_track", sb3.toString());
                if (iabResult.getResponse() != 1 || UniversalStoreManager.access300(this.this0) == null) {
                    if (iabResult.getResponse() != 6) {
                        if (iabResult.getResponse() == 7) {
                            if (UniversalStoreManager.access400(this.this0) != null) {
                                final UniversalStoreManager this0 = this.this0;
                                this0.restorePurchases(UniversalStoreManager.access400(this0));
                            }
                            return;
                        }
                    }
                }
                if (purchase != null) {
                    UniversalStoreManager.access300(this.this0).purchaseCanceled(purchase.getSku());
                }
                return;
            }
            if (this.this0.isHint(purchase.getSku())) {
                final MainNetworkManager instance3 = MainNetworkManager.getInstance();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("2.2 onIabPurchaseFinished consume hints with id - ");
                sb4.append(purchase.getSku());
                instance3.serverLog("payment_track", sb4.toString());
                UniversalStoreManager.access000(this.this0).consumeAsync(purchase, this.this0.mConsumeFinishedListener);
            }
            UniversalStoreManager.access300(this.this0).purchaseComplete(purchase.getSku(), Base64.encodeToString(this.this0.purchaseToString(purchase).getBytes(), 0));
        }
    }

    class UniversalStoreManager5 implements IabHelper.OnConsumeFinishedListener {
        final /* synthetic */ UniversalStoreManager this0;

        UniversalStoreManager5(final UniversalStoreManager this0) {
            super();
            this.this0 = this0;
        }

        public void onConsumeFinished(final Purchase purchase, final IabResult iabResult) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Consumption finished. Purchase: ");
            sb.append(purchase);
            sb.append(", result: ");
            sb.append(iabResult);
            Log.v("", sb.toString());
            Log.d("", "End consumption flow.");
        }
    }

    class UniversalStoreManager1 implements IabHelper.OnIabSetupFinishedListener {
        final /* synthetic */ UniversalStoreManager this0;

        UniversalStoreManager1(final UniversalStoreManager this0) {
            super();
            this.this0 = this0;
        }

        public void onIabSetupFinished(final IabResult iabResult) {
            final StringBuilder sb = new StringBuilder();
            sb.append(iabResult.getMessage());
            sb.append("  Setup finished.");
            sb.append(UniversalStoreManager.access000(this.this0).getConnectedAppstoreName());
            Log.v("", sb.toString());
            if (!iabResult.isSuccess()) {
                return;
            }
            UniversalStoreManager.access100(this.this0);
        }
    }

    class UniversalStoreManager2 implements DialogInterface.OnClickListener {
        final /* synthetic */ UniversalStoreManager this0;

        UniversalStoreManager2(final UniversalStoreManager this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    class UniversalStoreManager6 implements DialogInterface.OnClickListener {
        final /* synthetic */ UniversalStoreManager this0;

        UniversalStoreManager6(final UniversalStoreManager this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }
}
