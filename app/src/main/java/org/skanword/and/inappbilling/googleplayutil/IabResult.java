package org.skanword.and.inappbilling.googleplayutil;

public class IabResult
{
    String mMessage;
    int mResponse;
    
    public IabResult(final int mResponse, final String s) {
        super();
        this.mResponse = mResponse;
        if (s != null && s.trim().length() != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append(" (response: ");
            sb.append(IabHelper.getResponseDesc(mResponse));
            sb.append(")");
            this.mMessage = sb.toString();
            return;
        }
        this.mMessage = IabHelper.getResponseDesc(mResponse);
    }
    
    public String getMessage() {
        return this.mMessage;
    }
    
    public int getResponse() {
        return this.mResponse;
    }
    
    public boolean isFailure() {
        return this.isSuccess() ^ true;
    }
    
    public boolean isSuccess() {
        return this.mResponse == 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IabResult: ");
        sb.append(this.getMessage());
        return sb.toString();
    }
}
