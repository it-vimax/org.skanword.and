package org.skanword.and.inappbilling.googleplayutil;

public class Base64DecoderException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public Base64DecoderException() {
        super();
    }
    
    public Base64DecoderException(final String s) {
        super(s);
    }
}
