package org.skanword.and.inappbilling.googleplayutil;

public class IabException extends Exception
{
    private static final long serialVersionUID = 1301330018988259520L;
    IabResult mResult;
    
    public IabException(final int n, final String s) {
        this(new IabResult(n, s));
    }
    
    public IabException(final int n, final String s, final Exception ex) {
        this(new IabResult(n, s), ex);
    }
    
    public IabException(final IabResult iabResult) {
        this(iabResult, null);
    }
    
    public IabException(final IabResult mResult, final Exception ex) {
        super(mResult.getMessage(), ex);
        this.mResult = mResult;
    }
    
    public IabResult getResult() {
        return this.mResult;
    }
}
