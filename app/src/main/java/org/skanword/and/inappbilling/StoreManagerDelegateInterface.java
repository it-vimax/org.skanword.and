package org.skanword.and.inappbilling;

import java.util.*;

public interface StoreManagerDelegateInterface
{
    void initComplete();
    
    void purchaseCanceled(final String p0);
    
    void purchaseComplete(final String p0, final String p1);
    
    void purchaseOwned();
    
    void purchasesRestored(final List<String> p0);
}
