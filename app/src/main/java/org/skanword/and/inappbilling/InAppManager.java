package org.skanword.and.inappbilling;

import android.app.*;
import android.util.*;
import org.skanword.and.*;
import android.content.*;

import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;
import org.skanword.and.network.*;
import org.skanword.and.datamanager.*;
import java.util.*;

public class InAppManager implements StoreManagerDelegateInterface
{
    public static final String EVENT_PRICE_UPDATE = "EVENT_PRICE_UPDATE";
    static final int RC_REQUEST = 10001;
    private static InAppManager instance;
    private PurchaseSource lastPurchaseSource;
    private Activity mCallerActivity;
    private StoreManagerInterface mStoreManager;
    
    public InAppManager() {
        super();
        this.mStoreManager = new UniversalStoreManager();
    }
    
    public static InAppManager getInstance() {
        if (InAppManager.instance == null) {
            InAppManager.instance = new InAppManager();
        }
        return InAppManager.instance;
    }
    
    public void clear() {
        final StoreManagerInterface mStoreManager = this.mStoreManager;
        if (mStoreManager != null) {
            mStoreManager.clear();
        }
    }
    
    public void consumeProducts(final Map<String, String> map) {
        if (this.mStoreManager != null && map != null) {
            final ArrayList<String> list = new ArrayList<String>();
            list.addAll(map.values());
            this.mStoreManager.consumeProducts((List<String>)list);
        }
    }
    
    public String getPriceForProductId(final String s) {
        final StoreManagerInterface mStoreManager = this.mStoreManager;
        if (mStoreManager != null) {
            return mStoreManager.getPriceForProduct(s);
        }
        return "";
    }
    
    public void getProductsPrices(final List<String> list) {
        final StringBuilder sb = new StringBuilder();
        sb.append(" getProductsPrices ");
        sb.append(list);
        Log.v("", sb.toString());
        this.mStoreManager.updatePricesForProducts(list);
    }
    
    public String getStoreName() {
        return this.mStoreManager.getAppStoreName();
    }
    
    @Override
    public void initComplete() {
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_PRICE_UPDATE"));
    }
    
    public void initStoreManager(final Activity activity) {
        this.mStoreManager.initManager(this, activity);
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        Log.v("", "InApp Manger onactivity result");
        this.mStoreManager.handleActivityResult(n, n2, intent);
    }
    
    @Override
    public void purchaseCanceled(final String s) {
        final int n = InAppManager1.purchaseSource[this.lastPurchaseSource.ordinal()];
        if (n == 1) {
            final SmappsScanwords.ActionType user_ACTION = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb = new StringBuilder();
            sb.append("purchase_canceled_");
            sb.append(s);
            sb.append("_playscan_minishop");
            SmappsScanwords.sendGAEvent(user_ACTION, sb.toString());
            return;
        }
        if (n == 2) {
            final SmappsScanwords.ActionType user_ACTION2 = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("purchase_canceled_");
            sb2.append(s);
            sb2.append("_shop");
            SmappsScanwords.sendGAEvent(user_ACTION2, sb2.toString());
            return;
        }
        if (n == 3) {
            final SmappsScanwords.ActionType user_ACTION3 = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("purchase_canceled_");
            sb3.append(s);
            sb3.append("_lockedscanwin");
            SmappsScanwords.sendGAEvent(user_ACTION3, sb3.toString());
            return;
        }
        if (n != 4) {
            return;
        }
        final SmappsScanwords.ActionType user_ACTION4 = SmappsScanwords.ActionType.USER_ACTION;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("purchase_canceled_");
        sb4.append(s);
        sb4.append("_news");
        SmappsScanwords.sendGAEvent(user_ACTION4, sb4.toString());
    }
    
    @Override
    public void purchaseComplete(final String s, final String s2) {
        final MainNetworkManager instance = MainNetworkManager.getInstance();
        final StringBuilder sb = new StringBuilder();
        sb.append("3. purchaseComplete with id - ");
        sb.append(s);
        instance.serverLog("payment_track", sb.toString());
        MainDataManager.getInstance().getPurchasesData().addPurchase(s2, new Date().getTime(), false);
        MainDataManager.getInstance().storePurchases();
        final MainNetworkManager instance2 = MainNetworkManager.getInstance();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("4. purchaseComplete send data with id ");
        sb2.append(s);
        instance2.serverLog("payment_track", sb2.toString());
        MainNetworkManager.getInstance().purchaseComplete(this.mCallerActivity, true);
        if (this.lastPurchaseSource == null) {
            return;
        }
        final int n = InAppManager1.purchaseSource[this.lastPurchaseSource.ordinal()];
        if (n == 1) {
            final SmappsScanwords.ActionType user_ACTION = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("purchase_complete_");
            sb3.append(s);
            sb3.append("_playscan_minishop");
            SmappsScanwords.sendGAEvent(user_ACTION, sb3.toString());
            return;
        }
        if (n == 2) {
            final SmappsScanwords.ActionType user_ACTION2 = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("purchase_complete_");
            sb4.append(s);
            sb4.append("_shop");
            SmappsScanwords.sendGAEvent(user_ACTION2, sb4.toString());
            return;
        }
        if (n == 3) {
            final SmappsScanwords.ActionType user_ACTION3 = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("purchase_complete_");
            sb5.append(s);
            sb5.append("_lockedscanwin");
            SmappsScanwords.sendGAEvent(user_ACTION3, sb5.toString());
            return;
        }
        if (n != 4) {
            return;
        }
        final SmappsScanwords.ActionType user_ACTION4 = SmappsScanwords.ActionType.USER_ACTION;
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("purchase_complete_");
        sb6.append(s);
        sb6.append("_news");
        SmappsScanwords.sendGAEvent(user_ACTION4, sb6.toString());
    }
    
    @Override
    public void purchaseOwned() {
    }
    
    public void purchaseProduct(final String s, final Activity mCallerActivity, final PurchaseSource lastPurchaseSource) {
        this.lastPurchaseSource = lastPurchaseSource;
        this.mCallerActivity = mCallerActivity;
        final StoreManagerInterface mStoreManager = this.mStoreManager;
        if (mStoreManager != null) {
            mStoreManager.purchaseProduct(s, mCallerActivity);
            final int n = InAppManager1.purchaseSource[this.lastPurchaseSource.ordinal()];
            if (n != 1) {
                if (n == 2) {
                    final SmappsScanwords.ActionType user_ACTION = SmappsScanwords.ActionType.USER_ACTION;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("purchase_start_");
                    sb.append(s);
                    sb.append("_shop");
                    SmappsScanwords.sendGAEvent(user_ACTION, sb.toString());
                    return;
                }
                if (n == 3) {
                    final SmappsScanwords.ActionType user_ACTION2 = SmappsScanwords.ActionType.USER_ACTION;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("purchase_start_");
                    sb2.append(s);
                    sb2.append("_lockedscanwin");
                    SmappsScanwords.sendGAEvent(user_ACTION2, sb2.toString());
                    return;
                }
                if (n != 4) {
                    return;
                }
                final SmappsScanwords.ActionType user_ACTION3 = SmappsScanwords.ActionType.USER_ACTION;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("purchase_start_");
                sb3.append(s);
                sb3.append("_news");
                SmappsScanwords.sendGAEvent(user_ACTION3, sb3.toString());
            }
            else {
                final SmappsScanwords.ActionType user_ACTION4 = SmappsScanwords.ActionType.USER_ACTION;
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("purchase_start_");
                sb4.append(s);
                sb4.append("_playscan_minishop");
                SmappsScanwords.sendGAEvent(user_ACTION4, sb4.toString());
            }
        }
    }
    
    @Override
    public void purchasesRestored(final List<String> list) {
        final Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            MainDataManager.getInstance().getPurchasesData().addPurchase(iterator.next(), new Date().getTime(), true);
        }
        MainDataManager.getInstance().storePurchases();
        MainNetworkManager.getInstance().purchaseComplete(this.mCallerActivity, false);
    }
    
    public void restorePurchases(final Activity mCallerActivity) {
        final StoreManagerInterface mStoreManager = this.mStoreManager;
        if (mStoreManager != null) {
            mStoreManager.restorePurchases(this.mCallerActivity = mCallerActivity);
        }
    }

    public enum PurchaseSource {
        MINISHOP,
        LOCKED_TASK,
        SHOP,
        NEWS
    }


    static class InAppManager1 {
        // FF: synthetic field
        static final int[] purchaseSource = new int[PurchaseSource.values().length];

        static {
            try {
                purchaseSource[PurchaseSource.MINISHOP.ordinal()] = 1;
            } catch (NoSuchFieldError var4) {
            }

            try {
                purchaseSource[PurchaseSource.SHOP.ordinal()] = 2;
            } catch (NoSuchFieldError var3) {
            }

            try {
                purchaseSource[PurchaseSource.LOCKED_TASK.ordinal()] = 3;
            } catch (NoSuchFieldError var2) {
            }

            try {
                purchaseSource[PurchaseSource.NEWS.ordinal()] = 4;
            } catch (NoSuchFieldError var1) {
            }
        }
    }
}
