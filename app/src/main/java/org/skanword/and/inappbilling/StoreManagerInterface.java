package org.skanword.and.inappbilling;

import java.util.*;
import android.content.*;
import android.app.*;

public interface StoreManagerInterface
{
    void clear();
    
    void consumeProducts(final List<String> p0);
    
    String getAppStoreName();
    
    String getPriceForProduct(final String p0);
    
    void handleActivityResult(final int p0, final int p1, final Intent p2);
    
    void initManager(final StoreManagerDelegateInterface p0, final Activity p1);
    
    void purchaseProduct(final String p0, final Activity p1);
    
    void restorePurchases(final Activity p0);
    
    void updatePricesForProducts(final List<String> p0);
}
