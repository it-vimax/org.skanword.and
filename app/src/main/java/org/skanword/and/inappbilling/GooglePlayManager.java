package org.skanword.and.inappbilling;

import android.util.*;
import org.skanword.and.*;
import java.util.*;
import org.skanword.and.inappbilling.googleplayutil.*;
import android.content.*;
import android.app.*;

public class GooglePlayManager implements StoreManagerInterface
{
    String base64EncodedPublicKey;
    private StoreManagerDelegateInterface mDelegate;
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener;
    private IabHelper mHelper;
    private Inventory mInventory;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    private boolean mStarted;

    public GooglePlayManager() {
        super();
        this.base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjrJyt7vEpipN9aLbuGHAaUno/VOduGASRUtBXjzZaU+sFbAar/orgTOlvKjN2Izjnrn8/fAcJYvHpLQn6mj7qkX52oTOoOzQapUDLYRNAQ19kGEqxZo94RJdTLSBA+TOyaboWy5O4qU4O+R4hvjEZjYGRclwG7IEvzYB+yN4Ew4KgxO9SsEKoorFxxUMH0fnuMbFhX8geMum/B12bsFK41wma69DChE7HdnIAYDDSzj6CgmReFmV0n6qjyDTMWTFidwkgtixOZdoo4HLDXbMBbjHU8L1Mh9189s6NE6WY6yLe7eXST1DIDx9UkwehKxwupqMIcPFzZRX4OsSvYPO/QIDAQAB";
        this.mStarted = false;
        this.mDelegate = null;
        this.mInventory = null;
        this.mGotInventoryListener = (IabHelper.QueryInventoryFinishedListener)new GooglePlayManager.GooglePlayManager2(this);
        this.mPurchaseFinishedListener = (IabHelper.OnIabPurchaseFinishedListener)new GooglePlayManager3(this);
        (this.mHelper = new IabHelper(SmappsScanwords.getContext(), this.base64EncodedPublicKey)).enableDebugLogging(true);
        Log.d("", "Starting setup.");
    }
    
    static /* synthetic */ IabHelper access000(final GooglePlayManager googlePlayManager) {
        return googlePlayManager.mHelper;
    }
    
    static /* synthetic */ Inventory access102(final GooglePlayManager googlePlayManager, final Inventory mInventory) {
        return googlePlayManager.mInventory = mInventory;
    }
    
    static /* synthetic */ StoreManagerDelegateInterface access200(final GooglePlayManager googlePlayManager) {
        return googlePlayManager.mDelegate;
    }
    
    void alert(final String message) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SmappsScanwords.getContext());
        alertDialogBuilder.setMessage((CharSequence)message);
        alertDialogBuilder.setNeutralButton((CharSequence)"OK", (DialogInterface.OnClickListener)null);
        final StringBuilder sb = new StringBuilder();
        sb.append("Showing alert dialog: ");
        sb.append(message);
        Log.d("", sb.toString());
        alertDialogBuilder.create().show();
    }
    
    @Override
    public void clear() {
        Log.d("", "Destroying helper.");
        final IabHelper mHelper = this.mHelper;
        if (mHelper != null) {
            mHelper.dispose();
            this.mHelper = null;
        }
    }
    
    void complain(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("**** TrivialDrive Error: ");
        sb.append(s);
        Log.e("", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Error: ");
        sb2.append(s);
        this.alert(sb2.toString());
    }
    
    @Override
    public void consumeProducts(final List<String> list) {
    }
    
    @Override
    public String getAppStoreName() {
        return "google";
    }
    
    @Override
    public String getPriceForProduct(final String s) {
        final Inventory mInventory = this.mInventory;
        if (mInventory != null) {
            final SkuDetails skuDetails = mInventory.getSkuDetails(s);
            if (skuDetails != null) {
                return skuDetails.getPrice();
            }
        }
        return null;
    }
    
    @Override
    public void handleActivityResult(final int n, final int n2, final Intent intent) {
    }
    
    @Override
    public void initManager(final StoreManagerDelegateInterface mDelegate, final Activity activity) {
        this.mDelegate = mDelegate;
    }
    
    @Override
    public void purchaseProduct(final String s, final Activity activity) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Launching purchase flow for ");
        sb.append(s);
        Log.d("", sb.toString());
        this.mHelper.launchPurchaseFlow(activity, s, 1, this.mPurchaseFinishedListener, "");
    }
    
    @Override
    public void restorePurchases(final Activity activity) {
    }
    
    @Override
    public void updatePricesForProducts(final List<String> list) {
        if (list.size() != 0) {
            if (this.mStarted) {
                return;
            }
            this.mHelper.startSetup((IabHelper.OnIabSetupFinishedListener)new GooglePlayManager.GooglePlayManager1(this, (List)list));
            this.mStarted = true;
        }
    }
    
    boolean verifyDeveloperPayload(final Purchase purchase) {
        return true;
    }

    // ---------------- Class

    class GooglePlayManager1 implements IabHelper.OnIabSetupFinishedListener {
        final /* synthetic */ GooglePlayManager this0;
        final /* synthetic */ List valproductsIds;

        GooglePlayManager1(final GooglePlayManager this0, final List valproductsIds) {
            super();
            this.this0 = this0;
            this.valproductsIds = valproductsIds;
        }

        @Override
        public void onIabSetupFinished(final IabResult iabResult) {
            Log.d("", "onIabSetupFinished");
            if (!iabResult.isSuccess()) {
                final GooglePlayManager this0 = this.this0;
                final StringBuilder sb = new StringBuilder();
                sb.append("Problem setting up in-app billing: ");
                sb.append(iabResult);
                this0.complain(sb.toString());
                return;
            }
            if (GooglePlayManager.access000(this.this0) == null) {
                return;
            }
            Log.d("", "Setup successful. Querying inventory.");
            GooglePlayManager.access000(this.this0).queryInventoryAsync(true, this.valproductsIds, this.this0.mGotInventoryListener);
        }
    }

    class GooglePlayManager2 implements IabHelper.QueryInventoryFinishedListener {
        final /* synthetic */ GooglePlayManager this0;

        GooglePlayManager2(final GooglePlayManager this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void onQueryInventoryFinished(final IabResult iabResult, final Inventory inventory) {
            Log.d("", "Query inventory finished.");
            if (GooglePlayManager.access000(this.this0) == null) {
                return;
            }
            if (iabResult.isFailure()) {
                final GooglePlayManager this0 = this.this0;
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to query inventory: ");
                sb.append(iabResult);
                this0.complain(sb.toString());
                return;
            }
            Log.d("", "Query inventory was successful.");
            GooglePlayManager.access102(this.this0, inventory);
            if (GooglePlayManager.access200(this.this0) != null) {
                GooglePlayManager.access200(this.this0).initComplete();
            }
        }
    }

    class GooglePlayManager3 implements IabHelper.OnIabPurchaseFinishedListener {
        final /* synthetic */ GooglePlayManager this0;

        GooglePlayManager3(final GooglePlayManager this0) {
            super();
            this.this0 = this0;
        }

        public void onIabPurchaseFinished(final IabResult iabResult, final Purchase purchase) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Purchase finished: ");
            sb.append(iabResult);
            sb.append(", purchase: ");
            sb.append(purchase);
            Log.d("", sb.toString());
            if (GooglePlayManager.access000(this.this0) == null) {
                return;
            }
            if (iabResult.isFailure()) {
                final GooglePlayManager this0 = this.this0;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Error purchasing: ");
                sb2.append(iabResult);
                this0.complain(sb2.toString());
                return;
            }
            if (!this.this0.verifyDeveloperPayload(purchase)) {
                this.this0.complain("Error purchasing. Authenticity verification failed.");
                return;
            }
            if (GooglePlayManager.access200(this.this0) != null) {
                GooglePlayManager.access200(this.this0).purchaseComplete(purchase.getSku(), purchase.toString());
            }
        }
    }
}
