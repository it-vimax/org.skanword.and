package org.skanword.and.etc.versions;

import android.annotation.*;
import java.util.concurrent.*;
import java.util.*;
import java.net.*;
import org.apache.http.protocol.*;
import java.io.*;
import java.util.logging.*;
import android.app.*;

@TargetApi(9)
public class GingerbreadUtil extends DefaultUtil
{
    private static final boolean USE_DOWNLOAD_MANAGER = false;
    private static Map<Long, DownloadingFile> completedDownloads;
    private static ConcurrentMap<Long, DownloadingFile> waitingDownloads;
    
    static {
        GingerbreadUtil.waitingDownloads = new ConcurrentSkipListMap<Long, DownloadingFile>();
        GingerbreadUtil.completedDownloads = new HashMap<Long, DownloadingFile>();
    }
    
    public GingerbreadUtil() {
        super();
    }
    
    @Override
    public void downloadFile(final URL url, final Map<String, String> map, final File file, final boolean b, final String s, final HttpContext httpContext) throws IOException {
        super.downloadFile(url, map, file, b, s, httpContext);
    }
    
    @Override
    public void onFileDownloaded(final long n, final boolean b, final int n2) {
        final Long value = n;
        synchronized (GingerbreadUtil.completedDownloads) {
            final DownloadingFile downloadingFile = GingerbreadUtil.waitingDownloads.remove(value);
            if (downloadingFile != null) {
                synchronized (downloadingFile) {
                    downloadingFile.completed = true;
                    downloadingFile.succeeded = b;
                    downloadingFile.status = n2;
                    downloadingFile.notifyAll();
                    return;
                }
            }
            final Logger log = GingerbreadUtil.LOG;
            final StringBuilder sb = new StringBuilder();
            sb.append("No thread is waiting on download for id=");
            sb.append(n);
            log.info(sb.toString());
            final DownloadingFile downloadingFile2 = new DownloadingFile();
            downloadingFile2.completed = true;
            downloadingFile2.succeeded = b;
            downloadingFile2.status = n2;
            GingerbreadUtil.completedDownloads.put(value, downloadingFile2);
        }
    }
    
    protected void setNotificationVisibility(final DownloadManager.Request downloadManagerRequest, final boolean b) {
    }
    
    private static class DownloadingFile
    {
        public boolean completed;
        public int status;
        public boolean succeeded;
        
        private DownloadingFile() {
            super();
            this.completed = false;
            this.succeeded = false;
            this.status = -1;
        }
        
        DownloadingFile(final GingerbreadUtil1 object) {
            this();
        }
    }

    class GingerbreadUtil1 {
    }
}
