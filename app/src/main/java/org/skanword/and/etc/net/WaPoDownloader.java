package org.skanword.and.etc.net;

import java.util.*;

public class WaPoDownloader extends AbstractJPZDownloader
{
    private static final String NAME = "Washington Post";
    
    public WaPoDownloader() {
        super("http://cdn.games.arkadiumhosted.com/washingtonpost/crossynergy/", "Washington Post");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append("cs");
        sb.append(calendar.get(Calendar.YEAR) % 100);
        sb.append(WaPoDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(WaPoDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".jpz");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return (int)((Calendar.getInstance().getTimeInMillis() - calendar.getTimeInMillis()) / 86400000L) <= 14;
    }
}
