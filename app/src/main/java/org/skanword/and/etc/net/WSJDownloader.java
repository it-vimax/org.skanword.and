package org.skanword.and.etc.net;

import java.util.*;

public class WSJDownloader extends AbstractDownloader
{
    private static final String NAME = "Wall Street Journal";
    
    public WSJDownloader() {
        super("http://mazerlm.home.comcast.net/~mazerlm/", "Wall Street Journal");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append("wsj");
        sb.append(WSJDownloader.DEFAULT_NF.format(calendar.get(Calendar.YEAR) % 100));
        sb.append(WSJDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(WSJDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".puz");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 6;
    }
}
