package org.skanword.and.etc.net;

import java.text.*;
import java.util.logging.*;
import java.util.regex.*;
import org.skanword.and.etc.versions.*;
import java.util.*;
import java.net.*;
import org.apache.http.protocol.*;
import org.skanword.and.*;
import java.io.*;
import android.content.*;

public abstract class AbstractDownloader implements Downloader
{
    protected static final NumberFormat DEFAULT_NF;
    protected static final Map<String, String> EMPTY_MAP;
    protected static final Logger LOG;
    private static final Pattern SCRUB_URL_PATTERN;
    private static final String SCRUB_URL_REGEX = "\\b(username|password)=[^&]*";
    protected static final String[] SHORT_MONTHS;
    protected String baseUrl;
    private String downloaderName;
    protected final AndroidVersionUtils utils;
    
    static {
        LOG = Logger.getLogger("com.adamrosenfield.wordswithcrosses");
        EMPTY_MAP = Collections.emptyMap();
        SHORT_MONTHS = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        SCRUB_URL_PATTERN = Pattern.compile("\\b(username|password)=[^&]*");
        final NumberFormat instance = NumberFormat.getInstance();
        instance.setMinimumIntegerDigits(2);
        instance.setMaximumFractionDigits(0);
        DEFAULT_NF = instance;
    }
    
    protected AbstractDownloader(final String baseUrl, final String downloaderName) {
        super();
        this.utils = AndroidVersionUtils.Factory.getInstance();
        this.baseUrl = baseUrl;
        this.downloaderName = downloaderName;
    }
    
    protected static Calendar createDate(final int n, final int n2, final int n3) {
        final Calendar instance = Calendar.getInstance();
        instance.clear();
        instance.set(n, n2 - 1, n3);
        return instance;
    }
    
    public static String resolveUrl(final String s, final String s2) throws MalformedURLException {
        return new URL(new URL(s), s2).toString();
    }
    
    public static String scrubUrl(final String s) {
        return AbstractDownloader.SCRUB_URL_PATTERN.matcher(s).replaceAll("1=[redacted]");
    }
    
    public static String scrubUrl(final URL url) {
        return scrubUrl(url.toString());
    }
    
    protected abstract String createUrlSuffix(final Calendar p0);
    
    @Override
    public void download(final Calendar calendar) throws IOException {
        this.download(calendar, this.createUrlSuffix(calendar));
    }
    
    protected void download(final Calendar calendar, final String s) throws IOException {
        this.download(calendar, s, AbstractDownloader.EMPTY_MAP);
    }
    
    protected void download(final Calendar calendar, final String s, final Map<String, String> map) throws IOException {
        this.download(calendar, s, map, null);
    }
    
    protected void download(final Calendar calendar, final String s, final Map<String, String> map, final HttpContext httpContext) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.baseUrl);
        sb.append(s);
        final URL url = new URL(sb.toString());
        final Logger log = AbstractDownloader.LOG;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Downloading ");
        sb2.append(scrubUrl(url));
        log.info(sb2.toString());
        this.utils.downloadFile(url, map, new File(SmappsScanwords.CROSSWORDS_DIR, this.getFilename(calendar)), true, this.getName(), httpContext);
    }
    
    protected String downloadUrlToString(final String s) throws IOException {
        return this.downloadUrlToString(s, AbstractDownloader.EMPTY_MAP);
    }
    
    protected String downloadUrlToString(final String s, final Map<String, String> map) throws IOException {
        return this.downloadUrlToString(s, map, null);
    }
    
    protected String downloadUrlToString(final String s, final Map<String, String> map, final HttpContext httpContext) throws IOException {
        final Logger log = AbstractDownloader.LOG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Downloading to string: ");
        sb.append(s);
        log.info(sb.toString());
        return this.utils.downloadToString(new URL(s), map, httpContext);
    }
    
    @Override
    public String getFilename(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append(calendar.get(Calendar.YEAR));
        sb.append("-");
        sb.append(AbstractDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append("-");
        sb.append(AbstractDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append("-");
        sb.append(this.downloaderName.replaceAll(" ", "").replace("/", "_"));
        sb.append(".puz");
        return sb.toString();
    }
    
    @Override
    public String getName() {
        return this.downloaderName;
    }
    
    @Override
    public void setContext(final Context context) {
        this.utils.setContext(context);
    }
    
    @Override
    public String sourceUrl(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.baseUrl);
        sb.append(this.createUrlSuffix(calendar));
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
}
