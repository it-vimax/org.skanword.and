package org.skanword.and.etc.net;

import java.util.*;

public class ThinksDownloader extends AbstractDownloader
{
    private static final String NAME = "Thinks.com";
    
    public ThinksDownloader() {
        super("http://thinks.com/daily-crossword/puzzles/", "Thinks.com");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append(calendar.get(Calendar.YEAR));
        sb.append("-");
        sb.append(ThinksDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append("/dc1-");
        sb.append(calendar.get(Calendar.YEAR));
        sb.append("-");
        sb.append(ThinksDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append("-");
        sb.append(ThinksDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".puz");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return true;
    }
}
