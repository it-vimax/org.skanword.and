package org.skanword.and.etc;

import android.content.*;
import android.util.*;

public class SmappsReceiver extends BroadcastReceiver
{
    public SmappsReceiver() {
        super();
    }
    
    public void onReceive(final Context context, final Intent intent) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onReceive  SmappsReceiver ");
        sb.append(intent.getStringExtra("EXTRA_NOTIFICATION_MESSAGE"));
        Log.v("", sb.toString());
        final Intent intent2 = new Intent(context, (Class)SmappsAlarmService.class);
        intent2.putExtras(intent.getExtras());
        context.startService(intent2);
    }
}
