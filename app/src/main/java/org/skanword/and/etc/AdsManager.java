package org.skanword.and.etc;

import java.util.concurrent.atomic.*;
import org.skanword.and.*;
import android.util.*;
import android.content.*;
import com.google.android.gms.ads.*;
import org.skanword.and.datamanager.*;
import org.skanword.and.network.*;
import com.jirbo.adcolony.*;
import android.app.*;
import android.os.*;
import com.chartboost.sdk.*;
import java.util.*;
import com.google.android.gms.ads.reward.*;

public class AdsManager
{
    public static final String ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-9865553155301243/2739874619";
    public static final String ADMOB_MAIN = "ca-app-pub-9865553155301243~7309675018";
    public static final String ADMOB_REWARD_VIDEO_UNIT_ID = "ca-app-pub-9865553155301243/3454413415";
    public static final int ADS_INTERSTITIAL = 2;
    public static final int ADS_REWARDED_VIDEO = 4;
    public static final int ADS_VIDEO = 1;
    public static final int ADS_VIDEO_INTERSTITIAL = 8;
    private static AdsManager instance;
    private volatile InterstitialAd mAdmobInterstitial;
    private volatile RewardedVideoAd mAdmobRewardedVideo;
    private Activity mCallerActivity;
    private CheckerStartVideo mCheckerStartVideo;
    private final AtomicBoolean mIsInterstitialLoading;
    private final AtomicBoolean mIsVideoLoading;
    private boolean mOfferCountVideo;
    private String mOfferTarget;
    private RewardVideoListener mRewardVideoListener;
    private boolean showingInterstitial;
    private AdsEventsListener videoEventsListever;
    
    public AdsManager() {
        super();
        this.videoEventsListever = null;
        this.showingInterstitial = false;
        this.mOfferCountVideo = true;
        this.mAdmobInterstitial = null;
        this.mAdmobRewardedVideo = null;
        this.mIsVideoLoading = new AtomicBoolean();
        this.mIsInterstitialLoading = new AtomicBoolean();
        this.mRewardVideoListener = new RewardVideoListener(this);
        this.mCheckerStartVideo = new CheckerStartVideo(this);
    }
    
    static /* synthetic */ void access1000(final AdsManager adsManager, final InterstitialAd interstitialAd) {
        adsManager.startLoadInterstitial(interstitialAd);
    }
    
    static /* synthetic */ AtomicBoolean access1100(final AdsManager adsManager) {
        return adsManager.mIsVideoLoading;
    }
    
    static /* synthetic */ CheckerStartVideo access1200(final AdsManager adsManager) {
        return adsManager.mCheckerStartVideo;
    }
    
    static /* synthetic */ void access1300(final AdsManager adsManager) {
        adsManager.videoSkipped();
    }
    
    static /* synthetic */ void access1400(final AdsManager adsManager) {
        adsManager.videoNoVideo();
    }
    
    static /* synthetic */ void access1500(final AdsManager adsManager, final RewardedVideoAd rewardedVideoAd) {
        adsManager.startLoadVideo(rewardedVideoAd);
    }
    
    static /* synthetic */ void access1600(final AdsManager adsManager, final String s) {
        adsManager.videoComplete(s);
    }
    
    static /* synthetic */ void access200(final AdsManager adsManager, final Activity activity) {
        adsManager.requestAdmobInterstitial(activity);
    }
    
    static /* synthetic */ void access300(final AdsManager adsManager, final Activity activity) {
        adsManager.requestAdmobRewardedVideo(activity);
    }
    
    static /* synthetic */ RewardedVideoAd access400(final AdsManager adsManager) {
        return adsManager.mAdmobRewardedVideo;
    }
    
    static /* synthetic */ AdsEventsListener access500(final AdsManager adsManager) {
        return adsManager.videoEventsListever;
    }
    
    static /* synthetic */ AdsEventsListener access502(final AdsManager adsManager, final AdsEventsListener videoEventsListever) {
        return adsManager.videoEventsListever = videoEventsListever;
    }
    
    static /* synthetic */ AtomicBoolean access600(final AdsManager adsManager) {
        return adsManager.mIsInterstitialLoading;
    }
    
    static /* synthetic */ void access700(final AdsManager adsManager) {
        adsManager.interstitialShowFailed();
    }
    
    static /* synthetic */ InterstitialAd access800(final AdsManager adsManager) {
        return adsManager.mAdmobInterstitial;
    }
    
    static /* synthetic */ boolean access902(final AdsManager adsManager, final boolean showingInterstitial) {
        return adsManager.showingInterstitial = showingInterstitial;
    }
    
    private boolean checkInterstitialTimeDelay() {
        if (MainDataManager.getInstance().getAppAds() == null) {
            return false;
        }
        final long long1 = SmappsScanwords.getAppSharedPreferences("GENERAL_PREFERENCES_NAME").getLong("ads_show_time", 0L);
        if (long1 < 1L) {
            return true;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(" interstitials time delay   delta time-");
        sb.append(new Date().getTime() - long1);
        sb.append("  need to wait - ");
        sb.append(MainDataManager.getInstance().getAppAds().getShowDelay() * 1000);
        Log.v("SkanwordsAds", sb.toString());
        return new Date().getTime() - long1 >= MainDataManager.getInstance().getAppAds().getShowDelay() * 1000;
    }
    
    public static AdsManager getInstance() {
        if (AdsManager.instance == null) {
            AdsManager.instance = new AdsManager();
        }
        return AdsManager.instance;
    }
    
    private void interstitialShowFailed() {
        this.showAdsForAdsObject(false);
    }
    
    private void rememberInterstitialShowTime() {
        SmappsScanwords.getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putLong("ads_show_time", new Date().getTime()).commit();
    }

    private void requestAdmobInterstitial(Activity activity) {
        synchronized (this) {
            if (activity != null) {
                if (!(activity instanceof IAdsInterstitialActivity)) return;
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("requestAdmobInterstitial  ");
            stringBuilder.append((Object)activity);
            Log.v((String)"SkanwordsAds", (String)stringBuilder.toString());
            if (this.mAdmobInterstitial == null && activity != null) {
                this.mAdmobInterstitial = new InterstitialAd((Context)activity);
                this.mAdmobInterstitial.setAdUnitId(ADMOB_INTERSTITIAL_UNIT_ID);
                this.mAdmobInterstitial.setAdListener((AdListener)new AdsManager.AdsManager3(this));
                this.startLoadInterstitial(this.mAdmobInterstitial);
                return;
            }
            this.startLoadInterstitial(this.mAdmobInterstitial);
        }
    }
    
    private void requestAdmobRewardedVideo(final Activity activity) {
        synchronized (this) {
            if (activity instanceof IAdsRewardedVideoActivity) {
                final RewardedVideoAd rewardedVideoAdInstance = MobileAds.getRewardedVideoAdInstance((Context)activity);
                if (this.mAdmobRewardedVideo != rewardedVideoAdInstance) {
                    (this.mAdmobRewardedVideo = rewardedVideoAdInstance).setRewardedVideoAdListener(this.mRewardVideoListener);
                    this.startLoadVideo(this.mAdmobRewardedVideo);
                }
            }
        }
    }
    
    private boolean showAdmobInterstitial() {
        if (this.mAdmobInterstitial == null) {
            this.requestAdmobInterstitial(this.mCallerActivity);
        }
        else if (this.mAdmobInterstitial.isLoaded()) {
            this.mAdmobInterstitial.show();
            return true;
        }
        return false;
    }
    
    private boolean showAdmobRewardVideo() {
        // monitorenter(this)
        final boolean b = false;
        try {
            boolean b2;
            if (this.mAdmobRewardedVideo == null) {
                this.requestAdmobRewardedVideo(this.mCallerActivity);
                b2 = b;
            }
            else {
                b2 = b;
                if (this.mAdmobRewardedVideo.isLoaded()) {
                    final boolean nowShow = this.mRewardVideoListener.isNowShow();
                    b2 = b;
                    if (!nowShow) {
                        try {
                            this.mCheckerStartVideo.start();
                            this.mAdmobRewardedVideo.show();
                            b2 = true;
                        }
                        finally {
                            this.videoNoVideo();
                            if (this.mCallerActivity != null) {
                                this.requestAdmobRewardedVideo(this.mCallerActivity);
                            }
                        }
                    }
                }
            }
            return b2;
        }
        finally {
        }
        // monitorexit(this)
    }
    
    private void showAdsForAdsObject(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("showAdsForAdsObject  video ");
        sb.append(b);
        Log.v("SkanwordsAds", sb.toString());
        if (b) {
            this.showAdmobRewardVideo();
            return;
        }
        this.showAdmobInterstitial();
    }
    
    private boolean showAdsOfAdsObject(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(n);
        sb.append("  showAdsOfAdsObject");
        Log.v("SkanwordsAds", sb.toString());
        boolean showAdmobInterstitial;
        if ((n & 0x2) == 0x0 && (n & 0x8) == 0x0) {
            showAdmobInterstitial = ((n & 0x4) != 0x0 && this.showAdmobRewardVideo());
        }
        else {
            showAdmobInterstitial = this.showAdmobInterstitial();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("showAdsOfAdsObject result ");
        sb2.append(showAdmobInterstitial);
        Log.v("SkanwordsAds", sb2.toString());
        return showAdmobInterstitial;
    }
    
    private boolean showingAds() {
        return this.showingInterstitial || this.mRewardVideoListener.isNowShow();
    }
    
    private void startLoadInterstitial(final InterstitialAd interstitialAd) {
        if (interstitialAd != null && !interstitialAd.isLoaded() && this.mIsInterstitialLoading.compareAndSet(false, true)) {
            this.mAdmobInterstitial.loadAd(new AdRequest.Builder().addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB").build());
        }
    }
    
    private void startLoadVideo(final RewardedVideoAd rewardedVideoAd) {
        if (rewardedVideoAd != null && !rewardedVideoAd.isLoaded() && !this.mRewardVideoListener.isNowShow() && this.mIsVideoLoading.compareAndSet(false, true)) {
            this.mAdmobRewardedVideo.loadAd("ca-app-pub-9865553155301243/3454413415", new AdRequest.Builder().addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB").build());
        }
    }

    private void videoComplete(final String s) {
        final SmappsScanwords.ActionType app_ACTION = SmappsScanwords.ActionType.APP_ACTION;
        final StringBuilder sb = new StringBuilder();
        sb.append("offervideo_");
        sb.append(s);
        sb.append("_enter");
        SmappsScanwords.sendGAEvent(app_ACTION, sb.toString());
        if (this.mOfferCountVideo) {
            UserUpdatesManager.getInstance().offerUsed(this.mOfferTarget, s);
        }
        if (this.videoEventsListever != null) {
            this.mCallerActivity.runOnUiThread((Runnable)new AdsManager.AdsManager2(this));
        }
    }

    private void videoNoVideo() {
        Log.v("SkanwordsAds", "videoNoVideo");
        if (this.videoEventsListever != null) {
            this.mCallerActivity.runOnUiThread((Runnable)new AdsManager.AdsManager4(this));
        }
    }

    private void videoSkipped() {
        Log.v("SkanwordsAds", "videoSkipped");
        if (this.videoEventsListever != null) {
            this.mCallerActivity.runOnUiThread((Runnable)new AdsManager.AdsManager5(this));
        }
    }
    
    public void activityResumed(final Activity activity) {
        this.requestAdmobInterstitial(activity);
        this.requestAdmobRewardedVideo(activity);
    }
    
    public boolean hasRewardedVideo() {
        final boolean hasNetworkConnection = MainNetworkManager.getInstance().hasNetworkConnection();
        if (this.mAdmobRewardedVideo != null) {
            this.startLoadVideo(this.mAdmobRewardedVideo);
        }
        if (hasNetworkConnection && this.mAdmobRewardedVideo == null) {
            final Activity mCallerActivity = this.mCallerActivity;
            if (mCallerActivity != null) {
                this.requestAdmobRewardedVideo(mCallerActivity);
            }
        }
        return this.mAdmobRewardedVideo != null && this.mAdmobRewardedVideo.isLoaded();
    }

    public void init(final Activity activity) {
        MobileAds.initialize((Context)activity, "ca-app-pub-9865553155301243~7309675018");
        this.requestAdmobInterstitial(activity);
        this.requestAdmobRewardedVideo(activity);
        AdColonyBundleBuilder.setShowPrePopup(true);
        AdColonyBundleBuilder.setShowPostPopup(true);
        activity.getApplication().registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)new AdsManager.AdsManager1(this));
    }
    
    public void showAds(final AdsEventsListener adsEventsListener, final int n) {
        if (this.showingAds() || MainDataManager.getInstance().getUserData().isAdsOff() || MainDataManager.getInstance().getAppOffers().getVideoInterstitial() == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.showingAds());
            sb.append("  showAds  or adsoff ");
            sb.append(MainDataManager.getInstance().getUserData().isAdsOff());
            Log.v("SkanwordsAds", sb.toString());
            return;
        }
        this.rememberInterstitialShowTime();
        if (!this.showAdsOfAdsObject(n)) {
            Log.v("SkanwordsAds", "showAds  no more ads");
            adsEventsListener.onNoAds();
            return;
        }
        Log.v("SkanwordsAds", "showAds  has ads");
    }
    
    public void showInterstitial(final Activity mCallerActivity) {
        if (!this.showingAds() && this.checkInterstitialTimeDelay()) {
            if (MainDataManager.getInstance().getUserData().isAdsOff()) {
                return;
            }
            this.rememberInterstitialShowTime();
            Log.v("SkanwordsAds", " showInterstitial");
            this.mCallerActivity = mCallerActivity;
            this.showAdsForAdsObject(false);
        }
    }
    
    public void showVideoOffer(final Activity mCallerActivity, final AdsEventsListener videoEventsListever, final String mOfferTarget, final boolean mOfferCountVideo) {
        this.videoEventsListever = videoEventsListever;
        if (!MainNetworkManager.getInstance().hasNetworkConnection(mCallerActivity)) {
            this.videoEventsListever.onNoAds();
            return;
        }
        this.mOfferTarget = mOfferTarget;
        this.mOfferCountVideo = mOfferCountVideo;
        this.mCallerActivity = mCallerActivity;
        this.showAdsForAdsObject(true);
    }
    
    public static class AdsEventsListener
    {
        public AdsEventsListener() {
            super();
        }
        
        public void onComplete() {
        }
        
        public void onNoAds() {
        }
        
        public void onSkipped() {
        }
    }

    static private class CheckerStartVideo
    {
        private volatile TimerTask task;
        final /* synthetic */ AdsManager this0;
        private final Timer timer;

        private CheckerStartVideo(final AdsManager this0) {
            super();
            this.this0 = this0;
            this.timer = new Timer();
        }

        CheckerStartVideo(final AdsManager adsManager, final AdsManager1 adsManager1) {
            this(adsManager);
        }

         static TimerTask access1702(final CheckerStartVideo checkerStartVideo, final TimerTask task) {
            return checkerStartVideo.task = task;
        }

        public void reset() {
            synchronized (this) {
                if (this.task != null) {
                    this.task.cancel();
                    Log.v("SkanwordsAds", "timer.cancel();");
                }
                this.task = null;
            }
        }

        public void start() {
            synchronized (this) {
                this.reset();
                Log.v("SkanwordsAds", "timer.start();");
                this.task = (TimerTask)new CheckerStartVideo.AdsManagerCheckerStartVideo1(this);
                this.timer.schedule(this.task, 3000L);
            }
        }

        class AdsManagerCheckerStartVideo1 extends TimerTask {
            final /* synthetic */ CheckerStartVideo this1;

            AdsManagerCheckerStartVideo1(final CheckerStartVideo this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                Log.v("SkanwordsAds", "run");
                AdsManager.access1400(this.this1.this0);
                CheckerStartVideo.access1702(this.this1, (TimerTask)null);
            }
        }
    }
    
    private class RewardVideoListener implements RewardedVideoAdListener
    {
        private volatile boolean mIsNowShow;
        private volatile boolean mIsRewarded;
        final /* synthetic */ AdsManager this0;
        
        private RewardVideoListener(final AdsManager this0) {
            super();
            this.this0 = this0;
        }
        
        RewardVideoListener(final AdsManager adsManager, final AdsManager1 applicationActivityLifecycleCallbacks) {
            this(adsManager);
        }
        
        boolean isNowShow() {
            return this.mIsNowShow;
        }
        
        @Override
        public void onRewarded(final RewardItem rewardItem) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onRewarded ");
            sb.append(rewardItem);
            Log.v("SkanwordsAds", sb.toString());
            this.mIsRewarded = true;
            this.mIsNowShow = false;
            AdsManager.this.videoComplete("acv");
            final AdsManager this0 = this.this0;
            this0.startLoadVideo(this0.mAdmobRewardedVideo);
        }
        
        @Override
        public void onRewardedVideoAdClosed() {
            Log.v("SkanwordsAds", "onRewardedVideoAdClosed");
            if (this.mIsNowShow) {
                AdsManager.this.videoSkipped();
            }
            else if (!this.mIsRewarded) {
                AdsManager.this.videoNoVideo();
            }
            this.mIsNowShow = false;
            final AdsManager this0 = this.this0;
            this0.startLoadVideo(this0.mAdmobRewardedVideo);
        }
        
        @Override
        public void onRewardedVideoAdFailedToLoad(final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onRewardedVideoAdFailedToLoad, error:");
            sb.append(n);
            Log.v("SkanwordsAds", sb.toString());
            this.this0.mIsVideoLoading.set(false);
            this.mIsNowShow = false;
            final AdsManager this0 = this.this0;
            this0.startLoadVideo(this0.mAdmobRewardedVideo);
        }
        
        @Override
        public void onRewardedVideoAdLeftApplication() {
            Log.v("SkanwordsAds", "onRewardedVideoAdLeftApplication ");
        }
        
        @Override
        public void onRewardedVideoAdLoaded() {
            final StringBuilder sb = new StringBuilder();
            sb.append("onRewardedVideoAdLoaded ");
            sb.append(this.this0.mAdmobRewardedVideo.getClass()); //TODO getMediationAdapterClassName() не найден
            Log.v("SkanwordsAds", sb.toString());
            this.this0.mIsVideoLoading.set(false);
        }
        
        @Override
        public void onRewardedVideoAdOpened() {
            this.mIsRewarded = false;
            this.this0.mCheckerStartVideo.reset();
            Log.v("SkanwordsAds", "onRewardedVideoAdOpened");
        }
        
        @Override
        public void onRewardedVideoStarted() {
            this.mIsNowShow = true;
            Log.v("SkanwordsAds", "onRewardedVideoStarted");
        }
    }

    class AdsManager3 extends AdListener {
        final /* synthetic */ AdsManager this0;

        AdsManager3(final AdsManager this0) {
            super();
            this.this0 = this0;
        }

        public void onAdClosed() {
            AdsManager.access902(this.this0, false);
            final AdsManager this0 = this.this0;
            AdsManager.access1000(this0, AdsManager.access800(this0));
            super.onAdClosed();
        }

        public void onAdFailedToLoad(final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onAdFailedToLoad  ");
            sb.append(n);
            Log.v("SkanwordsAds", sb.toString());
            AdsManager.access600(this.this0).set(false);
            AdsManager.access700(this.this0);
            super.onAdFailedToLoad(n);
        }

        public void onAdLoaded() {
            super.onAdLoaded();
            AdsManager.access600(this.this0).set(false);
            final StringBuilder sb = new StringBuilder();
            sb.append("onAdLoaded ");
            sb.append(AdsManager.access800(this.this0).isLoaded());
            Log.v("SkanwordsAds", sb.toString());
        }

        public void onAdOpened() {
            AdsManager.access902(this.this0, true);
            super.onAdOpened();
        }
    }

    class AdsManager2 implements Runnable {
        final /* synthetic */ AdsManager this0;

        AdsManager2(final AdsManager this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            AdsManager.access500(this.this0).onComplete();
            AdsManager.access502(this.this0, (AdsManager.AdsEventsListener)null);
        }
    }

    class AdsManager4 implements Runnable {
        final /* synthetic */ AdsManager this0;

        AdsManager4(final AdsManager this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            AdsManager.access500(this.this0).onNoAds();
            AdsManager.access502(this.this0, (AdsManager.AdsEventsListener)null);
        }
    }

    class AdsManager5 implements Runnable {
        final /* synthetic */ AdsManager this0;

        AdsManager5(final AdsManager this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            AdsManager.access500(this.this0).onSkipped();
            AdsManager.access502(this.this0, (AdsManager.AdsEventsListener)null);
        }
    }

    class AdsManager1 implements Application.ActivityLifecycleCallbacks {
        final /* synthetic */ AdsManager this0;

        AdsManager1(final AdsManager this0) {
            super();
            this.this0 = this0;
        }

        public void onActivityCreated(final Activity activity, final Bundle bundle) {
            AdsManager.access200(this.this0, activity);
            AdsManager.access300(this.this0, activity);
        }

        public void onActivityDestroyed(final Activity activity) {
        }

        public void onActivityPaused(final Activity activity) {
            if (AdsManager.access400(this.this0) != null) {
                AdsManager.access400(this.this0).pause((Context)activity);
            }
            Chartboost.onPause(activity);
        }

        public void onActivityResumed(final Activity activity) {
            if (AdsManager.access400(this.this0) != null) {
                AdsManager.access400(this.this0).resume((Context)activity);
            }
            Chartboost.onResume(activity);
        }

        public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
        }

        public void onActivityStarted(final Activity activity) {
            AdsManager.access200(this.this0, activity);
            AdsManager.access300(this.this0, activity);
            Chartboost.onStart(activity);
        }

        public void onActivityStopped(final Activity activity) {
            Chartboost.onStop(activity);
        }
    }
}
