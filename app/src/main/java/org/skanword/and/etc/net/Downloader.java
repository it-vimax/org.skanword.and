package org.skanword.and.etc.net;

import java.util.*;
import java.io.*;
import android.content.*;

public interface Downloader
{
    void download(final Calendar p0) throws IOException;
    
    String getFilename(final Calendar p0);
    
    String getName();
    
    boolean isPuzzleAvailable(final Calendar p0);
    
    void setContext(final Context p0);
    
    String sourceUrl(final Calendar p0);
}
