package org.skanword.and.etc.net;

import java.util.*;

public class BEQDownloader extends AbstractPageScraper
{
    public BEQDownloader() {
        super("Brendan Emmett Quigley");
    }
    
    @Override
    protected String getScrapeURL(final Calendar calendar) {
        return "http://www.brendanemmettquigley.com/";
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        final int value = calendar.get(Calendar.DAY_OF_WEEK);
        return value == 2 || value == 5;
    }
}
