package org.skanword.and.etc.net;

import java.util.*;

public class PatrickBlindauerDownloader extends AbstractPageScraper
{
    private static final String[] BLINDAUER_SHORT_MONTHS;
    
    static {
        BLINDAUER_SHORT_MONTHS = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    }
    
    public PatrickBlindauerDownloader() {
        super("Patrick Blindauer");
    }
    
    @Override
    protected String getScrapeURL(final Calendar calendar) {
        final int value = calendar.get(Calendar.MONTH);
        String s = PatrickBlindauerDownloader.BLINDAUER_SHORT_MONTHS[value];
        Label_0048: {
            if (value == 8) {
                final int value2 = calendar.get(Calendar.YEAR);
                if (value2 != 2011) {
                    s = s;
                    if (value2 != 2012) {
                        break Label_0048;
                    }
                }
                s = "Sept";
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("http://www.patrickblindauer.com/Free_Monthly_Puzzles/");
        sb.append(s);
        sb.append("_");
        sb.append(calendar.get(Calendar.YEAR));
        sb.append("/");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DATE) == 1;
    }
}
