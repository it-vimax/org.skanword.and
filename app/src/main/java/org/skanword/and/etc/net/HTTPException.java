package org.skanword.and.etc.net;

import java.io.*;

public class HTTPException extends IOException
{
    private static final long serialVersionUID = -4463986330408368898L;
    private int mStatus;
    
    public HTTPException(final int mStatus) {
        super();
        this.mStatus = mStatus;
    }
    
    public int getStatus() {
        return this.mStatus;
    }
}
