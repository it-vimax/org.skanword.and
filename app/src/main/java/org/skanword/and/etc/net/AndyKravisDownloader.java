package org.skanword.and.etc.net;

import java.util.*;

public class AndyKravisDownloader extends BloggerDownloader
{
    private static final Calendar SWITCHOVER_DATE;
    
    static {
        SWITCHOVER_DATE = AbstractDownloader.createDate(2013, 10, 14);
    }
    
    public AndyKravisDownloader() {
        super("Andy Kravis", "http://cruciverbalistatlaw.blogspot.com");
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        final int compareTo = calendar.compareTo(AndyKravisDownloader.SWITCHOVER_DATE);
        final boolean b = false;
        boolean b2 = false;
        if (compareTo >= 0) {
            if (calendar.get(Calendar.DAY_OF_WEEK) == 1) {
                b2 = true;
            }
            return b2;
        }
        boolean b3 = b;
        if (calendar.get(Calendar.DAY_OF_WEEK) == 7) {
            b3 = true;
        }
        return b3;
    }
}
