package org.skanword.and.etc.net;

import android.os.*;
import android.content.*;

public class DownloadReceiver extends BroadcastReceiver
{
    private BroadcastReceiver impl;
    
    public DownloadReceiver() {
        super();
        if (Build.VERSION.SDK_INT >= 9) {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("com.adamrosenfield.wordswithcrosses.net.");
                sb.append("DownloadReceiverGinger");
                this.impl = (BroadcastReceiver)Class.forName(sb.toString()).newInstance();
            }
            catch (InstantiationException ex) {
                ex.printStackTrace();
            }
            catch (IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
            catch (ClassNotFoundException ex3) {
                ex3.printStackTrace();
            }
        }
        if (this.impl == null) {
            this.impl = new DownloadReceiverNoop();
        }
    }
    
    public void onReceive(final Context context, final Intent intent) {
        this.impl.onReceive(context, intent);
    }
}
