package org.skanword.and.etc.net;

import java.util.*;

public class ErikAgardDownloader extends BloggerDownloader
{
    public ErikAgardDownloader() {
        super("Erik Agard", "http://gluttonforpun.blogspot.com");
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 4;
    }
}
