package org.skanword.and.etc.net;

import java.util.*;

public class WaPoPuzzlerDownloader extends AbstractJPZDownloader
{
    private static final String NAME = "Washington Post Puzzler";
    
    public WaPoPuzzlerDownloader() {
        super("http://cdn.games.arkadiumhosted.com/washingtonpost/puzzler/", "Washington Post Puzzler");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append("puzzle_");
        sb.append(WaPoPuzzlerDownloader.DEFAULT_NF.format(calendar.get(Calendar.YEAR) % 100));
        sb.append(WaPoPuzzlerDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(WaPoPuzzlerDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".xml");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 1;
    }
}
