package org.skanword.and.etc;

import java.util.concurrent.*;
import android.content.*;
import android.app.*;
import android.os.*;
import android.util.*;
import java.util.*;

public class Foreground implements Application.ActivityLifecycleCallbacks
{
    public static final long CHECK_DELAY = 500L;
    public static final String TAG = "and.etc.Foreground";
    private static Foreground instance;
    private Runnable check;
    private boolean foreground;
    private Handler handler;
    private List<Listener> listeners;
    private boolean paused;
    
    public Foreground() {
        super();
        this.foreground = false;
        this.paused = true;
        this.handler = new Handler();
        this.listeners = new CopyOnWriteArrayList<Listener>();
    }
    
    static /* synthetic */ boolean access000(final Foreground foreground) {
        return foreground.foreground;
    }
    
    static /* synthetic */ boolean access002(final Foreground foreground, final boolean foreground2) {
        return foreground.foreground = foreground2;
    }
    
    static /* synthetic */ boolean access100(final Foreground foreground) {
        return foreground.paused;
    }
    
    static /* synthetic */ List<Listener> access200(final Foreground foreground) {
        return foreground.listeners;
    }
    
    public static Foreground get() {
        final Foreground instance = Foreground.instance;
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException("Foreground is not initialised - invoke at least once with parameterised init/get");
    }
    
    public static Foreground get(final Application application) {
        if (Foreground.instance == null) {
            init(application);
        }
        return Foreground.instance;
    }
    
    public static Foreground get(Context applicationContext) {
        final Foreground instance = Foreground.instance;
        if (instance == null) {
            applicationContext = applicationContext.getApplicationContext();
            if (applicationContext instanceof Application) {
                init((Application)applicationContext);
            }
            throw new IllegalStateException("Foreground is not initialised and cannot obtain the Application object");
        }
        return instance;
    }
    
    public static Foreground init(final Application application) {
        if (Foreground.instance == null) {
            application.registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)(Foreground.instance = new Foreground()));
        }
        return Foreground.instance;
    }
    
    public void addListener(final Listener listener) {
        this.listeners.add(listener);
    }
    
    public boolean isBackground() {
        return this.foreground ^ true;
    }
    
    public boolean isForeground() {
        return this.foreground;
    }
    
    public void onActivityCreated(final Activity activity, final Bundle bundle) {
    }
    
    public void onActivityDestroyed(final Activity activity) {
    }
    
    public void onActivityPaused(final Activity activity) {
        this.paused = true;
    }
    
    public void onActivityResumed(final Activity activity) {
        this.paused = false;
    }
    
    public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
    }
    
    public void onActivityStarted(final Activity activity) {
        final boolean foreground = this.foreground;
        this.foreground = true;
        final Runnable check = this.check;
        if (check != null) {
            this.handler.removeCallbacks(check);
        }
        if (foreground ^ true) {
            Log.i(Foreground.TAG, "went foreground");
            for (final Listener listener : this.listeners) {
                try {
                    listener.onBecameForeground(activity);
                }
                catch (Exception ex) {
                    Log.e(Foreground.TAG, "Listener threw exception!", (Throwable)ex);
                }
            }
        }
        else {
            Log.i(Foreground.TAG, "still foreground");
        }
    }

    public void onActivityStopped(final Activity activity) {
        final Runnable check = this.check;
        if (check != null) {
            this.handler.removeCallbacks(check);
        }
        this.handler.postDelayed(this.check = (Runnable)new Foreground.Foreground1(this, activity), 500L);
    }
    
    public void removeListener(final Listener listener) {
        this.listeners.remove(listener);
    }
    
    public interface Listener
    {
        void onBecameBackground(final Activity p0);
        
        void onBecameForeground(final Activity p0);
    }

    class Foreground1 implements Runnable {
        final /* synthetic */ Foreground this0;
        final /* synthetic */ Activity valactivity;

        Foreground1(final Foreground this0, final Activity valactivity) {
            super();
            this.this0 = this0;
            this.valactivity = valactivity;
        }

        @Override
        public void run() {
            if (Foreground.access000(this.this0) && Foreground.access100(this.this0)) {
                Foreground.access002(this.this0, false);
                Log.i(Foreground.TAG, "went background");
                for (final Foreground.Listener listener : Foreground.access200(this.this0)) {
                    try {
                        listener.onBecameBackground(this.valactivity);
                    }
                    catch (Exception ex) {
                        Log.e(Foreground.TAG, "Listener threw exception!", (Throwable)ex);
                    }
                }
            }
            else {
                Log.i(Foreground.TAG, "still foreground");
            }
        }
    }
}
