package org.skanword.and.etc.net;

import java.util.*;

public class LATimesDownloader extends AbstractJPZDownloader
{
    private static final String NAME = "Los Angeles Times";
    
    public LATimesDownloader() {
        super("http://cdn.games.arkadiumhosted.com/latimes/assets/DailyCrossword/", "Los Angeles Times");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append("la");
        sb.append(calendar.get(Calendar.YEAR) % 100);
        sb.append(LATimesDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(LATimesDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".xml");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return true;
    }
}
