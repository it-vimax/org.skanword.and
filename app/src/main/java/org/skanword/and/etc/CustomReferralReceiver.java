package org.skanword.and.etc;

import android.content.*;
import com.google.android.gms.analytics.*;
import java.net.*;

public class CustomReferralReceiver extends BroadcastReceiver
{
    public CustomReferralReceiver() {
        super();
    }
    
    public static String getReferrer(final Context context) {
        return context.getSharedPreferences("referrer", 0).getString("referrer", (String)null);
    }
    
    public void onReceive(final Context context, final Intent intent) {
        new CampaignTrackingReceiver().onReceive(context, intent);
        try {
            if (intent.getAction().equals("com.android.vending.INSTALL_REFERRER")) {
                final String stringExtra = intent.getStringExtra("referrer");
                if (stringExtra != null) {
                    context.getSharedPreferences("referrer", 0).edit().putString("referrer", URLDecoder.decode(stringExtra, "UTF-8")).commit();
                }
            }
        }
        catch (Exception ex) {}
    }
}
