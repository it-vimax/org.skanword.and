package org.skanword.and.etc.net;

import java.util.*;
import java.io.*;
import java.util.regex.*;
import java.util.logging.*;

public class NevilleFogartyDownloader extends AbstractDownloader
{
    private static final String BASE_URL = "http://nevillefogarty.wordpress.com/";
    private static final Pattern CONTINUE_READING_PATTERN;
    private static final String CONTINUE_READING_REGEX = "<a href=\"([^\"]*)\">Continue reading";
    private static final String NAME = "Neville Fogarty";
    private static final Pattern PUZZLE_PATTERN;
    private static final String PUZZLE_REGEX = "href=\"([^\"]*)\"><img [^>]*title=\"Download \\.puz file\"";
    
    static {
        CONTINUE_READING_PATTERN = Pattern.compile("<a href=\"([^\"]*)\">Continue reading");
        PUZZLE_PATTERN = Pattern.compile("href=\"([^\"]*)\"><img [^>]*title=\"Download \\.puz file\"");
    }
    
    public NevilleFogartyDownloader() {
        super("", "Neville Fogarty");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        return "";
    }
    
    @Override
    public void download(final Calendar calendar) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("http://nevillefogarty.wordpress.com/");
        sb.append(calendar.get(Calendar.YEAR));
        sb.append("/");
        sb.append(NevilleFogartyDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append("/");
        sb.append(NevilleFogartyDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append("/");
        final String string = sb.toString();
        final Matcher matcher = NevilleFogartyDownloader.CONTINUE_READING_PATTERN.matcher(this.downloadUrlToString(string));
        if (!matcher.find()) {
            final Logger log = NevilleFogartyDownloader.LOG;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to find \"Continue reading\" link on page: ");
            sb2.append(string);
            log.warning(sb2.toString());
            throw new IOException("Failed to scrape archives page");
        }
        final String resolveUrl = AbstractDownloader.resolveUrl(string, matcher.group(1));
        final Matcher matcher2 = NevilleFogartyDownloader.PUZZLE_PATTERN.matcher(this.downloadUrlToString(resolveUrl));
        if (matcher2.find()) {
            super.download(calendar, AbstractDownloader.resolveUrl(resolveUrl, matcher2.group(1)));
            return;
        }
        final Logger log2 = NevilleFogartyDownloader.LOG;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Failed to find puzzle link on page: ");
        sb3.append(resolveUrl);
        log2.warning(sb3.toString());
        throw new IOException("Failed to scrape puzzle link");
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 6;
    }
}
