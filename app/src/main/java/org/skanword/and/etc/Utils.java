package org.skanword.and.etc;

import android.app.*;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.*;
import org.skanword.and.*;
import android.provider.*;
import android.telephony.*;
import java.lang.reflect.*;
import java.security.*;
import java.util.*;

public class Utils
{
    public Utils() {
        super();
    }
    
    public static void brandAlertDialog(final AlertDialog alertDialog) {
        try {
            final Resources resources = alertDialog.getContext().getResources();
            alertDialog.getWindow().getDecorView().findViewById(resources.getIdentifier("titleDivider", "id", "android")).setBackgroundColor(resources.getColor(R.color.background_tab_pressed)); //TODO не точный цвет
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static <T> boolean compareLists(final List<T> list, final List<T> list2) {
        return list.equals(list2);
    }
    
    public static String getAdvertiserId() {
        return null;
    }
    
    public static String getAndroidId() {
        return Settings.Secure.getString(SmappsScanwords.getContext().getContentResolver(), "android_id");
    }

    public static String getAndroidUniqueDeviceId() {
        StringBuffer var3 = new StringBuffer();
        TelephonyManager var0 = (TelephonyManager)SmappsScanwords.getContext().getSystemService(Context.TELEPHONY_SERVICE);

        try {
            String var2 = var0.getDeviceId();
            StringBuilder var1 = new StringBuilder();
            var1.append("IMEI No : ");
            var1.append(var2);
            var1.append("\n");
            var3.append(var1.toString());
            String var4 = var0.getSubscriberId();
            StringBuilder var12 = new StringBuilder();
            var12.append("IMSI No : ");
            var12.append(var4);
            var12.append("\n");
            var3.append(var12.toString());

            String var13;
            label31: {
                try {
                    Class var14 = Class.forName("android.os.SystemProperties");
                    var13 = (String)((String)var14.getMethod("get", String.class, String.class).invoke(var14, "ro.serialno", "unknown"));
                } catch (Exception var11) {
                    var13 = null;
                    break label31;
                }

                try {
                    var1 = new StringBuilder();
                    var1.append("serial : ");
                    var1.append(var13);
                    var1.append("\n");
                    var3.append(var1.toString());
                } catch (Exception var9) {
                }
            }

            String var15;
            label27: {
                try {
                    Class var16 = Class.forName("android.os.SystemProperties");
                    Method[] var5 = var16.getMethods();
                    String var6 = new String("ro.serialno");
                    String var7 = new String("Unknown");
                    var15 = (String)((String)var5[2].invoke(var16, var6, var7));
                } catch (Exception var10) {
                    var15 = null;
                    break label27;
                }

                try {
                    StringBuilder var18 = new StringBuilder();
                    var18.append("serial2 : ");
                    var18.append(var15);
                    var18.append("\n");
                    var3.append(var18.toString());
                } catch (Exception var8) {
                }
            }

            String var19 = Settings.Secure.getString(SmappsScanwords.getContext().getContentResolver(), "android_id");
            StringBuilder var20 = new StringBuilder();
            var20.append("AndroidID : ");
            var20.append(var19);
            var20.append("\n");
            var3.append(var20.toString());
            StringBuilder var17 = new StringBuilder();
            var17.append(var2);
            var17.append(var4);
            var17.append(var19);
            var17.append(var13);
            var17.append(var15);
            return md5(var17.toString());
        } catch (SecurityException ex) {
            return  "SecurityException from getAndroidUniqueDeviceId()";
        }
    }

    public static String getDeviceId() {
        try {
            return ((TelephonyManager)SmappsScanwords.getContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        } catch (SecurityException ex) {
            return  "SecurityException from getDeviceId()";
        }
    }
    
    public static int getResId(final String s, final Class<?> clazz) {
        try {
            final Field declaredField = clazz.getDeclaredField(s);
            return declaredField.getInt(declaredField);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }
    
    public static String join(final List<Integer> list, final String s) {
        final StringBuilder sb = new StringBuilder();
        final Iterator<Integer> iterator = list.iterator();
        String s2 = "";
        while (iterator.hasNext()) {
            final Integer n = iterator.next();
            sb.append(s2);
            sb.append(n);
            s2 = s;
        }
        return sb.toString();
    }
    
    public static String md5(String s) {
        try {
            final MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(s.getBytes());
            final byte[] digest = instance.digest();
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < digest.length; ++i) {
                StringBuilder sb2;
                for (s = Integer.toHexString(digest[i] & 0xFF); s.length() < 2; s = sb2.toString()) {
                    sb2 = new StringBuilder();
                    sb2.append("0");
                    sb2.append(s);
                }
                sb.append(s);
            }
            s = sb.toString();
            return s;
        }
        catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return "";
        }
    }
    
    public static int randInt(final int n, final int n2) {
        return new Random().nextInt(n2 - n + 1) + n;
    }
    
    public static String secondsToMaxTimeType(final long n) {
        final int n2 = (int)n;
        final int n3 = n2 / 60;
        final int n4 = n2 / 3600;
        final int n5 = n2 / 86400;
        if (n5 > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(n5);
            sb.append(" ");
            sb.append(spellVariantForNumber(n5, "день", "дня", "дней"));
            return sb.toString();
        }
        if (n4 > 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(n4);
            sb2.append(" ");
            sb2.append(spellVariantForNumber(n4,  "час", "часа", "часов"));
            return sb2.toString();
        }
        if (n3 > 0) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(n3);
            sb3.append(" ");
            sb3.append(spellVariantForNumber(n3, "минута", "минуты", "минут"));
            return sb3.toString();
        }
        if (n2 > 0) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(n2);
            sb4.append(" ");
            sb4.append(spellVariantForNumber(n2, "секунда", "секунды", "секунд"));
            return sb4.toString();
        }
        return "unknown";
    }
    
    public static String secondsToTimerType(final long n) {
        final int n2 = (int)n;
        return String.format("%02d:%02d:%02d", n2 / 3600, n2 / 60 % 60, n2 % 60);
    }
    
    public static String spellVariantForNumber(final int n, final String s, final String s2, final String s3) {
        final int n2 = n % 100;
        if (n2 > 10 && n2 < 20) {
            return s3;
        }
        switch (n % 10) {
            default: {
                return s;
            }
            case 5:
            case 6:
            case 7:
            case 8:
            case 9: {
                return s3;
            }
            case 2:
            case 3:
            case 4: {
                return s2;
            }
            case 1: {
                return s;
            }
            case 0: {
                return s3;
            }
        }
    }
}
