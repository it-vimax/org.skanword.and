package org.skanword.and.etc.versions;

import java.util.logging.*;
import org.apache.http.impl.client.*;
import android.content.*;
import org.apache.http.params.*;
import org.apache.http.protocol.*;
import org.apache.http.client.methods.*;
import java.util.zip.*;
import java.net.*;
import org.skanword.and.etc.net.*;
import org.skanword.and.*;
import java.io.*;
import android.app.*;
import org.apache.http.client.*;
import android.view.*;
import android.preference.*;
import java.util.*;
import org.apache.http.*;

public class DefaultUtil implements AndroidVersionUtils
{
    protected static final Logger LOG;
    protected Context context;
    protected DefaultHttpClient mHttpClient;
    protected HttpParams mHttpParams;
    protected SharedPreferences prefs;
    
    static {
        LOG = Logger.getLogger("com.adamrosenfield.wordswithcrosses");
    }
    
    public DefaultUtil() {
        super();
        HttpConnectionParams.setConnectionTimeout(this.mHttpParams = (HttpParams)new BasicHttpParams(), 30000);
        HttpConnectionParams.setSoTimeout(this.mHttpParams, 30000);
        this.mHttpClient = new DefaultHttpClient(this.mHttpParams);
    }

    private void downloadHelper(URL paramURL, String paramString, Map<String, String> paramMap, HttpContext paramHttpContext, OutputStream paramOutputStream)  throws IOException
    {
        try
        {
            HttpGet httpGet = new HttpGet(paramURL.toURI());
            httpGet.setHeader("Accept-Encoding", "gzip, deflate");
            Iterator inter = paramMap.entrySet().iterator();
            while (inter.hasNext())
            {
                Map.Entry entry = (Map.Entry)inter.next();
                httpGet.setHeader((String)entry.getKey(), (String)entry.getValue());
            }
            HttpResponse httpResponse = this.mHttpClient.execute((HttpUriRequest)httpGet, paramHttpContext);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            HttpEntity httpEntity = httpResponse.getEntity();
            if (statusCode != 200) {
                final Logger log = DefaultUtil.LOG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Download failed: ");
                sb.append(paramString);
                sb.append(" status=");
                sb.append(statusCode);
                log.warning(sb.toString());
                if (httpEntity != null) {
                    ((HttpEntity)httpEntity).consumeContent();
                }
                throw new HTTPException(statusCode);
            }
            if (httpEntity != null) {
                final InputStream content = ((HttpEntity)httpEntity).getContent();
                final Header contentEncoding = ((HttpEntity)httpEntity).getContentEncoding();
                if (contentEncoding != null) {
                    if ("gzip".equals(contentEncoding.getValue())) {
                        new GZIPInputStream(content);
                    }
                    else if ("deflate".equals(contentEncoding.getValue())) {
                        final InflaterInputStream inflaterInputStream = new InflaterInputStream(content, new Inflater(true));
                    }
                }
                ((HttpEntity)httpEntity).consumeContent();
            }
        }
        catch (URISyntaxException ex) {
            ex.printStackTrace();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid URL: ");
            sb2.append(paramURL);
            throw new IOException(sb2.toString());
        }
    }

    @Override
    public void downloadFile(final URL url, final Map<String, String> map, final File file, final boolean b, final String s) throws IOException {
        this.downloadFile(url, map, file, b, s, null);
    }
    
    @Override
    public void downloadFile(final URL url, final Map<String, String> map, final File file, final boolean b, String scrubUrl, final HttpContext httpContext) throws IOException {
        scrubUrl = AbstractDownloader.scrubUrl(url);
        final File file2 = new File(SmappsScanwords.TEMP_DIR, file.getName());
        final Logger log = DefaultUtil.LOG;
        final StringBuilder sb = new StringBuilder();
        sb.append("DefaultUtil: Downloading ");
        sb.append(scrubUrl);
        sb.append(" ==> ");
        sb.append(file2);
        log.info(sb.toString());
        final FileOutputStream fileOutputStream = new FileOutputStream(file2);
        try {
            this.downloadHelper(url, scrubUrl, map, httpContext, fileOutputStream);
            fileOutputStream.close();
            if (!file2.equals(file) && !file2.renameTo(file)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to rename ");
                sb2.append(file2);
                sb2.append(" to ");
                sb2.append(file);
                throw new IOException(sb2.toString());
            }
            final Logger log2 = DefaultUtil.LOG;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("DefaultUtil: Download succeeded: ");
            sb3.append(scrubUrl);
            log2.info(sb3.toString());
        }
        finally {
            fileOutputStream.close();
        }
    }
    
    @Override
    public String downloadToString(final URL url, final Map<String, String> map) throws IOException {
        return this.downloadToString(url, map, null);
    }
    
    @Override
    public String downloadToString(final URL url, final Map<String, String> map, final HttpContext httpContext) throws IOException {
        final String scrubUrl = AbstractDownloader.scrubUrl(url);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.downloadHelper(url, scrubUrl, map, httpContext, byteArrayOutputStream);
        return new String(byteArrayOutputStream.toByteArray());
    }
    
    @Override
    public void finishOnHomeButton(final Activity activity) {
    }
    
    @Override
    public HttpClient getHttpClient() {
        return (HttpClient)this.mHttpClient;
    }
    
    @Override
    public void hideActionBar(final Activity activity) {
    }
    
    @Override
    public void hideWindowTitle(final Activity activity) {
    }
    
    @Override
    public void holographic(final Activity activity) {
    }
    
    @Override
    public View onActionBarCustom(final Activity activity, final int n) {
        return null;
    }
    
    @Override
    public void onActionBarWithText(final MenuItem menuItem) {
    }
    
    @Override
    public void onActionBarWithText(final SubMenu subMenu) {
    }
    
    @Override
    public void onFileDownloaded(final long n, final boolean b, final int n2) {
    }
    
    @Override
    public void setContext(final Context context) {
        this.context = context;
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }
}
