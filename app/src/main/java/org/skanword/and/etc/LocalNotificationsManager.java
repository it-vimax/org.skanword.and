package org.skanword.and.etc;

import org.skanword.and.*;
import android.util.*;
import android.app.*;
import android.content.*;
import org.skanword.and.datamanager.*;
import org.skanword.and.network.*;

import java.io.Serializable;
import java.util.*;

public class LocalNotificationsManager
{
    public static final String EXTRA_NOTIFICATION_ID = "EXTRA_NOTIFICATION_ID";
    public static final String EXTRA_NOTIFICATION_MESSAGE = "EXTRA_NOTIFICATION_MESSAGE";
    public static final String EXTRA_NOTIFICATION_TITLE = "EXTRA_NOTIFICATION_TITLE";
    private static Integer bonusNotifsMutext;

    static {
        LocalNotificationsManager.bonusNotifsMutext = 1;
    }

    public LocalNotificationsManager() {
        super();
    }

    static /* synthetic */ Integer access000() {
        return LocalNotificationsManager.bonusNotifsMutext;
    }

    public static void clearBonusNotifs() {
        SmappsScanwords.getExecutorService().execute((Runnable)new LocalNotificationsManager.LocalNotificationsManager2());
    }

    public static void createDailyBonusNotifications(final DailyBonusObject dailyBonusObject, final Context context) {
        SmappsScanwords.getExecutorService().execute((Runnable)new LocalNotificationsManager.LocalNotificationsManager1(context, dailyBonusObject));
    }

    public static void createLocalNotifications(final Context context, final boolean b) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        if (MainDataManager.getInstance().getOptions() != null && MainDataManager.getInstance().getLocalNotifications() != null) {
            final Calendar instance = Calendar.getInstance();
            final Iterator<ConfigObject.NotificationRemind> iterator = (Iterator<ConfigObject.NotificationRemind>)MainDataManager.getInstance().getLocalNotifications().getRemindOn().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final ConfigObject.NotificationRemind configObjectNotificationRemind = iterator.next();
                if (configObjectNotificationRemind.getDays().size() < 1) {
                    continue;
                }
                int customNotificationTimeHour;
                if (MainDataManager.getInstance().getOptions().isDefaultNotificationsTime()) {
                    customNotificationTimeHour = 0;
                }
                else {
                    customNotificationTimeHour = MainDataManager.getInstance().getOptions().getCustomNotificationTimeHour();
                }
                int customNotificationTimeMinute;
                if (MainDataManager.getInstance().getOptions().isDefaultNotificationsTime()) {
                    customNotificationTimeMinute = 0;
                }
                else {
                    customNotificationTimeMinute = MainDataManager.getInstance().getOptions().getCustomNotificationTimeMinute();
                }
                final Calendar instance2 = Calendar.getInstance();
                instance2.setTimeZone(TimeZone.getTimeZone("UTC"));
                instance2.set(Calendar.HOUR_OF_DAY, customNotificationTimeHour);
                instance2.set(Calendar.MINUTE, customNotificationTimeMinute);
                int n2;
                if (instance2.get(Calendar.HOUR_OF_DAY) <= instance.get(Calendar.HOUR_OF_DAY) && instance2.get(Calendar.MINUTE) <= instance.get(Calendar.MINUTE) && configObjectNotificationRemind.getDays().get(0) == 0) {
                    n2 = 1;
                }
                else {
                    n2 = 0;
                }
                if (n2 != 0) {
                    continue;
                }
                final Intent intent = new Intent(context, (Class)SmappsReceiver.class);
                intent.putExtra("EXTRA_NOTIFICATION_MESSAGE", configObjectNotificationRemind.getText());
                intent.putExtra("EXTRA_NOTIFICATION_TITLE", "Сканворд дня!");
                final Integer n3 = configObjectNotificationRemind.getDays().get(0);
                intent.putExtra("EXTRA_NOTIFICATION_ID", String.format("remind_on_%dd", n3));
                final PendingIntent broadcast = PendingIntent.getBroadcast(context, n, intent, Intent.FILL_IN_DATA);
                instance2.add(Calendar.DAY_OF_YEAR, n3 + n2);
                final StringBuilder sb = new StringBuilder();
                sb.append("set issue notification time  ");
                sb.append(instance2.get(Calendar.DAY_OF_YEAR));
                sb.append("  ");
                sb.append(instance2.get(Calendar.HOUR_OF_DAY));
                sb.append("  ");
                sb.append(instance2.get(Calendar.MINUTE));
                Log.v("SkanwordsNotif", sb.toString());
                if (MainDataManager.getInstance().getOptions().isInformNewIssues() && (instance2.getTimeInMillis() - TimeZone.getDefault().getRawOffset()) / 1000L - new Date().getTime() / 1000L > 0L) {
                    alarmManager.set(AlarmManager.RTC, instance2.getTimeInMillis() - TimeZone.getDefault().getRawOffset(), broadcast);
                }
                else {
                    alarmManager.cancel(broadcast);
                }
                ++n;
            }
        }
    }

    static final class LocalNotificationsManager2 implements Runnable {
        LocalNotificationsManager2() {
            super();
        }

        @Override
        public void run() {
            synchronized (LocalNotificationsManager.access000()) {
                Log.v("SkanwordsNotif", " clearBonusNotifs ");
                final AlarmManager alarmManager = (AlarmManager)SmappsScanwords.getContext().getSystemService(Context.ALARM_SERVICE);
                for (int i = 0; i < 15; ++i) {
                    alarmManager.cancel(PendingIntent.getBroadcast(SmappsScanwords.getContext(), i + 100, new Intent(SmappsScanwords.getContext(), (Class)SmappsReceiver.class), Intent.FILL_IN_DATA));
                }
            }
        }
    }


    static final class LocalNotificationsManager1 implements Runnable {
        final /* synthetic */ Context valcontext;
        final /* synthetic */ DailyBonusObject valdailyBonusObject;

        LocalNotificationsManager1(final Context valcontext, final DailyBonusObject valdailyBonusObject) {
            super();
            this.valcontext = valcontext;
            this.valdailyBonusObject = valdailyBonusObject;
        }

        @Override
        public void run() {
            Integer n = LocalNotificationsManager.access000();
            synchronized (n) {
                Calendar.getInstance().setTimeInMillis(new Date().getTime());
                AlarmManager alarmManager = (AlarmManager)this.valcontext.getSystemService(Context.ALARM_SERVICE);
                Serializable serializable = new StringBuilder();
                ((StringBuilder)serializable).append(" createDailyBonusNotifications ");
                ((StringBuilder)serializable).append(this.valdailyBonusObject.getNotification());
                ((StringBuilder)serializable).append(" ");
                ((StringBuilder)serializable).append(this.valdailyBonusObject.getTimeLeft());
                Log.v((String)"SkanwordsNotif", (String)((StringBuilder)serializable).toString());
                if (this.valdailyBonusObject == null) return;
                if (this.valdailyBonusObject.getNotification() == null) return;
                if (!SmappsScanwords.getAppSharedPreferences((String)"GENERAL_PREFERENCES_NAME").getBoolean("daily_bonus_notify", true)) return;
                serializable = Integer.valueOf(0);
                Iterator iterator = this.valdailyBonusObject.getNotification().iterator();
                int n2 = 0;
                Integer value = 0;
                while (iterator.hasNext()) {
                    Map map = (Map)iterator.next();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(new Date().getTime() + (long)(this.valdailyBonusObject.getTimeLeft() * 1000));
                    Intent intent = new Intent(this.valcontext, SmappsReceiver.class);
                    intent.putExtra("EXTRA_NOTIFICATION_MESSAGE", (String)map.get("text"));
                    intent.putExtra("EXTRA_NOTIFICATION_TITLE", (String)map.get("title"));
                    Integer value2;
                    if (map.get("title_id") == null) {
                        value2 = (n2 = 0);
                    }
                    else {
                        value2 = ((Double)map.get("title_id")).intValue();
                        n2 = ((Double)map.get("text_id")).intValue();
                    }
                    final Integer value3 = value + ((Double)map.get("hours")).intValue();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(String.format("hh_%dh", ((Double)map.get("hours")).intValue()));
                    sb2.append("_");
                    sb2.append(value2);
                    sb2.append("_");
                    sb2.append(n2);
                    final String string = sb2.toString();
                    intent.putExtra("EXTRA_NOTIFICATION_ID", string);
                    final PendingIntent broadcast = PendingIntent.getBroadcast(this.valcontext, n + 100, intent, Intent.FILL_IN_DATA);
//                    calendar.add(Calendar.DST_OFFSET, value3);
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(" set bonus notification taime  ");
                    sb3.append(calendar.get(Calendar.DAY_OF_YEAR));
                    sb3.append("  ");
                    sb3.append(calendar.get(Calendar.HOUR_OF_DAY));
                    sb3.append("  ");
                    sb3.append(calendar.get(Calendar.MINUTE));
                    sb3.append("  ");
                    sb3.append(string);
                    Log.v("SkanwordsNotif", sb3.toString());

                    if (calendar.getTimeInMillis() / 1000L - new Date().getTime() / 1000L > 0L) {
                        alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), (PendingIntent)broadcast);
                    } else {
                        alarmManager.cancel((PendingIntent)broadcast);
                    }
                    ++n2;
                }
            }
        }
    }
}