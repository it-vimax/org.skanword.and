package org.skanword.and.etc;

import android.support.v4.app.*;
import java.lang.reflect.*;
import android.util.*;
import org.skanword.and.*;
import com.google.android.gms.analytics.*;

public class CustomFragment extends Fragment
{
    private static final Field sChildFragmentManagerField;
    
    static {
        Field declaredField = null;
        try {
            final Field field = declaredField = Fragment.class.getDeclaredField("mChildFragmentManager");
            field.setAccessible(true);
            declaredField = field;
        }
        catch (NoSuchFieldException ex) {
            Log.e("", "Error getting mChildFragmentManager field", (Throwable)ex);
        }
        sChildFragmentManagerField = declaredField;
    }
    
    public CustomFragment() {
        super();
    }
    
    public void fragmentOpened() {
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        final Field sChildFragmentManagerField = CustomFragment.sChildFragmentManagerField;
        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            }
            catch (Exception ex) {
                Log.e("", "Error setting mChildFragmentManager field", (Throwable)ex);
            }
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        SmappsScanwords.instance.getTrackerSendHelper().send(this.getClass().getSimpleName(), (new HitBuilders.ScreenViewBuilder()).build());
    }
    
    public void updateFragment() {
        this.updateFragment(false);
    }
    
    public void updateFragment(final boolean b) {
    }
}
