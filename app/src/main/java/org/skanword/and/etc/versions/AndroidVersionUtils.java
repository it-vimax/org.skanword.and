package org.skanword.and.etc.versions;

import java.net.*;
import java.util.*;
import java.io.*;
import org.apache.http.protocol.*;
import android.app.*;
import org.apache.http.client.*;
import android.view.*;
import android.content.*;
import android.os.*;

public interface AndroidVersionUtils
{
    void downloadFile(final URL p0, final Map<String, String> p1, final File p2, final boolean p3, final String p4) throws IOException;
    
    void downloadFile(final URL p0, final Map<String, String> p1, final File p2, final boolean p3, final String p4, final HttpContext p5) throws IOException;
    
    String downloadToString(final URL p0, final Map<String, String> p1) throws IOException;
    
    String downloadToString(final URL p0, final Map<String, String> p1, final HttpContext p2) throws IOException;
    
    void finishOnHomeButton(final Activity p0);
    
    HttpClient getHttpClient();
    
    void hideActionBar(final Activity p0);
    
    void hideWindowTitle(final Activity p0);
    
    void holographic(final Activity p0);
    
    View onActionBarCustom(final Activity p0, final int p1);
    
    void onActionBarWithText(final MenuItem p0);
    
    void onActionBarWithText(final SubMenu p0);
    
    void onFileDownloaded(final long p0, final boolean p1, final int p2);
    
    void setContext(final Context p0);
    
    public static class Factory
    {
        private static AndroidVersionUtils INSTANCE;
        
        public Factory() {
            super();
        }
        
        public static AndroidVersionUtils getInstance() {
            final AndroidVersionUtils instance = Factory.INSTANCE;
            if (instance != null) {
                return instance;
            }
            try {
                final int sdk_INT = Build.VERSION.SDK_INT;
                if (sdk_INT < 9) {
                    return Factory.INSTANCE = new DefaultUtil();
                }
                if (sdk_INT < 11) {
                    return Factory.INSTANCE = (AndroidVersionUtils)Class.forName("com.adamrosenfield.wordswithcrosses.versions.GingerbreadUtil").newInstance();
                }
                return Factory.INSTANCE = (AndroidVersionUtils)Class.forName("com.adamrosenfield.wordswithcrosses.versions.HoneycombUtil").newInstance();
            }
            catch (InstantiationException ex) {
                ex.printStackTrace();
                return Factory.INSTANCE = new DefaultUtil();
            }
            catch (IllegalAccessException ex2) {
                ex2.printStackTrace();
                return Factory.INSTANCE = new DefaultUtil();
            }
            catch (ClassNotFoundException ex3) {
                ex3.printStackTrace();
                return Factory.INSTANCE = new DefaultUtil();
            }
        }
    }
}
