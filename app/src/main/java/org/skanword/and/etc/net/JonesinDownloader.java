package org.skanword.and.etc.net;

import java.util.*;

public class JonesinDownloader extends AbstractDownloader
{
    private static final String NAME = "Jonesin' Crosswords";
    
    public JonesinDownloader() {
        super("http://herbach.dnsalias.com/Jonesin/", "Jonesin' Crosswords");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append("jz");
        sb.append(calendar.get(Calendar.YEAR) % 100);
        sb.append(JonesinDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(JonesinDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".puz");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 5;
    }
}
