package org.skanword.and.etc.net;

import java.util.*;

public class InkwellDownloader extends AbstractDownloader
{
    private static final String NAME = "InkWellXWords.com";
    
    public InkwellDownloader() {
        super("http://herbach.dnsalias.com/Tausig/", "InkWellXWords.com");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append("vv");
        sb.append(calendar.get(Calendar.YEAR) % 100);
        sb.append(InkwellDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(InkwellDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".puz");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 6;
    }
}
