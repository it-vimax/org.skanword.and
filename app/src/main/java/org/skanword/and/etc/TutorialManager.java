package org.skanword.and.etc;

import org.skanword.and.*;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;

public class TutorialManager
{
    public static final String TUTORIAL_STATE_CHANGED = "TUTORIAL_STATE_CHANGED";
    private static TutorialManager instance;
    
    public TutorialManager() {
        super();
    }
    
    public static TutorialManager getInstance() {
        if (TutorialManager.instance == null) {
            TutorialManager.instance = new TutorialManager();
        }
        return TutorialManager.instance;
    }
    
    public TutorialStep currentTutorialStep() {
        return TutorialStep.values()[SmappsScanwords.getAppSharedPreferences("tutorial").getInt("tutorial_step", TutorialStep.TUTORIAL_DISABLED.ordinal())];
    }
    
    public void setTutorialStep(final TutorialStep tutorialStep) {
        SmappsScanwords.getAppSharedPreferencesEditor("tutorial").putInt("tutorial_step", tutorialStep.ordinal()).commit();
        SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("TUTORIAL_STATE_CHANGED"));
    }

    public enum TutorialStep {
        TUTORIAL_DISABLED,
        TUTORIAL_START,
        TUTORIAL_ISSUE_DOWNLOADING,
        TUTORIAL_ISSUE_DOWNLOADED,
        TUTORIAL_ISSUE_OPENED,
        TUTORIAL_SKANWORD_OPENED,
        TUTORIAL_WORD_SELECTED,
        TUTORIAL_LETTER_ENTERED,
        TUTORIAL_LETTER_ENTERED_BY_HINT,
        TUTORIAL_WORD_ENTERED,
        TUTORIAL_CELL_FOR_HINT_SELECTED,
        TUTORIAL_HINT_USED,
        TUTORIAL_SKANWORD_COMPLETE,
        TUTORIAL_COMPLETE

    }
}
