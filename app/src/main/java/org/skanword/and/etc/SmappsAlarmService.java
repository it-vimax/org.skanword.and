package org.skanword.and.etc;

import android.os.*;
import android.support.v4.app.*;
import android.content.*;

import org.skanword.and.R;
import org.skanword.and.menu.*;
import org.skanword.and.datamanager.*;
import android.util.*;
import android.app.*;

public class SmappsAlarmService extends Service
{
    public SmappsAlarmService() {
        super();
    }
    
    public IBinder onBind(final Intent intent) {
        return null;
    }
    
    public void onCreate() {
        super.onCreate();
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        if (intent == null) {
            return super.onStartCommand(intent, n, n2);
        }
        final NotificationCompat.Builder setContentText = new NotificationCompat.Builder((Context)this).setSmallIcon(R.drawable.icon).setContentTitle(intent.getExtras().getString("EXTRA_NOTIFICATION_TITLE")).setContentText(intent.getExtras().getString("EXTRA_NOTIFICATION_MESSAGE"));
        final Intent intent2 = new Intent(this.getApplicationContext(), (Class)MainMenuActivity.class);
        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //TODO 603979776 не найдено
        intent2.putExtra("local_notification", true);
        intent2.putExtra("i", intent.getExtras().getString("EXTRA_NOTIFICATION_ID"));
        setContentText.setContentIntent(PendingIntent.getActivity(this.getApplicationContext(), 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT));
        final NotificationManager notificationManager = (NotificationManager)this.getSystemService(NOTIFICATION_SERVICE);
        final Notification build = setContentText.build();
        build.flags |= 0x10;
        if (!MainDataManager.getInstance().getOptions().isDisabledNotificationsSounds()) {
            build.defaults |= 0x1;
        }
        build.defaults |= 0x2;
        notificationManager.notify(1, build);
        final StringBuilder sb = new StringBuilder();
        sb.append("onCreate  SmappsAlarmService ");
        sb.append(intent.getExtras().getString("EXTRA_NOTIFICATION_MESSAGE"));
        sb.append(" ");
        sb.append(intent.getExtras().getString("EXTRA_NOTIFICATION_ID"));
        Log.v("SkanwordsNotif", sb.toString());
        return super.onStartCommand(intent, n, n2);
    }
}
