package org.skanword.and.etc.net;

import java.util.*;
import java.io.*;
import java.util.regex.*;
import java.util.logging.*;

public abstract class BloggerDownloader extends AbstractDownloader
{
    private static final Pattern PUZZLE_PATTERN;
    private static final String PUZZLE_REGEX = "href=(?:\"|&quot;)(https?://(?:[^\"&]|&(?!quot;))*\\.puz(?:[^\"&]|&(?!quot;))*)(?:\"|&quot;)";
    private String baseUrl;
    
    static {
        PUZZLE_PATTERN = Pattern.compile("href=(?:\"|&quot;)(https?://(?:[^\"&]|&(?!quot;))*\\.puz(?:[^\"&]|&(?!quot;))*)(?:\"|&quot;)");
    }
    
    public BloggerDownloader(final String s, final String baseUrl) {
        super("", s);
        this.baseUrl = baseUrl;
    }
    
    private static String iso8601String(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append(calendar.get(Calendar.YEAR));
        sb.append("-");
        sb.append(BloggerDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append("-");
        sb.append(BloggerDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append("T00:00:00");
        return sb.toString();
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        return "";
    }
    
    @Override
    public void download(final Calendar calendar) throws IOException {
        final Calendar calendar2 = (Calendar)calendar.clone();
        calendar2.add(Calendar.DATE, 2);
        final StringBuilder sb = new StringBuilder();
        sb.append(this.baseUrl);
        sb.append("/feeds/posts/default?alt=atom&max-results=1&published-min=");
        sb.append(iso8601String(calendar));
        sb.append("&published-max=");
        sb.append(iso8601String(calendar2));
        final String string = sb.toString();
        final Matcher matcher = BloggerDownloader.PUZZLE_PATTERN.matcher(this.downloadUrlToString(string));
        if (matcher.find()) {
            super.download(calendar, matcher.group(1).replaceAll("&amp;amp;", "&"));
            return;
        }
        final Logger log = BloggerDownloader.LOG;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Failed to find puzzle URL in page: ");
        sb2.append(string);
        log.warning(sb2.toString());
        throw new IOException("Failed to scrape puzzle URL");
    }
}
