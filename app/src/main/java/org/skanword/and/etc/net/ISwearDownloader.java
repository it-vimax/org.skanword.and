package org.skanword.and.etc.net;

import java.util.*;

public class ISwearDownloader extends AbstractDownloader
{
    private static final String NAME = "I Swear";
    
    public ISwearDownloader() {
        super("http://wij.theworld.com/puzzles/dailyrecord/", "I Swear");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append("DR");
        sb.append(calendar.get(Calendar.YEAR) % 100);
        sb.append(ISwearDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(ISwearDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".puz");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 6;
    }
}
