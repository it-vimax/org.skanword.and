package org.skanword.and.etc.net;

import java.util.*;

public class NYTClassicDownloader extends AbstractDownloader
{
    private static final String NAME = "New York Times Classic";
    
    public NYTClassicDownloader() {
        super("http://www.nytimes.com/specials/puzzles/", "New York Times Classic");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        return "classic.puz";
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        if (calendar.get(Calendar.DAY_OF_WEEK) != 2) {
            return false;
        }
        final Calendar instance = Calendar.getInstance();
        return instance.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && instance.get(Calendar.WEEK_OF_YEAR) == calendar.get(Calendar.WEEK_OF_YEAR);
    }
}
