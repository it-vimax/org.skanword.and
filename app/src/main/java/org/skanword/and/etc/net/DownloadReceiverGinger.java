package org.skanword.and.etc.net;

import android.annotation.*;
import java.util.logging.*;
import android.content.*;
import android.app.*;
import org.skanword.and.etc.versions.*;
import android.database.*;

@TargetApi(9)
public class DownloadReceiverGinger extends BroadcastReceiver
{
    protected static final Logger LOG;
    
    static {
        LOG = Logger.getLogger("com.adamrosenfield.wordswithcrosses");
    }
    
    public DownloadReceiverGinger() {
        super();
    }
    
    public void onReceive(final Context context, final Intent intent) {
        final DownloadManager downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
        final long longExtra = intent.getLongExtra("extra_download_id", -1L);
        final DownloadManager.Query downloadManagerQuery = new DownloadManager.Query();
        boolean b = true;
        downloadManagerQuery.setFilterById(new long[] { longExtra });
        downloadManagerQuery.setFilterByStatus(24);
        final Cursor query = downloadManager.query(downloadManagerQuery);
        if (query.getCount() < 1) {
            query.close();
            return;
        }
        query.moveToFirst();
        final int int1 = query.getInt(query.getColumnIndex("status"));
        final int int2 = query.getInt(query.getColumnIndex("reason"));
        final String string = query.getString(query.getColumnIndex("media_type"));
        query.close();
        final Logger log = DownloadReceiverGinger.LOG;
        final StringBuilder sb = new StringBuilder();
        sb.append("Download completed: id=");
        sb.append(longExtra);
        sb.append(" status=");
        sb.append(int1);
        sb.append(" reason=");
        sb.append(int2);
        sb.append(" mediaType=");
        sb.append(string);
        log.info(sb.toString());
        if (int1 != 8) {
            b = false;
        }
        AndroidVersionUtils.Factory.getInstance().onFileDownloaded(longExtra, b, int2);
    }
}
