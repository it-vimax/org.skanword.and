package org.skanword.and.etc;

import com.google.android.gms.ads.*;
import android.content.*;
import android.widget.*;

public class ToastAdListener extends AdListener
{
    private Context mContext;
    
    public ToastAdListener(final Context mContext) {
        super();
        this.mContext = mContext;
    }
    
    @Override
    public void onAdClosed() {
        Toast.makeText(this.mContext, (CharSequence)"onAdClosed()", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onAdFailedToLoad(final int n) {
        String s;
        if (n != 0) {
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        s = "";
                    }
                    else {
                        s = "No fill";
                    }
                }
                else {
                    s = "Network Error";
                }
            }
            else {
                s = "Invalid request";
            }
        }
        else {
            s = "Internal error";
        }
        Toast.makeText(this.mContext, (CharSequence)String.format("onAdFailedToLoad(%s)", s), Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onAdLeftApplication() {
        Toast.makeText(this.mContext, (CharSequence)"onAdLeftApplication()", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onAdLoaded() {
        Toast.makeText(this.mContext, (CharSequence)"onAdLoaded()", Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onAdOpened() {
        Toast.makeText(this.mContext, (CharSequence)"onAdOpened()", Toast.LENGTH_SHORT).show();
    }
}
