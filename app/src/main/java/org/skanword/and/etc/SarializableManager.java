package org.skanword.and.etc;

import java.io.*;

public class SarializableManager
{
    public SarializableManager() {
        super();
    }
    
    public static <T extends Serializable> T fromBlob(final byte[] array, final Object o) throws IOException, ClassNotFoundException {
        final ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(array));
        objectInputStream.close();
        return (T)objectInputStream.readObject();
    }
    
    public static byte[] toBlobe(final Serializable s) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(s);
        objectOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }
}
