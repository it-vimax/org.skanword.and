package org.skanword.and.etc;

import android.support.v4.content.*;
import android.content.*;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver
{
    public GcmBroadcastReceiver() {
        super();
    }
    
    public void onReceive(final Context context, final Intent intent) {
        WakefulBroadcastReceiver.startWakefulService(context, intent.setComponent(new ComponentName(context.getPackageName(), GcmIntentService.class.getName())));
        this.setResultCode(-1);
    }
}
