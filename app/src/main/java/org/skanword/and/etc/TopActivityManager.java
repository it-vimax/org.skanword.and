package org.skanword.and.etc;

import android.app.*;
import android.content.*;
import android.os.*;

public class TopActivityManager implements Application.ActivityLifecycleCallbacks
{
    private static TopActivityManager instance;
    private Activity mTopActivity;
    
    public TopActivityManager() {
        super();
    }
    
    private TopActivity canBeTopActivity(final Activity activity) {
        try {
            return (TopActivity)activity;
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    public static TopActivityManager get() {
        final TopActivityManager instance = TopActivityManager.instance;
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException("TopActivityManager is not initialised - invoke at least once with parameterised init/get");
    }
    
    public static TopActivityManager get(final Application application) {
        if (TopActivityManager.instance == null) {
            init(application);
        }
        return TopActivityManager.instance;
    }
    
    public static TopActivityManager get(Context applicationContext) {
        final TopActivityManager instance = TopActivityManager.instance;
        if (instance == null) {
            applicationContext = applicationContext.getApplicationContext();
            if (applicationContext instanceof Application) {
                init((Application)applicationContext);
            }
            throw new IllegalStateException("TopActivityManager is not initialised and cannot obtain the Application object");
        }
        return instance;
    }
    
    public static TopActivityManager init(final Application application) {
        if (TopActivityManager.instance == null) {
            application.registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)(TopActivityManager.instance = new TopActivityManager()));
        }
        return TopActivityManager.instance;
    }
    
    public Activity getTopActivity() {
        return this.mTopActivity;
    }
    
    public void onActivityCreated(final Activity activity, final Bundle bundle) {
    }
    
    public void onActivityDestroyed(final Activity activity) {
    }
    
    public void onActivityPaused(final Activity activity) {
        if (activity == this.mTopActivity) {
            this.mTopActivity = null;
        }
    }
    
    public void onActivityResumed(final Activity mTopActivity) {
        if (this.canBeTopActivity(mTopActivity) != null) {
            this.mTopActivity = mTopActivity;
        }
    }
    
    public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
    }
    
    public void onActivityStarted(final Activity activity) {
    }
    
    public void onActivityStopped(final Activity activity) {
    }
    
    public interface TopActivity
    {
    }
}
