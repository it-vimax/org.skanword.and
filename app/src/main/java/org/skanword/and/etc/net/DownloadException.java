package org.skanword.and.etc.net;

import java.io.*;

public class DownloadException extends IOException
{
    private static final long serialVersionUID = -5973110073382723812L;
    private int mResource;
    
    public DownloadException(final int mResource) {
        super();
        this.mResource = mResource;
    }
    
    public int getResource() {
        return this.mResource;
    }
}
