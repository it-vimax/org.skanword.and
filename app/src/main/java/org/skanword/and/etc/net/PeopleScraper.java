package org.skanword.and.etc.net;

import java.util.*;

public class PeopleScraper extends AbstractPageScraper
{
    public PeopleScraper() {
        super("People Magazine");
    }
    
    @Override
    protected String getScrapeURL(final Calendar calendar) {
        return "http://www.people.com/people/puzzler/";
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        final int value = calendar.get(Calendar.DAY_OF_WEEK);
        boolean b = false;
        if (value != 6) {
            return false;
        }
        final Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE, -((instance.get(Calendar.DAY_OF_WEEK) + 7 - 6) % 7));
        instance.set(Calendar.HOUR, 0);
        instance.set(Calendar.MINUTE, 0);
        instance.set(Calendar.SECOND, 0);
        instance.set(Calendar.MILLISECOND, 0);
        if (calendar.compareTo(instance) >= 0) {
            b = true;
        }
        return b;
    }
}
