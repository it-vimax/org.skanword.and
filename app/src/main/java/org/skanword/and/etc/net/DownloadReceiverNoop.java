package org.skanword.and.etc.net;

import android.content.*;

public class DownloadReceiverNoop extends BroadcastReceiver
{
    public DownloadReceiverNoop() {
        super();
    }
    
    public void onReceive(final Context context, final Intent intent) {
    }
}
