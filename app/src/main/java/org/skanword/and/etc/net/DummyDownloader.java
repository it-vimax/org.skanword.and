package org.skanword.and.etc.net;

import android.content.*;

import org.skanword.and.R;

import java.util.*;
import java.io.*;

public class DummyDownloader implements Downloader
{
    private Context mContext;
    
    public DummyDownloader() {
        super();
    }
    
    public DummyDownloader(final Context mContext) {
        super();
        this.mContext = mContext;
    }
    
    @Override
    public void download(final Calendar calendar) throws IOException {
    }
    
    @Override
    public String getFilename(final Calendar calendar) {
        return null;
    }
    
    @Override
    public String getName() {
        return null;
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return false;
    }
    
    @Override
    public void setContext(final Context mContext) {
        this.mContext = mContext;
    }
    
    @Override
    public String sourceUrl(final Calendar calendar) {
        return null;
    }
    
    @Override
    public String toString() {
        return this.mContext.getResources().getString(R.string.source_all_available);
    }
}
