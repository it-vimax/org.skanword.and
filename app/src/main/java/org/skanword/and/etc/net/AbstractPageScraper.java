package org.skanword.and.etc.net;

import java.util.regex.*;
import java.net.*;
import java.util.*;
import java.io.*;

public abstract class AbstractPageScraper extends AbstractDownloader
{
    private static final Pattern PAT;
    private static final String REGEX = "http://[^ ^']*\\.puz";
    private static final Pattern REL_PAT;
    private static final String REL_REGEX = "href=\"(.*\\.puz)\"";
    
    static {
        PAT = Pattern.compile("http://[^ ^']*\\.puz");
        REL_PAT = Pattern.compile("href=\"(.*\\.puz)\"");
    }
    
    protected AbstractPageScraper(final String s) {
        super("", s);
    }
    
    public static List<String> getPuzzleRelativeURLs(final String s, final String s2) throws MalformedURLException {
        final URL url = new URL(s);
        final ArrayList<String> list = new ArrayList<String>();
        final Matcher matcher = AbstractPageScraper.REL_PAT.matcher(s2);
        while (matcher.find()) {
            list.add(new URL(url, matcher.group(1)).toString());
        }
        return list;
    }
    
    public static List<String> getPuzzleURLs(final String s) {
        final ArrayList<String> list = new ArrayList<String>();
        final Matcher matcher = AbstractPageScraper.PAT.matcher(s);
        while (matcher.find()) {
            list.add(matcher.group());
        }
        return list;
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        return "";
    }
    
    @Override
    public void download(final Calendar calendar) throws IOException {
        this.scrapePage(calendar, this.getScrapeURL(calendar));
    }
    
    protected abstract String getScrapeURL(final Calendar p0);
    
    protected void scrapePage(final Calendar calendar, final String s) throws IOException {
    }
}
