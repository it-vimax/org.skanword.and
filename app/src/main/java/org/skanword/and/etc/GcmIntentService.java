package org.skanword.and.etc;

import android.support.v4.app.*;

import org.skanword.and.R;
import org.skanword.and.menu.*;
import android.content.*;
import org.skanword.and.datamanager.*;
import android.app.*;
import com.google.android.gms.gcm.*;
import android.util.*;
import android.support.v4.content.*;
import android.os.*;

public class GcmIntentService extends IntentService
{
    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    
    public GcmIntentService() {
        super("GcmIntentService");
    }
    
    private void sendNotification(final String contentTitle, final String contentText, final String s) {
        final NotificationCompat.Builder setContentText = new NotificationCompat.Builder((Context)this).setSmallIcon(R.drawable.icon).setContentTitle(contentTitle).setContentText(contentText);
        final Intent intent = new Intent(this.getApplicationContext(), (Class)MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("push_notification", true);
        intent.putExtra("i", s);
        setContentText.setContentIntent(PendingIntent.getActivity(this.getApplicationContext(), 0, intent, PendingIntent.FLAG_IMMUTABLE));
        final NotificationManager notificationManager = (NotificationManager)this.getSystemService(NOTIFICATION_SERVICE);
        final Notification build = setContentText.build();
        build.flags |= 0x10;
        if (!MainDataManager.getInstance().getOptions().isDisabledNotificationsSounds()) {
            build.defaults |= 0x1;
        }
        build.defaults |= 0x2;
        notificationManager.notify(2, build);
    }
    
    protected void onHandleIntent(final Intent intent) {
        final Bundle extras = intent.getExtras();
        final String messageType = GoogleCloudMessaging.getInstance((Context)this).getMessageType(intent);
        if (!extras.isEmpty()) {
            if ("send_error".equals(messageType)) {
                this.sendNotification(extras.getString("title", "unknown"), extras.getString("message", "unknown"), extras.getString("i", "unknown"));
            }
            else if ("deleted_messages".equals(messageType)) {
                this.sendNotification(extras.getString("title", "unknown"), extras.getString("message", "unknown"), extras.getString("i", "unknown"));
            }
            else if ("gcm".equals(messageType)) {
                this.sendNotification(extras.getString("title", "unknown"), extras.getString("message", "unknown"), extras.getString("i", "unknown"));
                final StringBuilder sb = new StringBuilder();
                sb.append("Received: ");
                sb.append(extras.toString());
                Log.i("", sb.toString());
            }
        }
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }
}
