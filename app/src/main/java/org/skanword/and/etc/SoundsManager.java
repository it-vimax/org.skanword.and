package org.skanword.and.etc;

import android.media.*;
import android.content.*;
import org.skanword.and.datamanager.*;

public class SoundsManager
{
    public static final int SOUND_BONUS_SCRATCH = 2131689474;
    public static final int SOUND_BONUS_SCRATCH_REST = 2131689475;
    public static final int SOUND_HINT_LETTER = 2131689473;
    public static final int SOUND_WIN = 2131689476;
    public static final int SOUND_WORD_CORRECT = 2131689477;
    private static MediaPlayer mp;
    
    public SoundsManager() {
        super();
    }
    
    public static void playSound(final Context context, final Integer n) {
        if (MainDataManager.getInstance().getOptions().isSounds()) {
            final MediaPlayer mp = SoundsManager.mp;
            if (mp != null) {
                if (mp.isPlaying()) {
                    SoundsManager.mp.stop();
                }
                SoundsManager.mp.reset();
                SoundsManager.mp.release();
            }
            (SoundsManager.mp = MediaPlayer.create(context, (int)n)).start();
        }
    }
}
