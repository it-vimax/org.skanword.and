package org.skanword.and.etc.versions;

import android.annotation.*;
import android.support.v4.app.*;
import android.view.*;
import android.app.*;

import org.skanword.and.R;

@TargetApi(11)
public class HoneycombUtil extends GingerbreadUtil
{
    public HoneycombUtil() {
        super();
    }

    public void finishOnHomeButton(final Activity activity) {
        final ActionBar actionBar = activity.getActionBar();
        if (actionBar == null) {
            return;
        }
        actionBar.setDisplayHomeAsUpEnabled(true);
        final View viewById = activity.findViewById(R.id.home);
        if (viewById != null) {
            viewById.setOnClickListener((View.OnClickListener)new HoneycombUtil.HoneycombUtil1(this, activity));
        }
    }

    public void hideActionBar(final Activity activity) {
        final ActionBar actionBar = activity.getActionBar();
        if (actionBar == null) {
            return;
        }
        actionBar.hide();
    }

    public void holographic(final Activity activity) {
        activity.setTheme(R.style.CardView_Dark); //TODO не извистная тема 16973931
        final ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public View onActionBarCustom(final Activity activity, final int customView) {
        final ActionBar actionBar = activity.getActionBar();
        if (actionBar == null) {
            return null;
        }
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM); //TODO не исзвесные параметры 18, 26
        actionBar.setCustomView(customView);
        return actionBar.getCustomView();
    }

    public void onActionBarWithText(final MenuItem menuItem) {
        menuItem.setShowAsAction(5);
    }

    public void onActionBarWithText(final SubMenu subMenu) {
        this.onActionBarWithText(subMenu.getItem());
    }

    protected void setNotificationVisibility(final DownloadManager.Request request, final boolean b) {
        int notificationVisibility;
        if (b) {
            notificationVisibility = 0;
        }
        else {
            notificationVisibility = 2;
        }
        request.setNotificationVisibility(notificationVisibility);
    }

    class HoneycombUtil1 implements View.OnClickListener {
        final /* synthetic */ HoneycombUtil this0;
        final /* synthetic */ Activity vala;

        HoneycombUtil1(final HoneycombUtil this0, final Activity vala) {
            super();
            this.this0 = this0;
            this.vala = vala;
        }

        public void onClick(final View view) {
            this.vala.finish();
        }
    }
}