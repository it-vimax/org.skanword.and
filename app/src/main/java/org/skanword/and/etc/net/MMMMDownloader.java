package org.skanword.and.etc.net;

import java.util.*;
import java.io.*;
import java.util.regex.*;
import java.util.logging.*;

public class MMMMDownloader extends AbstractDownloader
{
    private static final String BASE_URL = "http://pmxwords.com/wp-content/";
    private static final String NAME = "Muller Monthly Music Meta";
    
    public MMMMDownloader() {
        super("", "Muller Monthly Music Meta");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        return "";
    }
    
    @Override
    public void download(final Calendar calendar) throws IOException {
        final int value = calendar.get(Calendar.YEAR);
        String string = "http://pmxwords.com/wp-content/";
        if (value != 2012) {
            final StringBuilder sb = new StringBuilder();
            sb.append("http://pmxwords.com/wp-content/");
            sb.append(calendar.get(Calendar.YEAR));
            string = sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append("puzzles/");
        final String string2 = sb2.toString();
        final String downloadUrlToString = this.downloadUrlToString(string2);
        Pattern pattern;
        if (calendar.get(Calendar.MONTH) == 11 && calendar.get(Calendar.DATE) == 31) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("<a href=\"([^\"]*\\.puz)\">[^<]*</a>\\s*[23]\\d-");
            sb3.append(MMMMDownloader.SHORT_MONTHS[11]);
            sb3.append("-");
            sb3.append(calendar.get(Calendar.YEAR));
            pattern = Pattern.compile(sb3.toString());
        }
        else {
            final String s = MMMMDownloader.SHORT_MONTHS[calendar.get(Calendar.MONTH)];
            final String s2 = MMMMDownloader.SHORT_MONTHS[(calendar.get(Calendar.MONTH) + 11) % 12];
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("<a href=\"([^\"]*\\.puz)\">[^<]*</a>\\s*([01]\\d-");
            sb4.append(s);
            sb4.append("|[23]\\d-");
            sb4.append(s2);
            sb4.append(")-");
            sb4.append(calendar.get(Calendar.YEAR));
            pattern = Pattern.compile(sb4.toString());
        }
        final Matcher matcher = pattern.matcher(downloadUrlToString);
        if (matcher.find()) {
            super.download(calendar, AbstractDownloader.resolveUrl(string2, matcher.group(1)));
            return;
        }
        final Logger log = MMMMDownloader.LOG;
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("Failed to find puzzle link for ");
        sb5.append(calendar);
        sb5.append(" on page: ");
        sb5.append(string2);
        log.warning(sb5.toString());
        throw new IOException("Failed to find puzzle link");
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        boolean b = true;
        final int value = calendar.get(Calendar.YEAR);
        final int value2 = calendar.get(Calendar.MONTH);
        final int value3 = calendar.get(Calendar.DATE);
        final int value4 = calendar.get(Calendar.DAY_OF_WEEK);
        if ((value == 2012 && value2 >= 3) || (value > 2012 && value2 >= 1)) {
            if (value4 == 3 && value3 <= 7) {
                return b;
            }
            if (value2 == 11 && value3 == 31) {
                return true;
            }
        }
        b = false;
        return b;
    }
}
