package org.skanword.and.etc.net;

import java.util.*;

public class CHEDownloader extends AbstractDownloader
{
    private static final String NAME = "Chronicle of Higher Education";
    
    public CHEDownloader() {
        super("http://chronicle.com/items/biz/puzzles/", "Chronicle of Higher Education");
    }
    
    @Override
    protected String createUrlSuffix(final Calendar calendar) {
        final StringBuilder sb = new StringBuilder();
        sb.append(calendar.get(Calendar.YEAR));
        sb.append(CHEDownloader.DEFAULT_NF.format(calendar.get(Calendar.MONTH) + 1));
        sb.append(CHEDownloader.DEFAULT_NF.format(calendar.get(Calendar.DATE)));
        sb.append(".puz");
        return sb.toString();
    }
    
    @Override
    public boolean isPuzzleAvailable(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_WEEK) == 6;
    }
}
