package org.skanword.and.etc.versions;

import android.os.*;

public class Factory
{
    private static AndroidVersionUtils INSTANCE;
    
    public Factory() {
        super();
    }
    
    public static AndroidVersionUtils getInstance() {
        final AndroidVersionUtils instance = Factory.INSTANCE;
        if (instance != null) {
            return instance;
        }
        try {
            final int sdk_INT = Build.VERSION.SDK_INT;
            if (sdk_INT < 9) {
                return Factory.INSTANCE = new DefaultUtil();
            }
            if (sdk_INT < 11) {
                return Factory.INSTANCE = (AndroidVersionUtils)Class.forName("com.adamrosenfield.wordswithcrosses.versions.GingerbreadUtil").newInstance();
            }
            return Factory.INSTANCE = (AndroidVersionUtils)Class.forName("com.adamrosenfield.wordswithcrosses.versions.HoneycombUtil").newInstance();
        }
        catch (InstantiationException ex) {
            ex.printStackTrace();
            return Factory.INSTANCE = new DefaultUtil();
        }
        catch (IllegalAccessException ex2) {
            ex2.printStackTrace();
            return Factory.INSTANCE = new DefaultUtil();
        }
        catch (ClassNotFoundException ex3) {
            ex3.printStackTrace();
            return Factory.INSTANCE = new DefaultUtil();
        }
    }
}
