package org.skanword.and.menu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class ClosedTaskBgView extends View
{
    private Paint mPaint;
    private Path mPath;

    public ClosedTaskBgView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mPaint = new Paint();
        this.mPath = new Path();
        final Display defaultDisplay = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        final Point point = new Point();
        defaultDisplay.getSize(point);
        final float n = (float)(int)(this.getContext().getResources().getDisplayMetrics().density * 15.0f + 0.5f);
        this.mPaint.setStyle(Paint.Style.FILL);
        this.mPaint.setColor(Color.parseColor("#FCF2D6"));
        this.mPath.reset();
        this.mPath.moveTo(0.0f, 0.0f);
        final Path mPath = this.mPath;
        final float n2 = (float)point.x;
        final float n3 = 2.0f * n;
        mPath.lineTo(n2 - n3, 0.0f);
        this.mPath.lineTo(point.x - n3, point.y * 0.45f);
        this.mPath.lineTo(point.x / 2 - n, point.y * 0.5f);
        this.mPath.lineTo(0.0f, point.y * 0.45f);
        this.mPath.lineTo(0.0f, 0.0f);
    }

    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(this.mPath, this.mPaint);
    }
}
