package org.skanword.and.menu.skanwordsmenu;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListDirrectionType;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.MainNetworkManager.RequestCompletionBlock;
import android.support.v4.app.FragmentPagerAdapter;

public class MenuSkanwords extends CustomFragment implements ActionBar.TabListener
{
    private static Runnable mUpdateRunnable;
    MenuSkanwords.AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    private View mMainView;
    private int mTabToOpen;
    private PagerSlidingTabStrip mTabs;
    private EventListener mTutorialStateListener;
    private EventListener mUpdateListEventListener;
    private ViewPager mViewPager;

    public MenuSkanwords() {
        super();
        this.mAppSectionsPagerAdapter = null;
        this.mTabToOpen = 0;
        Log.v("", "MenuScanwords --  ");
    }

    static /* synthetic */ void access000(final MenuSkanwords menuSkanwords, final int n, final boolean b, final boolean b2) {
        menuSkanwords.openTabForPosition(n, b, b2);
    }

    static /* synthetic */ void access100(final MenuSkanwords menuSkanwords, final boolean b) {
        menuSkanwords.updateCurrentFragmentPage(b);
    }

    static /* synthetic */ ViewPager access200(final MenuSkanwords menuSkanwords) {
        return menuSkanwords.mViewPager;
    }

    static /* synthetic */ int access300(final MenuSkanwords menuSkanwords) {
        return menuSkanwords.mTabToOpen;
    }

    static /* synthetic */ View access400(final MenuSkanwords menuSkanwords) {
        return menuSkanwords.mMainView;
    }

    static /* synthetic */ void access500(final MenuSkanwords menuSkanwords, final boolean b, final boolean b2) {
        menuSkanwords.updateCurrentFragmentPage(b, b2);
    }

    private static String makeFragmentName(final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("android:switcher:");
        sb.append(n);
        sb.append(":");
        sb.append(n2);
        return sb.toString();
    }

    private void openTabForPosition(final int n, final boolean b) {
        this.openTabForPosition(n, b, false);
    }

    private void openTabForPosition(final int mTabToOpen, final boolean b, final boolean b2) {
        if (this.getRegisteredFragment(this.mViewPager, this.mTabToOpen) != null) {
            this.getRegisteredFragment(this.mViewPager, this.mTabToOpen).clearTab();
        }
        this.mTabToOpen = mTabToOpen;
        final StringBuilder sb = new StringBuilder();
        sb.append("openTabForPosition ");
        sb.append(b);
        Log.v("SkanwordsFunc", sb.toString());
        if (b) {
            this.updateCurrentFragmentPage(true, b2);
        }
    }

    private void requestPageUpdate(final SkanwordsDataManager.TaskListType skanwordsDataManagerTaskListType, final SkanwordsDataManager.TaskListDirrectionType dirType, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("requestIssuesListExecWithCompleteBlock  ");
        sb.append(skanwordsDataManagerTaskListType);
        sb.append("  ");
        sb.append(dirType);
        sb.append("  ");
        sb.append(b);
        Log.v("SkanwordsFunc", sb.toString());
        if (!MainNetworkManager.getInstance().userLoggedIn()) {
            MainDataManager.getInstance().getSkanwordsDataManager().notifyLocalDataUsegae(skanwordsDataManagerTaskListType, dirType);
            return;
        }
        Log.v("SkanwordsFunc", "requestIssuesListExecWithCompleteBlock");
        final MainNetworkManager instance = MainNetworkManager.getInstance();
        final MenuSkanwords4 menuSkanwords4 = new MenuSkanwords.MenuSkanwords4(this, skanwordsDataManagerTaskListType, dirType);
        Object activity;
        if (b) {
            activity = this.getActivity();
        }
        else {
            activity = null;
        }
        instance.requestIssuesListExecWithCompleteBlock(skanwordsDataManagerTaskListType, dirType, (MainNetworkManager.RequestCompletionBlock)menuSkanwords4, (Activity)activity);
    }

    private void updateCurrentFragmentPage(final boolean b) {
        this.updateCurrentFragmentPage(b, false);
    }

    private void updateCurrentFragmentPage(final boolean b, final boolean b2) {
        if (this.getRegisteredFragment(this.mViewPager, this.mTabToOpen) != null) {
            final SkanwordsIssuesList registeredFragment = this.getRegisteredFragment(this.mViewPager, this.mTabToOpen);
            try {
                registeredFragment.showDownloadingState(true);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            final SkanwordsDataManager.TaskListType skanwordsDataManagerTaskListType = SkanwordsDataManager.TaskListType.values()[this.mTabToOpen];
            SkanwordsDataManager.TaskListDirrectionType skanwordsDataManagerTaskListDirrectionType;
            if (b) {
                skanwordsDataManagerTaskListDirrectionType = SkanwordsDataManager.TaskListDirrectionType.TOP_DIRRECTION;
            }
            else {
                skanwordsDataManagerTaskListDirrectionType = SkanwordsDataManager.TaskListDirrectionType.CURRECT_DIRRECTION;
            }
            this.requestPageUpdate(skanwordsDataManagerTaskListType, skanwordsDataManagerTaskListDirrectionType, b2);
            return;
        }
        if (MenuSkanwords.mUpdateRunnable == null) {
            MenuSkanwords.mUpdateRunnable = (Runnable)new MenuSkanwords6(this, b, b2);
        }
        SmappsScanwords.getScheduleExecutorService().schedule(MenuSkanwords.mUpdateRunnable, 2L, TimeUnit.SECONDS);
    }

    protected void checkTutorialState() {
        final TutorialManager.TutorialStep currentTutorialStep = TutorialManager.getInstance().currentTutorialStep();
        final TutorialManager.TutorialStep tutorial_START = TutorialManager.TutorialStep.TUTORIAL_START;
        boolean b = true;
        Label_0133: {
            if (currentTutorialStep == tutorial_START) {
                if (this.getRegisteredFragment(this.mViewPager, this.mTabToOpen) != null && this.getRegisteredFragment(this.mViewPager, this.mTabToOpen).getSetsInfosCount() > 0) {
                    break Label_0133;
                }
            }
            else if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_ISSUE_DOWNLOADED && this.getRegisteredFragment(this.mViewPager, this.mTabToOpen) != null && this.getRegisteredFragment(this.mViewPager, this.mTabToOpen).getSetsInfosCount() > 0 && MainDataManager.getInstance().getSkanwordsDataManager().getSetsInfos() != null && MainDataManager.getInstance().getSkanwordsDataManager().getSetsInfos().size() > 0) {
                break Label_0133;
            }
            b = false;
        }
        if (this.getActivity() != null) {
            this.getActivity().runOnUiThread((Runnable)new MenuSkanwords.MenuSkanwords5(this, b));
        }
    }

    public SkanwordsIssuesList getRegisteredFragment(final ViewPager viewPager, final int n) {
        final String fragmentName = makeFragmentName(viewPager.getId(), n);
        if (this.getActivity() == null) {
            return null;
        }
        return (SkanwordsIssuesList)this.getChildFragmentManager().findFragmentByTag(fragmentName);
    }

    public Fragment getVisibleFragment() {
        final MenuSkanwords.AppSectionsPagerAdapter mAppSectionsPagerAdapter = this.mAppSectionsPagerAdapter;
        final ViewPager mViewPager = this.mViewPager;
        final Fragment fragment = (Fragment)mAppSectionsPagerAdapter.instantiateItem((ViewGroup)mViewPager, mViewPager.getCurrentItem());
        if (fragment != null) {
            return fragment;
        }
        return null;
    }

    public void onActivityCreated(final Bundle bundle) {
        this.selectTab(this.mTabToOpen, false);
        super.onActivityCreated(bundle);
    }

    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mMainView = layoutInflater.inflate(R.layout.menu_page_slider, viewGroup, false);
        this.mAppSectionsPagerAdapter = new MenuSkanwords.AppSectionsPagerAdapter(this, this.getChildFragmentManager());
        (this.mTabs = (PagerSlidingTabStrip)this.mMainView.findViewById(R.id.tabs)).setTextColorResource(R.color.action_bar_title_color);
        this.mTabs.setOnPageChangeListener((ViewPager.OnPageChangeListener)new MenuSkanwords1(this));
        (this.mViewPager = (ViewPager)this.mMainView.findViewById(R.id.pager)).setAdapter((PagerAdapter)this.mAppSectionsPagerAdapter);
        this.mViewPager.removeAllViewsInLayout();
        this.mAppSectionsPagerAdapter.notifyDataSetChanged();
        this.mTabs.setViewPager(this.mViewPager);
        this.mUpdateListEventListener = (EventListener)new MenuSkanwords2(this);
        SmappsScanwords.getEventsDispatcher().addListener("UPDATE_ISSUES_LIST", this.mUpdateListEventListener);
        this.mTutorialStateListener = (EventListener)new MenuSkanwords3(this);
        SmappsScanwords.getEventsDispatcher().addListener("TUTORIAL_STATE_CHANGED", this.mTutorialStateListener);
        return this.mMainView;
    }

    public void onDestroy() {
        Log.v("", "onDestroy -------MenuScanwords------");
        SmappsScanwords.getEventsDispatcher().removeListener("UPDATE_ISSUES_LIST", this.mUpdateListEventListener);
        SmappsScanwords.getEventsDispatcher().removeListener("TUTORIAL_STATE_CHANGED", this.mTutorialStateListener);
        super.onDestroy();
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onPause() {
        Log.v("", "onPause -------MenuScanwords------");
        super.onPause();
    }

    public void onResume() {
        final StringBuilder sb = new StringBuilder();
        sb.append("onResume -------MenuScanwords------  ");
        sb.append(this.getView());
        Log.v("", sb.toString());
        super.onResume();
    }

    public void onStart() {
        Log.v("SkanwordsFunc", "onStart -------MenuScanwords------");
        this.updateCurrentFragmentPage(false);
        super.onStart();
    }

    public void onStop() {
        Log.v("", "onStop -------MenuScanwords------");
        super.onStop();
    }

    public void onTabReselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
        this.mViewPager.setCurrentItem(actionBarTab.getPosition());
        final StringBuilder sb = new StringBuilder();
        sb.append("onTabReselected ");
        sb.append(actionBarTab.getPosition());
        Log.v("", sb.toString());
    }

    public void onTabSelected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onTabSelected ");
        sb.append(actionBarTab.getPosition());
        Log.v("", sb.toString());
    }

    public void onTabUnselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
        this.mViewPager.setCurrentItem(actionBarTab.getPosition());
        final StringBuilder sb = new StringBuilder();
        sb.append("onTabUnselected ");
        sb.append(actionBarTab.getPosition());
        Log.v("", sb.toString());
    }

    public void openSortDialog() {
        if (this.getVisibleFragment() != null) {
            ((SkanwordsIssuesList)this.getVisibleFragment()).openSortDialog();
        }
    }

    public void selectTab(final int n) {
        this.selectTab(n, true);
    }

    public void selectTab(final int n, final boolean b) {
        if (this.mViewPager == null) {
            return;
        }
        this.openTabForPosition(n, b);
        this.mViewPager.setCurrentItem(this.mTabToOpen, true);
    }

    public void updateFragment(final boolean b) {
        final MainNetworkManager instance = MainNetworkManager.getInstance();
        Object activity;
        if (b) {
            activity = this.getActivity();
        }
        else {
            activity = null;
        }
        if (instance.hasNetworkConnection((Activity)activity)) {
            this.updateCurrentFragmentPage(false, b);
        }
        super.updateFragment(b);
    }

    public class AppSectionsPagerAdapter extends FragmentPagerAdapter
    {
        final /* synthetic */ MenuSkanwords this0;

        public AppSectionsPagerAdapter(final MenuSkanwords this0, final FragmentManager fragmentManager) {
            super(fragmentManager);
            this.this0 = this0;
        }

        public int getCount() {
            return 3;
        }

        public Fragment getItem(final int fragmentPosition) {
            final SkanwordsIssuesList list = new SkanwordsIssuesList();
            list.setFragmentPosition(fragmentPosition);
            final Bundle arguments = new Bundle();
            SkanwordsDataManager.TaskListType skanwordsDataManagerTaskListType = SkanwordsDataManager.TaskListType.ISSUE_TASKS;
            if (fragmentPosition != 1) {
                if (fragmentPosition == 2) {
                    skanwordsDataManagerTaskListType = SkanwordsDataManager.TaskListType.FINISHED_TASKS;
                }
            }
            else {
                skanwordsDataManagerTaskListType = SkanwordsDataManager.TaskListType.STARTED_TASKS;
            }
            arguments.putSerializable("list_type", (Serializable)skanwordsDataManagerTaskListType);
            list.setArguments(arguments);
            return (Fragment)list;
        }

        public int getItemPosition(final Object o) {
            return -2;
        }

        public CharSequence getPageTitle(final int n) {
            if (n == 0) {
                return "НОВЫЕ";
            }
            if (n == 1) {
                return "НАЧАТЫЕ";
            }
            if (n != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Section ");
                sb.append(n + 1);
                return sb.toString();
            }
            return "РЕШЕННЫЕ";
        }
    }

    class MenuSkanwords4 implements MainNetworkManager.RequestCompletionBlock {
        final /* synthetic */ MenuSkanwords this0;
        final /* synthetic */ SkanwordsDataManager.TaskListDirrectionType valdirrectionType;
        final /* synthetic */ SkanwordsDataManager.TaskListType vallistType;

        MenuSkanwords4(final MenuSkanwords this0, final SkanwordsDataManager.TaskListType vallistType, final SkanwordsDataManager.TaskListDirrectionType valdirrectionType) {
            super();
            this.this0 = this0;
            this.vallistType = vallistType;
            this.valdirrectionType = valdirrectionType;
        }

        public void complete(final boolean b) {
            final StringBuilder sb = new StringBuilder();
            sb.append("requestIssuesListExecWithCompleteBlock complete ");
            sb.append(b);
            Log.v("SkanwordsFunc", sb.toString());
            if (!b) {
                if (!MainNetworkManager.getInstance().hasNetworkConnection()) {
                    MainDataManager.getInstance().getSkanwordsDataManager().notifyLocalDataUsegae(this.vallistType, this.valdirrectionType);
                }
                else if (this.this0.getActivity() != null) {
                    this.this0.getActivity().runOnUiThread((Runnable)new MenuSkanwords4.MenuSkanwords41(this));
                }
            }
            this.this0.checkTutorialState();
        }

        class MenuSkanwords41 implements Runnable {
            final /* synthetic */ MenuSkanwords4 this1;

            MenuSkanwords41(final MenuSkanwords4 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                if (this.this1.this0.getRegisteredFragment(MenuSkanwords.access200(this.this1.this0), MenuSkanwords.access300(this.this1.this0)) != null) {
                    this.this1.this0.getRegisteredFragment(MenuSkanwords.access200(this.this1.this0), MenuSkanwords.access300(this.this1.this0)).showLoadingError();
                }
            }
        }
    }


    class MenuSkanwords6 implements Runnable {
        final /* synthetic */ MenuSkanwords this0;
        final /* synthetic */ boolean valbyUser;
        final /* synthetic */ boolean valtoTop;

        MenuSkanwords6(final MenuSkanwords this0, final boolean valtoTop, final boolean valbyUser) {
            super();
            this.this0 = this0;
            this.valtoTop = valtoTop;
            this.valbyUser = valbyUser;
        }

        @Override
        public void run() {
            MenuSkanwords.access500(this.this0, this.valtoTop, this.valbyUser);
        }
    }

    class MenuSkanwords5 implements Runnable {
        final /* synthetic */ MenuSkanwords this0;
        final /* synthetic */ boolean valfShow;

        MenuSkanwords5(final MenuSkanwords this0, final boolean valfShow) {
            super();
            this.this0 = this0;
            this.valfShow = valfShow;
        }

        @Override
        public void run() {
            final View viewById = MenuSkanwords.access400(this.this0).findViewById(R.id.tutorialView);
            int visibility;
            if (this.valfShow) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_ISSUE_DOWNLOADED) {
                ((TextView)MenuSkanwords.access400(this.this0).findViewById(R.id.tutorialText)).setText("Теперь можно открыть выпуск.\nнажмите на него.");
                ((ImageView)MenuSkanwords.access400(this.this0).findViewById(R.id.tutorialImage)).setImageResource(R.drawable.tutorial_img_issue_open);
            }
        }
    }

    class MenuSkanwords1 extends ViewPager.SimpleOnPageChangeListener {
        final /* synthetic */ MenuSkanwords this0;

        MenuSkanwords1(final MenuSkanwords this0) {
            super();
            this.this0 = this0;
        }

        public void onPageScrollStateChanged(final int n) {
        }

        public void onPageScrolled(final int n, final float n2, final int n3) {
        }

        public void onPageSelected(final int n) {
            if (n != 0) {
                if (n != 1) {
                    if (n == 2) {
                        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_solved_enter");
                    }
                }
                else {
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_began_enter");
                }
            }
            else {
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_new_enter");
            }
            MenuSkanwords.access000(this.this0, n, true, false);
        }
    }


    class MenuSkanwords2 implements EventListener {
        final /* synthetic */ MenuSkanwords this0;

        MenuSkanwords2(final MenuSkanwords this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.getActivity().runOnUiThread((Runnable)new MenuSkanwords2.MenuSkanwords21(this));
        }

        class MenuSkanwords21 implements Runnable {
            final /* synthetic */ MenuSkanwords2 this1;

            MenuSkanwords21(final MenuSkanwords2 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                MenuSkanwords.access100(this.this1.this0, true);
            }
        }
    }


    class MenuSkanwords3 implements EventListener {
        final /* synthetic */ MenuSkanwords this0;

        MenuSkanwords3(final MenuSkanwords this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.checkTutorialState();
        }
    }
}