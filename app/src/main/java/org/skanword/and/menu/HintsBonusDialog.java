package org.skanword.and.menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import org.skanword.and.libs.qustom.QustomDialogBuilder;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.SoundsManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.network.DailyBonusObject;

public class HintsBonusDialog extends QustomDialogBuilder
{
    private static HintsBonusDialogDelegate mDelegate;
    private static AlertDialog mDialog;
    public View.OnClickListener doubleClickListener;
    public DialogInterface.OnCancelListener mCompleteDialogCancelListener;
    private DailyBonusObject mDailyBonus;
    private View mDialogView;
    private boolean mHadRewardedVideo;
    private boolean mScratched;
    Integer otherViewNum1;
    Integer otherViewNum2;
    public View.OnClickListener scratchClickListener;
    public View.OnClickListener takeClickListener;

    public HintsBonusDialog(final Context context) {
        super(context);
        this.mScratched = false;
        this.mDailyBonus = null;
        this.mHadRewardedVideo = false;
        this.mDialogView = null;
        this.scratchClickListener = (View.OnClickListener)new HintsBonusDialog3(this);
        this.takeClickListener = (View.OnClickListener)new HintsBonusDialog4(this);
        this.doubleClickListener = (View.OnClickListener)new HintsBonusDialog5(this);
        this.mCompleteDialogCancelListener = (DialogInterface.OnCancelListener)new HintsBonusDialog6(this);
    }

    public HintsBonusDialog(final Context context, final int n) {
        super(context);
        this.mScratched = false;
        this.mDailyBonus = null;
        this.mHadRewardedVideo = false;
        this.mDialogView = null;
        this.scratchClickListener = (View.OnClickListener)new HintsBonusDialog.HintsBonusDialog3(this);
        this.takeClickListener = (View.OnClickListener)new HintsBonusDialog.HintsBonusDialog4(this);
        this.doubleClickListener = (View.OnClickListener)new HintsBonusDialog.HintsBonusDialog5(this);
        this.mCompleteDialogCancelListener = (DialogInterface.OnCancelListener)new HintsBonusDialog.HintsBonusDialog6(this);
    }

    static /* synthetic */ AlertDialog access000() {
        return HintsBonusDialog.mDialog;
    }

    static /* synthetic */ AlertDialog access002(final AlertDialog mDialog) {
        return HintsBonusDialog.mDialog = mDialog;
    }

    static /* synthetic */ HintsBonusDialog.HintsBonusDialogDelegate access100() {
        return HintsBonusDialog.mDelegate;
    }

    static /* synthetic */ boolean access200(final HintsBonusDialog hintsBonusDialog) {
        return hintsBonusDialog.mHadRewardedVideo;
    }

    static /* synthetic */ View access300(final HintsBonusDialog hintsBonusDialog) {
        return hintsBonusDialog.mDialogView;
    }

    public static AlertDialog getDialog() {
        return HintsBonusDialog.mDialog;
    }

    public static boolean isShowing() {
        return getDialog() != null && getDialog().isShowing();
    }

    private void scratchViews(int identifier, final boolean b) {
        final Handler handler = new Handler(Looper.getMainLooper());
        final HintsBonusDialog.HintsBonusDialog7 hintsBonusDialog7 = new HintsBonusDialog.HintsBonusDialog7(this, b);
        long n;
        if (b) {
            n = 0L;
        }
        else {
            n = 1000L;
        }
        handler.postDelayed((Runnable)hintsBonusDialog7, n);
        this.mScratched = true;
        this.mDialogView.findViewById(R.id.chooseRandomPanel).setVisibility(View.GONE);
        this.mDialogView.findViewById(R.id.bonusPanel).setVisibility(View.VISIBLE);
        this.mHadRewardedVideo = AdsManager.getInstance().hasRewardedVideo();
        if (this.mHadRewardedVideo) {
            ((TextView)this.mDialogView.findViewById(R.id.lowLabel)).setText("Просмотр видеоролика удвоит подсказки");
            ((Button)this.mDialogView.findViewById(R.id.getButton)).setText("Не удваивать");
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "hourly_bonus_video_available");
        }
        else {
            this.mDialogView.findViewById(R.id.lowLabel).setVisibility(View.INVISIBLE);
            this.mDialogView.findViewById(R.id.doubleButton).setVisibility(View.GONE);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "hourly_bonus_video_unavailable");
        }
        final Resources resources = SmappsScanwords.getContext().getResources();
        final StringBuilder sb = new StringBuilder();
        sb.append("scratch_");
        sb.append(identifier);
        identifier = resources.getIdentifier(sb.toString(), "id", SmappsScanwords.getContext().getPackageName());
        final AnimationDrawable animationDrawable = (AnimationDrawable)this.mDialogView.findViewById(identifier).getBackground();
        if (animationDrawable instanceof AnimationDrawable) {
            final AnimationDrawable animationDrawable2 = animationDrawable;
            animationDrawable2.stop();
            if (b) {
                animationDrawable2.selectDrawable(7);
            }
            else {
                animationDrawable2.selectDrawable(0);
                animationDrawable2.start();
            }
        }
        if (!b && getDialog() != null) {
            try {
                SoundsManager.playSound(this.getContext(), Integer.valueOf(R.raw.hourly_bonus));
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void showCompleteBonusDialogInContext(final Context context, final DailyBonusObject dailyBonus, final boolean b) {
        final View inflate = LayoutInflater.from(context).inflate(R.layout.bonus_complete_dialog_view, (ViewGroup)null);
        final HintsBonusDialog hintsBonusDialog = new HintsBonusDialog(context, R.style.Scanword_style_dialog);
        hintsBonusDialog.setCustomView(1, inflate.getContext());
        final AlertDialog show = hintsBonusDialog.show();
        hintsBonusDialog.setDialogView(inflate);
        hintsBonusDialog.setDailyBonus(dailyBonus);
        hintsBonusDialog.setDialog(show);
        final TextView textView = (TextView)inflate.findViewById(R.id.headLabel);
        final int win = dailyBonus.getWin();
        int n;
        if (b) {
            n = 2;
        }
        else {
            n = 1;
        }
        final int n2 = win * n;
        if (!AdsManager.getInstance().hasRewardedVideo()) {
            textView.setText("Поздравляем!");
            inflate.findViewById(R.id.simpleHintsCountsArea).setVisibility(View.VISIBLE);
            final TextView textView2 = (TextView)inflate.findViewById(R.id.hintsReceivedLabel);
            final StringBuilder sb = new StringBuilder();
            sb.append("Вам начислено ");
            sb.append(n2);
            sb.append(" ");
            sb.append(Utils.spellVariantForNumber(n2, "подсказку", "подсказки", "подсказок"));
            textView2.setText((CharSequence)sb.toString());
        }
        else {
            inflate.findViewById(R.id.videoHintsCountsArea).setVisibility(View.VISIBLE);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Начислено ");
            sb2.append(n2);
            sb2.append(" ");
            sb2.append(Utils.spellVariantForNumber(n2, "подсказку", "подсказки", "подсказок"));
            sb2.append(":");
            textView.setText((CharSequence)sb2.toString());
//            final TextView textView3 = (TextView)inflate.findViewById(R.id.videoHintsCountsArea); //TODO layout to textView
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("");
            sb3.append(dailyBonus.getWin());
//            textView3.setText((CharSequence)sb3.toString());
            final TextView textView4 = (TextView)inflate.findViewById(R.id.doubledLabel);
            String text;
            if (b) {
                text =  "да";
            }
            else {
                text = " нет";
            }
            textView4.setText((CharSequence)text);
            final TextView textView5 = (TextView)inflate.findViewById(R.id.doubledLabel);
            String s;
            if (b) {
                s = "#99cc00";
            }
            else {
                s = "#ff4444";
            }
            textView5.setTextColor(Color.parseColor(s));
        }
        ((CheckBox)inflate.findViewById(R.id.checkBox1)).setChecked(true);
        inflate.findViewById(R.id.closeButton).setOnClickListener((View.OnClickListener)new HintsBonusDialog2(show));
        show.setOnCancelListener(hintsBonusDialog.mCompleteDialogCancelListener);
    }

    public static void showDialogInContext(final Context context, final HintsBonusDialog.HintsBonusDialogDelegate mDelegate, final DailyBonusObject dailyBonus) {
        if (dailyBonus == null) {
            return;
        }
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hourly_bonus_show");
        HintsBonusDialog.mDelegate = mDelegate;
        final View inflate = LayoutInflater.from(context).inflate(R.layout.hints_bonus_dialog_view, (ViewGroup)null);
        final HintsBonusDialog hintsBonusDialog = new HintsBonusDialog(context, R.style.Scanword_style_dialog);
        hintsBonusDialog.setDividerColor("#f38d02").setTitle("Бесплатные подсказки").setTitleColor("#f38d02");
        hintsBonusDialog.setCustomView(1, inflate.getContext());
        final AlertDialog show = hintsBonusDialog.show();
        hintsBonusDialog.setDailyBonus(dailyBonus);
        hintsBonusDialog.setDialogView(inflate);
        show.setCancelable(false);
        hintsBonusDialog.setDialog(show);
        if (dailyBonus.getScratchedPosition() > 0) {
            hintsBonusDialog.placeVariants(dailyBonus.getScratchedPosition());
            hintsBonusDialog.scratchViews(dailyBonus.getScratchedPosition(), true);
        }
        else {
            final ImageView imageView = (ImageView)inflate.findViewById(R.id.scratch_1);
            imageView.setOnClickListener(hintsBonusDialog.scratchClickListener);
            imageView.setSoundEffectsEnabled(false);
            final ImageView imageView2 = (ImageView)inflate.findViewById(R.id.scratch_2);
            imageView2.setOnClickListener(hintsBonusDialog.scratchClickListener);
            imageView2.setSoundEffectsEnabled(false);
            final ImageView imageView3 = (ImageView)inflate.findViewById(R.id.scratch_3);
            imageView3.setOnClickListener(hintsBonusDialog.scratchClickListener);
            imageView3.setSoundEffectsEnabled(false);
        }
        inflate.findViewById(R.id.chooseRandomButton).setOnClickListener((View.OnClickListener)new HintsBonusDialog1(inflate, hintsBonusDialog));
        inflate.findViewById(R.id.getButton).setOnClickListener(hintsBonusDialog.takeClickListener);
        inflate.findViewById(R.id.doubleButton).setOnClickListener(hintsBonusDialog.doubleClickListener);
    }

    public View getDialogView() {
        return this.mDialogView;
    }

//    protected int getLayoutresId() {
//        this.layoutResId = 2131492919;
//        return super.getLayoutresId();
//    }

    public void placeVariants(int n) {
        final Integer value = 3;
        final Integer value2 = 2;
        final Integer value3 = 1;
        if (n == 1) {
            this.otherViewNum1 = value2;
            this.otherViewNum2 = value;
        }
        else if (n == 2) {
            this.otherViewNum1 = value3;
            this.otherViewNum2 = value;
        }
        else {
            if (n != 3) {
                return;
            }
            this.otherViewNum1 = value3;
            this.otherViewNum2 = value2;
        }
        this.mDailyBonus.getWin();
        final Resources resources = SmappsScanwords.getContext().getResources();
        final StringBuilder sb = new StringBuilder();
        sb.append("textHints");
        sb.append(n);
        final TextView textView = (TextView)this.mDialogView.findViewById(resources.getIdentifier(sb.toString(), "id", SmappsScanwords.getContext().getPackageName()));
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(this.mDailyBonus.getWin());
        textView.setText((CharSequence)sb2.toString());
        textView.setTextColor(Color.parseColor("#f38d02"));
        final Resources resources2 = SmappsScanwords.getContext().getResources();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("textHintsLabel");
        sb3.append(n);
        n = resources2.getIdentifier(sb3.toString(), "id", SmappsScanwords.getContext().getPackageName());
        final TextView textView2 = (TextView)this.mDialogView.findViewById(n);
        textView2.setText((CharSequence)Utils.spellVariantForNumber(this.mDailyBonus.getWin(),  "подсказка", "подсказки", "подсказок"));
        textView2.setTextColor(Color.parseColor("#f38d02"));
        n = this.mDailyBonus.getOtherVariant(1);
        final Resources resources3 = SmappsScanwords.getContext().getResources();
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("textHints");
        sb4.append(this.otherViewNum1);
        final TextView textView3 = (TextView)this.mDialogView.findViewById(resources3.getIdentifier(sb4.toString(), "id", SmappsScanwords.getContext().getPackageName()));
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("");
        sb5.append(n);
        textView3.setText((CharSequence)sb5.toString());
        final Resources resources4 = SmappsScanwords.getContext().getResources();
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("textHintsLabel");
        sb6.append(this.otherViewNum1);
        ((TextView)this.mDialogView.findViewById(resources4.getIdentifier(sb6.toString(), "id", SmappsScanwords.getContext().getPackageName()))).setText((CharSequence)Utils.spellVariantForNumber(n, "подсказка", "подсказки", "подсказок"));
        n = this.mDailyBonus.getOtherVariant(2);
        final Resources resources5 = SmappsScanwords.getContext().getResources();
        final StringBuilder sb7 = new StringBuilder();
        sb7.append("textHints");
        sb7.append(this.otherViewNum2);
        final TextView textView4 = (TextView)this.mDialogView.findViewById(resources5.getIdentifier(sb7.toString(), "id", SmappsScanwords.getContext().getPackageName()));
        final StringBuilder sb8 = new StringBuilder();
        sb8.append("");
        sb8.append(n);
        textView4.setText((CharSequence)sb8.toString());
        final Resources resources6 = SmappsScanwords.getContext().getResources();
        final StringBuilder sb9 = new StringBuilder();
        sb9.append("textHintsLabel");
        sb9.append(this.otherViewNum2);
        ((TextView)this.mDialogView.findViewById(resources6.getIdentifier(sb9.toString(), "id", SmappsScanwords.getContext().getPackageName()))).setText((CharSequence)Utils.spellVariantForNumber(n, "подсказка", "подсказки", "подсказок"));
    }

    public void scratch(final View view) {
        if (this.mScratched) {
            return;
        }
        int scratchedPosition;
        if (view.getId() == R.id.scratch_1) {
            scratchedPosition = 1;
        }
        else if (view.getId() == R.id.scratch_2) {
            scratchedPosition = 2;
        }
        else {
            if (view.getId() != R.id.scratch_3) {
                return;
            }
            scratchedPosition = 3;
        }
        this.placeVariants(scratchedPosition);
        this.scratchViews(scratchedPosition, false);
        this.mDailyBonus.setScratchedPosition(scratchedPosition);
        MainDataManager.getInstance().rewriteDailyBonusObjectRecieved(this.mDailyBonus);
    }

    public void setDailyBonus(final DailyBonusObject mDailyBonus) {
        this.mDailyBonus = mDailyBonus;
    }

    public void setDialog(final AlertDialog mDialog) {
        HintsBonusDialog.mDialog = mDialog;
    }

    public void setDialogView(final View mDialogView) {
        this.mDialogView = mDialogView;
    }

    public interface HintsBonusDialogDelegate
    {
        void onCancel();

        void onDouble();

        void onTake(final boolean p0);
    }

    class HintsBonusDialog3 implements View.OnClickListener {
        private boolean mScratched = false;
        final /* synthetic */ HintsBonusDialog this0;

        HintsBonusDialog3(final HintsBonusDialog this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            if (!this.mScratched) {
                this.mScratched = true;
                this.this0.scratch(view);
            }
        }
    }

    class HintsBonusDialog4 implements View.OnClickListener {
        final /* synthetic */ HintsBonusDialog this0;

        HintsBonusDialog4(final HintsBonusDialog this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            final AlertDialog access000 = HintsBonusDialog.access000();
            HintsBonusDialog.access002((AlertDialog)null);
            if (HintsBonusDialog.access100() != null) {
                HintsBonusDialog.access100().onTake(HintsBonusDialog.access200(this.this0));
            }
            if (access000 != null) {
                access000.dismiss();
            }
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "hourly_bonus_take");
        }
    }

    class HintsBonusDialog5 implements View.OnClickListener {
        final /* synthetic */ HintsBonusDialog this0;

        HintsBonusDialog5(final HintsBonusDialog this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            final AlertDialog access000 = HintsBonusDialog.access000();
            HintsBonusDialog.access002((AlertDialog)null);
            if (HintsBonusDialog.access100() != null) {
                HintsBonusDialog.access100().onDouble();
            }
            if (access000 != null) {
                access000.dismiss();
            }
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "hourly_bonus_double");
        }
    }


    class HintsBonusDialog6 implements DialogInterface.OnCancelListener {
        final /* synthetic */ HintsBonusDialog this0;

        HintsBonusDialog6(final HintsBonusDialog this0) {
            super();
            this.this0 = this0;
        }

        public void onCancel(final DialogInterface dialogInterface) {
            final CheckBox checkBox = (CheckBox)this.this0.getDialogView().findViewById(R.id.checkBox1);
            if (checkBox.isChecked()) {
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hourly_bonus_end_notify");
            }
            else {
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hourly_bonus_end_not_notify");
            }
            SmappsScanwords.getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putBoolean("daily_bonus_notify", checkBox.isChecked());
        }
    }

    class HintsBonusDialog7 implements Runnable {
        final /* synthetic */ HintsBonusDialog this0;
        final /* synthetic */ boolean valb;

        HintsBonusDialog7(final HintsBonusDialog this0, final boolean valb) {
            super();
            this.this0 = this0;
            this.valb = valb;
        }

        @Override
        public void run() {
            final Resources resources = SmappsScanwords.getContext().getResources();
            final StringBuilder sb = new StringBuilder();
            sb.append("scratch_");
            sb.append(this.this0.otherViewNum1);
            final int identifier = resources.getIdentifier(sb.toString(), "id", SmappsScanwords.getContext().getPackageName());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("resId - ");
            sb2.append(identifier);
            Log.v("SkanwordsFunc", sb2.toString());
            final AnimationDrawable animationDrawable = (AnimationDrawable)HintsBonusDialog.access300(this.this0).findViewById(identifier).getBackground();
            if (animationDrawable instanceof AnimationDrawable) {
                final AnimationDrawable animationDrawable2 = animationDrawable;
                animationDrawable2.stop();
                if (this.valb) {
                    animationDrawable2.selectDrawable(7);
                }
                else {
                    animationDrawable2.selectDrawable(0);
                    animationDrawable2.start();
                }
            }
            final Resources resources2 = SmappsScanwords.getContext().getResources();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("scratch_");
            sb3.append(this.this0.otherViewNum2);
            final int identifier2 = resources2.getIdentifier(sb3.toString(), "id", SmappsScanwords.getContext().getPackageName());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("resId - ");
            sb4.append(identifier2);
            Log.v("SkanwordsFunc", sb4.toString());
            final AnimationDrawable animationDrawable3 = (AnimationDrawable)HintsBonusDialog.access300(this.this0).findViewById(identifier2).getBackground();
            if (animationDrawable3 instanceof AnimationDrawable) {
                final AnimationDrawable animationDrawable4 = animationDrawable3;
                animationDrawable4.stop();
                if (this.valb) {
                    animationDrawable4.selectDrawable(7);
                }
                else {
                    animationDrawable4.selectDrawable(0);
                    animationDrawable4.start();
                }
            }
            if (!this.valb && HintsBonusDialog.getDialog() != null) {
                SoundsManager.playSound(this.this0.getContext(), Integer.valueOf(R.raw.hourly_bonus_ding));
            }
        }
    }

    static final class HintsBonusDialog2 implements View.OnClickListener {
        final /* synthetic */ AlertDialog valdialog;

        HintsBonusDialog2(final AlertDialog valdialog) {
            super();
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            HintsBonusDialog.access002((AlertDialog)null);
            this.valdialog.cancel();
        }
    }

    static final class HintsBonusDialog1 implements View.OnClickListener {
        final /* synthetic */ HintsBonusDialog valqustomDialogBuilder;
        final /* synthetic */ View valview;

        HintsBonusDialog1(final View valview, final HintsBonusDialog valqustomDialogBuilder) {
            super();
            this.valview = valview;
            this.valqustomDialogBuilder = valqustomDialogBuilder;
        }

        public void onClick(final View view) {
            final int randInt = Utils.randInt(1, 3);
            final Resources resources = SmappsScanwords.getContext().getResources();
            final StringBuilder sb = new StringBuilder();
            sb.append("scratch_");
            sb.append(randInt);
            this.valqustomDialogBuilder.scratchClickListener.onClick((View)this.valview.findViewById(resources.getIdentifier(sb.toString(), "id", SmappsScanwords.getContext().getPackageName())));
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "hourly_bonus_any");
        }
    }
}