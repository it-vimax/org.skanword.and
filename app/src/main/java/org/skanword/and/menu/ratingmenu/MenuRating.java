package org.skanword.and.menu.ratingmenu;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import org.skanword.and.datamanager.RatingDataManager;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import de.hdodenhof.circleimageview.CircleImageView;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.RatingDataManager.RatingType;
import org.skanword.and.datamanager.RatingDataManager.UsersRating;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;

public class MenuRating extends CustomFragment implements ActionBar.TabListener
{
    private static Activity sActivity;
    private MenuRating.AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    private View mAuthButtons;
    private View mMainView;
    private PagerSlidingTabStrip mTabs;
    private EventListener mUserInfoUpdateEventListener;
    private ViewPager mViewPager;

    public MenuRating() {
        super();
        Log.v("", "MenuShop constructor");
    }

    static /* synthetic */ View access000(final MenuRating menuRating) {
        return menuRating.mAuthButtons;
    }

    public static Activity getRatingActivity() {
        return MenuRating.sActivity;
    }

    public void findUser() {
        if (!MainNetworkManager.getInstance().isUserAuthorised()) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.getActivity(), R.style.Theme_AppCompat_DayNight);
            alertDialogBuilder.setMessage("Авторизируйтесь, чтобы учавствовать в рейтинге.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new MenuRating1(this));
            alertDialogBuilder.create().show();
        }
        else {
            ((RatingTabFragment)this.getVisibleFragment()).scrolltoUser();
        }
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_rating_finduser");
    }

    public void fragmentOpened() {
        this.updateFragment();
        super.fragmentOpened();
    }

    public Fragment getVisibleFragment() {
        final MenuRating.AppSectionsPagerAdapter mAppSectionsPagerAdapter = this.mAppSectionsPagerAdapter;
        final ViewPager mViewPager = this.mViewPager;
        final Fragment fragment = (Fragment)mAppSectionsPagerAdapter.instantiateItem((ViewGroup)mViewPager, mViewPager.getCurrentItem());
        final StringBuilder sb = new StringBuilder();
        sb.append(fragment);
        sb.append(" getVisibleFragment android:switcher:");
        sb.append(R.id.pager);
        sb.append(":");
        sb.append(this.mViewPager.getCurrentItem());
        Log.v("", sb.toString());
        if (fragment != null) {
            return fragment;
        }
        return null;
    }

    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Log.v("", " onCreateView  MenuRating");
        int visibility = 0;
        this.mMainView = layoutInflater.inflate(R.layout.rating_menu_layout, viewGroup, false);
        this.mAuthButtons = this.mMainView.findViewById(R.id.ratingAuthButtons);
        final View mAuthButtons = this.mAuthButtons;
        if (MainNetworkManager.getInstance().isUserAuthorised()) {
            visibility = 8;
        }
        mAuthButtons.setVisibility(visibility);
        this.mAppSectionsPagerAdapter = new MenuRating.AppSectionsPagerAdapter(this.getChildFragmentManager());
        (this.mViewPager = (ViewPager)this.mMainView.findViewById(R.id.shop_pager)).setAdapter((PagerAdapter)this.mAppSectionsPagerAdapter);
        (this.mTabs = (PagerSlidingTabStrip)this.mMainView.findViewById(R.id.shop_tabs)).setTextColorResource(R.color.action_bar_title_color);
        this.mTabs.setOnPageChangeListener((ViewPager.OnPageChangeListener)new MenuRating2(this));
        this.mTabs.setViewPager(this.mViewPager);
        MenuRating.sActivity = (Activity)this.getActivity();
        final MenuRating3 onClickListener = new MenuRating3(this, (Activity)MainMenuActivity.instance);
        ((Button)this.mAuthButtons.findViewById(R.id.okAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
        ((Button)this.mAuthButtons.findViewById(R.id.fbAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
        ((Button)this.mAuthButtons.findViewById(R.id.vkAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
        this.mUserInfoUpdateEventListener = (EventListener)new MenuRating4(this);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_USER_INFO_UPDATE", this.mUserInfoUpdateEventListener);
        return this.mMainView;
    }

    public void onDestroy() {
        Log.v("", "onDestroy -------MenuShop------");
        MenuRating.sActivity = null;
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_USER_INFO_UPDATE", this.mUserInfoUpdateEventListener);
        super.onDestroy();
    }

    public void onPause() {
        Log.v("", "onPause -------MenuShop------");
        super.onPause();
    }

    public void onResume() {
        Log.v("", "onResume -------MenuShop------");
        super.onResume();
    }

    public void onStart() {
        Log.v("", "onStart -------MenuShop------");
        super.onStart();
    }

    public void onStop() {
        Log.v("", "onStop -------MenuShop------");
        super.onStop();
    }

    public void onTabReselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
        this.mViewPager.setCurrentItem(actionBarTab.getPosition());
    }

    public void onTabSelected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
    }

    public void onTabUnselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
    }

    public void updateFragment(final boolean b) {
        final MainNetworkManager instance = MainNetworkManager.getInstance();
        Object activity;
        if (b) {
            activity = this.getActivity();
        }
        else {
            activity = null;
        }
        if (instance.hasNetworkConnection((Activity)activity)) {
            MainNetworkManager.getInstance().requestInfoWithCompleteBlock(4, (Activity)this.getActivity(), (MainNetworkManager.RequestCompletionBlock)null);
        }
        super.updateFragment(b);
    }

    // --- Classes

    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter
    {
        private Set<Object> fragments;

        public AppSectionsPagerAdapter(final FragmentManager fragmentManager) {
            super(fragmentManager);
            this.fragments = new HashSet();
        }

        public void destroyItem(final ViewGroup viewGroup, final int n, final Object o) {
            super.destroyItem(viewGroup, n, o);
        }

        public int getCount() {
            return 2;
        }

        public Fragment getItem(final int n) {
            final RatingTabFragment tabFragment = new RatingTabFragment();
            final Bundle arguments = new Bundle();
            RatingDataManager.RatingType ratingDataManagerRatingType;
            if (n == 0) {
                ratingDataManagerRatingType = RatingDataManager.RatingType.SCANWORD_COMMON;
            }
            else {
                ratingDataManagerRatingType = RatingDataManager.RatingType.SCANWORD_FRIENDS;
            }
            arguments.putSerializable("LIST_TYPE", (Serializable)ratingDataManagerRatingType);
            tabFragment.setArguments(arguments);
            return (Fragment)tabFragment;
        }

        public CharSequence getPageTitle(int var1) {
            if (var1 != 0) {
                if (var1 != 1) {
                    StringBuilder var2 = new StringBuilder();
                    var2.append("Section ");
                    var2.append(var1 + 1);
                    return var2.toString();
                } else {
                    return "СРЕДИ ДРУЗЕЙ";
                }
            } else {
                return "СРЕДИ ВСЕХ";
            }
        }
        public Object instantiateItem(final ViewGroup viewGroup, final int n) {
            final Object instantiateItem = super.instantiateItem(viewGroup, n);
            this.fragments.add(instantiateItem);
            return instantiateItem;
        }
    }

    class MenuRating2 implements ViewPager.OnPageChangeListener {
        final /* synthetic */ MenuRating this0;

        MenuRating2(final MenuRating this0) {
            super();
            this.this0 = this0;
        }

        public void onPageScrollStateChanged(final int n) {
        }

        public void onPageScrolled(final int n, final float n2, final int n3) {
        }

        public void onPageSelected(final int n) {
            if (n == 0) {
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_rating_all_enter");
                return;
            }
            if (n != 1) {
                return;
            }
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_rating_friends_enter");
        }
    }

    class MenuRating1 implements DialogInterface.OnClickListener {
        final /* synthetic */ MenuRating this0;

        MenuRating1(final MenuRating this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    public static class RatingTabFragment extends Fragment
    {
        private MenuRating.RatingItemsAdapter mAdapter;
        private ListView mList;
        private RatingDataManager.RatingType mListType;
        private EventListener mRatingUpdateEventListener;
        private View mView;

        public RatingTabFragment() {
            super();
        }

        static /* synthetic */ void access100(final RatingTabFragment ratingTabFragment) {
            ratingTabFragment.setAdapter();
        }

        private void setAdapter() {
            List list;
            if (this.mListType == RatingDataManager.RatingType.SCANWORD_COMMON) {
                list = MainDataManager.getInstance().getRatingDataManager().getCommonRating();
            }
            else {
                list = MainDataManager.getInstance().getRatingDataManager().getFriendsRating();
            }
            if (list != null && list.size() != 0) {
                this.mList.setVisibility(View.VISIBLE);
                this.mView.findViewById(R.id.ratingEmptyLabel).setVisibility(View.GONE);
                this.mAdapter = new RatingItemsAdapter((Context)this.getActivity(), R.layout.rating_user_row_item_layout, list);
                this.mList.setAdapter((ListAdapter)this.mAdapter);
                return;
            }
            if (this.mListType == RatingDataManager.RatingType.SCANWORD_FRIENDS) {
                this.mList.setVisibility(View.GONE);
                this.mView.findViewById(R.id.ratingEmptyLabel).setVisibility(View.VISIBLE);
            }
        }

        public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
            this.mView = layoutInflater.inflate(R.layout.rating_list_layout, viewGroup, false);
            this.mList = (ListView)this.mView.findViewById(R.id.rating_list);
            this.mListType = (RatingDataManager.RatingType)this.getArguments().getSerializable("LIST_TYPE");
            this.mList.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            this.mRatingUpdateEventListener = (EventListener)new RatingTabFragment.RatingTabFragment1(this);
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_RATING_UPDATE", this.mRatingUpdateEventListener);
            this.setAdapter();
            return this.mView;
        }

        public void onDestroy() {
            super.onDestroy();
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_RATING_UPDATE", this.mRatingUpdateEventListener);
        }

        public void onSaveInstanceState(final Bundle bundle) {
            super.onSaveInstanceState(bundle);
            Log.v("", "  onSaveInstanceState ");
        }

        public void onStart() {
            super.onStart();
        }

        public void scrolltoUser() {
            final RatingItemsAdapter mAdapter = this.mAdapter;
            if (mAdapter != null) {
                if (mAdapter.getUserPosition() < 0) {
                    return;
                }
                this.mList.smoothScrollToPositionFromTop(this.mAdapter.getUserPosition(), this.mList.getHeight() / 2 - RatingItemsAdapter.sRowHeight / 2, 100);
            }
        }

        public void update() {
            if (this.getActivity() != null) {
                this.getActivity().runOnUiThread((Runnable)new RatingTabFragment.RatingTabFragment2(this));
            }
        }

        class RatingTabFragment1 implements EventListener {
            final /* synthetic */ RatingTabFragment this0;

            RatingTabFragment1(final RatingTabFragment this0) {
                super();
                this.this0 = this0;
            }

            public void onEvent(final Event event) {
                this.this0.update();
            }
        }

        class RatingTabFragment2 implements Runnable {
            final /* synthetic */ RatingTabFragment this0;

            RatingTabFragment2(final RatingTabFragment this0) {
                super();
                this.this0 = this0;
            }

            @Override
            public void run() {
                RatingTabFragment.access100(this.this0);
            }
        }
    }

    public static class RatingItemsAdapter extends ArraySectionedListAdapter<RatingDataManager.UsersRating>
    {
        public static int sRowHeight;
        private final List<RatingDataManager.UsersRating> objectsList;
        private int rowsInFirstSection;
        private int sectionsCount;

        public RatingItemsAdapter(final Context context, int i, final List<RatingDataManager.UsersRating> objectsList) {
            super(context, i, (List)objectsList);
            this.sectionsCount = 1;
            this.rowsInFirstSection = 0;
            Log.v("", "SetsAdapter");
            this.objectsList = objectsList;
            this.rowsInFirstSection = this.objectsList.size();
            for (i = 1; i < this.objectsList.size(); ++i) {
                if (this.objectsList.get(i).getUserPlace() - this.objectsList.get(i - 1).getUserPlace() > 1) {
                    this.sectionsCount = 2;
                    this.rowsInFirstSection = i + 1;
                    return;
                }
            }
        }

        private void buildRowView(final View view, final RatingDataManager.UsersRating ratingDataManagerUsersRating) {
            if (view == null) {
                return;
            }
            ((TextView)view.findViewById(R.id.userNameLabel)).setText((CharSequence)ratingDataManagerUsersRating.getSocialUser().getUserName());
            final TextView textView = (TextView)view.findViewById(R.id.placeLabel);
            final StringBuilder sb = new StringBuilder();
            sb.append(ratingDataManagerUsersRating.getUserPlace());
            sb.append(" место");
            textView.setText((CharSequence)sb.toString());
            final TextView textView2 = (TextView)view.findViewById(R.id.pointsLabel);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(ratingDataManagerUsersRating.getUserScore());
            sb2.append(" ");
            sb2.append(Utils.spellVariantForNumber(Integer.parseInt(ratingDataManagerUsersRating.getUserScore()),  "очко", "очка", "очков"));
            textView2.setText((CharSequence)sb2.toString());
            final CircleImageView circleImageView = (CircleImageView)view.findViewById(R.id.profile_image);
            if (ratingDataManagerUsersRating.getSocialUser().getUserPhoto() != null && ratingDataManagerUsersRating.getSocialUser().getUserPhoto().length() > 2) {
                UrlImageViewHelper.setUrlDrawable((ImageView)circleImageView, ratingDataManagerUsersRating.getSocialUser().getUserPhoto(), R.drawable.avatar);
            }
            else {
                circleImageView.setImageResource(R.drawable.avatar);
            }
            if (MainNetworkManager.getInstance().isUserAuthorised() && ratingDataManagerUsersRating.getSocialUser().getUserId().equals(MainNetworkManager.getInstance().getUserAuth().getUserSocialId())) {
                circleImageView.setBorderColor(-9987063);
            }
            else {
                circleImageView.setBorderColor(-592138);
            }
            view.setClickable(true);
        }

        protected void bindRowViewInSection(final Context context, final int n, final int n2, final View view) {
            final RatingDataManager.UsersRating itemInSectionAndRow = this.getItemInSectionAndRow(n, n2);
            if (itemInSectionAndRow != null) {
                this.buildRowView(view, itemInSectionAndRow);
            }
        }


        protected void bindSectionView(Context var1, int var2, View var3) {
            if (var3 != null) {
                if (this.objectsList.size() < 1) {
                    return;
                }

                ((TextView)var3.findViewById(R.id.setLabel)).setOnClickListener(new RatingItemsAdapter1(this));
                TextView var5;
                StringBuilder var6;
                if (var2 == 0) {
                    var5 = (TextView)var3.findViewById(R.id.setLabel);
                    var6 = new StringBuilder();
                    var6.append(((UsersRating)this.objectsList.get(0)).getUserPlace());
                    var6.append("-");
                    var6.append(((UsersRating)this.objectsList.get(this.rowsInFirstSection - 1)).getUserPlace());
                    var6.append(" место");
                    var5.setText(var6.toString());
                    return;
                }

                var5 = (TextView)var3.findViewById(R.id.setLabel);
                var6 = new StringBuilder();
                var6.append(((UsersRating)this.objectsList.get(this.rowsInFirstSection)).getUserPlace());
                var6.append("-");
                List var4 = this.objectsList;
                var6.append(((UsersRating)var4.get(var4.size() - 1)).getUserPlace());
                var6.append(" место");
                var5.setText(var6.toString());
            }

        }

        public RatingDataManager.UsersRating getItemInSectionAndRow(final int n, final int n2) {
            if (n == 0) {
                return (RatingDataManager.UsersRating)this.getItem(n2);
            }
            return (RatingDataManager.UsersRating)this.getItem(n2 + this.rowsInFirstSection);
        }

        protected int getNumberOfRowsInSection(final int n) {
            if (n == 0) {
                return this.rowsInFirstSection;
            }
            return this.objectsList.size() - this.rowsInFirstSection;
        }

        protected int getNumberOfSections() {
            return this.sectionsCount;
        }

        public int getUserPosition() {
            if (!MainNetworkManager.getInstance().isUserAuthorised()) {
                return -1;
            }
            for (final RatingDataManager.UsersRating ratingDataManagerUsersRating : this.objectsList) {
                if (ratingDataManagerUsersRating.getSocialUser().getUserId().equals(MainNetworkManager.getInstance().getUserAuth().getUserSocialId())) {
                    return this.objectsList.indexOf(ratingDataManagerUsersRating) + this.sectionsCount;
                }
            }
            return -1;
        }

        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            return super.getView(n, view, viewGroup);
        }

        protected int getViewTypeForRowInSection(final int n, final int n2) {
            return 1;
        }

        protected View newRowViewInSection(final Context context, final int n, final int n2, final ViewGroup viewGroup) {
            final View inflate = LayoutInflater.from(this.getContext()).inflate(R.layout.rating_user_row_item_layout, viewGroup, false);
            RatingItemsAdapter.sRowHeight = inflate.getHeight();
            return inflate;
        }

        protected View newSectionView(final Context context, final int n, final ViewGroup viewGroup) {
            return ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header, viewGroup, false);
        }
    }

    static class RatingItemsAdapter1 implements View.OnClickListener {
        final /* synthetic */ RatingItemsAdapter this0;

        RatingItemsAdapter1(final RatingItemsAdapter this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
        }
    }

    class MenuRating3 implements View.OnClickListener {
        final /* synthetic */ MenuRating this0;
        final /* synthetic */ Activity valact;

        MenuRating3(final MenuRating this0, final Activity valact) {
            super();
            this.this0 = this0;
            this.valact = valact;
        }

        public void onClick(final View view) {
            if (view.getId() == R.id.fbAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB, this.valact, true);
                return;
            }
            if (view.getId() == R.id.okAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK, this.valact, true);
                return;
            }
            if (view.getId() == R.id.vkAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK, this.valact, true);
            }
        }
    }

    class MenuRating4 implements EventListener {
        final /* synthetic */ MenuRating this0;

        MenuRating4(final MenuRating this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            if (this.this0.getActivity() != null) {
                this.this0.getActivity().runOnUiThread((Runnable)new MenuRating4.MenuRating41(this));
            }
        }

        class MenuRating41 implements Runnable {
            final /* synthetic */ MenuRating4 this1;

            MenuRating41(final MenuRating4 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                final View access000 = MenuRating.access000(this.this1.this0);
                int visibility;
                if (MainNetworkManager.getInstance().isUserAuthorised()) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                access000.setVisibility(visibility);
            }
        }
    }
}