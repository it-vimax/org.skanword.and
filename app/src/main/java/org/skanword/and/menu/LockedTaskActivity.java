package org.skanword.and.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.ShopDataManager;
import org.skanword.and.datamanager.ShopDataManager.ShopProductType;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.AdsManager.AdsEventsListener;
import org.skanword.and.etc.IAdsRewardedVideoActivity;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.etc.TopActivityManager.TopActivity;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.inappbilling.InAppManager.PurchaseSource;
import org.skanword.and.network.MainNetworkManager;


public class LockedTaskActivity extends Activity implements TopActivityManager.TopActivity, IAdsRewardedVideoActivity
{
    public static final String EXTRA_TASK_ID = "EXTRA_TASK_ID";
    public static final String EXTRA_TASK_OFFER_TARGET = "EXTRA_TASK_OFFER_TARGET";
    public static final String EXTRA_TASK_WITH_OFFER = "EXTRA_TASK_WITH_OFFER";
    public static final int LOCKED_TASK_ACTIVITY_REQUEST_CODE = 105;
    private boolean mOfferEnabled;

    public LockedTaskActivity() {
        super();
        this.mOfferEnabled = false;
    }

    static /* synthetic */ void access000(final LockedTaskActivity lockedTaskActivity) {
        lockedTaskActivity.activatePremium();
    }

    static /* synthetic */ void access100(final LockedTaskActivity lockedTaskActivity) {
        lockedTaskActivity.playFree();
    }

    private void activatePremium() {
        InAppManager.getInstance().purchaseProduct(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getId(), (Activity)this, InAppManager.PurchaseSource.LOCKED_TASK);
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_lockedscanwin_vipactivate");
    }

    private void playFree() {
        Log.v("", " ---------playFree---------  ");
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_lockedscanwin_iwantfree");
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_playfreewin_enter");
        this.startActivityForResult(new Intent("android.intent.action.EDIT", (Uri)null, (Context)this, (Class)FreePlayActivity.class), 99);
    }

    protected void onActivityResult(final int n, final int result, final Intent intent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(n);
        sb.append(" ---------onActivityResult---------  ");
        sb.append(n);
        Log.v("", sb.toString());
        if (n == 99 && result != 0) {
            this.setResult(result);
            this.finish();
        }
        InAppManager.getInstance().onActivityResult(n, result, intent);
    }

    protected void onCreate(Bundle extras) {
        super.onCreate(extras);
        this.setContentView(R.layout.locked_task_activity_layout);
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        extras = this.getIntent().getExtras();
        this.mOfferEnabled = extras.getBoolean("EXTRA_TASK_WITH_OFFER");
        final String string = extras.getString("EXTRA_TASK_OFFER_TARGET");
        final int int1 = extras.getInt("EXTRA_TASK_ID");
        if (this.mOfferEnabled) {
            ((Button)this.findViewById(R.id.openButton)).setVisibility(View.INVISIBLE);
        }
        else {
            ((Button)this.findViewById(R.id.openOfferButton)).setVisibility(View.INVISIBLE);
        }
        ((Button)this.findViewById(R.id.openOfferButton)).setOnClickListener((View.OnClickListener)new LockedTaskActivity.LockedTaskActivity1(this, Integer.valueOf(int1), string));
        ((Button)this.findViewById(R.id.activatePremiumButton)).setOnClickListener((View.OnClickListener)new LockedTaskActivity.LockedTaskActivity2(this));
        ((Button)this.findViewById(R.id.openButton)).setOnClickListener((View.OnClickListener)new LockedTaskActivity.LockedTaskActivity3(this));
        this.setTitle("Сканворд закрыт");
        final TextView textView = (TextView)this.findViewById(R.id.bonusLabel);
        final StringBuilder sb = new StringBuilder();
        sb.append("<font size=\"30\"><b>");
        sb.append(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getVipScanwordsNew());
        sb.append("</b></font> ");
        sb.append(Utils.spellVariantForNumber(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getVipScanwordsNew(), "новый", "новых", "новых"));
        sb.append(" ");
        sb.append(Utils.spellVariantForNumber(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getVipScanwordsNew(), "сканворд", "сканворда", "сканвордов"));
        sb.append(" \u0438 <font size=\"30\"><b>");
        sb.append(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getVipScanwordsArchive());
        sb.append("</b></font> ");
        sb.append(Utils.spellVariantForNumber(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getVipScanwordsArchive(), "сканворд", "сканворда", "сканвордов"));
        sb.append(" из архива<br><br>Бонус - отключение рекламы!");
        textView.setText((CharSequence)Html.fromHtml(sb.toString()));
        ((TextView)this.findViewById(R.id.priceLabel)).setText((CharSequence)Html.fromHtml("Доступ ко всем<br>сканвордам всего:"));
        final TextView textView2 = (TextView)this.findViewById(R.id.priceCountLabel);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("<b>");
        sb2.append(InAppManager.getInstance().getPriceForProductId(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getId()));
        sb2.append("</b>");
        textView2.setText((CharSequence)Html.fromHtml(sb2.toString()));
        final TextView textView3 = (TextView)this.findViewById(R.id.durationLabel);
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("<b>за ");
        sb3.append(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getName());
        sb3.append("</b>");
        textView3.setText((CharSequence)Html.fromHtml(sb3.toString()));
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.finish();
        return true;
    }

    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
        if (b) {
            final Display defaultDisplay = ((WindowManager)this.getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
            final Point point = new Point();
            defaultDisplay.getSize(point);
            final LinearLayout linearLayout = (LinearLayout)this.findViewById(R.id.priceLayout);
            final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)linearLayout.getLayoutParams();
            layoutParams.height = (int)(point.y * 0.49f);
            linearLayout.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            final TextView textView = (TextView)this.findViewById(R.id.bonusLabel);
            final RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams)textView.getLayoutParams();
            layoutParams2.height = this.findViewById(R.id.openButton).getTop() - (int)(point.y * 0.51f);
            textView.setLayoutParams((ViewGroup.LayoutParams)layoutParams2);
            final StringBuilder sb = new StringBuilder();
            sb.append(this.findViewById(R.id.openButton).getTop());
            sb.append(" params.height ");
            sb.append(layoutParams2.height);
            Log.v("", sb.toString());
        }
    }

    class LockedTaskActivity1 implements View.OnClickListener {
        final /* synthetic */ LockedTaskActivity this0;
        final /* synthetic */ String valofferTarget;
        final /* synthetic */ Integer valtaskId;

        LockedTaskActivity1(final LockedTaskActivity this0, final Integer valtaskId, final String valofferTarget) {
            super();
            this.this0 = this0;
            this.valtaskId = valtaskId;
            this.valofferTarget = valofferTarget;
        }

        public void onClick(final View view) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_lockedscanwin_offerbtn");
            AdsManager.getInstance().showVideoOffer((Activity)this.this0, (AdsManager.AdsEventsListener)new LockedTaskActivity1.LockedTaskActivity11(this), this.valofferTarget, true);
        }

        class LockedTaskActivity11 extends AdsManager.AdsEventsListener {
            final /* synthetic */ LockedTaskActivity1 this1;

            LockedTaskActivity11(final LockedTaskActivity1 this1) {
                super();
                this.this1 = this1;
            }

            public void onComplete() {
                super.onComplete();
                MainDataManager.getInstance().getSkanwordsDataManager().openScanwordForOffer(this.this1.valtaskId);
                MainNetworkManager.getInstance().requestSyncExec();
                this.this1.this0.finish();
            }

            public void onNoAds() {
                super.onNoAds();
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.this1.this0, R.style.Scanword_style_dialog);
                alertDialogBuilder.setMessage("Извините, сейчас у нас нет предложений для вас. Пожалуйста, попробуйте позже.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new LockedTaskActivity11.LockedTaskActivity111(this));
                alertDialogBuilder.create().show();
            }

            class LockedTaskActivity111 implements DialogInterface.OnClickListener {
                final /* synthetic */ LockedTaskActivity11 this2;

                LockedTaskActivity111(final LockedTaskActivity11 this2) {
                    super();
                    this.this2 = this2;
                }

                public void onClick(final DialogInterface dialogInterface, final int n) {
                }
            }
        }
    }

    class LockedTaskActivity11 extends AdsManager.AdsEventsListener {
        final /* synthetic */ LockedTaskActivity1 this1;

        LockedTaskActivity11(final LockedTaskActivity1 this1) {
            super();
            this.this1 = this1;
        }

        public void onComplete() {
            super.onComplete();
            MainDataManager.getInstance().getSkanwordsDataManager().openScanwordForOffer(this.this1.valtaskId);
            MainNetworkManager.getInstance().requestSyncExec();
            this.this1.this0.finish();
        }

        public void onNoAds() {
            super.onNoAds();
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.this1.this0, R.style.Scanword_style_dialog);
            alertDialogBuilder.setMessage("Извините, сейчас у нас нет предложений для вас. Пожалуйста, попробуйте позже.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new LockedTaskActivity11.LockedTaskActivity111(this));
            alertDialogBuilder.create().show();
        }

        class LockedTaskActivity111 implements DialogInterface.OnClickListener {
            final /* synthetic */ LockedTaskActivity11 this2;

            LockedTaskActivity111(final LockedTaskActivity11 this2) {
                super();
                this.this2 = this2;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class LockedTaskActivity2 implements View.OnClickListener {
        final /* synthetic */ LockedTaskActivity this0;

        LockedTaskActivity2(final LockedTaskActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            LockedTaskActivity.access000(this.this0);
        }
    }

    class LockedTaskActivity3 implements View.OnClickListener {
        final /* synthetic */ LockedTaskActivity this0;

        LockedTaskActivity3(final LockedTaskActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            LockedTaskActivity.access100(this.this0);
        }
    }
}
