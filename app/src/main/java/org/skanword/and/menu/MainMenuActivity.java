package org.skanword.and.menu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;

import org.skanword.and.datamanager.ShopDataManager;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;

import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.sbstrm.appirater.Appirater;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.CustomReferralReceiver;
import org.skanword.and.etc.IAdsInterstitialActivity;
import org.skanword.and.etc.IAdsRewardedVideoActivity;
import org.skanword.and.etc.LocalNotificationsManager;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.menu.newsmenu.MenuNews;
import org.skanword.and.menu.ratingmenu.MenuRating;
import org.skanword.and.menu.shopmenu.MenuShop;
import org.skanword.and.menu.skanwordsmenu.MenuSkanwords;
import org.skanword.and.network.DailyBonusObject;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager;

public class MainMenuActivity extends FragmentActivity implements TopActivityManager.TopActivity, IAdsRewardedVideoActivity, IAdsInterstitialActivity
{
    private static final String CAMPAIGN_SOURCE_PARAM = "utm_source";
    private static final int MY_PERMISSIONS_REQUEST = 100000;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static boolean hasPermissions = false;
    public static MainMenuActivity instance;
    private static MainMenuActivity.BonusCompleteData mBonusComplete;
    String SENDER_ID;
    private CustomFragment mCurrentFragment;
    private int mCurrentTag;
    private DrawerLayout mDrawerLayout;
    private CharSequence mDrawerTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIsRunning;
    private LeftPanel mLeftPanelView;
    private MenuNews mMenuNews;
    private MenuRating mMenuRating;
    private MenuSkanwords mMenuScanwords;
    private MenuShop mMenuShop;
    private EventListener mNetworkStateChangedEventListener;
    private EventListener mNotificationsEventListener;
    private EventListener mPushTokenEventListener;
    private String mReferrer;
    private EventListener mTimerEventListener;
    private CharSequence mTitle;
    private EventListener mUserInfoEventListener;
    private Bundle openIntentExtras;
    private Bundle savedInstanceState;

    public MainMenuActivity() {
        super();
        this.SENDER_ID = "496252195507";
        this.mCurrentTag = -1;
        this.openIntentExtras = null;
        this.mCurrentFragment = null;
        this.mNetworkStateChangedEventListener = (EventListener)new MainMenuActivity.MainMenuActivity11(this);
        Log.v("", " MainMenuActivity  --");
    }

    static /* synthetic */ CharSequence access000(final MainMenuActivity mainMenuActivity) {
        return mainMenuActivity.mTitle;
    }

    static /* synthetic */ CharSequence access100(final MainMenuActivity mainMenuActivity) {
        return mainMenuActivity.mDrawerTitle;
    }

    static /* synthetic */ LeftPanel access200(final MainMenuActivity mainMenuActivity) {
        return mainMenuActivity.mLeftPanelView;
    }

    static /* synthetic */ void access300(final MainMenuActivity mainMenuActivity) {
        mainMenuActivity.getPushToken();
    }

    static /* synthetic */ CustomFragment access400(final MainMenuActivity mainMenuActivity) {
        return mainMenuActivity.mCurrentFragment;
    }

    static /* synthetic */ GoogleApiClient access500(final MainMenuActivity mainMenuActivity) {
        return mainMenuActivity.mGoogleApiClient;
    }

    static /* synthetic */ void access600(final MainMenuActivity mainMenuActivity, final DailyBonusObject dailyBonusObject, final boolean b) {
        mainMenuActivity.showComplete(dailyBonusObject, b);
    }

    static /* synthetic */ void access800(final MainMenuActivity mainMenuActivity, final String s) {
        mainMenuActivity.sendRegistrationIdToBackend(s);
    }

    static /* synthetic */ void access900(final MainMenuActivity mainMenuActivity, final Context context, final String s) {
        mainMenuActivity.storeRegistrationId(context, s);
    }

    private boolean checkPlayServices() {
        final int googlePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable((Context)this);
        if (googlePlayServicesAvailable != 0) {
            if (GooglePlayServicesUtil.isUserRecoverableError(googlePlayServicesAvailable)) {
                GooglePlayServicesUtil.getErrorDialog(googlePlayServicesAvailable, (Activity)this, 9000).show();
            }
            else {
                Log.i("", "This device is not supported.");
                this.finish();
            }
            return false;
        }
        return true;
    }

    private void checkStartIntent(final Intent intent) {
        if (intent != null) {
            this.openIntentExtras = intent.getExtras();
        }
    }

    private static int getAppVersion(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        }
        catch (PackageManager.NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not get package name: ");
            sb.append(ex);
            throw new RuntimeException(sb.toString());
        }
    }

    private SharedPreferences getGCMPreferences(final Context context) {
        return this.getSharedPreferences(MainMenuActivity.class.getSimpleName(), 0);
    }

    private MenuNews getMenuNews() {
        return this.mMenuNews;
    }

    private MenuRating getMenuRating() {
        return this.mMenuRating;
    }

    private void getPushToken() {
        Log.v("", " getPushToken getpush token");
        if (!this.checkPlayServices()) {
            Log.i("", "No valid Google Play Services APK found.");
            return;
        }
        Log.v("", "getPushToken checkPlayServices ");
        final String registrationId = this.getRegistrationId(this.getApplicationContext());
        final StringBuilder sb = new StringBuilder();
        sb.append("getPushToken getRegistrationId ");
        sb.append(registrationId);
        Log.v("", sb.toString());
        if (registrationId.isEmpty()) {
            this.registerInBackground();
            return;
        }
        this.sendRegistrationIdToBackend(registrationId);
    }

    private String getRegistrationId(final Context context) {
        final SharedPreferences gcmPreferences = this.getGCMPreferences(context);
        final String string = gcmPreferences.getString("registration_id", "");
        if (string.isEmpty()) {
            Log.i("", "Registration not found.");
            return "";
        }
        if (gcmPreferences.getInt("appVersion", Integer.MIN_VALUE) != getAppVersion(context)) {
            Log.i("", "App version changed.");
            return "";
        }
        return string;
    }

    public static boolean hasPermissions() {
        return hasPermissions(null);
    }

    public static boolean hasPermissions(final Context context) {
        boolean hasPermissions = true;
        if (context == null) {
            return MainMenuActivity.hasPermissions;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(context, "android.permission.READ_PHONE_STATE") != 0 || ContextCompat.checkSelfPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                hasPermissions = false;
            }
            return MainMenuActivity.hasPermissions = hasPermissions;
        }
        return MainMenuActivity.hasPermissions = true;
    }

    public static void openLockedTaskactivity(final Activity activity, final Skanword skanword) {
        if (MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION) != null && InAppManager.getInstance().getPriceForProductId(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopDataManager.ShopProductType.SUBSCRIPTION).getId()).equals("")) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)activity,  R.style.Scanword_style_dialog);
            String message;
            if (MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordOffered(Integer.valueOf(skanword.getId()))) {
                message = "Активируйте премиум-доступ или откройте этот сканворд за просмотр видео (нужен Интернет).";
            }
            else {
                message = "Активируйте премиум-доступ в магазине, чтобы играть во все закрытые сканворды.";
            }
            alertDialogBuilder.setMessage((CharSequence)message).setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainMenuActivity.MainMenuActivity15()).setTitle("Сканворд закрыт");
            alertDialogBuilder.create().show();
            return;
        }
        final Intent intent = new Intent("android.intent.action.EDIT", (Uri)null, (Context)activity, (Class)LockedTaskActivity.class);
        final StringBuilder sb = new StringBuilder();
        sb.append("c");
        sb.append(skanword.getId());
        intent.putExtra("EXTRA_TASK_OFFER_TARGET", sb.toString());
        intent.putExtra("EXTRA_TASK_ID", skanword.getId());
        intent.putExtra("EXTRA_TASK_WITH_OFFER", MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordOffered(Integer.valueOf(skanword.getId())));
        activity.startActivityForResult(intent, 105);
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_lockedscanwin_enter");
        final SmappsScanwords.ActionType user_ACTION = SmappsScanwords.ActionType.USER_ACTION;
        String s;
        if (MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordOffered(Integer.valueOf(skanword.getId()))) {
            s = "click_menu_scans_offeredscan_locked";
        }
        else {
            s = "click_menu_scans_lockedscan";
        }
        SmappsScanwords.sendGAEvent(user_ACTION, s);
    }

    private void registerInBackground() {
        SmappsScanwords.getExecutorService().execute((Runnable)new MainMenuActivity.MainMenuActivity12(this));
    }

    private void sendRegistrationIdToBackend(final String s) {
        SmappsScanwords.getExecutorService().execute((Runnable)new MainMenuActivity.MainMenuActivity13(this, s));
    }

    private void setupGoogleApiClient() {
        this.mGoogleApiClient = new GoogleApiClient.Builder((Context)this).addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks)new MainMenuActivity.MainMenuActivity8(this)).addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener)new MainMenuActivity.MainMenuActivity7(this)).addApi(Games.API).addScope(Games.SCOPE_GAMES).addScope(Drive.SCOPE_APPFOLDER).build();
    }

    private void showComplete(final DailyBonusObject dailyBonusObject, final boolean b) {
        if (dailyBonusObject != null) {
            if (this.mIsRunning) {
                MainMenuActivity.mBonusComplete = null;
                this.runOnUiThread((Runnable)new MainMenuActivity.MainMenuActivity10(this, dailyBonusObject, b));
                return;
            }
            MainMenuActivity.mBonusComplete = new MainMenuActivity.BonusCompleteData(dailyBonusObject, b);
        }
    }

    private boolean showPermissionsWindow() {
        final ArrayList<String> list = new ArrayList<String>(2);
        if (ContextCompat.checkSelfPermission((Context)this, "android.permission.READ_PHONE_STATE") != 0) {
            list.add("android.permission.READ_PHONE_STATE");
        }
        if (ContextCompat.checkSelfPermission((Context)this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            list.add("android.permission.WRITE_EXTERNAL_STORAGE");
        }
        if (!list.isEmpty()) {
            ActivityCompat.requestPermissions((Activity)this, (String[])list.toArray(new String[0]), 1);
        }
        return list.isEmpty() ^ true;
    }

    private void startDataApp(final Bundle bundle) {
        SmappsScanwords.instance.startDataApp();
        final Uri data = this.getIntent().getData();
        final StringBuilder sb = new StringBuilder();
        sb.append("uri-some  ");
        sb.append(data);
        Log.v("", sb.toString());
        Appirater.appLaunched((Context) this, new Appirater.RatingButtonListener() {
            @Override
            public void onRate() {
                Log.v("", "Appirater.RatingButtonListener onRate");
            }

            @Override
            public void onLater() {
                Log.v("", "Appirater.RatingButtonListener onLater");
            }

            @Override
            public void onDeclined() {
                Log.v("", "Appirater.RatingButtonListener onDeclined");
            }
        });
        this.setContentView(R.layout.main_menu_activity_layout);
        SmappsScanwords.getAppSharedPreferencesEditor("GENERAL_PREFERENCES_NAME").putBoolean("first_enter", false).commit();
        this.checkStartIntent(this.getIntent());
        this.mDrawerLayout = (DrawerLayout)this.findViewById(R.id.drawer_layout);
        (this.mLeftPanelView = (LeftPanel)this.findViewById(R.id.left_drawer)).setController(this);
        final CharSequence title = this.getTitle();
        this.mDrawerTitle = title;
        this.mTitle = title;
        this.mMenuScanwords = new MenuSkanwords();
        this.mMenuShop = new MenuShop();
        this.mMenuRating = new MenuRating();
        this.mMenuNews = new MenuNews();
        MainMenuActivity1 mma1 = new MainMenuActivity.MainMenuActivity1((Activity)this, this.mDrawerLayout,  R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close);
        mma1.mma = this;
        this.mDrawerToggle = (ActionBarDrawerToggle) mma1;
        this.mDrawerLayout.setDrawerListener((DrawerLayout.DrawerListener)this.mDrawerToggle);
        UserUpdatesManager.getInstance();
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getActionBar().setHomeButtonEnabled(true);
        if (bundle != null && bundle.containsKey("curMenu")) {
            this.openMenu(bundle.getInt("curMenu"));
        }
        else {
            this.openMenu(0);
        }
        this.mUserInfoEventListener = (EventListener)new MainMenuActivity.MainMenuActivity2(this);
        this.mPushTokenEventListener = (EventListener)new MainMenuActivity.MainMenuActivity3(this);
        this.mNotificationsEventListener = (EventListener)new MainMenuActivity.MainMenuActivity4(this);
        this.mTimerEventListener = (EventListener)new MainMenuActivity.MainMenuActivity5(this);
        SmappsScanwords.getEventsDispatcher().addListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        if (this.mNotificationsEventListener != null) {
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_NOTIFICATIONS_UPDATE", this.mNotificationsEventListener);
        }
        if (this.mNetworkStateChangedEventListener != null) {
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_NETWORK_STATE_CHANGED_ALL", this.mNetworkStateChangedEventListener);
        }
        if (this.mPushTokenEventListener != null) {
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_NO_TOKEN", this.mPushTokenEventListener);
        }
        if (this.mUserInfoEventListener != null) {
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_USER_INFO_UPDATE", this.mUserInfoEventListener);
        }
        MainNetworkManager.getInstance().requestConfigExecWithCompleteBlock((MainNetworkManager.RequestCompletionBlock)new MainMenuActivity.MainMenuActivity6(this));

        Context c = getApplicationContext();

//        AdsManager.getInstance().init((Activity)MainMenuActivity.getInstance());
        this.setupGoogleApiClient();
    }

    private void storeRegistrationId(final Context context, final String s) {
        final SharedPreferences gcmPreferences = this.getGCMPreferences(context);
        final int appVersion = getAppVersion(context);
        final StringBuilder sb = new StringBuilder();
        sb.append("Saving regId on app version ");
        sb.append(appVersion);
        Log.i("", sb.toString());
        final SharedPreferences.Editor edit = gcmPreferences.edit();
        edit.putString("registration_id", s);
        edit.putInt("appVersion", appVersion);
        edit.commit();
    }

    public void clearOpenIntentExtras() {
        this.openIntentExtras = null;
    }

    public MenuSkanwords getMenuScanwords() {
        return this.mMenuScanwords;
    }

    public MenuShop getMenuShop() {
        return this.mMenuShop;
    }

    public Bundle getOpenIntentExtras() {
        return this.openIntentExtras;
    }

    public void navigateToAppPage(MainMenuActivity.AppPage appPage) {
        final int n = MainMenuActivity.MainMenuActivity16.swith[appPage.ordinal()];
        if (n == 1) {
            this.mLeftPanelView.selectItem(1);
            return;
        }
        if (n == 2) {
            this.mLeftPanelView.selectItem(1);
            return;
        }
        if (n != 3) {
            return;
        }
        this.mLeftPanelView.selectItem(0);
    }

    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        final StringBuilder sb = new StringBuilder();
        sb.append(n2);
        sb.append("  onActivityResult   ");
        sb.append(n);
        Log.v("", sb.toString());
        if (n != 99) {
            if (n == 101) {
                if (MainMenuActivity.mBonusComplete == null) {
                    AdsManager.getInstance().showInterstitial((Activity)this);
                }
            }
            else if (n == 105 && n2 == 100) {
                this.navigateToAppPage(MainMenuActivity.AppPage.PAGE_SUBSCRIPTION);
            }
            if (n2 == 556) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this, R.style.Scanword_style_dialog);
                alertDialogBuilder.setMessage("Загруженные файлы для этого выпуска повреждены. Пожалуйста, загрузите выпуск снава.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainMenuActivity.MainMenuActivity14(this)).setTitle("Ошибка");
                alertDialogBuilder.create().show();
            }
            InAppManager.getInstance().onActivityResult(n, n2, intent);
            MainNetworkManager.getInstance().onActivityResult((Activity)this, n, n2, intent);
            return;
        }
        if (n2 == 1) {
            MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB, (Activity)this, true);
            return;
        }
        if (n2 == 2) {
            MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK, (Activity)this, true);
            return;
        }
        if (n2 == 3) {
            MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK, (Activity)this, true);
        }
    }

    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.mDrawerToggle.onConfigurationChanged(configuration);
    }

    @SuppressLint({ "NewApi" })
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainMenuActivity.hasPermissions) {
            this.startDataApp(savedInstanceState);
            return;
        }
        if (Build.VERSION.SDK_INT < 23) {
            MainMenuActivity.hasPermissions = true;
            this.startDataApp(savedInstanceState);
            return;
        }
        if (this.showPermissionsWindow()) {
            this.savedInstanceState = savedInstanceState;
            this.setContentView(R.layout.activity_no_permissions);
            return;
        }
        MainMenuActivity.hasPermissions = true;
        this.startDataApp(savedInstanceState);
    }

    public boolean onCreateOptionsMenu(final Menu menu) {
        final int mCurrentTag = this.mCurrentTag;
        if (mCurrentTag != 0) {
            if (mCurrentTag != 1) {
                if (mCurrentTag != 2) {
                    if (mCurrentTag == 3) {
                        menu.add(0, 5, 0, "Обновить").setIcon(R.drawable.update).setShowAsAction(2);
                    }
                }
                else {
                    menu.add(0, 8, 0, "Найти меня").setIcon(R.drawable.icon_find_me).setShowAsAction(2);
                    menu.add(0, 5, 0, "Обновить").setIcon(R.drawable.update).setShowAsAction(2);
                }
            }
            else {
                menu.add(0, 7, 0, "ВОССТАНОВИТЬ").setShowAsAction(6);
            }
        }
        else {
            menu.add(0, 6, 0, "Сортировка").setIcon(R.drawable.filter).setShowAsAction(2);
            menu.add(0, 5, 0, "Обновить").setIcon(R.drawable.update).setShowAsAction(2);
        }
        Log.v("", "onCreateOptionsMenu");
        return true;
    }

    public static MainMenuActivity getInstance() {
        if (instance == null) {
            instance = new MainMenuActivity();
        }
        return instance;
    }

    protected void onDestroy() {
        Log.v("", "onDestroy  main activity ");
        UserUpdatesManager.getInstance().saveCurrentData();
        SmappsScanwords.getEventsDispatcher().removeListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        if (this.mUserInfoEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_USER_INFO_UPDATE", this.mUserInfoEventListener);
        }
        if (this.mNotificationsEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_NOTIFICATIONS_UPDATE", this.mNotificationsEventListener);
        }
        if (this.mPushTokenEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_NO_TOKEN", this.mPushTokenEventListener);
        }
        if (this.mNetworkStateChangedEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_NETWORK_STATE_CHANGED_ALL", this.mNetworkStateChangedEventListener);
        }
        super.onDestroy();
    }

    protected void onNewIntent(final Intent intent) {
        this.checkStartIntent(intent);
        super.onNewIntent(intent);
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final StringBuilder sb = new StringBuilder();
        sb.append("menu item ");
        sb.append(menuItem.getItemId());
        Log.v("  ", sb.toString());
        if (this.mDrawerToggle.onOptionsItemSelected(menuItem)) {
            return true;
        }
        if (menuItem.getItemId() == 5) {
            this.mCurrentFragment.updateFragment(true);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_update");
        }
        else if (menuItem.getItemId() == 6) {
            Log.v("", "open sort dialog  MainMenuActivity");
            this.getMenuScanwords().openSortDialog();
        }
        else if (menuItem.getItemId() == 7) {
            if (MainNetworkManager.getInstance().hasNetworkConnection((Activity)this)) {
                InAppManager.getInstance().restorePurchases((Activity)MainMenuActivity.instance);
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_shop_restorepurchases");
            }
        }
        else if (menuItem.getItemId() == 8) {
            this.getMenuRating().findUser();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    protected void onPause() {
        super.onPause();
        this.mIsRunning = false;
    }

    protected void onPostCreate(final Bundle bundle) {
        super.onPostCreate(bundle);
        if (MainMenuActivity.hasPermissions) {
            this.mDrawerToggle.syncState();
        }
    }

    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        super.onRequestPermissionsResult(n, array, array2);
        final StringBuilder sb = new StringBuilder();
        sb.append("Permission: ");
        sb.append(Arrays.toString(array));
        sb.append("was ");
        sb.append(Arrays.toString(array2));
        Log.v("", sb.toString());
        if (hasPermissions((Context)this)) {
            MainMenuActivity.hasPermissions = true;
            SmappsScanwords.makeDirs();
            this.startDataApp(this.savedInstanceState);
            this.savedInstanceState = null;
            return;
        }
        this.showPermissionsWindow();
    }

    protected void onResume() {
        InAppManager.getInstance().initStoreManager((Activity)MainMenuActivity.instance);
        AdsManager.getInstance().activityResumed((Activity)this);
        super.onResume();
        this.mIsRunning = true;
        final MainMenuActivity.BonusCompleteData mBonusComplete = MainMenuActivity.mBonusComplete;
        if (mBonusComplete != null) {
            this.showComplete(mBonusComplete.getBonus(), MainMenuActivity.mBonusComplete.isDoubled());
        }
    }

    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("curMenu", this.mCurrentTag);
    }

    protected void onStart() {
        super.onStart();
        if (MainMenuActivity.hasPermissions) {
            this.updateNotifications(false);
            this.getIntent();
            this.mReferrer = CustomReferralReceiver.getReferrer(this.getApplicationContext());
            final StringBuilder sb = new StringBuilder();
            sb.append(" mReferrer ");
            sb.append(this.mReferrer);
            Log.v("", sb.toString());
        }
    }

    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
    }

    public void openMenu(final int mCurrentTag) {
        final StringBuilder sb = new StringBuilder();
        sb.append("open menu with tag ");
        sb.append(mCurrentTag);
        final String string = sb.toString();
        String s = "";
        Log.v("", string);
        if (mCurrentTag == this.mCurrentTag) {
            this.mCurrentFragment.fragmentOpened();
            this.mDrawerLayout.closeDrawer((View)this.mLeftPanelView);
            return;
        }
        this.mDrawerLayout.closeDrawer((View)this.mLeftPanelView);
        Object mCurrentFragment = null;
        if ((this.mCurrentTag = mCurrentTag) != 0) {
            if (mCurrentTag != 1) {
                if (mCurrentTag != 2) {
                    if (mCurrentTag == 3) {
                        mCurrentFragment = this.getMenuNews();
                        MainDataManager.getInstance().getNewsDataManager().updateReadedNews();
                        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_news_enter");
                        s = "Сканворды";
                    }
                }
                else {
                    mCurrentFragment = this.getMenuRating();
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_rating_enter");
                    s = "Рейтинг";
                }
            }
            else {
                mCurrentFragment = this.getMenuShop();
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_shop_enter");
                s = "Магазин";
            }
        }
        else {
            mCurrentFragment = this.getMenuScanwords();
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_enter");
            s = "Новости";
        }
        this.mDrawerTitle = s;
        this.setTitle(this.mTitle = s);
        if (mCurrentFragment != null) {
            if (((CustomFragment)mCurrentFragment).isAdded()) {
                return;
            }
            ((CustomFragment)mCurrentFragment).setArguments(new Bundle());
            (this.mCurrentFragment = (CustomFragment)mCurrentFragment).fragmentOpened();
            this.getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, (Fragment)this.mCurrentFragment).commitAllowingStateLoss();
            this.invalidateOptionsMenu();
        }
    }

    public void showBonusPla() {
        if (MainNetworkManager.getInstance().hasNetworkConnection((Activity)this) && !HintsBonusDialog.isShowing()) {
            if (MainDataManager.getInstance().getDailyBonus().getScratchedPosition() > 0) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("scratched position ");
            sb.append(MainDataManager.getInstance().getDailyBonus().getScratchedPosition());
            Log.v("SkanwordsFunc", sb.toString());
            HintsBonusDialog.showDialogInContext((Context)this, (HintsBonusDialog.HintsBonusDialogDelegate)new MainMenuActivity.MainMenuActivity9(this), MainDataManager.getInstance().getDailyBonus());
        }
    }

    public void updateNotifications(final boolean b) {
        LocalNotificationsManager.createLocalNotifications(this.getApplicationContext(), b);
    }

    class BonusCompleteData {
        private final DailyBonusObject mBonus;
        private final boolean mDoubled;

        BonusCompleteData(DailyBonusObject var1, boolean var2) {
            this.mBonus = var1;
            this.mDoubled = var2;
        }

        public DailyBonusObject getBonus() {
            return this.mBonus;
        }

        public boolean isDoubled() {
            return this.mDoubled;
        }
    }

    class MainMenuActivity11 implements EventListener {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity11(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            Log.v("SkanwordsFunc", " mNetworkStateChangedEventListener event");
            if (MainMenuActivity.access400(this.this0) != null) {
                this.this0.runOnUiThread((Runnable)new MainMenuActivity11.MainMenuActivity111(this));
            }
        }

        class MainMenuActivity111 implements Runnable {
            final /* synthetic */ MainMenuActivity11 this1;

            MainMenuActivity111(final MainMenuActivity11 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                MainMenuActivity.access400(this.this1.this0).updateFragment();
            }
        }
    }

    static final class MainMenuActivity15 implements DialogInterface.OnClickListener {
        MainMenuActivity15() {
            super();
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    class MainMenuActivity12 implements Runnable {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity12(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            Log.v("", "registerInBackground ");
            Label_0033: {
                GoogleCloudMessaging googleCloudMessaging;
                if ((googleCloudMessaging = GoogleCloudMessaging.getInstance((Context)this.this0)) != null) {
                    break Label_0033;
                }
                try {
                    googleCloudMessaging = GoogleCloudMessaging.getInstance(this.this0.getApplicationContext());
                    final String register = googleCloudMessaging.register(new String[] { this.this0.SENDER_ID });
                    final StringBuilder sb = new StringBuilder();
                    sb.append("registerInBackground ");
                    sb.append(register);
                    Log.v("", sb.toString());
                    MainMenuActivity.access800(this.this0, register);
                    MainMenuActivity.access900(this.this0, this.this0.getApplicationContext(), register);
                }
                catch (IOException ex) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("registerInBackground IOException");
                    sb2.append(ex.getMessage());
                    Log.v("", sb2.toString());
                }
            }
        }
    }

    class MainMenuActivity13 implements Runnable {
        final /* synthetic */ MainMenuActivity this0;
        final /* synthetic */ String valregid;

        MainMenuActivity13(final MainMenuActivity this0, final String valregid) {
            super();
            this.this0 = this0;
            this.valregid = valregid;
        }

        @Override
        public void run() {
            MainNetworkManager.getInstance().sendPushToken(this.valregid);
        }
    }

    class MainMenuActivity8 implements GoogleApiClient.ConnectionCallbacks {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity8(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onConnected(final Bundle bundle) {
            Log.v("SkanwordsFunc", "onConnected");
        }

        public void onConnectionSuspended(final int n) {
            MainMenuActivity.access500(this.this0).connect();
            Log.v("SkanwordsFunc", "onConnectionSuspended");
        }
    }

    class MainMenuActivity10 implements Runnable {
        final /* synthetic */ MainMenuActivity this0;
        final /* synthetic */ DailyBonusObject valbonus;
        final /* synthetic */ boolean valdoubled;

        MainMenuActivity10(final MainMenuActivity this0, final DailyBonusObject valbonus, final boolean valdoubled) {
            super();
            this.this0 = this0;
            this.valbonus = valbonus;
            this.valdoubled = valdoubled;
        }

        @Override
        public void run() {
            HintsBonusDialog.showCompleteBonusDialogInContext((Context)this.this0, this.valbonus, this.valdoubled);
        }
    }


    class MainMenuActivity7 implements GoogleApiClient.OnConnectionFailedListener {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity7(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onConnectionFailed(final ConnectionResult connectionResult) {
            try {
                connectionResult.startResolutionForResult((Activity)this.this0, 200);
            }
            catch (IntentSender.SendIntentException ex) {
                ex.printStackTrace();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("onConnectionFailed ");
            sb.append(connectionResult.getErrorCode());
            Log.v("SkanwordsFunc", sb.toString());
        }
    }

    class MainMenuActivity1 extends ActionBarDrawerToggle {
        public  /* synthetic */ MainMenuActivity mma;

        MainMenuActivity1(
                Activity activity,
                DrawerLayout drawerLayout,
                int n,
                int n2,
                int n3) {
            super(activity, drawerLayout, n, n2, n3);
        }

        public void onDrawerClosed(final View view) {
            super.onDrawerClosed(view);
            this.mma.getActionBar().setTitle(MainMenuActivity.access000(this.mma));
        }

        public void onDrawerOpened(final View view) {
            super.onDrawerOpened(view);
            this.mma.getActionBar().setTitle(MainMenuActivity.access100(this.mma));
        }
    }

    class MainMenuActivity2 implements EventListener {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity2(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new MainMenuActivity2.MainMenuActivity21(this));
        }

        class MainMenuActivity21 implements Runnable {
            final /* synthetic */ MainMenuActivity2 this1;

            MainMenuActivity21(final MainMenuActivity2 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                MainMenuActivity.access200(this.this1.this0).updateUserInfo();
            }
        }
    }

    class MainMenuActivity4 implements EventListener {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity4(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new MainMenuActivity4.MainMenuActivity41(this));
        }

        class MainMenuActivity41 implements Runnable {
            final /* synthetic */ MainMenuActivity4 this1;

            MainMenuActivity41(final MainMenuActivity4 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                this.this1.this0.updateNotifications(true);
            }
        }
    }

    class MainMenuActivity3 implements EventListener {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity3(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new MainMenuActivity3.MainMenuActivity31(this));
        }

        class MainMenuActivity31 implements Runnable {
            final /* synthetic */ MainMenuActivity3 this1;

            MainMenuActivity31(final MainMenuActivity3 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                MainMenuActivity.access300(this.this1.this0);
            }
        }
    }

    class MainMenuActivity5 implements EventListener {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity5(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new MainMenuActivity5.MainMenuActivity51(this));
        }

        class MainMenuActivity51 implements Runnable {
            final /* synthetic */ MainMenuActivity5 this1;

            MainMenuActivity51(final MainMenuActivity5 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                if (MainDataManager.getInstance().getDailyBonus() != null && MainDataManager.getInstance().getDailyBonus().getDownloadedTime() - new Date().getTime() + MainDataManager.getInstance().getDailyBonus().getTimeLeft() * 1000 < 0L && MainNetworkManager.getInstance().hasNetworkConnection()) {
                    this.this1.this0.showBonusPla();
                }
            }
        }
    }


    class MainMenuActivity6 implements MainNetworkManager.RequestCompletionBlock {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity6(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void complete(final boolean b) {
            MainNetworkManager.getInstance().requestSync((MainNetworkManager.RequestCompletionBlock)new MainMenuActivity6.MainMenuActivity61(this));
        }

        class MainMenuActivity61 implements MainNetworkManager.RequestCompletionBlock {
            final /* synthetic */ MainMenuActivity6 this1;

            MainMenuActivity61(final MainMenuActivity6 this1) {
                super();
                this.this1 = this1;
            }

            public void complete(final boolean b) {
                if (MainMenuActivity.access400(this.this1.this0) != null) {
                    this.this1.this0.runOnUiThread((Runnable)new MainMenuActivity61.MainMenuActivity611(this));
                }
            }

            class MainMenuActivity611 implements Runnable {
                final /* synthetic */ MainMenuActivity61 this2;

                MainMenuActivity611(final MainMenuActivity61 this2) {
                    super();
                    this.this2 = this2;
                }

                @Override
                public void run() {
                    MainMenuActivity.access400(this.this2.this1.this0).updateFragment();
                }
            }
        }
    }

    public enum AppPage {
        PAGE_HINTS,
        PAGE_SKANWORDS,
        PAGE_SUBSCRIPTION;
    }

    static class MainMenuActivity16 {
        static final /* synthetic */ int[] swith;

        static {
            swith = new int[MainMenuActivity.AppPage.values().length];
            try {
                MainMenuActivity16.swith[MainMenuActivity.AppPage.PAGE_SUBSCRIPTION.ordinal()] = 1;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                MainMenuActivity16.swith[MainMenuActivity.AppPage.PAGE_HINTS.ordinal()] = 2;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                MainMenuActivity16.swith[MainMenuActivity.AppPage.PAGE_SKANWORDS.ordinal()] = 3;
            }
            catch (NoSuchFieldError noSuchFieldError) {
            }
        }
    }

    class MainMenuActivity14 implements DialogInterface.OnClickListener {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity14(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    class MainMenuActivity9 implements HintsBonusDialog.HintsBonusDialogDelegate {
        final /* synthetic */ MainMenuActivity this0;

        MainMenuActivity9(final MainMenuActivity this0) {
            super();
            this.this0 = this0;
        }

          /* synthetic */ void access700(final MainMenuActivity9 mainMenuActivity9, final boolean b, final boolean b2) {
            mainMenuActivity9.showCompl(b, b2);
        }

        private void showCompl(final boolean b, final boolean b2) {
            MainDataManager.getInstance().getDailyBonus().setScratchedPosition(0);
            final DailyBonusObject dailyBonus = MainDataManager.getInstance().getDailyBonus();
            if (dailyBonus == null) {
                return;
            }
            MainDataManager.getInstance().addUserHints(dailyBonus.getWin());
            UserUpdatesManager.getInstance().bonusRecieved(b, b2, true);
            MainDataManager.getInstance().rewriteDailyBonusObjectRecieved((DailyBonusObject)null);
            MainNetworkManager.getInstance().requestSyncExec();
            MainMenuActivity.access600(this.this0, dailyBonus, b);
        }

        public void onCancel() {
        }

        public void onDouble() {
            AdsManager.getInstance().showVideoOffer((Activity)this.this0, (AdsManager.AdsEventsListener)new MainMenuActivity9.MainMenuActivity91(this), "hh", false);
        }

        public void onTake(final boolean b) {
            this.showCompl(false, b);
        }

        class MainMenuActivity91 extends AdsManager.AdsEventsListener {
            final /* synthetic */ MainMenuActivity9 this1;

            MainMenuActivity91(final MainMenuActivity9 this1) {
                super();
                this.this1 = this1;
            }

            public void onComplete() {
                super.onComplete();
                Log.v("SkanwordsAds", "onComplete listener");
                MainDataManager.getInstance().getDailyBonus().setScratchedPosition(0);
                this1.access700(this.this1, true, true);
            }

            public void onNoAds() {
                Log.v("SkanwordsAds", "onNoAds listener");
                MainDataManager.getInstance().getDailyBonus().setScratchedPosition(0);
            }

            public void onSkipped() {
                super.onSkipped();
                MainDataManager.getInstance().getDailyBonus().setScratchedPosition(0);
                Log.v("SkanwordsAds", "videoSkipped listener");
                this1.access700(this.this1, false, true);
            }
        }
    }
}
