package org.skanword.and.menu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import java.util.ArrayList;
import java.util.List;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.optionsmenu.OptionsActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager;

public class LeftPanel extends LinearLayout
{
    private PanelListAdapter mAdapter;
    private MainMenuActivity mController;
    private View mMainView;
    private ListView mOptionsList;

    public LeftPanel(final Context context) {
        super(context);
        this.initView();
    }

    public LeftPanel(final Context context, final AttributeSet set) {
        super(context, set);
        this.initView();
    }

    @SuppressLint({ "NewApi" })
    public LeftPanel(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.initView();
    }

    static /* synthetic */ MainMenuActivity access000(final LeftPanel leftPanel) {
        return leftPanel.mController;
    }

    private void initView() {
        (this.mMainView = LayoutInflater.from(this.getContext().getApplicationContext()).inflate(R.layout.left_panel_view_layout, (ViewGroup)null)).setLayoutParams((ViewGroup.LayoutParams)new LinearLayout.LayoutParams(-1, -1));
        this.setClickable(true);
        this.mMainView.findViewById(R.id.panelPremiumButton).setOnClickListener((View.OnClickListener)new LeftPanel1(this));
        this.mMainView.findViewById(R.id.panelHintsButton).setOnClickListener((View.OnClickListener)new LeftPanel.LeftPanel2(this));
        final LeftPanel.LeftPanel3 leftPanel3 = new LeftPanel.LeftPanel3(this);
        ((ImageView)this.mMainView.findViewById(R.id.authOKButton)).setOnClickListener((View.OnClickListener)leftPanel3);
        ((ImageView)this.mMainView.findViewById(R.id.authFBButton)).setOnClickListener((View.OnClickListener)leftPanel3);
        ((ImageView)this.mMainView.findViewById(R.id.authVKButton)).setOnClickListener((View.OnClickListener)leftPanel3);
        ((ImageView)this.mMainView.findViewById(R.id.exitIcon)).setOnClickListener((View.OnClickListener)leftPanel3);
        this.addView(this.mMainView);
        final ArrayList<String> list = new ArrayList<String>();
        list.add("Сканворды");
        list.add("Магазин");
        list.add("Рейтинг");
        list.add("Новости");
        list.add("НАСТРОЙКИ");
        list.add("ОБРАТНАЯ СВЯЗЬ");
        this.mAdapter = new LeftPanel.PanelListAdapter(this.getContext(), R.layout.left_panel_item_layout, (List)list);
        (this.mOptionsList = (ListView)this.findViewById(R.id.options_list)).setAdapter((ListAdapter)this.mAdapter);
        this.mOptionsList.setOnItemClickListener((AdapterView.OnItemClickListener)new DrawerItemClickListener(this, (LeftPanel.LeftPanel1)null));
        this.updateUserInfo();
    }

    public MainMenuActivity getController() {
        return this.mController;
    }

    public void selectItem(final int selectedItem) {
        this.mAdapter.setSelectedItem(selectedItem);
        if (this.mController != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("selectItem ");
            sb.append(selectedItem);
            Log.v("", sb.toString());
            this.mController.openMenu(selectedItem);
        }
        this.mAdapter.notifyDataSetChanged();
    }

    public void setController(final MainMenuActivity mController) {
        this.mController = mController;
    }

    public void updateUserInfo() {
        if (MainDataManager.getInstance().getUserData() == null) {
            return;
        }
        final int hints = MainDataManager.getInstance().getUserData().getHints();
        final TextView textView = (TextView)this.mMainView.findViewById(R.id.panelHintsLabel);
        final TextView textView2 = (TextView)this.mMainView.findViewById(R.id.panelHintsCountLabel);
        textView.setText((CharSequence)Utils.spellVariantForNumber(hints, "ПОДСКАЗКА", "ПОДСКАЗКИ", "ПОДСКАЗОК"));
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(hints);
        textView2.setText((CharSequence)sb.toString());
        final View viewById = this.mMainView.findViewById(R.id.authorisePanel);
        final boolean userAuthorised = MainNetworkManager.getInstance().isUserAuthorised();
        final int n = 0;
        int visibility;
        if (!userAuthorised) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        viewById.setVisibility(visibility);
        final View viewById2 = this.mMainView.findViewById(R.id.userPanel);
        int visibility2;
        if (MainNetworkManager.getInstance().isUserAuthorised()) {
            visibility2 = n;
        }
        else {
            visibility2 = 8;
        }
        viewById2.setVisibility(visibility2);
        if (!MainNetworkManager.getInstance().isUserAuthorised()) {
            final Resources resources = this.getResources();
            final TextView textView3 = (TextView)this.mMainView.findViewById(R.id.guestNumberLabel);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(resources.getString(R.string.guest_label));
            sb2.append(MainNetworkManager.getInstance().getUserName());
            textView3.setText((CharSequence)sb2.toString());
            return;
        }
        final ImageView imageView = (ImageView)this.mMainView.findViewById(R.id.socialNetworkIcon);
        final int n2 = LeftPanel.LeftPanel4.socNet[MainNetworkManager.getInstance().getUserAuth().getSocialNetwork().ordinal()];
        if (n2 != 1) {
            if (n2 != 2) {
                if (n2 == 3) {
                    imageView.setImageResource(R.drawable.icon_mini_vk);
                }
            }
            else {
                imageView.setImageResource(R.drawable.icon_mini_ok);
            }
        }
        else {
            imageView.setImageResource(R.drawable.icon_mini_fb);
        }
        if (MainNetworkManager.getInstance().getUserAuth().getAvatarUrl() != null) {
            ImageLoader.getInstance().displayImage(MainNetworkManager.getInstance().getUserAuth().getAvatarUrl(), (ImageView)this.findViewById(R.id.profile_image), new DisplayImageOptions.Builder().cacheInMemory(true).showImageForEmptyUri(R.drawable.avatar).showImageOnFail(R.drawable.avatar).build());
        }
        else {
            ((ImageView)this.findViewById(R.id.profile_image)).setImageResource(R.drawable.avatar);
        }
        ((TextView)this.mMainView.findViewById(R.id.userNameLabel)).setText((CharSequence)MainNetworkManager.getInstance().getUserAuth().getFullName());
    }

    public static class PanelListAdapter extends ArrayAdapter<String>
    {
        private int mResourceId;
        private int selectedItem;

        public PanelListAdapter(final Context context, final int mResourceId, final List<String> list) {
            super(context, mResourceId, (List)list);
            this.mResourceId = mResourceId;
        }

        private View createRowView(final ViewGroup viewGroup, int mResourceId) {
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            if (mResourceId == 0) {
                mResourceId = this.mResourceId;
            }
            else {
                mResourceId = R.layout.left_panel_additional_item_layout;
            }
            return from.inflate(mResourceId, viewGroup, false);
        }

        private void rebuildView(View viewById, final String text, final int n) {
            final ImageView imageView = (ImageView)viewById.findViewById(R.id.menuIcon);
            final View viewById2 = viewById.findViewById(R.id.listDevider);
            int visibility = 8;
            int visibility2;
            if (n < 3) {
                visibility2 = 8;
            }
            else {
                visibility2 = 0;
            }
            viewById2.setVisibility(visibility2);
            if (n != 0) {
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            if (n != 4) {
                                if (n == 5) {
                                    imageView.setBackgroundResource(R.drawable.icon_menu_mail);
                                }
                            }
                            else {
                                imageView.setBackgroundResource(R.drawable.icon_menu_setting);
                            }
                        }
                        else {
                            imageView.setBackgroundResource(R.drawable.icon_menu_news);
                        }
                    }
                    else {
                        imageView.setBackgroundResource(R.drawable.icon_menu_rating);
                    }
                }
                else {
                    imageView.setBackgroundResource(R.drawable.icon_menu_shop);
                }
            }
            else {
                imageView.setBackgroundResource(R.drawable.icon_scanwords_issue);
            }
            final TextView textView = (TextView)viewById.findViewById(R.id.menuName);
            textView.setText((CharSequence)text);
            int notReadNewsCount;
            if (n == 3) {
                notReadNewsCount = MainDataManager.getInstance().getNewsDataManager().getNotReadNewsCount();
            }
            else {
                notReadNewsCount = 0;
            }
            final TextView textView2 = (TextView)viewById.findViewById(R.id.markerCount);
            viewById = viewById.findViewById(R.id.markerView);
            if (viewById != null) {
                if (notReadNewsCount > 0) {
                    visibility = 0;
                }
                viewById.setVisibility(visibility);
            }
            if (textView2 != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(notReadNewsCount);
                textView2.setText((CharSequence)sb.toString());
            }
            if (n == this.selectedItem) {
                textView.setTypeface((Typeface)null, Typeface.BOLD);
                return;
            }
            textView.setTypeface((Typeface)null, Typeface.NORMAL);
        }

        public int getItemViewType(final int n) {
            if (n > 3) {
                return 1;
            }
            return 0;
        }

        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View rowView = view;
            if (view == null) {
                rowView = this.createRowView(viewGroup, this.getItemViewType(n));
            }
            this.rebuildView(rowView, (String)this.getItem(n), n);
            return rowView;
        }

        public int getViewTypeCount() {
            return 2;
        }

        public void setSelectedItem(final int selectedItem) {
            this.selectedItem = selectedItem;
        }
    }


    class LeftPanel1 implements View.OnClickListener {
        final /* synthetic */ LeftPanel this0;

        LeftPanel1(final LeftPanel this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            LeftPanel.access000(this.this0).navigateToAppPage(MainMenuActivity.AppPage.PAGE_SUBSCRIPTION);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_leftpanel_subscription");
        }
    }

    class LeftPanel2 implements View.OnClickListener {
        final /* synthetic */ LeftPanel this0;

        LeftPanel2(final LeftPanel this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            LeftPanel.access000(this.this0).navigateToAppPage(MainMenuActivity.AppPage.PAGE_HINTS);
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_leftpanel_hints");
        }
    }

    class LeftPanel3 implements View.OnClickListener {
        final /* synthetic */ LeftPanel this0;

        LeftPanel3(final LeftPanel this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            if (view.getId() == R.id.authFBButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB, (Activity)LeftPanel.access000(this.this0), true);
                return;
            }
            if (view.getId() == R.id.authOKButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK, (Activity)LeftPanel.access000(this.this0), true);
                return;
            }
            if (view.getId() == R.id.authVKButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK, (Activity)LeftPanel.access000(this.this0), true);
                return;
            }
            if (view.getId() == R.id.exitIcon) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_NO, (Activity)LeftPanel.access000(this.this0), true);
            }
        }
    }


    private class DrawerItemClickListener implements AdapterView.OnItemClickListener
    {
        final /* synthetic */ LeftPanel this0;

        private DrawerItemClickListener(final LeftPanel this0) {
            super();
            this.this0 = this0;
        }

        DrawerItemClickListener(final LeftPanel leftPanel, final LeftPanel1 leftPanel1) {
            this(leftPanel);
        }

        public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
            if (n == 5) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Сканворд дня (Android) [");
                String userId;
                if (MainNetworkManager.getInstance().getUserAuth() != null) {
                    userId = MainNetworkManager.getInstance().getUserAuth().getUserId();
                }
                else {
                    userId = "(Неизвестно)";
                }
                sb.append(userId);
                sb.append("|");
                sb.append(SmappsScanwords.getVendorId());
                sb.append("]");
                final String string = sb.toString();
                final Intent intent = new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", "support@socialgames.bz", (String)null));
                intent.putExtra("android.intent.extra.SUBJECT", string);
                this.this0.getController().startActivity(Intent.createChooser(intent, (CharSequence)"Send mail"));
                return;
            }
            if (n == 4) {
                this.this0.getController().startActivity(new Intent("android.intent.action.EDIT", (Uri)null, (Context)this.this0.getController(), (Class)OptionsActivity.class));
                return;
            }
            this.this0.selectItem(n);
        }
    }


    static class LeftPanel4 {
    static final /* synthetic */ int[] socNet;

    static {
        socNet = new int[SocialNetworkManager.SocialNetwork.values().length];
        try {
            LeftPanel4.socNet[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            LeftPanel4.socNet[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            LeftPanel4.socNet[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError) {
        }
    }
}


}

