package org.skanword.and.menu.newsmenu;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import org.skanword.and.datamanager.NewsDataManager;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import org.skanword.and.libs.qustom.QustomDialogBuilder;
import java.util.List;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.NewsDataManager.News;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.menu.MainMenuActivity.AppPage;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;

public class MenuNews extends CustomFragment implements ActionBar.TabListener
{
    private ListAdapter mAdapter;
    private ListView mList;
    private View mMainView;
    private EventListener mNewsUpdateEvent;

    public MenuNews() {
        super();
        Log.v("", "MenuShop constructor");
    }

    static /* synthetic */ ListView access000(final MenuNews menuNews) {
        return menuNews.mList;
    }

    static /* synthetic */ void access100(final MenuNews menuNews) {
        menuNews.setAdapter();
    }

    private void setAdapter() {
        if (MainDataManager.getInstance().getNewsDataManager().getNews() == null) {
            return;
        }
        this.mAdapter = (ListAdapter)new MenuNews.NewsItemsAdapter((Context)this.getActivity(), R.layout.news_row_item, (Activity)this.getActivity(), MainDataManager.getInstance().getNewsDataManager().getNews());
        this.mList.setAdapter(this.mAdapter);
    }

    public void fragmentOpened() {
        this.updateFragment();
        super.fragmentOpened();
    }

    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mMainView = layoutInflater.inflate(R.layout.activity_news, viewGroup, false);
        (this.mList = (ListView)this.mMainView.findViewById(R.id.news_list)).setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        this.mNewsUpdateEvent = (EventListener)new MenuNews.MenuNews1(this);
        if (this.mNewsUpdateEvent != null) {
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_NEWS_UPDATE", this.mNewsUpdateEvent);
            SmappsScanwords.getEventsDispatcher().addListener("EVENT_USER_INFO_UPDATE", this.mNewsUpdateEvent);
        }
        this.update();
        return this.mMainView;
    }

    public void onDestroy() {
        if (this.mNewsUpdateEvent != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_NEWS_UPDATE", this.mNewsUpdateEvent);
            SmappsScanwords.getEventsDispatcher().removeListener("EVENT_USER_INFO_UPDATE", this.mNewsUpdateEvent);
        }
        super.onDestroy();
    }

    public void onTabReselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
    }

    public void onTabSelected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
    }

    public void onTabUnselected(final ActionBar.Tab actionBarTab, final FragmentTransaction fragmentTransaction) {
    }

    public void update() {
        this.getActivity().runOnUiThread((Runnable)new MenuNews.MenuNews2(this));
    }

    public void updateFragment(final boolean b) {
        final MainNetworkManager instance = MainNetworkManager.getInstance();
        Object activity;
        if (b) {
            activity = this.getActivity();
        }
        else {
            activity = null;
        }
        if (instance.hasNetworkConnection((Activity)activity)) {
            MainNetworkManager.getInstance().requestInfoWithCompleteBlock(1, (Activity)this.getActivity(), (MainNetworkManager.RequestCompletionBlock)null);
        }
        super.updateFragment(b);
    }

    // ------------------- Classes

    public static class NewsItemsAdapter extends ArrayAdapter<NewsDataManager.News>
    {
        private int mResourceId;

        public NewsItemsAdapter(final Context context, final int mResourceId, final Activity activity, final List<NewsDataManager.News> list) {
            super(context, mResourceId, (List)list);
            this.mResourceId = mResourceId;
        }

        static void access200(final NewsItemsAdapter newsItemsAdapter, final NewsDataManager.News newsDataManagerNews) {
            newsItemsAdapter.doActionForNews(newsDataManagerNews);
        }

        private String actionToName(final String s) {
            if (s.equals("issues")) {
                return "Перейти к сканвордам";
            }
            if (s.equals("shop")) {
                return "Перейти в магазин";
            }
            if (s.equals("follow_link")) {
                return "Перейти";
            }
            if (s.equals("activate_premium")) {
                return "Активировать";
            }
            if (s.equals("authorise")) {
                return "Авторизоваться";
            }
            return "Клик";
        }

        private View createRowView(final ViewGroup viewGroup) {
            return LayoutInflater.from(this.getContext()).inflate(this.mResourceId, viewGroup, false);
        }

        private void doActionForNews(final NewsDataManager.News newsDataManagerNews) {
            if (newsDataManagerNews.getAction() == null) {
                return;
            }
            final SmappsScanwords.ActionType user_ACTION = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb = new StringBuilder();
            sb.append("click_news_action_");
            sb.append(newsDataManagerNews.getAction());
            SmappsScanwords.sendGAEvent(user_ACTION, sb.toString());
            if (newsDataManagerNews.getAction().equals("issues")) {
                if (MainMenuActivity.instance != null) {
                    MainMenuActivity.instance.navigateToAppPage(MainMenuActivity.AppPage.PAGE_SKANWORDS);
                }
            }
            else if (newsDataManagerNews.getAction().equals("shop")) {
                if (MainMenuActivity.instance != null) {
                    MainMenuActivity.instance.navigateToAppPage(MainMenuActivity.AppPage.PAGE_HINTS);
                }
            }
            else {
                if (newsDataManagerNews.getAction().equals("follow_link") && newsDataManagerNews.getLink() != null) {
                    MainMenuActivity.instance.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(newsDataManagerNews.getLink())));
                    return;
                }
                if (newsDataManagerNews.getAction().equals("activate_premium")) {
                    return;
                }
                if (newsDataManagerNews.getAction().equals("authorise")) {
                    this.showAuthDialog();
                }
            }
        }

        private void rebuildView(View viewById, final NewsDataManager.News newsDataMan) {
            ((TextView)viewById.findViewById(R.id.titleLabel)).setText((CharSequence)newsDataMan.getTitle());
            ((TextView)viewById.findViewById(R.id.dateLabel)).setText((CharSequence)newsDataMan.getDate());
            ((TextView)viewById.findViewById(R.id.newsText)).setText((CharSequence)newsDataMan.getText());
            boolean b2;
            final boolean b = b2 = (newsDataMan.getAction() != null && !newsDataMan.getAction().equals(""));
            Label_0141: {
                if (b) {
                    if (!newsDataMan.getAction().equals("authorise") || !MainNetworkManager.getInstance().isUserAuthorised()) {
                        b2 = b;
                        if (!newsDataMan.getAction().equals("activate_premium")) {
                            break Label_0141;
                        }
                        b2 = b;
                        if (!MainDataManager.getInstance().getUserData().isVip()) {
                            break Label_0141;
                        }
                    }
                    b2 = false;
                }
            }
            if (b2) {
                final Button button = (Button)viewById.findViewById(R.id.actionButton);
                button.setText((CharSequence)this.actionToName(newsDataMan.getAction()));
                button.setOnClickListener(new NewsItemsAdapter1(this, newsDataMan));
            }
            final ImageView imageView = (ImageView)viewById.findViewById(R.id.newsIcon);
            if (newsDataMan.getImage() != null && !newsDataMan.getImage().equals("")) {
                UrlImageViewHelper.setUrlDrawable(imageView, newsDataMan.getImage(), R.drawable.icon);
            }
            else {
                imageView.setImageResource(R.drawable.icon);
            }
            final View viewById2 = viewById.findViewById(R.id.devideLine);
            final int n = 8;
            int visibility;
            if (b2) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById2.setVisibility(visibility);
            viewById = viewById.findViewById(R.id.actionButton);
            int visibility2 = n;
            if (b2) {
                visibility2 = 0;
            }
            viewById.setVisibility(visibility2);
        }

        @SuppressLint({ "InflateParams" })
        private void showAuthDialog() {
            final View inflate = LayoutInflater.from((Context)MainMenuActivity.instance).inflate(R.layout.auth_dialog_layout, (ViewGroup)null);
            final QustomDialogBuilder setTitleColor = new QustomDialogBuilder((Context)MainMenuActivity.instance).setDividerColor("#44000000").setTitle("Авторизироваться через:").setTitleColor("#000000");
            setTitleColor.setPositiveButton("Отмена", (DialogInterface.OnClickListener)new NewsItemsAdapter2(this));
            setTitleColor.setCustomView(R.attr.corpusId, inflate.getContext());
            final MenuNews.NewsItemsAdapter3 onClickListener = new NewsItemsAdapter3(this, setTitleColor.show());
            ((Button)inflate.findViewById(R.id.okAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
            ((Button)inflate.findViewById(R.id.fbAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
            ((Button)inflate.findViewById(R.id.vkAuthButton)).setOnClickListener((View.OnClickListener)onClickListener);
        }

        public View getView(final int n, View rowView, final ViewGroup viewGroup) {
            if (rowView == null) {
                rowView = this.createRowView(viewGroup);
            }
            this.rebuildView(rowView, (NewsDataManager.News)this.getItem(n));
            return rowView;
        }
    }


    static class NewsItemsAdapter1 implements View.OnClickListener {
        final /* synthetic */ NewsItemsAdapter this0;
        final /* synthetic */ NewsDataManager.News valnews;

        NewsItemsAdapter1(final NewsItemsAdapter this0, final News valnews) {
            super();
            this.this0 = this0;
            this.valnews = valnews;
        }

        public void onClick(final View view) {
            NewsItemsAdapter.access200(this.this0, this.valnews);
        }
    }

    class MenuNews1 implements EventListener {
        final /* synthetic */ MenuNews this0;

        MenuNews1(final MenuNews this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.update();
        }
    }

    class MenuNews2 implements Runnable {
        final /* synthetic */ MenuNews this0;

        MenuNews2(final MenuNews this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (MenuNews.access000(this.this0) == null) {
                return;
            }
            MenuNews.access100(this.this0);
        }
    }

    static class NewsItemsAdapter2 implements DialogInterface.OnClickListener {
        final /* synthetic */ NewsItemsAdapter this0;

        NewsItemsAdapter2(final NewsItemsAdapter this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    static class NewsItemsAdapter3 implements View.OnClickListener {
        final /* synthetic */ NewsItemsAdapter this0;
        final /* synthetic */ AlertDialog valdialog;

        NewsItemsAdapter3(final NewsItemsAdapter this0, final AlertDialog valdialog) {
            super();
            this.this0 = this0;
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            if (view.getId() == R.id.fbAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB, (Activity)MainMenuActivity.instance, true);
            }
            else if (view.getId() == R.id.okAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK, (Activity)MainMenuActivity.instance, true);
            }
            else if (view.getId() == R.id.vkAuthButton) {
                MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK, (Activity)MainMenuActivity.instance, true);
            }
            this.valdialog.dismiss();
        }
    }
}