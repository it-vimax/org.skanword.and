package org.skanword.and.menu;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import java.util.List;

public abstract class ArraySectionedListAdapter<T> extends ArrayAdapter<T>
{
    public static final int INVALID_ROW = -1;
    protected static final int TYPE_HEADER = 0;
    public static final int TYPE_HEADER_VIEW = 0;
    protected static final int TYPE_ROW = 1;
    public static final int TYPE_ROW_VIEW = 1;
    private Context mContext;
    private int[] mItemsInSections;

    public ArraySectionedListAdapter(final Context mContext, final int n) {
        super(mContext, n);
        this.mItemsInSections = null;
        this.mContext = mContext;
    }

    public ArraySectionedListAdapter(final Context mContext, final int n, final int n2) {
        super(mContext, n, n2);
        this.mItemsInSections = null;
        this.mContext = mContext;
    }

    public ArraySectionedListAdapter(final Context mContext, final int n, final List<T> list) {
        super(mContext, n, (List)list);
        this.mItemsInSections = null;
        this.mContext = mContext;
    }

    protected abstract void bindRowViewInSection(final Context p0, final int p1, final int p2, final View p3);

    protected abstract void bindSectionView(final Context p0, final int p1, final View p2);

    public Context getContext() {
        return this.mContext;
    }

    public int getCount() {
        final int numberOfSections = this.getNumberOfSections();
        this.mItemsInSections = new int[numberOfSections];
        int n = numberOfSections + 0;
        for (int i = 0; i < numberOfSections; ++i) {
            final int numberOfRowsInSection = this.getNumberOfRowsInSection(i);
            this.mItemsInSections[i] = numberOfRowsInSection;
            n += numberOfRowsInSection;
        }
        return n;
    }

    public long getItemId(final int n) {
        return n;
    }

    public abstract Object getItemInSectionAndRow(final int p0, final int p1);

    public int getItemType(int n) {
        int n2 = n;
        n = 0;
        while (true) {
            final int[] mItemsInSections = this.mItemsInSections;
            if (n >= mItemsInSections.length) {
                return -1;
            }
            final int n3 = mItemsInSections[n];
            if (n2 == 0) {
                return 0;
            }
            if (n2 <= n3) {
                return 1;
            }
            n2 -= n3 + 1;
            ++n;
        }
    }

    public int getItemViewType(int n) {
        final int n2 = 0;
        int n3 = n;
        n = n2;
        while (true) {
            final int[] mItemsInSections = this.mItemsInSections;
            if (n >= mItemsInSections.length) {
                return -1;
            }
            final int n4 = mItemsInSections[n];
            if (n3 == 0) {
                return this.getViewTypeForSection(n);
            }
            if (n3 <= n4) {
                return this.getViewTypeForRowInSection(n, n3 - 1);
            }
            n3 -= n4 + 1;
            ++n;
        }
    }

    protected abstract int getNumberOfRowsInSection(final int p0);

    protected abstract int getNumberOfSections();

    protected int getNumberOfViewTypeForRows() {
        return 1;
    }

    protected int getNumberOfViewTypeForSections() {
        return 1;
    }

    public int getRowForPosition(int n) {
        final int n2 = 0;
        int n3 = n;
        n = n2;
        while (true) {
            final int[] mItemsInSections = this.mItemsInSections;
            if (n >= mItemsInSections.length) {
                return -1;
            }
            final int n4 = mItemsInSections[n];
            if (n3 <= n4) {
                return n3 - 1;
            }
            n3 -= n4 + 1;
            ++n;
        }
    }

    public int getSectionForPosition(int n) {
        final int n2 = 0;
        int n3 = n;
        n = n2;
        while (true) {
            final int[] mItemsInSections = this.mItemsInSections;
            if (n >= mItemsInSections.length) {
                return -1;
            }
            final int n4 = mItemsInSections[n];
            if (n3 == 0) {
                return n;
            }
            if (n3 <= n4) {
                return n;
            }
            n3 -= n4 + 1;
            ++n;
        }
    }

    public View getView(int n, final View view, final ViewGroup viewGroup) {
        final int n2 = 0;
        int n3 = n;
        n = n2;
        while (true) {
            final int[] mItemsInSections = this.mItemsInSections;
            if (n >= mItemsInSections.length) {
                return null;
            }
            final int n4 = mItemsInSections[n];
            if (n3 == 0) {
                View sectionView;
                if ((sectionView = view) == null) {
                    sectionView = this.newSectionView(this.mContext, n, viewGroup);
                }
                this.bindSectionView(this.mContext, n, sectionView);
                return sectionView;
            }
            if (n3 <= n4) {
                View rowViewInSection;
                if ((rowViewInSection = view) == null) {
                    rowViewInSection = this.newRowViewInSection(this.mContext, n, n3 - 1, viewGroup);
                }
                this.bindRowViewInSection(this.mContext, n, n3 - 1, rowViewInSection);
                return rowViewInSection;
            }
            n3 -= n4 + 1;
            ++n;
        }
    }

    public int getViewTypeCount() {
        return this.getNumberOfViewTypeForSections() + this.getNumberOfViewTypeForRows();
    }

    protected int getViewTypeForRowInSection(final int n, final int n2) {
        return 1;
    }

    protected int getViewTypeForSection(final int n) {
        return 0;
    }

    protected abstract View newRowViewInSection(final Context p0, final int p1, final int p2, final ViewGroup p3);

    protected abstract View newSectionView(final Context p0, final int p1, final ViewGroup p2);
}
