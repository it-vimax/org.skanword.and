package org.skanword.and.menu.skanwordsmenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
//import com.google.android.gms.identity.intents.AddressConstants.ErrorCodes;
import com.sbstrm.appirater.Appirater;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.Skanword.Keyword;
import org.skanword.and.datamanager.Skanword.Keyword.KeywordCell;
import org.skanword.and.datamanager.SkanwordsDataManager.SkanwordsSetsUpdateEvent;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.IAdsInterstitialActivity;
import org.skanword.and.etc.TopActivityManager.TopActivity;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.scanwordgame.SkanwordGameActivity;

public class SkanwordsTasksList extends Activity implements TopActivityManager.TopActivity, IAdsInterstitialActivity
{
    protected static final String SKANWORDS_COUNT_OVER_LIMIT = "SKANWORDS_COUNT_OVER_LIMIT";
    public static final int TYPE_ROW_DELETE_ISSUE_VIEW = 2;
    private static int sCurrentSortType;
    private SkanwordsTasksAdapter mAdapter;
    private boolean mCurrentSortTypeChanged;
    private Integer mIssueId;
    private SkanwordsDataManager.TaskListType mListType;
    private ListView mListView;
    private EventListener mProgressUpdateListener;
    private List<Skanword> mSkanwordsList;
    private EventListener mSkanwordsUpdateEventListener;
    private EventListener mTimerEventListener;
    private EventListener mTutorialStateListener;

    public SkanwordsTasksList() {
        super();
        this.mListView = null;
        this.mIssueId = 0;
    }

    static /* synthetic */ List access002(final SkanwordsTasksList list, final List mSkanwordsList) {
        return list.mSkanwordsList = (List<Skanword>)mSkanwordsList;
    }

    static /* synthetic */ Integer access100(final SkanwordsTasksList list) {
        return list.mIssueId;
    }

    static /* synthetic */ void access1000(final SkanwordsTasksList list, final Skanword skanword) {
        list.startSkanword(skanword);
    }

    static /* synthetic */ void access1100(final SkanwordsTasksList list, final Skanword skanword) {
        list.openLockedTask(skanword);
    }

    static /* synthetic */ SkanwordsDataManager.TaskListType access200(final SkanwordsTasksList list) {
        return list.mListType;
    }

    static /* synthetic */ SkanwordsTasksList.SkanwordsTasksAdapter access300(final SkanwordsTasksList list) {
        return list.mAdapter;
    }

    static /* synthetic */ boolean access400(final SkanwordsTasksList list) {
        return list.mCurrentSortTypeChanged;
    }

    static /* synthetic */ boolean access402(final SkanwordsTasksList list, final boolean mCurrentSortTypeChanged) {
        return list.mCurrentSortTypeChanged = mCurrentSortTypeChanged;
    }

    static /* synthetic */ void access500(final SkanwordsTasksList list) {
        list.changeSortType();
    }

    static /* synthetic */ int access600() {
        return SkanwordsTasksList.sCurrentSortType;
    }

    static /* synthetic */ int access602(final int sCurrentSortType) {
        return SkanwordsTasksList.sCurrentSortType = sCurrentSortType;
    }

    static /* synthetic */ ListView access700(final SkanwordsTasksList list) {
        return list.mListView;
    }

    static /* synthetic */ void access800(final SkanwordsTasksList list) {
        list.setAdapter();
    }

    private void changeSortType() {
        final StringBuilder sb = new StringBuilder();
        sb.append("changeSortType  ");
        sb.append(SkanwordsTasksList.sCurrentSortType);
        Log.v("", sb.toString());
        final List<Skanword> mSkanwordsList = this.mSkanwordsList;
        if (mSkanwordsList != null) {
            if (mSkanwordsList.size() == 0) {
                return;
            }
            Collections.sort(this.mSkanwordsList, (Comparator<? super Skanword>)new SkanwordsTasksList8(this));
            if (this.mAdapter != null) {
                this.runOnUiThread((Runnable)new SkanwordsTasksList9(this));
            }
        }
    }

    private void openLockedTask(final Skanword skanword) {
        MainMenuActivity.openLockedTaskactivity((Activity)this, skanword);
    }

    private void openSortDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this, R.style.Scanword_style_dialog);
        final ArrayList<String> list = new ArrayList<String>();
        list.add("По номеру");
        list.add("Нерешенные сверху");
        alertDialogBuilder.setSingleChoiceItems((CharSequence[])list.toArray(new CharSequence[list.size()]), SkanwordsTasksList.sCurrentSortType, (DialogInterface.OnClickListener)new SkanwordsTasksList7(this)).setPositiveButton("Ок", (DialogInterface.OnClickListener)new SkanwordsTasksList6(this));
        final AlertDialog create = alertDialogBuilder.create();
        create.getWindow().setBackgroundDrawable((Drawable)new ColorDrawable(0));
        create.setCancelable(false);
        create.show();
    }

    private void setAdapter() {
        Log.v("", "setAdapter tasks 2");
        this.changeSortType();
        final List<Skanword> mSkanwordsList = this.mSkanwordsList;
        final int intValue = this.mIssueId;
        boolean b = true;
        if (intValue >= 1) {
            b = false;
        }
        this.mAdapter = new SkanwordsTasksAdapter((Context)this, R.layout.scanword_row_item, (List)mSkanwordsList, b);
        this.mListView.setAdapter((ListAdapter)this.mAdapter);
        this.mListView.setOnItemClickListener((AdapterView.OnItemClickListener)new SkanwordsTasksList11(this));
    }

    private void startSkanword(final Skanword skanword) {
        final Intent intent = new Intent("android.intent.action.EDIT", (Uri)null, (Context)this, (Class)SkanwordGameActivity.class);
        intent.putExtra("cross_id", skanword.getId());
        intent.putExtra("set_id", (Serializable)this.mIssueId);
        this.startActivityForResult(intent, 101);
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_scanword_enter");
        if (MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordOffered(Integer.valueOf(skanword.getId()))) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_offeredscan_opened");
        }
        MainNetworkManager.getInstance().requestSyncExec();
    }

    protected void checkTutorialState() {
        this.runOnUiThread((Runnable)new SkanwordsTasksList5(this, TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_ISSUE_OPENED));
    }

    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
    {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        if (paramInt1 == 101)
        {
            if ((paramInt2 != 556) && (paramInt2 != 555))
            {
                long l1 = new Date().getTime();
//                long l2 = Appirater.getAppiraterSharedPrefernces(this).getLong("date_last_show", 0L);
                long l2 = 0L;
                int i = 1;
                if (l1 - l2 > 414000L) {
                    paramInt1 = 1;
                } else {
                    paramInt1 = 0;
                }
                paramInt2 = i;
//                if (!Appirater.getAppiraterSharedPrefernces(this).getBoolean("rateclicked", false)) {
                if (!false) {
//                    if (Appirater.getAppiraterSharedPrefernces(this).getBoolean("dontshow", false)) {
                    if (false) {
                        paramInt2 = i;
                    } else {
                        paramInt2 = 0;
                    }
                }
                boolean bool = MainDataManager.getInstance().getSkanwordsDataManager().readyToRateTheApp();
                if ((paramInt1 != 0) && (paramInt2 == 0) && (bool)) {
//                    Appirater.showRateDialog(this);
                } else {
                    AdsManager.getInstance().showInterstitial(this);
                }
                MainNetworkManager.getInstance().requestSyncExec();
                return;
            }
            setResult(paramInt2);
            finish();
            return;
        }
        if ((paramInt1 == 105) && (paramInt2 == 100))
        {
            Log.v("", "onActivityResult - scanwordsTaskList  ");
            setResult(paramInt2);
            finish();
        }
    }

    @SuppressLint({ "NewApi" })
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(R.layout.scanwords_tasks_list_view);
        final Bundle extras = this.getIntent().getExtras();
        this.mListType = (SkanwordsDataManager.TaskListType)extras.getSerializable("LIST_TYPE");
        if (this.mListType == SkanwordsDataManager.TaskListType.ISSUE_TASKS) {
            this.mIssueId = extras.getInt("ISSUE_ID", 0);
        }
        if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_ISSUE_DOWNLOADED) {
            TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_ISSUE_OPENED);
            SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SkanwordsDataManager.SkanwordsSetsUpdateEvent("EVENT_SCANWORDS_SETS_INFOS_UPDATE"));
        }
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle((CharSequence)extras.getString("LIST_TITLE"));
        List<Skanword> list;
        if (this.mIssueId > 0) {
            list = MainDataManager.getInstance().getSkanwordsDataManager().getSkanwordsForListType(this.mListType, this.mIssueId);
        }
        else {
            list = MainDataManager.getInstance().getSkanwordsDataManager().getSkanwordsForListType(this.mListType);
        }
        this.mSkanwordsList = new ArrayList<Skanword>();
        for (Skanword skanword : list) {
            if (skanword != null && skanword.hasFiles()) {
                this.mSkanwordsList.add(skanword);
            }
        }
        if (extras.getBoolean("SKANWORDS_COUNT_OVER_LIMIT", false) && this.mSkanwordsList.size() >= MainDataManager.getInstance().getSkanwordsDataManager().getMaxSkanwordsOnPageCount()) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this, R.style.Scanword_style_dialog);
            final StringBuilder sb = new StringBuilder();
            sb.append("Список отображает ");
            sb.append(MainDataManager.getInstance().getSkanwordsDataManager().getMaxSkanwordsOnPageCount());
            sb.append(" сканвордов.\nЧтобы увидеть больше - перейдите к списку выпусков.");
            alertDialogBuilder.setMessage((CharSequence)sb.toString()).setPositiveButton("Ок", (DialogInterface.OnClickListener)new SkanwordsTasksList1(this));
            alertDialogBuilder.create().show();
        }
        this.mSkanwordsUpdateEventListener = (EventListener)new SkanwordsTasksList2(this);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_SCANWORDS_TASKS_UPDATE", this.mSkanwordsUpdateEventListener);
        (this.mListView = (ListView)this.findViewById(R.id.tasks_list)).setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        this.mListView.setBackgroundResource(R.color.accent_green);
        this.mTimerEventListener = (EventListener)new SkanwordsTasksList3(this);
        if (this.mTimerEventListener != null) {
            SmappsScanwords.getEventsDispatcher().addListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        }
        this.mProgressUpdateListener = (EventListener)new SkanwordsTasksList4(this);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_PROGRESS_UPDATE", this.mProgressUpdateListener);
        this.update();
    }

    public boolean onCreateOptionsMenu(final Menu menu) {
        menu.add(0, 5, 0, (CharSequence)"mytext").setIcon(R.drawable.filter).setShowAsAction(2);
        return true;
    }

    protected void onDestroy() {
        super.onDestroy();
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_PROGRESS_UPDATE", this.mProgressUpdateListener);
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_SCANWORDS_TASKS_UPDATE", this.mSkanwordsUpdateEventListener);
        if (this.mTimerEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        }
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        if (itemId == 5) {
            this.openSortDialog();
            return true;
        }
        if (itemId != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.finish();
        return true;
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStart() {
        this.checkTutorialState();
        super.onStart();
    }

    public void update() {
        this.runOnUiThread((Runnable)new SkanwordsTasksList10(this));
    }

    @SuppressLint({ "UseSparseArrays" })
    public static class SkanwordsTasksAdapter extends ArraySectionedListAdapter<Skanword>
    {
        private boolean buildForVip;
        private boolean mOneList;
        private final List<Skanword> objectsList;
        private final int resourceId;
        private volatile Map<Integer, View> tasksRowViews;

        public SkanwordsTasksAdapter(final Context context, final int resourceId, final List<Skanword> objectsList, final boolean mOneList) {
            super(context, resourceId, (List)objectsList);
            this.tasksRowViews = new HashMap();
            this.objectsList = objectsList;
            this.resourceId = resourceId;
            this.mOneList = mOneList;
            this.buildForVip = (MainDataManager.getInstance().getUserData() != null && MainDataManager.getInstance().getUserData().isVip());
        }

        static /* synthetic */ Skanword access900(final SkanwordsTasksAdapter skanwordsTasksAdapter, final int n) {
            return skanwordsTasksAdapter.getItemForPosition(n);
        }

        private static void buildKeyword(final LinearLayout linearLayout, final Skanword.Keyword skanwordKeyword) {
            if (skanwordKeyword == null) {
                linearLayout.setVisibility(View.GONE);
                return;
            }
            linearLayout.setVisibility(View.VISIBLE);
            final LayoutInflater from = LayoutInflater.from(SmappsScanwords.getContext());
            linearLayout.removeAllViewsInLayout();
            for (final Skanword.Keyword.KeywordCell skanwordKeywordKeywordCell : skanwordKeyword.getCells()) {
                final String character = skanwordKeywordKeywordCell.getCharacter();
                final View inflate = from.inflate(R.layout.question_letter, (ViewGroup)null);
                linearLayout.addView(inflate);
                final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)inflate.getLayoutParams();
                layoutParams.rightMargin = 10;
                layoutParams.bottomMargin = 2;
                inflate.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                final View viewById = inflate.findViewById(R.id.letter_view);
                int backgroundResource;
                if (skanwordKeywordKeywordCell.isPersist()) {
                    backgroundResource = R.drawable.guessed_keyword_letter_cell;
                }
                else {
                    backgroundResource = R.drawable.unguessed_keyword_letter_cell;
                }
                viewById.setBackgroundResource(backgroundResource);
                if (!character.equalsIgnoreCase(" ")) {
                    ((TextView)inflate.findViewById(R.id.letter_text)).setText((CharSequence)character);
                }
            }
        }

        public static void buildRowViewForScanword(final View view, final Skanword skanword) {
            if (view == null) {
                return;
            }
            final TextView textView = (TextView)view.findViewById(R.id.scanwordIdLabel);
            final ImageView imageView = (ImageView)view.findViewById(R.id.scanwordTypeIcon);
            final TextView textView2 = (TextView)view.findViewById(R.id.statusLabel);
            textView2.setVisibility(View.VISIBLE);
            textView2.setTextColor(-30720);
            final String cover = skanword.getCover();
            final StringBuilder sb = new StringBuilder();
            sb.append(skanword.getImageDir());
            sb.append(cover);
            imageView.setImageBitmap(BitmapFactory.decodeFile(sb.toString()));
            if (skanword.isStarted() && !skanword.isFinished()) {
                textView2.setVisibility(View.GONE);
                view.findViewById(R.id.progressBar1).setVisibility(View.VISIBLE);
                ((ProgressBar)view.findViewById(R.id.progressBar1)).setProgress((int)(skanword.getFinishedQuestionsCount() / (float)skanword.getQuestionsCount() * 100.0f));
            }
            else {
                view.findViewById(R.id.progressBar1).setVisibility(View.GONE);
                if (skanword.isFinished()) {
                    textView2.setText("Сканворд решен");
                    textView2.setTextColor(-10053376);
                }
                else {
                    textView2.setText("Новый сканворд");
                }
            }
            String s;
            if (skanword.getFinishedQuestionsCount() > 0 && !skanword.isFinished()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("решено ");
                sb2.append(skanword.getFinishedQuestionsCount());
                sb2.append(" из ");
                sb2.append(skanword.getQuestionsCount());
                s = sb2.toString();
            }
            else {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(skanword.getQuestionsCount());
                sb3.append(" ");
                sb3.append(Utils.spellVariantForNumber(skanword.getQuestionsCount(),  "слово", "слова", "слов"));
                s = sb3.toString();
            }
            final int level = skanword.getLevel();
            String s2;
            if (level != 1) {
                if (level != 2) {
                    if (level != 3) {
                        s2 = "";
                    }
                    else {
                        s2 = "сложный";
                    }
                }
                else {
                    s2 = "средний";
                }
            }
            else {
                s2 = "легкий";
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(skanword.toString());
            sb4.append(" • ");
            sb4.append(s2);
            sb4.append(" • ");
            sb4.append(s);
            textView.setText((CharSequence)sb4.toString());
            textView.setBackgroundColor(0);
            buildKeyword((LinearLayout)view.findViewById(R.id.keywordLayout), skanword.getKeyword());
        }

        private Skanword getItemForPosition(final int n) {
            int i = 0;
            int n2 = 0;
            while (i < this.getNumberOfSections()) {
                n2 += this.getNumberOfRowsInSection(i) + 1;
                if (n < n2) {
                    return (Skanword)super.getItem(n - i - 1);
                }
                ++i;
            }
            return null;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        protected void bindRowViewInSection(final Context context, final int n, final int n2, final View view) {
            if (this.getViewTypeForRowInSection(n, n2) == 1) {
                this.buildRowScanwordTask(view, (Skanword)super.getItem((n2 + 1) * (n + 1) - 1));
            }
        }

        protected void bindSectionView(final Context context, final int n, final View view) {
            if (view == null) {
                return;
            }
            final TextView textView = (TextView)view.findViewById(R.id.setLabel);
            String text;
            if (SkanwordsTasksList.access600() == 0) {
                text = "Сканворды по номеру:";
            }
            else {
                text = "Нерешенные сверху:";
            }
            textView.setText((CharSequence)text);
        }

        public void buildRowScanwordTask(final View view, final Skanword skanword) {
            buildRowViewForScanword(view, skanword);
            if (view != null) {
                this.tasksRowViews.put(skanword.getId(), view);
            }
        }

        public Object getItemInSectionAndRow(final int n, final int n2) {
            return null;
        }

        protected int getNumberOfRowsInSection(final int n) {
            final List objectsList = this.objectsList;
            if (objectsList == null) {
                return 0;
            }
            return objectsList.size() + ((this.mOneList ^ true) ? 1 : 0);
        }

        protected int getNumberOfSections() {
            return 1;
        }

        protected int getNumberOfViewTypeForRows() {
            return 3;
        }

        protected int getViewTypeForRowInSection(final int n, final int n2) {
            if (n2 == this.getNumberOfRowsInSection(n) - 1 && !this.mOneList) {
                return 2;
            }
            return 1;
        }

        public boolean isBuildForVip() {
            return this.buildForVip;
        }

        public boolean isEnabled(final int n) {
            final int itemViewType = this.getItemViewType(n);
            boolean b = true;
            if (itemViewType != 1) {
                if (this.getItemViewType(n) == 2) {
                    return true;
                }
                b = false;
            }
            return b;
        }

        protected View newRowViewInSection(final Context context, final int n, final int n2, final ViewGroup viewGroup) {
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            if (this.getViewTypeForRowInSection(n, n2) == 2) {
                return from.inflate(R.layout.set_delete_row_item, viewGroup, false);
            }
            return from.inflate(this.resourceId, viewGroup, false);
        }

        protected View newSectionView(final Context context, final int n, final ViewGroup viewGroup) {
            return ((LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header, viewGroup, false);
        }

        public void notifyDataSetInvalidated() {
            this.buildForVip = MainDataManager.getInstance().getUserData().isVip();
            super.notifyDataSetInvalidated();
        }
    }

    class SkanwordsTasksList8 implements Comparator<Skanword> {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList8(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public int compare(final Skanword skanword, final Skanword skanword2) {
            int n;
            int n2;
            if (SkanwordsTasksList.access600() == 0) {
                n = skanword.getId();
                n2 = skanword2.getId();
            }
            else if (skanword.isFinished() && skanword2.isFinished()) {
                n = skanword.getId();
                n2 = skanword2.getId();
            }
            else {
                if (skanword.isFinished()) {
                    return 1;
                }
                if (skanword2.isFinished()) {
                    return -1;
                }
                return skanword.getQuestionsCount() - skanword.getFinishedQuestionsCount() - (skanword2.getQuestionsCount() - skanword2.getFinishedQuestionsCount());
            }
            return n - n2;
        }
    }

    class SkanwordsTasksList9 implements Runnable {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList9(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            SkanwordsTasksList.access300(this.this0).notifyDataSetChanged();
        }
    }

    class SkanwordsTasksList7 implements DialogInterface.OnClickListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList7(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
            SkanwordsTasksList.access602(n);
            SkanwordsTasksList.access402(this.this0, true);
        }
    }

    class SkanwordsTasksList6 implements DialogInterface.OnClickListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList6(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
            if (SkanwordsTasksList.access400(this.this0)) {
                SkanwordsTasksList.access500(this.this0);
            }
            SkanwordsTasksList.access402(this.this0, false);
        }
    }

    class SkanwordsTasksList11 implements AdapterView.OnItemClickListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList11(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
            if (SkanwordsTasksList.access300(this.this0).getItemViewType(n) == 2) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.this0, R.style.Scanword_style_dialog);
                alertDialogBuilder.setMessage("Выпуск будет удален с сохранением прогресса. Вы всегда можете закачать выпуск заново и продолжить игру.").setPositiveButton("Удалить", (DialogInterface.OnClickListener)new SkanwordsTasksList11.SkanwordsTasksList111(this)).setNegativeButton("Отмена", (DialogInterface.OnClickListener)null);
                alertDialogBuilder.create().show();
                return;
            }
            if (!MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordFree(Integer.valueOf(SkanwordsTasksList.SkanwordsTasksAdapter.access900(SkanwordsTasksList.access300(this.this0), n).getId())) && !MainDataManager.getInstance().getUserData().isVip()) {
                final SkanwordsTasksList this0 = this.this0;
                SkanwordsTasksList.access1100(this0, SkanwordsTasksList.SkanwordsTasksAdapter.access900(SkanwordsTasksList.access300(this0), n));
                return;
            }
            final SkanwordsTasksList this2 = this.this0;
            SkanwordsTasksList.access1000(this2, SkanwordsTasksList.SkanwordsTasksAdapter.access900(SkanwordsTasksList.access300(this2), n));
        }


        class SkanwordsTasksList111 implements DialogInterface.OnClickListener {
            final /* synthetic */ SkanwordsTasksList11 this1;

            SkanwordsTasksList111(final SkanwordsTasksList11 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
                MainDataManager.getInstance().getSkanwordsDataManager().deleteIssuesWithId((int)SkanwordsTasksList.access100(this.this1.this0));
                this.this1.this0.finish();
            }
        }
    }

    class SkanwordsTasksList5 implements Runnable {
        final /* synthetic */ SkanwordsTasksList this0;
        final /* synthetic */ boolean valfShow;

        SkanwordsTasksList5(final SkanwordsTasksList this0, final boolean valfShow) {
            super();
            this.this0 = this0;
            this.valfShow = valfShow;
        }

        @Override
        public void run() {
            final View viewById = this.this0.findViewById(R.id.tutorialView);
            int visibility;
            if (this.valfShow) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            viewById.setVisibility(visibility);
        }
    }

    class SkanwordsTasksList1 implements DialogInterface.OnClickListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList1(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    class SkanwordsTasksList2 implements EventListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList2(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            final SkanwordsTasksList this0 = this.this0;
            List list;
            if (SkanwordsTasksList.access100(this0) > 0) {
                list = MainDataManager.getInstance().getSkanwordsDataManager().getSkanwordsForListType(SkanwordsTasksList.access200(this.this0), SkanwordsTasksList.access100(this.this0));
            }
            else {
                list = MainDataManager.getInstance().getSkanwordsDataManager().getSkanwordsForListType(SkanwordsTasksList.access200(this.this0));
            }
            SkanwordsTasksList.access002(this0, list);
            this.this0.update();
        }
    }


    class SkanwordsTasksList3 implements EventListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList3(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new SkanwordsTasksList31(this));
        }


        class SkanwordsTasksList31 implements Runnable {
            final /* synthetic */ SkanwordsTasksList3 this1;

            SkanwordsTasksList31(final SkanwordsTasksList3 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                if (SkanwordsTasksList.access300(this.this1.this0) == null) {
                    return;
                }
                if (SkanwordsTasksList.access300(this.this1.this0).isBuildForVip() != MainDataManager.getInstance().getUserData().isVip()) {
                    SkanwordsTasksList.access300(this.this1.this0).notifyDataSetInvalidated();
                }
            }
        }
    }

    class SkanwordsTasksList4 implements EventListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList4(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.runOnUiThread((Runnable)new SkanwordsTasksList4.SkanwordsTasksList41(this));
        }

        class SkanwordsTasksList41 implements Runnable {
            final /* synthetic */ SkanwordsTasksList4 this1;

            SkanwordsTasksList41(final SkanwordsTasksList4 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                this.this1.this0.setResult(555, (Intent)null);
                this.this1.this0.finish();
            }
        }
    }

    class SkanwordsTasksList10 implements Runnable {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList10(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (SkanwordsTasksList.access700(this.this0) == null) {
                return;
            }
            SkanwordsTasksList.access800(this.this0);
        }
    }
}