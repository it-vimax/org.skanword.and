package org.skanword.and.menu;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.skanword.and.R;
import org.skanword.and.datamanager.DataBaseManager;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;

public class AuthorisationAcceptActivity extends Activity
{
    private StatsListAdapter mAdapter;
    private ListView mOptionsList;

    public AuthorisationAcceptActivity() {
        super();
    }

    protected void onCreate(Bundle jsonObject) {
        super.onCreate(jsonObject);
        this.setContentView(R.layout.authorisation_accept_activity_layout);
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        Object stringExtra = this.getIntent().getStringExtra("user_info");
        final SocialNetworkManager.SocialUser socialNetworkManagerSocialUser = (SocialNetworkManager.SocialUser)this.getIntent().getSerializableExtra("user_social");
        final TextView textView = (TextView)this.findViewById(R.id.userNameLabel);
        final StringBuilder sb = new StringBuilder();
        sb.append(socialNetworkManagerSocialUser.getFirstName());
        sb.append(" ");
        sb.append(socialNetworkManagerSocialUser.getSecondName());
        sb.append(" ");
        sb.append(this.getResources().getString(R.string.progress_exist_label));
        textView.setText((CharSequence)sb.toString());
        if (socialNetworkManagerSocialUser.getAvatarUrl() != null) {
            ImageLoader.getInstance().displayImage(socialNetworkManagerSocialUser.getAvatarUrl(), (ImageView)this.findViewById(R.id.profile_image));
        }
        jsonObject = null;
        try {
            JSONObject stringExtra2 = new JSONObject((String)stringExtra);
            try {
//                jsonObject = (Bundle)stringExtra2.getJSONObject("user_info");
                jsonObject.putString("user_info", stringExtra2.getJSONObject("user_info").toString());
            }
            catch (JSONException ex) {
                jsonObject = (Bundle)stringExtra;
                stringExtra = ex;
            }
        }
        catch (JSONException ex3) {}
        ((JSONException)stringExtra).printStackTrace();
        ((Button)this.findViewById(R.id.cancelAuthButton)).setOnClickListener((View.OnClickListener)new AuthorisationAcceptActivity1(this));
        ((Button)this.findViewById(R.id.acceptAuthButton)).setOnClickListener((View.OnClickListener)new AuthorisationAcceptActivity2(this));
        this.setTitle("Внимание!");
        final ArrayList<AuthorisationAcceptActivity.StatData> list = new ArrayList<AuthorisationAcceptActivity.StatData>();
        list.add(new AuthorisationAcceptActivity.StatData("", ""));
        if (jsonObject != null) {
            while (true) {
                while (true) {
                    Label_0521: {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(jsonObject.getInt("cross_amount_solved"));
                        sb2.append("");
                        list.add(new AuthorisationAcceptActivity.StatData("Решено сканвордов:", sb2.toString()));
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append(jsonObject.getInt("cross_amount_started"));
                        sb3.append("");
                        list.add(new AuthorisationAcceptActivity.StatData("Начато сканвордов:", sb3.toString()));
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append(jsonObject.getInt("cross_user_score"));
                        sb4.append("");
                        list.add(new AuthorisationAcceptActivity.StatData("Очков сканвордов:", sb4.toString()));
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append(jsonObject.getInt("hints"));
                        sb5.append("");
                        list.add(new AuthorisationAcceptActivity.StatData("Подсказок:", sb5.toString()));
                        if (jsonObject.getInt("vip") <= 0) {
                            break Label_0521;
                        }
                        final String s =  "Активен";
                        list.add(new AuthorisationAcceptActivity.StatData("Премиум:", s));
                        break;
                    }
                    final String s = "Не активен";
                    continue;
                }
            }
        }
        this.mAdapter = new AuthorisationAcceptActivity.StatsListAdapter((Context)this, R.layout.statistic_row_layout, (List)list);
        (this.mOptionsList = (ListView)this.findViewById(R.id.statsListView)).setAdapter((ListAdapter)this.mAdapter);
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.finish();
        return true;
    }

    public static class StatsListAdapter extends ArrayAdapter<StatData>
    {
        private int mResourceId;

        public StatsListAdapter(final Context context, final int mResourceId, final List<AuthorisationAcceptActivity.StatData> list) {
            super(context, mResourceId, (List)list);
            this.mResourceId = mResourceId;
        }

        private View createRowView(final ViewGroup viewGroup) {
            return LayoutInflater.from(this.getContext()).inflate(this.mResourceId, viewGroup, false);
        }

        private void rebuildView(final View view, final StatData authorisationAcceptActivityStatData, final int n) {
            final TextView textView = (TextView)view.findViewById(R.id.statNameLabel);
            final TextView textView2 = (TextView)view.findViewById(R.id.statNameLabel);
            textView.setText((CharSequence)authorisationAcceptActivityStatData.getName());
            textView2.setText((CharSequence)authorisationAcceptActivityStatData.getValue());
            view.setClickable(true);
        }

        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View rowView = view;
            if (view == null) {
                rowView = this.createRowView(viewGroup);
            }
            this.rebuildView(rowView, (StatData)this.getItem(n), n);
            return rowView;
        }
    }

    public static class StatData
    {
        private String mName;
        private String mValue;

        public StatData(final String mName, final String mValue) {
            super();
            this.mName = mName;
            this.mValue = mValue;
        }

        public String getName() {
            return this.mName;
        }

        public String getValue() {
            return this.mValue;
        }

        public void setName(final String mName) {
            this.mName = mName;
        }

        public void setValue(final String mValue) {
            this.mValue = mValue;
        }
    }

    class AuthorisationAcceptActivity1 implements View.OnClickListener {
        final /* synthetic */ AuthorisationAcceptActivity this0;

        AuthorisationAcceptActivity1(final AuthorisationAcceptActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            this.this0.setResult(0);
            this.this0.finish();
        }
    }


    class AuthorisationAcceptActivity2 implements View.OnClickListener {
        final /* synthetic */ AuthorisationAcceptActivity this0;

        AuthorisationAcceptActivity2(final AuthorisationAcceptActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            MainNetworkManager.getInstance().acceptAuthorisation((Activity)MainMenuActivity.instance, true);
            this.this0.setResult(-1);
            this.this0.finish();
        }
    }
}