package org.skanword.and.menu.skanwordsmenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.skanword.and.datamanager.Skanword;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventDispatcher;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.MainDataManager.IDowloadSetProgress;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager.SkanwordsSetsUpdateEvent;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListDirrectionType;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.datamanager.SkanwordsDataManager.VisibilityListType;
import org.skanword.and.datamanager.SkanwordsSetInfo;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.IssuesList;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.menu.skanwordsmenu.SkanwordsTasksList.SkanwordsTasksAdapter;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.MainNetworkManager.RequestCompletionBlock;
import org.skanword.and.scanwordgame.SkanwordGameActivity;

public class SkanwordsIssuesList extends IssuesList {
    public static final String EVENT_SET_DOWNLOAD_PROGRESS = "EVENT_SET_DOWNLOAD_PROGRESS";
    public static final int TYPE_ROW_LAST_SOLVED_VIEW = 5;
    public static final int TYPE_ROW_ONE_LIST_VIEW = 3;
    public static final int TYPE_ROW_OTHER_PAGE_VIEW = 2;
    public static final int TYPE_ROW_TIMER_VIEW = 4;
    private static int mCurrentSortType;
    private int FragmentPosition;
    private SkanwordsDataManager.TaskListType listType;
    private ItemAction mActions;
    private SetsAdapter mAdapter;
    private boolean mCurrentSortTypeChanged;
    private EventListener mDownloadTodayIssueEventListener;
    private ListView mIssuesList;
    private EventListener mSetDownloadEvent;
    private EventListener mSetsInfoUpdateEventListener;
    List<SkanwordsSetInfo> mSetsInfos;
    private EventListener mTimerEventListener;
    private boolean mVisible;

    public SkanwordsIssuesList() {
        super();
        this.mIssuesList = null;
        this.mActions = null;
        this.mAdapter = null;
        this.mCurrentSortTypeChanged = false;
        this.mActions = (SkanwordsIssuesList.ItemAction) new SkanwordsIssuesList1(this);
        this.mSetsInfoUpdateEventListener = (EventListener) new SkanwordsIssuesList2(this);
        this.mDownloadTodayIssueEventListener = (EventListener) new SkanwordsIssuesList3(this);
        this.mSetDownloadEvent = (EventListener) new SkanwordsIssuesList.SkanwordsIssuesList4(this);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_SET_DOWNLOAD_PROGRESS", this.mSetDownloadEvent);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_SCANWORDS_SETS_INFOS_UPDATE", this.mSetsInfoUpdateEventListener);
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_DOWNLOAD_TODAY_ISSUE", this.mDownloadTodayIssueEventListener);
    }

    static /* synthetic */ SkanwordsDataManager.TaskListType access000(final SkanwordsIssuesList list) {
        return list.listType;
    }

    static /* synthetic */ SkanwordsIssuesList.SetsAdapter access100(final SkanwordsIssuesList list) {
        return list.mAdapter;
    }

    static /* synthetic */ boolean access200(final SkanwordsIssuesList list) {
        return list.mVisible;
    }

    static /* synthetic */ void access300(final SkanwordsIssuesList list, final SkanwordsSetInfo skanwordsSetInfo, final int n) {
        list.setDownloadEvent(skanwordsSetInfo, n);
    }

    static /* synthetic */ int access400() {
        return SkanwordsIssuesList.mCurrentSortType;
    }

    static /* synthetic */ int access402(final int mCurrentSortType) {
        return SkanwordsIssuesList.mCurrentSortType = mCurrentSortType;
    }

    static /* synthetic */ ListView access500(final SkanwordsIssuesList list) {
        return list.mIssuesList;
    }

    static /* synthetic */ boolean access600(final SkanwordsIssuesList list) {
        return list.mCurrentSortTypeChanged;
    }

    static /* synthetic */ boolean access602(final SkanwordsIssuesList list, final boolean mCurrentSortTypeChanged) {
        return list.mCurrentSortTypeChanged = mCurrentSortTypeChanged;
    }

    static /* synthetic */ SkanwordsIssuesList.ItemAction access700(final SkanwordsIssuesList list) {
        return list.mActions;
    }

    static /* synthetic */ void access900(final SkanwordsIssuesList list, final boolean adapter) {
        list.setAdapter(adapter);
    }

    public static int currentSortType() {
        return SkanwordsIssuesList.mCurrentSortType;
    }

    private void setAdapter(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setAdapter ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        if (this.mIssuesList != null) {
            if (this.getView() == null) {
                return;
            }
            this.showDownloadingState(false);
            final int mCurrentSortType = SkanwordsIssuesList.mCurrentSortType;
            if (mCurrentSortType == 1) {
                this.mSetsInfos = (List<SkanwordsSetInfo>) MainDataManager.getInstance().getSkanwordsDataManager().getSortedSetsInfosList(this.getListType(), SkanwordsDataManager.VisibilityListType.DOWNLOADED_ISSUES);
            } else if (mCurrentSortType == 2) {
                this.mSetsInfos = (List<SkanwordsSetInfo>) MainDataManager.getInstance().getSkanwordsDataManager().getSortedSetsInfosList(this.getListType(), SkanwordsDataManager.VisibilityListType.NOT_DOWNLOADED_ISSUES);
            } else {
                this.mSetsInfos = (List<SkanwordsSetInfo>) MainDataManager.getInstance().getSkanwordsDataManager().getSortedSetsInfosList(this.getListType());
            }
            final TextView textView = (TextView) this.getView().findViewById(R.id.emptyIssuesListLabel);
            final List<SkanwordsSetInfo> mSetsInfos = this.mSetsInfos;
            if (mSetsInfos != null && mSetsInfos.size() != 0) {
                this.mIssuesList.setVisibility(View.VISIBLE);
                textView.setVisibility(View.INVISIBLE);
                this.mAdapter = new SkanwordsIssuesList.SetsAdapter((Context) this.getActivity(), R.layout.set_row_item, (List) this.mSetsInfos, this.mActions, this.getListType(), SkanwordsIssuesList.mCurrentSortType);
                final Parcelable onSaveInstanceState = this.mIssuesList.onSaveInstanceState();
                this.mIssuesList.setAdapter((ListAdapter) this.mAdapter);
                if (!b) {
                    this.mIssuesList.onRestoreInstanceState(onSaveInstanceState);
                }
                this.mIssuesList.setOnItemClickListener((AdapterView.OnItemClickListener) new SkanwordsIssuesList11(this));
                Log.v("", "setAdapter 2");
                this.downloadTodayIssue();
                return;
            }
            textView.setVisibility(View.VISIBLE);
            this.mIssuesList.setVisibility(View.INVISIBLE);
        }
    }

    private void setDownloadEvent(final SkanwordsSetInfo skanwordsSetInfo, final int n) {
        final SkanwordsIssuesList.SetsAdapter mAdapter = this.mAdapter;
        if (mAdapter == null) {
            return;
        }
        final View setRowView = mAdapter.getSetRowView(skanwordsSetInfo);
        TextView textView;
        if (setRowView != null) {
            textView = (TextView) setRowView.findViewById(R.id.setLabel);
        } else {
            textView = null;
        }
        if (n == 0) {
            MainDataManager.getInstance().addDownloadingProgress(skanwordsSetInfo);
            if (textView != null && textView.getText().equals(skanwordsSetInfo.getDisplayName())) {
                this.mAdapter.buildRowScanwordSetInfo(setRowView, skanwordsSetInfo);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("StarDownloading set ");
            sb.append(skanwordsSetInfo.getDisplayName());
            Log.v("", sb.toString());
            return;
        }
        MainDataManager.getInstance().updateProgress(skanwordsSetInfo, n);
        if (n == -1) {
            MainDataManager.getInstance().removeDownloadingProgress(skanwordsSetInfo);
            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_ISSUE_DOWNLOADING) {
                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_START);
            }
        } else if (n == 100) {
            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_ISSUE_DOWNLOADING) {
                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_ISSUE_DOWNLOADED);
            }
            MainDataManager.getInstance().removeDownloadingProgress(skanwordsSetInfo);
            MainMenuActivity.instance.runOnUiThread((Runnable) new SkanwordsIssuesList.SkanwordsIssuesList5(this, skanwordsSetInfo));
        }
        final FragmentActivity activity = this.getActivity();
        if (activity != null) {
            ((Activity) activity).runOnUiThread((Runnable) new SkanwordsIssuesList6(this, textView, skanwordsSetInfo, n, setRowView));
        }
    }

    private void setTimerEventListener(final boolean b) {
        if (!b) {
            if (this.mTimerEventListener != null) {
                SmappsScanwords.getEventsDispatcher().removeListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
            }
            this.mTimerEventListener = null;
            return;
        }
        if (this.listType == SkanwordsDataManager.TaskListType.ISSUE_TASKS) {
            this.setTimerEventListener(false);
            this.mTimerEventListener = (EventListener) new SkanwordsIssuesList8(this);
            SmappsScanwords.getEventsDispatcher().addListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        }
    }

    public void clearTab() {
        this.clearTab(false);
    }

    public void clearTab(final boolean b) {
        if (this.getView() != null) {
            if (!b) {
                this.getView().findViewById(R.id.loadingSpinner).setVisibility(View.GONE);
            }
            this.getView().findViewById(R.id.emptyIssuesListLabel).setVisibility(View.GONE);
            this.getView().findViewById(R.id.issues_list).setVisibility(View.GONE);
        }
    }

    public void downloadTodayIssue() {
        Log.v("", "downloadTodayIssue gor event");
        if (MainDataManager.getInstance().getOptions().isAutoDownload()) {
            if (this.mAdapter == null) {
                return;
            }
            Log.v("", "downloadTodayIssue call adapter");
            this.mAdapter.downloadTodayIssue();
        }
    }

    public int getFragmentPosition() {
        return this.FragmentPosition;
    }

    public SkanwordsDataManager.TaskListType getListType() {
        return this.listType;
    }

    public int getSetsInfosCount() {
        final List<SkanwordsSetInfo> mSetsInfos = this.mSetsInfos;
        if (mSetsInfos == null) {
            return 0;
        }
        return mSetsInfos.size();
    }

    public void onActivityCreated(final Bundle bundle) {
        this.clearTab(this.mVisible);
        super.onActivityCreated(bundle);
    }

    public void onCreate(final Bundle bundle) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onCreate -------ScanwordsIssuesList------ ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        super.onCreate(bundle);
    }

    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.setListType((SkanwordsDataManager.TaskListType) this.getArguments().getSerializable("list_type"));
        this.setTimerEventListener(true);
        final View inflate = layoutInflater.inflate(R.layout.scanword_sets_list, viewGroup, false);
        (this.mIssuesList = (ListView) inflate.findViewById(R.id.issues_list)).setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        this.setEmptyListCaption((TextView) inflate.findViewById(R.id.emptyIssuesListLabel));
        final StringBuilder sb = new StringBuilder();
        sb.append("onCreateView  skanwordissueslist");
        sb.append(this.listType);
        Log.v("", sb.toString());
        return inflate;
    }

    public void onDestroy() {
        final StringBuilder sb = new StringBuilder();
        sb.append("onDestroy -------ScanwordsIssuesList------ ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        this.setTimerEventListener(false);
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_SCANWORDS_SETS_INFOS_UPDATE", this.mSetsInfoUpdateEventListener);
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_DOWNLOAD_TODAY_ISSUE", this.mDownloadTodayIssueEventListener);
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_DOWNLOAD_TODAY_ISSUE", this.mSetDownloadEvent);
        super.onDestroy();
    }

    public void onPause() {
        final StringBuilder sb = new StringBuilder();
        sb.append("onPause -------ScanwordsIssuesList------ ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        super.onPause();
    }

    public void onResume() {
        final StringBuilder sb = new StringBuilder();
        sb.append("onResume -------ScanwordsIssuesList------ ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        super.onResume();
    }

    public void onSaveInstanceState(final Bundle bundle) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onSaveInstanceState -------ScanwordsIssuesList------ ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        super.onSaveInstanceState(bundle);
    }

    public void onStart() {
        final StringBuilder sb = new StringBuilder();
        sb.append("onStart -------ScanwordsIssuesList------ ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        super.onStart();
    }

    public void onStop() {
        final StringBuilder sb = new StringBuilder();
        sb.append("onStop -------ScanwordsIssuesList------ ");
        sb.append(this.listType);
        Log.v("", sb.toString());
        super.onStop();
    }

    public void openSortDialog() {
        Log.v("", "open sort dialog  ScanwordIssuesList  ");
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context) this.getActivity(), R.style.Theme_AppCompat_DayNight);
        final ArrayList<String> list = new ArrayList<String>();
        list.add("Показать все");
        list.add("Только загруженные");
        alertDialogBuilder.setSingleChoiceItems((CharSequence[]) list.toArray(new CharSequence[list.size()]), SkanwordsIssuesList.mCurrentSortType, (DialogInterface.OnClickListener) new SkanwordsIssuesList10(this)).setPositiveButton((CharSequence) "\u041e\u043a", (DialogInterface.OnClickListener) new SkanwordsIssuesList9(this));
        final AlertDialog create = alertDialogBuilder.create();
        create.setCancelable(false);
        create.show();
    }

    protected void setEmptyListCaption(final TextView textView) {
        final int mCurrentSortType = SkanwordsIssuesList.mCurrentSortType;
        if (mCurrentSortType != 0) {
            if (mCurrentSortType == 1) {
                if (this.getListType() == SkanwordsDataManager.TaskListType.ISSUE_TASKS) {
                    textView.setText("У вас нет ни одного сканворда,\nкоторый начат но не решен.\n\nНачните разгадывать сканворд,\nи его выпуск появится тут.");
                    return;
                }
                if (this.getListType() == SkanwordsDataManager.TaskListType.STARTED_TASKS) {
                    textView.setText("Вы пока что не закончили\nни одного сканворда.\n\nРазгадайте все слова в\nсканворде, и его выпуск\nпоявится тут.");
                    return;
                }
                textView.setText("Выпуски будут загружены,\nкогда появится соединение с Интернет.");
            }
            return;
        }
        if (this.getListType() == SkanwordsDataManager.TaskListType.STARTED_TASKS) {
            textView.setText("Список пуст.\nУ вас нет загруженных\nновых выпусков.");
            return;
        }
        if (this.getListType() == SkanwordsDataManager.TaskListType.FINISHED_TASKS) {
            textView.setText("Список пуст.\nУ вас нет загруженных\nначатых выпусков.");
            return;
        }
        textView.setText("Список пуст.\nУ вас нет загруженных\nрешенных выпусков.");
    }

    public void setFragmentPosition(final int fragmentPosition) {
        this.FragmentPosition = fragmentPosition;
    }

    public void setListType(final SkanwordsDataManager.TaskListType listType) {
        this.listType = listType;
    }

    public void setMenuVisibility(final boolean mVisible) {
        super.setMenuVisibility(this.mVisible = mVisible);
    }

    public void showDownloadingState(final boolean b) {
        this.getActivity().runOnUiThread((Runnable) new SkanwordsIssuesList7(this, b));
    }

    public void showLoadingError() {
        Log.v("Skanwordsfunc", "show loading error");
        this.showDownloadingState(false);
        final TextView textView = (TextView) this.getView().findViewById(R.id.emptyIssuesListLabel);
        if (textView != null) {
            textView.setText("Произошла ошибка\nпри получении списка выпусков.\nПожалуйста, повторите попытку.");
        }
        this.setAdapter(false);
    }

    public void update(final boolean b) {
        if (this.getActivity() != null) {
            this.getActivity().runOnUiThread((Runnable) new SkanwordsIssuesList12(this, b));
        }
    }

    public void updateSetInfoRow() {
    }

    private interface ItemAction {
        void clickDownload(final SkanwordsSetInfo p0, final View p1);

        void clickItem();

        void clickItem(final SkanwordsSetInfo p0);

        void requestPage(final boolean p0);
    }

    class SkanwordsIssuesList12 implements Runnable {
        final /* synthetic */ SkanwordsIssuesList this0;
        final /* synthetic */ boolean valscrollToTop;

        SkanwordsIssuesList12(final SkanwordsIssuesList this0, final boolean valscrollToTop) {
            super();
            this.this0 = this0;
            this.valscrollToTop = valscrollToTop;
        }

        @Override
        public void run() {
            SkanwordsIssuesList.access900(this.this0, this.valscrollToTop);
        }
    }

    public static enum SetStatus {
        DEFAULT,
        DOWNLOAD_DATA,
        DOWNLOAD_IMAGE,
        DOWNLOADING,
        DOWNLOADED

    }

    @SuppressLint({"UseSparseArrays"})
    protected static class SetsAdapter extends ArraySectionedListAdapter<SkanwordsSetInfo> {
        private boolean buildForVip;
        public boolean hasOneList;
        private final ItemAction itemAction;
        private SkanwordsDataManager.TaskListType listType;
        private Context mContext;
        private int mCurrentSortType;
        private List<SkanwordsSetInfo> objectsList;
        private final int resourceId;
        private volatile Map<Integer, View> setsRowViews;

        public SetsAdapter(final Context mContext, final int resourceId, final List<SkanwordsSetInfo> objectsList, final ItemAction itemAction, final SkanwordsDataManager.TaskListType listType, final int mCurrentSortType) {
            super(mContext, resourceId, (List) objectsList);
            final boolean b = false;
            this.buildForVip = false;
            this.hasOneList = false;
            this.setsRowViews = new HashMap();
            this.mContext = mContext;
            this.objectsList = objectsList;
            this.resourceId = resourceId;
            this.mCurrentSortType = mCurrentSortType;
            this.itemAction = itemAction;
            this.listType = listType;
            this.hasOneList = this.oneList();
            boolean buildForVip = b;
            if (MainDataManager.getInstance().getUserData() != null) {
                buildForVip = b;
                if (MainDataManager.getInstance().getUserData().isVip()) {
                    buildForVip = true;
                }
            }
            this.buildForVip = buildForVip;
        }

        static /* synthetic */ Context access1000(final SetsAdapter setsAdapter) {
            return setsAdapter.mContext;
        }

        static /* synthetic */ ItemAction access1100(final SetsAdapter setsAdapter) {
            return setsAdapter.itemAction;
        }

        static /* synthetic */ SkanwordsSetInfo access800(final SetsAdapter setsAdapter, final int n) {
            return setsAdapter.getItemForPosition(n);
        }

        private void customizeOtherPageRow(final View view, final boolean b) {
            final TextView textView = (TextView) view.findViewById(R.id.otherPageLabelText);
            if (b) {
                textView.setText("Показать следующие выпуски");
                return;
            }
            textView.setText("Показать новые выпуски");
        }

        private SkanwordsSetInfo getItemForPosition(final int n) {
            int i = 0;
            int n2 = 0;
            while (i < this.getNumberOfSections()) {
                if (n < this.getNumberOfRowsInSection(i) + 1 + n2) {
                    return this.getScanwordSetInfoForRowAndSection(n - n2 - 1, i);
                }
                n2 += this.getNumberOfRowsInSection(i) + 1;
                ++i;
            }
            return null;
        }

        private boolean isPageAvailable(final boolean b) {
            return MainDataManager.getInstance().getSkanwordsDataManager().isPageAvailable(b) && this.mCurrentSortType == 0;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        protected void bindRowViewInSection(final Context context, final int n, final int n2, final View view) {
            final int viewTypeForRowInSection = this.getViewTypeForRowInSection(n, n2);
            boolean b = true;
            if (viewTypeForRowInSection == 1) {
                this.buildRowScanwordSetInfo(view, this.getScanwordSetInfoForRowAndSection(n2, n));
            }
            if (viewTypeForRowInSection == 4) {
                this.buildRowScanwordSetInfo(view, (SkanwordsSetInfo) this.getItem(0));
            }
            if (viewTypeForRowInSection == 2) {
                if (n2 == 0) {
                    b = false;
                }
                this.customizeOtherPageRow(view, b);
            }
            if (viewTypeForRowInSection == 5) {
                SkanwordsTasksAdapter.buildRowViewForScanword(view, MainDataManager.getInstance().getSkanwordsDataManager().getLastPlayedSkanword());
            }
        }

        protected void bindSectionView(final Context context, final int n, final View view) {
            view.setClickable(true);
            if (this.getNumberOfSections() > 1 && n == 0) {
                if (this.getViewTypeForRowInSection(n, 0) == 4) {
                    return;
                }
                if (view.findViewById(R.id.setLabel) != null) {
                    ((TextView) view.findViewById(R.id.setLabel)).setText("последний начатый:");
                }
            } else if (view.findViewById(R.id.setLabel) != null) {
                ((TextView) view.findViewById(R.id.setLabel)).setText("выпуски сканвордов от:");
            }
        }

        public void buildRowScanwordSetInfo(View view, final SkanwordsSetInfo isDownloading) {
            View view2 = view;
            if (view == null) {
                view = this.setsRowViews.get(isDownloading.getId());
                if ((view2 = view) == null) {
                    return;
                }
            }
            if (view2.findViewById(R.id.timer) != null) {
                final TextView textView = (TextView) view2.findViewById(R.id.timer);
                final long timeTillTomorrowIssue = MainDataManager.getInstance().getSkanwordsDataManager().timeTillTomorrowIssue();
                String secondsToTimerType;
                if (timeTillTomorrowIssue > 0L) {
                    secondsToTimerType = Utils.secondsToTimerType(timeTillTomorrowIssue);
                } else {
                    secondsToTimerType = "";
                }
                textView.setText((CharSequence) secondsToTimerType);
                final TextView textView2 = (TextView) view2.findViewById(R.id.setLabel);
                String text;
                if (timeTillTomorrowIssue > 0L) {
                    text = "Новый выпуск через:";
                } else {
                    text = "Выпуск готов! Обновите список.";
                }
                textView2.setText((CharSequence) text);
                return;
            }
            final TextView textView3 = (TextView) view2.findViewById(R.id.setLabel);
            textView3.setText((CharSequence) isDownloading.toString());
            textView3.setBackgroundColor(0);
            final TextView textView4 = (TextView) view2.findViewById(R.id.setProgress);
            this.setsRowViews.put(isDownloading.getId(), view2);
            SetStatus setStatus;
            if (isDownloading.hasDownloadedFiles()) {
                setStatus = SetStatus.DOWNLOADED;
            } else {
                setStatus = SetStatus.DEFAULT;
            }
            if (MainDataManager.getInstance().setIsDownloading(isDownloading)) {
                setStatus = SetStatus.DOWNLOADING;
            }
            final Button button = (Button) view2.findViewById(R.id.downloadButton);
            final View viewById = view2.findViewById(R.id.arrowIcon);
            if (viewById != null) {
                int visibility;
                if (setStatus == SetStatus.DOWNLOADED) {
                    visibility = 0;
                } else {
                    visibility = 8;
                }
                viewById.setVisibility(visibility);
            }
            if (setStatus != SetStatus.DEFAULT) {
                button.setVisibility(View.INVISIBLE);
                if (setStatus == SetStatus.DOWNLOADED) {
                    textView4.setVisibility(View.VISIBLE);
                    textView4.setText((CharSequence) MainDataManager.getInstance().getSkanwordsDataManager().getProgressOfSetInfo(isDownloading, this.listType));
                }
            } else {
                button.setVisibility(View.VISIBLE);
                textView4.setVisibility(View.GONE);
                button.setOnClickListener((View.OnClickListener) new SetsAdapter1(this, isDownloading, view2));
            }
            view2.setClickable(isDownloading.hasDownloadedFiles() ^ true);
            this.updateProgressView(isDownloading);
        }

        public void downloadTodayIssue() {
            for (final SkanwordsSetInfo skanwordsSetInfo : this.objectsList) {
                if (skanwordsSetInfo.isToday() && !skanwordsSetInfo.hasDownloadedFiles()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(skanwordsSetInfo.getDisplayName());
                    sb.append(" downloadTodayIssue setinfo  ");
                    sb.append(skanwordsSetInfo.isToday());
                    Log.v("", sb.toString());
                    this.itemAction.clickDownload(skanwordsSetInfo, (View) this.setsRowViews.get(skanwordsSetInfo.getId()));
                    return;
                }
            }
            Log.v("", "downloadTodayIssue  not found setinfo");
        }

        public Object getItemInSectionAndRow(final int n, final int n2) {
            return null;
        }

        public SkanwordsDataManager.TaskListType getListType() {
            return this.listType;
        }

        protected int getNumberOfRowsInSection(int i) {
            if (getNumberOfSections() > 1 && i == 0) {
                return 1;
            }
            i = this.objectsList.size();
            if (getNumberOfSections() > 1 && this.listType == TaskListType.ISSUE_TASKS) {
                i--;
            }
//            return (/*(i + this.hasOneList) + */isPageAvailable(true)) + isPageAvailable(false);
            return 1;
        }

        protected int getNumberOfSections() {
            if ((MainDataManager.getInstance().getSkanwordsDataManager().getTomorrowSet() != null && this.listType == SkanwordsDataManager.TaskListType.ISSUE_TASKS) || (MainDataManager.getInstance().getSkanwordsDataManager().getLastPlayedSkanword() != null && this.listType == SkanwordsDataManager.TaskListType.STARTED_TASKS)) {
                return 2;
            }
            return 1;
        }

        protected int getNumberOfViewTypeForRows() {
            return 6;
        }

        public List<SkanwordsSetInfo> getObjectsList() {
            return (List<SkanwordsSetInfo>) this.objectsList;
        }

        public SkanwordsSetInfo getScanwordSetInfoForRowAndSection(int i, int i2) {
            i2 = 0;
//            boolean i3 = (i - this.hasOneList) - isPageAvailable(false);
            if (getViewTypeForRowInSection(0, 0) == 4) {
                i2 = 1;
            }
            return (SkanwordsSetInfo) super.getItem(i + i2);
        }

        public View getSetRowView(final SkanwordsSetInfo skanwordsSetInfo) {
            return this.setsRowViews.get(skanwordsSetInfo.getId());
        }

        protected int getViewTypeForRowInSection(final int n, final int n2) {
            if (this.getNumberOfSections() > 1 && n == 0 && this.listType == SkanwordsDataManager.TaskListType.STARTED_TASKS) {
                return 5;
            }
            if (this.getNumberOfSections() > 1 && n == 0 && this.listType == SkanwordsDataManager.TaskListType.ISSUE_TASKS) {
                return 4;
            }
            if (this.isPageAvailable(false) && n2 == 0) {
                return 2;
            }
            if ((n2 == 0 && !this.isPageAvailable(false) && this.hasOneList) || (this.isPageAvailable(false) && n2 == 1 && this.hasOneList)) {
                return 3;
            }
            if (this.isPageAvailable(true) && n2 == this.getNumberOfRowsInSection(n) - 1) {
                return 2;
            }
            return 1;
        }

        public boolean isBuildForVip() {
            return this.buildForVip;
        }

        public boolean isEnabled(final int n) {
            return this.getItemViewType(n) == 1 || this.getItemViewType(n) == 5 || this.getItemViewType(n) == 3 || this.getItemViewType(n) == 2;
        }

        protected View newRowViewInSection(final Context context, int viewTypeForRowInSection, final int n, final ViewGroup viewGroup) {
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            viewTypeForRowInSection = this.getViewTypeForRowInSection(viewTypeForRowInSection, n);
            if (viewTypeForRowInSection == 4) {
                return from.inflate(R.layout.set_timer_row, viewGroup, false);
            }
            if (viewTypeForRowInSection == 5) {
                return from.inflate(R.layout.scanword_row_item, viewGroup, false);
            }
            if (viewTypeForRowInSection == 3) {
                return from.inflate(R.layout.one_list_row_item, viewGroup, false);
            }
            if (viewTypeForRowInSection == 2) {
                return from.inflate(R.layout.other_page_row_layout, viewGroup, false);
            }
            return from.inflate(this.resourceId, viewGroup, false);
        }

        protected View newSectionView(final Context context, final int n, final ViewGroup viewGroup) {
            if (this.getNumberOfSections() > 1 && n == 0 && this.getViewTypeForRowInSection(n, 0) == 4) {
                return new View(this.getContext());
            }
            return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header, viewGroup, false);
        }

        public void notifyDataSetInvalidated() {
            this.buildForVip = MainDataManager.getInstance().getUserData().isVip();
            super.notifyDataSetInvalidated();
        }

        public boolean oneList() {
            if (this.listType == SkanwordsDataManager.TaskListType.ISSUE_TASKS) {
                return false;
            }
            final Iterator<SkanwordsSetInfo> iterator = this.objectsList.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                if (iterator.next().hasDownloadedFiles() && ++n > 1) {
                    return true;
                }
            }
            return false;
        }

        public void remove(final SkanwordsSetInfo skanwordsSetInfo) {
            this.objectsList.remove(skanwordsSetInfo);
            this.notifyDataSetChanged();
        }

        public void setObjectsList(final List<SkanwordsSetInfo> objectsList) {
            this.objectsList = objectsList;
        }

        public void updateProgressView(final SkanwordsSetInfo isDownloading) {
            final View view = this.setsRowViews.get(isDownloading.getId());
            if (view == null) {
                return;
            }
            final TextView textView = (TextView) view.findViewById(R.id.setLabel);
            final boolean setIsDownloading = MainDataManager.getInstance().setIsDownloading(isDownloading);
            final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            if (!textView.getText().equals(isDownloading.getDisplayName())) {
                return;
            }
            final Button button = (Button) view.findViewById(R.id.downloadButton);
            final TextView textView2 = (TextView) view.findViewById(R.id.setProgress);
            if (setIsDownloading) {
                progressBar.setVisibility(View.VISIBLE);
                button.setVisibility(View.GONE);
                textView2.setVisibility(View.GONE);
                final int setDownloadingProgress = MainDataManager.getInstance().getSetDownloadingProgress(isDownloading.getId());
                progressBar.setMax(100);
                progressBar.setProgress(setDownloadingProgress);
                return;
            }
            progressBar.setVisibility(View.GONE);
            if (!isDownloading.hasDownloadedFiles()) {
                button.setVisibility(View.VISIBLE);
            }
        }

        public void updateVisibleRows(final ListView listView) {
            final int firstVisiblePosition = listView.getFirstVisiblePosition();
            for (int lastVisiblePosition = listView.getLastVisiblePosition(), i = firstVisiblePosition; i < lastVisiblePosition; ++i) {
                listView.getAdapter().getView(i, listView.getChildAt(i - firstVisiblePosition), (ViewGroup) listView);
            }
            Log.v("", " updateVisibleRowupdateVisibleRows ");
        }
    }

    static class SetsAdapter1 implements View.OnClickListener {
        final /* synthetic */ SetsAdapter this0;
        final /* synthetic */ View valconvertView;
        final /* synthetic */ SkanwordsSetInfo valinfo;

        SetsAdapter1(final SetsAdapter this0, final SkanwordsSetInfo valinfo, final View valconvertView) {
            super();
            this.this0 = this0;
            this.valinfo = valinfo;
            this.valconvertView = valconvertView;
        }

        public void onClick(final View view) {
            if (MainNetworkManager.getInstance().hasNetworkConnection((Activity) SetsAdapter.access1000(this.this0))) {
                SetsAdapter.access1100(this.this0).clickDownload(this.valinfo, this.valconvertView);
            }
        }
    }

    class SkanwordsIssuesList1 implements ItemAction {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList1(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void clickDownload(final SkanwordsSetInfo setInfo, final View view) {
            if (TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_START) {
                TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_ISSUE_DOWNLOADING);
            }
            SmappsScanwords.getEventsDispatcher().dispatchEvent((Event) new SetDownloadEvent(this.this0, "EVENT_SET_DOWNLOAD_PROGRESS").setSetInfo(setInfo).setProgress(0));
            MainDataManager.getInstance().downloadSetWithId((Activity) this.this0.getActivity(), setInfo, (MainDataManager.IDowloadSetProgress) new SkanwordsIssuesList12(this, setInfo));
        }

        public void clickItem() {
            final Intent intent = new Intent((Context) this.this0.getActivity(), (Class) SkanwordsTasksList.class);
            intent.putExtra("LIST_TYPE", (Serializable) SkanwordsIssuesList.access100(this.this0).getListType());
            intent.putExtra("SKANWORDS_COUNT_OVER_LIMIT", true);
            String s;
            if (SkanwordsIssuesList.access100(this.this0).getListType() == SkanwordsDataManager.TaskListType.STARTED_TASKS) {
                s = "Начатые";
            } else {
                s = "Решенные";
            }
            intent.putExtra("LIST_TITLE", s);
            this.this0.getActivity().startActivityForResult(intent, 105);
            if (this.this0.getListType() == SkanwordsDataManager.TaskListType.STARTED_TASKS) {
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_begun_onelist_enter");
                return;
            }
            if (this.this0.getListType() == SkanwordsDataManager.TaskListType.FINISHED_TASKS) {
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_solved_onelist_enter");
            }
        }

        public void clickItem(final SkanwordsSetInfo skanwordsSetInfo) {
            if (skanwordsSetInfo != null) {
                if (!skanwordsSetInfo.hasDownloadedFiles()) {
                    return;
                }
                Log.v("", " scanword Issue ScanwordsIssuesList 105");
                final Intent intent = new Intent((Context) this.this0.getActivity(), (Class) SkanwordsTasksList.class);
                intent.putExtra("LIST_TYPE", (Serializable) SkanwordsDataManager.TaskListType.ISSUE_TASKS);
                intent.putExtra("ISSUE_ID", (Serializable) skanwordsSetInfo.getId());
                final StringBuilder sb = new StringBuilder();
                sb.append("Выпуск от ");
                sb.append(skanwordsSetInfo.getName());
                intent.putExtra("LIST_TITLE", sb.toString());
                this.this0.getActivity().startActivityForResult(intent, 105);
                if (this.this0.getListType() == SkanwordsDataManager.TaskListType.ISSUE_TASKS) {
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_new_issue_enter");
                    return;
                }
                if (this.this0.getListType() == SkanwordsDataManager.TaskListType.STARTED_TASKS) {
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_begun_issue_enter");
                    return;
                }
                if (this.this0.getListType() == SkanwordsDataManager.TaskListType.FINISHED_TASKS) {
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "menu_scans_solved_issue_enter");
                }
            }
        }

        public void requestPage(final boolean b) {
            this.this0.showDownloadingState(true);
            final MainNetworkManager instance = MainNetworkManager.getInstance();
            final SkanwordsDataManager.TaskListType access000 = SkanwordsIssuesList.access000(this.this0);
            SkanwordsDataManager.TaskListDirrectionType skanwordsDataManagerTaskListDirrectionType;
            if (b) {
                skanwordsDataManagerTaskListDirrectionType = SkanwordsDataManager.TaskListDirrectionType.NEXT_DIRRECTION;
            } else {
                skanwordsDataManagerTaskListDirrectionType = SkanwordsDataManager.TaskListDirrectionType.PREVIOUS_DIRRECTION;
            }
            instance.requestIssuesListExecWithCompleteBlock(access000, skanwordsDataManagerTaskListDirrectionType, (MainNetworkManager.RequestCompletionBlock) new SkanwordsIssuesList1.SkanwordsIssuesList11(this), (Activity) this.this0.getActivity());
        }

        class SkanwordsIssuesList12 implements MainDataManager.IDowloadSetProgress {
            final /* synthetic */ SkanwordsIssuesList1 this1;
            final /* synthetic */ SkanwordsSetInfo valscanwordsSetInfo;

            SkanwordsIssuesList12(final SkanwordsIssuesList1 this1, final SkanwordsSetInfo valscanwordsSetInfo) {
                super();
                this.this1 = this1;
                this.valscanwordsSetInfo = valscanwordsSetInfo;
            }

            public void onProgress(final int progress) {
                SmappsScanwords.getEventsDispatcher().dispatchEvent((Event) new SetDownloadEvent(this.this1.this0, "EVENT_SET_DOWNLOAD_PROGRESS").setSetInfo(this.valscanwordsSetInfo).setProgress(progress));
            }
        }

        class SkanwordsIssuesList11 implements MainNetworkManager.RequestCompletionBlock {
            final /* synthetic */ SkanwordsIssuesList1 this1;

            SkanwordsIssuesList11(final SkanwordsIssuesList1 this1) {
                super();
                this.this1 = this1;
            }

            public void complete(final boolean b) {
            }
        }
    }

    public class SetDownloadEvent extends SimpleEvent {
        private SkanwordsSetInfo mSetInfo;
        private int progress;
        final /* synthetic */ SkanwordsIssuesList this0;

        public SetDownloadEvent(final SkanwordsIssuesList this0, final String s) {
            super(s);
            this.this0 = this0;
            this.mSetInfo = null;
            this.progress = -1;
        }

        public int getProgress() {
            return this.progress;
        }

        public SkanwordsSetInfo getSetInfo() {
            return this.mSetInfo;
        }

        public SetDownloadEvent setProgress(final int progress) {
            this.progress = progress;
            return this;
        }

        public SetDownloadEvent setSetInfo(final SkanwordsSetInfo mSetInfo) {
            this.mSetInfo = mSetInfo;
            return this;
        }
    }


    class SkanwordsIssuesList2 implements EventListener {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList2(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            final SkanwordsDataManager.SkanwordsSetsUpdateEvent skanwordsDataManagerSkanwordsSetsUpdateEvent = (SkanwordsDataManager.SkanwordsSetsUpdateEvent) event;
            if (skanwordsDataManagerSkanwordsSetsUpdateEvent.getTaskListType() == SkanwordsIssuesList.access000(this.this0) || (skanwordsDataManagerSkanwordsSetsUpdateEvent.getTaskListType() == SkanwordsDataManager.TaskListType.ALL_TASKS && SkanwordsIssuesList.access200(this.this0))) {
                this.this0.update(skanwordsDataManagerSkanwordsSetsUpdateEvent.scrollToTop());
            }
        }
    }

    class SkanwordsIssuesList3 implements EventListener {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList3(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.downloadTodayIssue();
        }
    }

    class SkanwordsIssuesList4 implements EventListener {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList4(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            final SetDownloadEvent skanwordsIssuesListSetDownloadEvent = (SetDownloadEvent) event;
            SkanwordsIssuesList.access300(this.this0, skanwordsIssuesListSetDownloadEvent.getSetInfo(), skanwordsIssuesListSetDownloadEvent.getProgress());
        }
    }

    class SkanwordsTasksList11 implements AdapterView.OnItemClickListener {
        final /* synthetic */ SkanwordsTasksList this0;

        SkanwordsTasksList11(final SkanwordsTasksList this0) {
            super();
            this.this0 = this0;
        }

        public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
            if (SkanwordsTasksList.access300(this.this0).getItemViewType(n) == 2) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context) this.this0, R.style.Scanword_style_dialog);
                alertDialogBuilder.setMessage("Выпуск будет удален с сохранением прогресса. Вы всегда можете закачать выпуск заново и продолжить игру.").setPositiveButton("Удалить", (DialogInterface.OnClickListener) new SkanwordsTasksList11.SkanwordsTasksList111(this)).setNegativeButton("Отмена", (DialogInterface.OnClickListener) null);
                alertDialogBuilder.create().show();
                return;
            }
            if (!MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordFree(Integer.valueOf(SkanwordsTasksList.SkanwordsTasksAdapter.access900(SkanwordsTasksList.access300(this.this0), n).getId())) && !MainDataManager.getInstance().getUserData().isVip()) {
                final SkanwordsTasksList this0 = this.this0;
                SkanwordsTasksList.access1100(this0, SkanwordsTasksList.SkanwordsTasksAdapter.access900(SkanwordsTasksList.access300(this0), n));
                return;
            }
            final SkanwordsTasksList this2 = this.this0;
            SkanwordsTasksList.access1000(this2, SkanwordsTasksList.SkanwordsTasksAdapter.access900(SkanwordsTasksList.access300(this2), n));
        }

        class SkanwordsTasksList111 implements DialogInterface.OnClickListener {
            final /* synthetic */ SkanwordsTasksList11 this1;

            SkanwordsTasksList111(final SkanwordsTasksList11 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
                MainDataManager.getInstance().getSkanwordsDataManager().deleteIssuesWithId((int) SkanwordsTasksList.access100(this.this1.this0));
                this.this1.this0.finish();
            }
        }
    }

    class SkanwordsIssuesList11 implements AdapterView.OnItemClickListener {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList11(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
            final int itemViewType = SkanwordsIssuesList.access100(this.this0).getItemViewType(n);
            boolean b = true;
            if (itemViewType == 2) {
                final ItemAction access700 = SkanwordsIssuesList.access700(this.this0);
                if (n != SkanwordsIssuesList.access100(this.this0).getCount() - 1) {
                    b = false;
                }
                access700.requestPage(b);
                return;
            }
            if (SkanwordsIssuesList.access100(this.this0).getItemViewType(n) == 3) {
                SkanwordsIssuesList.access700(this.this0).clickItem();
                return;
            }
            if (SkanwordsIssuesList.access100(this.this0).getItemViewType(n) == 1) {
                SkanwordsIssuesList.access700(this.this0).clickItem(SetsAdapter.access800(SkanwordsIssuesList.access100(this.this0), n));
                return;
            }
            if (SkanwordsIssuesList.access100(this.this0).getItemViewType(n) == 5) {
                final Skanword lastPlayedSkanword = MainDataManager.getInstance().getSkanwordsDataManager().getLastPlayedSkanword();
                if (!MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordFree(Integer.valueOf(lastPlayedSkanword.getId())) && !MainDataManager.getInstance().getUserData().isVip()) {
                    MainMenuActivity.openLockedTaskactivity((Activity) this.this0.getActivity(), lastPlayedSkanword);
                    return;
                }
                final Intent intent = new Intent("android.intent.action.EDIT", (Uri) null, (Context) this.this0.getActivity(), (Class) SkanwordGameActivity.class);
                intent.putExtra("cross_id", lastPlayedSkanword.getId());
                this.this0.getActivity().startActivityForResult(intent, 101);
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "playscan_scanword_enter");
                if (MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordOffered(Integer.valueOf(lastPlayedSkanword.getId()))) {
                    SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_offeredscan_opened");
                }
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_begun_thelast");
                UserUpdatesManager.getInstance().saveTime();
                MainNetworkManager.getInstance().requestSyncExec();
            }
        }
    }

    class SkanwordsIssuesList5 implements Runnable {
        final /* synthetic */ SkanwordsIssuesList this0;
        final /* synthetic */ SkanwordsSetInfo valscanwordsSetInfo;

        SkanwordsIssuesList5(final SkanwordsIssuesList this0, final SkanwordsSetInfo valscanwordsSetInfo) {
            super();
            this.this0 = this0;
            this.valscanwordsSetInfo = valscanwordsSetInfo;
        }

        @Override
        public void run() {
            Log.v("", " set downloaded 2");
            if (SkanwordsIssuesList.access100(this.this0).hasOneList != SkanwordsIssuesList.access100(this.this0).oneList()) {
                this.this0.update(false);
                return;
            }
            if (SkanwordsIssuesList.access400() == 2 && this.this0.mSetsInfos != null) {
                this.this0.mSetsInfos.remove(this.valscanwordsSetInfo);
                SkanwordsIssuesList.access100(this.this0).remove(this.valscanwordsSetInfo);
                Log.v("", " set downloaded 3");
            } else if (SkanwordsIssuesList.access400() == 1 && this.this0.mSetsInfos != null) {
                this.this0.mSetsInfos.add(this.valscanwordsSetInfo);
                SkanwordsIssuesList.access100(this.this0).notifyDataSetChanged();
            }
            if (this.this0.mSetsInfos.size() == 0) {
                this.this0.update(false);
                return;
            }
            Log.v("", " set downloaded  notifyDataSetChanged");
            SkanwordsIssuesList.access100(this.this0).notifyDataSetChanged();
        }
    }

    class SkanwordsIssuesList6 implements Runnable {
        final /* synthetic */ SkanwordsIssuesList this0;
        final /* synthetic */ TextView valnameView;
        final /* synthetic */ int valprogress;
        final /* synthetic */ View valrowView;
        final /* synthetic */ SkanwordsSetInfo valscanwordsSetInfo;

        SkanwordsIssuesList6(final SkanwordsIssuesList this0, final TextView valnameView, final SkanwordsSetInfo valscanwordsSetInfo, final int valprogress, final View valrowView) {
            super();
            this.this0 = this0;
            this.valnameView = valnameView;
            this.valscanwordsSetInfo = valscanwordsSetInfo;
            this.valprogress = valprogress;
            this.valrowView = valrowView;
        }

        @Override
        public void run() {
            final TextView valnameView = this.valnameView;
            if (valnameView != null && valnameView.getText().equals(this.valscanwordsSetInfo.getDisplayName())) {
                final int valprogress = this.valprogress;
                if (valprogress == 100 || valprogress == -1) {
                    SkanwordsIssuesList.access100(this.this0).buildRowScanwordSetInfo(this.valrowView, this.valscanwordsSetInfo);
                    return;
                }
            }
            SkanwordsIssuesList.access100(this.this0).updateProgressView(this.valscanwordsSetInfo);
        }
    }

    class SkanwordsIssuesList8 implements EventListener {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList8(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            if (this.this0.getActivity() != null) {
                this.this0.getActivity().runOnUiThread((Runnable) new SkanwordsIssuesList8.SkanwordsIssuesList81(this, MainDataManager.getInstance().getSkanwordsDataManager().timeTillTomorrowIssue()));
            }
        }

        class SkanwordsIssuesList81 implements Runnable {
            final /* synthetic */ SkanwordsIssuesList8 this1;
            final /* synthetic */ long valtimeLeft;

            SkanwordsIssuesList81(final SkanwordsIssuesList8 this1, final long valtimeLeft) {
                super();
                this.this1 = this1;
                this.valtimeLeft = valtimeLeft;
            }

            @Override
            public void run() {
                if (SkanwordsIssuesList.access100(this.this1.this0) == null) {
                    return;
                }
                if (SkanwordsIssuesList.access100(this.this1.this0).getNumberOfSections() > 1) {
                    final View child = SkanwordsIssuesList.access500(this.this1.this0).getChildAt(1 - SkanwordsIssuesList.access500(this.this1.this0).getFirstVisiblePosition());
                    if (child == null) {
                        return;
                    }
                    if (MainDataManager.getInstance().getSkanwordsDataManager().getTomorrowSet() == null) {
                        return;
                    }
                    final TextView textView = (TextView) child.findViewById(R.id.timer);
                    if (textView != null) {
                        final long valtimeLeft = this.valtimeLeft;
                        String secondsToTimerType;
                        if (valtimeLeft > 0L) {
                            secondsToTimerType = Utils.secondsToTimerType(valtimeLeft);
                        } else {
                            secondsToTimerType = "";
                        }
                        textView.setText((CharSequence) secondsToTimerType);
                    }
                    final TextView textView2 = (TextView) child.findViewById(R.id.setLabel);
                    if (textView2 != null) {
                        String text;
                        if (this.valtimeLeft > 0L) {
                            text = "Новый выпуск через:";
                        } else {
                            text = "Выпуск готов! Обновите список.";
                        }
                        textView2.setText((CharSequence) text);
                    }
                }
                if (SkanwordsIssuesList.access100(this.this1.this0).isBuildForVip() != MainDataManager.getInstance().getUserData().isVip()) {
                    SkanwordsIssuesList.access100(this.this1.this0).notifyDataSetInvalidated();
                }
            }
        }
    }

    class SkanwordsIssuesList7 implements Runnable {
        final /* synthetic */ SkanwordsIssuesList this0;
        final /* synthetic */ boolean valshow;

        SkanwordsIssuesList7(final SkanwordsIssuesList this0, final boolean valshow) {
            super();
            this.this0 = this0;
            this.valshow = valshow;
        }

        @Override
        public void run() {
            if (this.this0.getView() != null) {
                final View viewById = this.this0.getView().findViewById(R.id.loadingSpinner);
                int visibility;
                if (this.valshow) {
                    visibility = 0;
                } else {
                    visibility = 8;
                }
                viewById.setVisibility(visibility);
            }
        }
    }

    class SkanwordsIssuesList10 implements DialogInterface.OnClickListener {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList10(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onClick change sort type dialog ");
            sb.append(n);
            Log.v("SkanwordsFunc", sb.toString());
            SkanwordsIssuesList.access402(n);
            SkanwordsIssuesList.access602(this.this0, true);
        }
    }

    class SkanwordsIssuesList9 implements DialogInterface.OnClickListener {
        final /* synthetic */ SkanwordsIssuesList this0;

        SkanwordsIssuesList9(final SkanwordsIssuesList this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onClick accept sort type dialog ");
            sb.append(SkanwordsIssuesList.access600(this.this0));
            Log.v("SkanwordsFunc", sb.toString());
            if (SkanwordsIssuesList.access600(this.this0)) {
                SmappsScanwords.getEventsDispatcher().dispatchEvent((Event) new SimpleEvent("UPDATE_ISSUES_LIST"));
            }
            SkanwordsIssuesList.access602(this.this0, false);
            this.this0.setEmptyListCaption((TextView) this.this0.getView().findViewById(R.id.emptyIssuesListLabel));
        }
    }
}