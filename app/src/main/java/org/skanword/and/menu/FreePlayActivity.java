package org.skanword.and.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.etc.TopActivityManager.TopActivity;

public class FreePlayActivity extends Activity implements TopActivityManager.TopActivity
{
    public static final int CLOSE_ACTIVITY_RESULT = 500;

    public FreePlayActivity() {
        super();
    }

    static /* synthetic */ void access000(final FreePlayActivity freePlayActivity) {
        freePlayActivity.closeToMainPage();
    }

    private void closeToMainPage() {
        this.finish();
    }

    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(R.layout.free_play_activity_layout);
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle(R.string.play_free_activity_title);
        this.getWindow().setSoftInputMode(3);
        ((Button)this.findViewById(R.id.premiumButton)).setOnClickListener((View.OnClickListener)new FreePlayActivity1(this));
        ((LinearLayout)this.findViewById(R.id.toScanwordsButton)).setOnClickListener((View.OnClickListener)new FreePlayActivity2(this));
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.setResult(0, (Intent)null);
        this.finish();
        return true;
    }

    class FreePlayActivity1 implements View.OnClickListener {
        final /* synthetic */ FreePlayActivity this0;

        FreePlayActivity1(final FreePlayActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_iwantfree_gopremium");
            this.this0.setResult(100);
            FreePlayActivity.access000(this.this0);
        }
    }

    class FreePlayActivity2 implements View.OnClickListener {
        final /* synthetic */ FreePlayActivity this0;

        FreePlayActivity2(final FreePlayActivity this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_menu_scans_iwantfree_goissues");
            this.this0.setResult(-1);
            FreePlayActivity.access000(this.this0);
        }
    }
}
