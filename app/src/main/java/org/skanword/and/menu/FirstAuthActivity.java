package org.skanword.and.menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;

public class FirstAuthActivity extends Activity
{
    public FirstAuthActivity() {
        super();
    }

    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(R.layout.first_auth_activity_layout);
        final FirstAuthActivity1 firstAuthActivity1 = new FirstAuthActivity.FirstAuthActivity1(this, (Activity)this);
        ((Button)this.findViewById(R.id.okAuthButton)).setOnClickListener((View.OnClickListener)firstAuthActivity1);
        ((Button)this.findViewById(R.id.fbAuthButton)).setOnClickListener((View.OnClickListener)firstAuthActivity1);
        ((Button)this.findViewById(R.id.vkAuthButton)).setOnClickListener((View.OnClickListener)firstAuthActivity1);
        ((Button)this.findViewById(R.id.cancelAuthButton)).setOnClickListener((View.OnClickListener)firstAuthActivity1);
    }

    class FirstAuthActivity1 implements View.OnClickListener {
        final /* synthetic */ FirstAuthActivity this0;
        final /* synthetic */ Activity valactivity;

        FirstAuthActivity1(final FirstAuthActivity this0, final Activity valactivity) {
            super();
            this.this0 = this0;
            this.valactivity = valactivity;
        }

        public void onClick(final View view) {
            int result;
            if (view.getId() == R.id.fbAuthButton) {
                result = 1;
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_popup_login_fb");
            }
            else if (view.getId() == R.id.okAuthButton) {
                result = 2;
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_popup_login_ok");
            }
            else if (view.getId() == R.id.vkAuthButton) {
                result = 3;
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_popup_login_vk");
            }
            else if (view.getId() == R.id.cancelAuthButton) {
                result = 4;
                SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.USER_ACTION, "click_popup_login_later");
            }
            else {
                result = 0;
            }
            this.valactivity.setResult(result);
            this.valactivity.finish();
        }
    }
}
