package org.skanword.and.menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import org.skanword.and.libs.qustom.QustomDialogBuilder;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.etc.Utils;

public class HintForVideoDialog extends QustomDialogBuilder
{
    private static HintForVideoDialogDelegate mDelegate;
    private static AlertDialog mDialog;
    private View mDialogView;

    public HintForVideoDialog(final Context context) {
        super(context);
        this.mDialogView = null;
    }

    public HintForVideoDialog(final Context context, final int n) {
        super(context);
        this.mDialogView = null;
    }

    static /* synthetic */ HintForVideoDialog.HintForVideoDialogDelegate access000() {
        return HintForVideoDialog.mDelegate;
    }

    static /* synthetic */ HintForVideoDialogDelegate access002(final HintForVideoDialogDelegate mDelegate) {
        return HintForVideoDialog.mDelegate = mDelegate;
    }

    static /* synthetic */ AlertDialog access102(final AlertDialog mDialog) {
        return HintForVideoDialog.mDialog = mDialog;
    }

    public static AlertDialog getDialog() {
        return HintForVideoDialog.mDialog;
    }

    public static boolean isShowing() {
        return getDialog() != null && getDialog().isShowing();
    }

    public static void showCompleteWatchDialogInContext(final Context context) {
        final View inflate = LayoutInflater.from(context).inflate(R.layout.bonus_complete_dialog_view, (ViewGroup)null);
        final HintForVideoDialog hintForVideoDialog = new HintForVideoDialog(context, R.style.Scanword_style_dialog);
        hintForVideoDialog.setCustomView(1, inflate.getContext());
        final AlertDialog show = hintForVideoDialog.show();
        hintForVideoDialog.setDialogView(inflate);
        hintForVideoDialog.setDialog(show);
        ((TextView)inflate.findViewById(R.id.headLabel)).setText("Поздравляем!");
        inflate.findViewById(R.id.simpleHintsCountsArea).setVisibility(View.VISIBLE);
        final TextView textView = (TextView)inflate.findViewById(R.id.hintsReceivedLabel);
        final StringBuilder sb = new StringBuilder();
        sb.append("Вам начислено ");
        sb.append(1);
        sb.append(" ");
        sb.append(Utils.spellVariantForNumber(1, "подсказку", "подсказки", "подсказок"));
        textView.setText((CharSequence)sb.toString());
        inflate.findViewById(R.id.dev1).setVisibility(View.GONE);
        inflate.findViewById(R.id.notificationArea).setVisibility(View.GONE);
        inflate.findViewById(R.id.closeButton).setOnClickListener((View.OnClickListener)new HintForVideoDialog4(show));
    }

    public static void showsDialogInContext(final Context context, final HintForVideoDialog.HintForVideoDialogDelegate mDelegate) {
        final View inflate = LayoutInflater.from(context).inflate(R.layout.hint_for_video_dialog_view, (ViewGroup)null);
        HintForVideoDialog.mDelegate = mDelegate;
        final HintForVideoDialog hintForVideoDialog = new HintForVideoDialog(context, R.style.Scanword_style_dialog);
        hintForVideoDialog.setCustomView(1, inflate.getContext());
        final AlertDialog show = hintForVideoDialog.show();
        hintForVideoDialog.setDialogView(inflate);
        hintForVideoDialog.setDialog(show);
        SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hfv_dialog_show");
        inflate.findViewById(R.id.closeButton).setOnClickListener((View.OnClickListener)new HintForVideoDialog1(show));
        inflate.findViewById(R.id.watchButton).setOnClickListener((View.OnClickListener)new HintForVideoDialog2(show));
        show.setOnCancelListener((DialogInterface.OnCancelListener)new HintForVideoDialog3());
    }

    public View getDialogView() {
        return this.mDialogView;
    }

//    protected int getLayoutresId() {
//        this.layoutResId = R.layout.custom_dialog_layout;
//        return super.getLayoutresId();
//    }

    public void setDialog(final AlertDialog mDialog) {
        HintForVideoDialog.mDialog = mDialog;
    }

    public void setDialogView(final View mDialogView) {
        this.mDialogView = mDialogView;
    }

    public interface HintForVideoDialogDelegate
    {
        void onCancel();

        void onWatch();
    }

    static final class HintForVideoDialog4 implements View.OnClickListener {
        final /* synthetic */ AlertDialog valdialog;

        HintForVideoDialog4(final AlertDialog valdialog) {
            super();
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            HintForVideoDialog.access102((AlertDialog)null);
            this.valdialog.cancel();
        }
    }

    static final class HintForVideoDialog1 implements View.OnClickListener {
        final /* synthetic */ AlertDialog valdialog;

        HintForVideoDialog1(final AlertDialog valdialog) {
            super();
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            if (HintForVideoDialog.access000() != null) {
                HintForVideoDialog.access000().onCancel();
            }
            HintForVideoDialog.access102((AlertDialog)null);
            HintForVideoDialog.access002((HintForVideoDialogDelegate)null);
            this.valdialog.cancel();
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hfv_dialog_close");
        }
    }


    static final class HintsBonusDialog2 implements View.OnClickListener {
        final /* synthetic */ AlertDialog valdialog;

        HintsBonusDialog2(final AlertDialog valdialog) {
            super();
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            HintForVideoDialog.access002(null);
            this.valdialog.cancel();
        }
    }

    static final class HintForVideoDialog2 implements View.OnClickListener {
        final /* synthetic */ AlertDialog valdialog;

        HintForVideoDialog2(final AlertDialog valdialog) {
            super();
            this.valdialog = valdialog;
        }

        public void onClick(final View view) {
            if (HintForVideoDialog.access000() != null) {
                HintForVideoDialog.access000().onWatch();
            }
            HintForVideoDialog.access102((AlertDialog)null);
            HintForVideoDialog.access002((HintForVideoDialogDelegate)null);
            this.valdialog.cancel();
            SmappsScanwords.sendGAEvent(SmappsScanwords.ActionType.APP_ACTION, "hfv_dialog_watch");
        }
    }

    static final class HintForVideoDialog3 implements DialogInterface.OnCancelListener {
        HintForVideoDialog3() {
            super();
        }

        public void onCancel(final DialogInterface dialogInterface) {
            if (HintForVideoDialog.access000() != null) {
                HintForVideoDialog.access000().onCancel();
            }
            HintForVideoDialog.access102((AlertDialog)null);
            HintForVideoDialog.access002((HintForVideoDialogDelegate)null);
            SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hfv_dialog_canceled");
        }
    }
}
