package org.skanword.and.menu.optionsmenu;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import org.skanword.and.datamanager.OptionsData;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;
import org.skanword.and.libs.qustom.QustomDialogBuilder;
import com.sbstrm.appirater.Appirater;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;

public class OptionsActivity extends FragmentActivity
{
    public static final int TYPE_ROW_CHECKBOX_VIEW = 2;
    public static final int TYPE_ROW_RADIO_VIEW = 3;
    public static final int TYPE_ROW_TUGGLE_VIEW = 4;
    private final FragmentActivity mActivity;
    private OptionsActivity.OptionsItemsAdapter mAdapter;
    private ListView mList;

    public OptionsActivity() {
        super();
        this.mActivity = this;
    }

    static /* synthetic */ OptionsActivity.OptionsItemsAdapter access300(final OptionsActivity optionsActivity) {
        return optionsActivity.mAdapter;
    }

    static /* synthetic */ FragmentActivity access400(final OptionsActivity optionsActivity) {
        return optionsActivity.mActivity;
    }

    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(R.layout.activity_options);
        this.getActionBar().setDisplayHomeAsUpEnabled(true);
        this.mList = (ListView)this.findViewById(R.id.options_list);
        this.mAdapter = new OptionsActivity.OptionsItemsAdapter((Context)this, R.layout.shop_item_row, (Activity)this.mActivity);
        this.mList.setAdapter((ListAdapter)this.mAdapter);
        this.mList.setOnItemClickListener((AdapterView.OnItemClickListener)new OptionsItemClickListener(this, (OptionsActivity.OptionsActivity1)null));
        this.mList.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.home) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.finish();
        return true;
    }

    // ------ Classes

    public static class OptionsItemsAdapter extends ArraySectionedListAdapter<String>
    {
        private Activity mActivity;
        private Map<View, Integer> radioButtonsGroup;

        public OptionsItemsAdapter(final Context context, final int n, final Activity mActivity) {
            super(context, n);
            this.radioButtonsGroup = new HashMap();
            this.mActivity = mActivity;
        }

        static /* synthetic */ void access100(final OptionsItemsAdapter optionsItemsAdapter, final int n, final int n2, final boolean b) {
            optionsItemsAdapter.optionClicked(n, n2, b);
        }

        static /* synthetic */ Activity access200(final OptionsItemsAdapter optionsItemsAdapter) {
            return optionsItemsAdapter.mActivity;
        }

        private void bindConnectionSection(final View view, final int n) {
            final TextView textView = (TextView)view.findViewById(R.id.optionName);
            final CheckBox checkBox = (CheckBox)view.findViewById(R.id.checkBox1);
            if (n == 0) {
                textView.setText("Загружать свежие выпуски автоматически");
                checkBox.setChecked(MainDataManager.getInstance().getOptions().isAutoDownload());
                return;
            }
            textView.setText("Использовать только WiFi");
            checkBox.setChecked(MainDataManager.getInstance().getOptions().isOnlyWifi());
        }

        private void bindInfoSection(final View view, final int n) {
            final TextView textView = (TextView)view.findViewById(R.id.optionName);
            if (n == 0) {
                textView.setText("О программе");
                return;
            }
            textView.setText("Оставить отзыв на Google Play");
        }

        private void bindInformSection(final View view, final int n) {
            final TextView textView = (TextView)view.findViewById(R.id.optionName);
            final RadioButton radioButton = (RadioButton)view.findViewById(R.id.radioButton1);
            if (radioButton != null) {
                this.radioButtonsGroup.put(radioButton, 1);
                if ((MainDataManager.getInstance().getOptions().isDefaultNotificationsTime() && n == 1) || (!MainDataManager.getInstance().getOptions().isDefaultNotificationsTime() && n == 2)) {
                    radioButton.setChecked(true);
                }
                radioButton.setOnClickListener((View.OnClickListener)new OptionsItemsAdapter4(this, radioButton, n));
            }
            if (n == 0) {
                textView.setText("Уведомлять о новых выпусках");
                ((Switch)view.findViewById(R.id.switchButton1)).setChecked(MainDataManager.getInstance().getOptions().isInformNewIssues());
                return;
            }
            if (n == 1) {
                textView.setText("Сразу");
                ((TextView)view.findViewById(R.id.timeLabel)).setVisibility(View.GONE);
                view.findViewById(R.id.defaultTimeLabel).setVisibility(View.VISIBLE);
                textView.setGravity(80);
                return;
            }
            if (n == 2) {
                textView.setText("Задать вручную");
                final TextView textView2 = (TextView)view.findViewById(R.id.timeLabel);
                textView2.setVisibility(View.VISIBLE);
                textView.setGravity(16);
                view.findViewById(R.id.defaultTimeLabel).setVisibility(View.GONE);
                textView2.setPaintFlags(textView2.getPaintFlags() | 0x8);
                final StringBuilder sb = new StringBuilder();
                sb.append(String.format("%02d", MainDataManager.getInstance().getOptions().getCustomNotificationTimeHour()));
                sb.append(":");
                sb.append(String.format("%02d", MainDataManager.getInstance().getOptions().getCustomNotificationTimeMinute()));
                textView2.setText((CharSequence)sb.toString());
                textView2.setOnClickListener((View.OnClickListener)new OptionsItemsAdapter5(this, textView2));
                return;
            }
            ((CheckBox)view.findViewById(R.id.checkBox1)).setChecked(MainDataManager.getInstance().getOptions().isDisabledNotificationsSounds());
            textView.setText("Без звука");
        }

        private void bindScanwordSection(final View view, final int n) {
            final TextView textView = (TextView)view.findViewById(R.id.optionName);
            final CheckBox checkBox = (CheckBox)view.findViewById(R.id.checkBox1);
            if (n == 0) {
                textView.setText("Звуки");
                ((Switch)view.findViewById(R.id.switchButton1)).setChecked(MainDataManager.getInstance().getOptions().isSounds());
                return;
            }
            if (n == 1) {
                textView.setText("Пропускать разгаданные буквы при вводе слова");
                checkBox.setChecked(MainDataManager.getInstance().getOptions().isSkipGuessedLetters());
                return;
            }
            if (n == 2) {
                textView.setText("Увеличить макс. масштаб");
                checkBox.setChecked(MainDataManager.getInstance().getOptions().isIncreasedRatio());
            }
        }

        private void optionClicked(final int n, final int n2, final boolean disabledNotificationsSounds) {
            if (n == 0) {
                if (n2 == 0) {
                    MainDataManager.getInstance().getOptions().setSounds(disabledNotificationsSounds);
                }
                else if (n2 == 1) {
                    MainDataManager.getInstance().getOptions().setSkipGuessedLetters(disabledNotificationsSounds);
                }
                else {
                    MainDataManager.getInstance().getOptions().setIncreasedRatio(disabledNotificationsSounds);
                }
            }
            else if (n == 1) {
                if (n2 == 0) {
                    MainDataManager.getInstance().getOptions().setAutoDownload(disabledNotificationsSounds);
                    SmappsScanwords.getEventsDispatcher().dispatchEvent((Event)new SimpleEvent("EVENT_DOWNLOAD_TODAY_ISSUE"));
                }
                else {
                    MainDataManager.getInstance().getOptions().setOnlyWifi(disabledNotificationsSounds);
                }
            }
            else if (n == 2) {
                if (n2 == 0) {
                    MainDataManager.getInstance().getOptions().setInformNewIssues(disabledNotificationsSounds);
                }
                else {
                    MainDataManager.getInstance().getOptions().setDisabledNotificationsSounds(disabledNotificationsSounds);
                }
                if (MainMenuActivity.instance != null) {
                    MainMenuActivity.instance.updateNotifications(true);
                }
            }
            MainDataManager.getInstance().storeOptions();
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        protected void bindRowViewInSection(final Context context, final int n, final int n2, final View view) {
            final TextView textView = (TextView)view.findViewById(R.id.optionName);
            if (n != 0) {
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            textView.setText((CharSequence)"unknown");
                        }
                        else {
                            this.bindInfoSection(view, n2);
                        }
                    }
                    else {
                        this.bindInformSection(view, n2);
                    }
                }
                else {
                    this.bindConnectionSection(view, n2);
                }
            }
            else {
                this.bindScanwordSection(view, n2);
            }
            final Switch switch1 = (Switch)view.findViewById(R.id.switchButton1);
            if (switch1 != null) {
                switch1.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new OptionsItemsAdapter2(this, n, n2, switch1));
            }
            final CheckBox checkBox = (CheckBox)view.findViewById(R.id.checkBox1);
            if (checkBox != null) {
                checkBox.setOnClickListener((View.OnClickListener)new OptionsItemsAdapter3(this, n, n2, checkBox));
            }
        }

        protected void bindSectionView(final Context context, final int n, final View view) {
            if (view == null) {
                return;
            }
            final TextView textView = (TextView)view.findViewById(R.id.setLabel);
            textView.setOnClickListener((View.OnClickListener)new OptionsItemsAdapter1(this));
            String text;
            if (n != 0) {
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            text = "";
                        }
                        else {
                            text = "ИНФОРМАЦИЯ";
                        }
                    }
                    else {
                        text = "УВЕДОМЛЕНИЕ О НОВЫХ ВЫПУСКАХ";
                    }
                }
                else {
                    text = "ПОДКЛЮЧЕНИЕ";
                }
            }
            else {
                text = "СКАНВОРД";
            }
            textView.setText((CharSequence)text);
        }

        public Object getItemInSectionAndRow(final int n, final int n2) {
            return null;
        }

        protected int getNumberOfRowsInSection(final int n) {
            int n2 = 3;
            if (n != 0) {
                if (n != 1) {
                    if (n == 2) {
                        return 4;
                    }
                    if (n != 3) {
                        return 0;
                    }
                }
                n2 = 2;
            }
            return n2;
        }

        protected int getNumberOfSections() {
            return 4;
        }

        protected int getNumberOfViewTypeForRows() {
            return 4;
        }

        protected int getViewTypeForRowInSection(final int n, final int n2) {
            if (n2 == 0 && (n == 0 || n == 2)) {
                return 4;
            }
            if (n == 1 || (n == 0 && n2 != 0)) {
                return 2;
            }
            if (n == 2 && n2 == 3) {
                return 2;
            }
            if (n == 2 && (n2 == 1 || n2 == 2)) {
                return 3;
            }
            return 1;
        }

        public boolean isEnabled(final int n) {
            return true;
        }

        protected View newRowViewInSection(final Context context, final int n, final int n2, final ViewGroup viewGroup) {
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            if (n2 == 0 && (n == 0 || n == 2)) {
                return from.inflate(R.layout.options_tuggle_item_row, viewGroup, false);
            }
            if (n == 1 || (n == 0 && n2 != 0) || (n == 2 && n2 == 3)) {
                return from.inflate(R.layout.options_checkbox_item_row, viewGroup, false);
            }
            if (n == 2 && (n2 == 1 || n2 == 2)) {
                return from.inflate(R.layout.options_radiobtn_item_row, viewGroup, false);
            }
            return from.inflate(R.layout.options_simple_item_row, viewGroup, false);
        }

        protected View newSectionView(final Context context, final int n, final ViewGroup viewGroup) {
            return ((LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.options_list_header, viewGroup, false);
        }

        public void onRadioButtonClicked(RadioButton var1, int var2) {
            Iterator var6 = this.radioButtonsGroup.entrySet().iterator();

            while(true) {
                boolean var5 = var6.hasNext();
                boolean var4 = false;
                boolean var3 = false;
                if (!var5) {
                    OptionsData var8 = MainDataManager.getInstance().getOptions();
                    var3 = var4;
                    if (var2 == 1) {
                        var3 = true;
                    }

                    var8.setDefaultNotificationsTime(var3);
                    MainDataManager.getInstance().storeOptions();
                    if (MainMenuActivity.instance != null) {
                        MainMenuActivity.instance.updateNotifications(true);
                    }

                    return;
                }

                RadioButton var7 = (RadioButton)((Entry)var6.next()).getKey();
                if (var7 == var1) {
                    var3 = true;
                }

                var7.setChecked(var3);
            }
        }
    }

    static class OptionsItemsAdapter4 implements View.OnClickListener {
        final /* synthetic */ OptionsItemsAdapter this0;
        final /* synthetic */ RadioButton valradioButton;
        final /* synthetic */ int valrow;

        OptionsItemsAdapter4(final OptionsItemsAdapter this0, final RadioButton valradioButton, final int valrow) {
            super();
            this.this0 = this0;
            this.valradioButton = valradioButton;
            this.valrow = valrow;
        }

        public void onClick(final View view) {
            this.this0.onRadioButtonClicked(this.valradioButton, this.valrow);
        }
    }

    private class OptionsItemClickListener implements AdapterView.OnItemClickListener
    {
        final /* synthetic */ OptionsActivity this0;

        private OptionsItemClickListener(final OptionsActivity this0) {
            super();
            this.this0 = this0;
        }

        OptionsItemClickListener(final OptionsActivity optionsActivity, final OptionsActivity1 optionsActivity1) {
            this(optionsActivity);
        }

        public void onItemClick(final AdapterView<?> adapterView, View inflate, int versionCode, final long n) {
            final int rowForPosition = OptionsActivity.access300(this.this0).getRowForPosition(versionCode);
            if (OptionsActivity.access300(this.this0).getSectionForPosition(versionCode) == 3) {
                if (rowForPosition == 0) {
                    inflate = LayoutInflater.from((Context)OptionsActivity.access400(this.this0)).inflate(R.layout.info_dialog_layout, (ViewGroup)null);
                    final QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder((Context)OptionsActivity.access400(this.this0));
                    qustomDialogBuilder.setPositiveButton((CharSequence)"O\u043a", (DialogInterface.OnClickListener)new OptionsItemClickListener2(this)).setNegativeButton("Условия", (DialogInterface.OnClickListener)new OptionsItemClickListener1(this));
                    final TextView textView = (TextView)inflate.findViewById(R.id.versionLabel);
                    if (textView != null) {
                        while (true) {
                            while (true) {
                                Label_0323: {
                                    try {
                                        final PackageInfo packageInfo = SmappsScanwords.getContext().getPackageManager().getPackageInfo(this.this0.getPackageName(), 0);
                                        final String versionName = packageInfo.versionName;
                                        versionCode = packageInfo.versionCode;
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append("Версия: ");
                                        sb.append(versionName);
                                        sb.append(" (");
                                        sb.append(versionCode);
                                        sb.append(")");
                                        if (!MainNetworkManager.isRelese()) {
                                            break Label_0323;
                                        }
                                        final String s = "p";
                                        sb.append(s);
                                        textView.setText((CharSequence)sb.toString());
                                    }
                                    catch (PackageManager.NameNotFoundException ex) {
                                        ex.printStackTrace();
                                    }
                                    break;
                                }
                                final String s = "t";
                                continue;
                            }
                        }
                    }
                    final TextView textView2 = (TextView)inflate.findViewById(R.id.userIDLabel);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Пользователь: ");
                    String userId;
                    if (MainNetworkManager.getInstance().getUserAuth() != null) {
                        userId = MainNetworkManager.getInstance().getUserAuth().getUserId();
                    }
                    else {
                        userId = "Неизвестно";
                    }
                    sb2.append(userId);
                    textView2.setText((CharSequence)sb2.toString());
                    qustomDialogBuilder.setCustomView(R.attr.corpusId, inflate.getContext());
                    qustomDialogBuilder.show();
                    return;
                }
                Appirater.rateApp((Context)OptionsActivity.access400(this.this0));
            }
        }
    }

    static class OptionsActivity1 {}

    class ItemsAdapter4 implements View.OnClickListener {
        final /* synthetic */ OptionsItemsAdapter this0;
        final /* synthetic */ RadioButton valradioButton;
        final /* synthetic */ int valrow;

        ItemsAdapter4(final OptionsItemsAdapter this0, final RadioButton valradioButton, final int valrow) {
            super();
            this.this0 = this0;
            this.valradioButton = valradioButton;
            this.valrow = valrow;
        }

        public void onClick(final View view) {
            this.this0.onRadioButtonClicked(this.valradioButton, this.valrow);
        }
    }

    static class OptionsItemsAdapter5 implements View.OnClickListener {
        final /* synthetic */ OptionsItemsAdapter this0;
        final /* synthetic */ TextView valtimeLabel;

        OptionsItemsAdapter5(final OptionsItemsAdapter this0, final TextView valtimeLabel) {
            super();
            this.this0 = this0;
            this.valtimeLabel = valtimeLabel;
        }

        public void onClick(final View view) {
            final String[] split = ((String)this.valtimeLabel.getText()).split(":");
            final TimePickerDialog timePickerDialog = new TimePickerDialog((Context)OptionsItemsAdapter.access200(this.this0), (TimePickerDialog.OnTimeSetListener)new OptionsItemsAdapter5.OptionsActivityOptionsItemsAdapter51(this), Integer.parseInt(split[0]), Integer.parseInt(split[1]), true);
            timePickerDialog.setTitle((CharSequence)"Select Time");
            timePickerDialog.show();
        }

        class OptionsActivityOptionsItemsAdapter51 implements TimePickerDialog.OnTimeSetListener {
            final /* synthetic */ OptionsActivity.OptionsItemsAdapter5 this1;

            OptionsActivityOptionsItemsAdapter51(final OptionsItemsAdapter5 this1) {
                super();
                this.this1 = this1;
            }

            public void onTimeSet(final TimePicker timePicker, final int customNotificationTimeHour, final int customNotificationTimeMinute) {
                final TextView valtimeLabel = this.this1.valtimeLabel;
                final StringBuilder sb = new StringBuilder();
                sb.append(String.format("%02d", customNotificationTimeHour));
                sb.append(":");
                sb.append(String.format("%02d", customNotificationTimeMinute));
                valtimeLabel.setText((CharSequence)sb.toString());
                MainDataManager.getInstance().getOptions().setCustomNotificationTimeHour(customNotificationTimeHour);
                MainDataManager.getInstance().getOptions().setCustomNotificationTimeMinute(customNotificationTimeMinute);
                MainDataManager.getInstance().storeOptions();
                if (MainMenuActivity.instance != null) {
                    MainMenuActivity.instance.updateNotifications(true);
                }
            }
        }
    }

    class OptionsItemClickListener2 implements DialogInterface.OnClickListener {
        final /* synthetic */ OptionsItemClickListener this1;

        OptionsItemClickListener2(final OptionsItemClickListener this1) {
            super();
            this.this1 = this1;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
        }
    }

    static class OptionsItemsAdapter2 implements CompoundButton.OnCheckedChangeListener {
        final /* synthetic */ OptionsItemsAdapter this0;
        final /* synthetic */ int valrow;
        final /* synthetic */ int valsection;
        final /* synthetic */ Switch valswitchbutton;

        OptionsItemsAdapter2(final OptionsItemsAdapter this0, final int valsection, final int valrow, final Switch valswitchbutton) {
            super();
            this.this0 = this0;
            this.valsection = valsection;
            this.valrow = valrow;
            this.valswitchbutton = valswitchbutton;
        }

        public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
            OptionsItemsAdapter.access100(this.this0, this.valsection, this.valrow, this.valswitchbutton.isChecked());
        }
    }

    static class OptionsItemsAdapter3 implements View.OnClickListener {
        final /* synthetic */ OptionsItemsAdapter this0;
        final /* synthetic */ CheckBox valcheckBox;
        final /* synthetic */ int valrow;
        final /* synthetic */ int valsection;

        OptionsItemsAdapter3(final OptionsItemsAdapter this0, final int valsection, final int valrow, final CheckBox valcheckBox) {
            super();
            this.this0 = this0;
            this.valsection = valsection;
            this.valrow = valrow;
            this.valcheckBox = valcheckBox;
        }

        public void onClick(final View view) {
            OptionsItemsAdapter.access100(this.this0, this.valsection, this.valrow, this.valcheckBox.isChecked());
        }
    }

    static class OptionsItemsAdapter1 implements View.OnClickListener {
        final /* synthetic */ OptionsItemsAdapter this0;

        OptionsItemsAdapter1(final OptionsItemsAdapter this0) {
            super();
            this.this0 = this0;
        }

        public void onClick(final View view) {
        }
    }

    class OptionsItemClickListener1 implements DialogInterface.OnClickListener {
        final /* synthetic */ OptionsItemClickListener this1;

        OptionsItemClickListener1(final OptionsItemClickListener this1) {
            super();
            this.this1 = this1;
        }

        public void onClick(final DialogInterface dialogInterface, final int n) {
            this.this1.this0.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://skanword.org/privacy")));
        }
    }
}
