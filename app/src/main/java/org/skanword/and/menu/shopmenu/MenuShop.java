package org.skanword.and.menu.shopmenu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.skanword.and.datamanager.ShopDataManager;
import org.skanword.and.libs.musselwhizzle.mvc.events.Event;
import org.skanword.and.libs.musselwhizzle.mvc.events.EventListener;
import java.util.List;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.ShopDataManager.ShopProductType;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.inappbilling.InAppManager.PurchaseSource;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.network.MainNetworkManager;

public class MenuShop extends CustomFragment
{
    public static final int TYPE_ROW_CANCEL_SUBSCRIPTION_VIEW = 4;
    public static final int TYPE_ROW_DESCRIPTION_VIEW = 5;
    public static final int TYPE_ROW_SALE_VIEW = 3;
    public static final int TYPE_ROW_TIMER_VIEW = 2;
    private ShopItemsAdapter mAdapter;
    private ListView mList;
    private ShopDataManager.ShopProductType mListType;
    private View mMainView;
    private EventListener mPriceUpdateEventListener;
    private EventListener mTimerEventListener;

    public MenuShop() {
        super();
    }

    static /* synthetic */ MenuShop.ShopItemsAdapter access000(final MenuShop menuShop) {
        return menuShop.mAdapter;
    }

    static /* synthetic */ void access100(final MenuShop menuShop) {
        menuShop.setAdapter();
    }

    private void setAdapter() {
        final List shopItemsOfType = MainDataManager.getInstance().getShopDataManager().getShopItemsOfType(this.mListType);
        if (shopItemsOfType == null) {
            return;
        }
        this.mAdapter = new MenuShop.ShopItemsAdapter((Context)this.getActivity(), R.layout.shop_item_row, shopItemsOfType, this.mListType);
        this.mList.setAdapter((ListAdapter)this.mAdapter);
    }

    public void fragmentOpened() {
        this.updateFragment();
        super.fragmentOpened();
    }

    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Log.v("", " onCreateView  MenuShop");
        this.mMainView = layoutInflater.inflate(R.layout.shop_menu_layout, viewGroup, false);
        this.mList = (ListView)this.mMainView.findViewById(R.id.shop_list);
        this.mListType = ShopDataManager.ShopProductType.HINTS;
        this.mTimerEventListener = (EventListener)new MenuShop1(this);
        this.mList.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        this.mPriceUpdateEventListener = (EventListener)new MenuShop2(this);
        if (this.mTimerEventListener != null) {
            SmappsScanwords.getEventsDispatcher().addListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        }
        SmappsScanwords.getEventsDispatcher().addListener("EVENT_PRICE_UPDATE", this.mPriceUpdateEventListener);
        this.setAdapter();
        return this.mMainView;
    }

    public void onDestroy() {
        Log.v("", "onDestroy -------MenuShop------");
        if (this.mTimerEventListener != null) {
            SmappsScanwords.getEventsDispatcher().removeListener("APPLICATION_TIMER_EVENT", this.mTimerEventListener);
        }
        SmappsScanwords.getEventsDispatcher().removeListener("EVENT_PRICE_UPDATE", this.mPriceUpdateEventListener);
        super.onDestroy();
    }

    public void onPause() {
        Log.v("", "onPause -------MenuShop------");
        super.onPause();
    }

    public void onResume() {
        Log.v("", "onResume -------MenuShop------");
        super.onResume();
    }

    public void onStart() {
        Log.v("", "onStart -------MenuShop------");
        super.onStart();
    }

    public void onStop() {
        Log.v("", "onStop -------MenuShop------");
        super.onStop();
    }

    public void update() {
        if (this.getActivity() != null) {
            this.getActivity().runOnUiThread((Runnable)new MenuShop3(this));
        }
    }

    public void updateFragment(final boolean b) {
        final MainNetworkManager instance = MainNetworkManager.getInstance();
        Object activity;
        if (b) {
            activity = this.getActivity();
        }
        else {
            activity = null;
        }
        if (instance.hasNetworkConnection((Activity)activity)) {
            MainNetworkManager.getInstance().requestInfoWithCompleteBlock(2, (Activity)this.getActivity(), (MainNetworkManager.RequestCompletionBlock)null);
        }
        super.updateFragment(b);
    }

    class MenuShop3 implements Runnable {
        final /* synthetic */ MenuShop this0;

        MenuShop3(final MenuShop this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            MenuShop.access100(this.this0);
        }
    }

    public static class ShopItemsAdapter extends ArraySectionedListAdapter<ShopDataManager.ShopItem>
    {
        protected Activity mContext;
        private final ShopDataManager.ShopProductType mType;
        private final List<ShopDataManager.ShopItem> objectsList;
        private View premiumTimerItemView;
        private View saleItemView;

        public ShopItemsAdapter(final Context context, final int n, final List<ShopDataManager.ShopItem> objectsList, final ShopDataManager.ShopProductType mType) {
            super(context, n, (List)objectsList);
            this.premiumTimerItemView = null;
            this.saleItemView = null;
            this.mContext = (Activity)context;
            this.objectsList = objectsList;
            this.mType = mType;
        }

        private void buildAdditionalRowView(final View view, final int n) {
            if (n == 3) {
                this.saleItemView = view;
            }
            if (n == 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("buildAdditionalRowView  ");
                sb.append(n);
                Log.v("", sb.toString());
                this.premiumTimerItemView = view;
            }
            if (n == 4) {
                ((Button)view.findViewById(R.id.cancelSubscription)).setOnClickListener((View.OnClickListener)new ShopItemsAdapter1(this));
            }
            this.updateTimerItems();
            view.setClickable(true);
        }

        private void buildRowView(final View view, final ShopDataManager.ShopItem shopItem) {
            ((TextView)view.findViewById(R.id.itemNameLabel)).setText((CharSequence)shopItem.getName());
            view.setClickable(true);
            final LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.sale_label);
            int visibility;
            if (shopItem.isSale() && MainDataManager.getInstance().getShopDataManager().saleEnabled()) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            linearLayout.setVisibility(visibility);
            ((ImageView)view.findViewById(R.id.itemTypeIcon)).setBackgroundResource(getIconId(shopItem, this.mType));
            final Button button = (Button)view.findViewById(R.id.buyButton);
            button.setOnClickListener((View.OnClickListener)new ShopItemsAdapter2(this, shopItem));
            String priceForProductId;
            if ((priceForProductId = InAppManager.getInstance().getPriceForProductId(shopItem.getId())).equals("")) {
                priceForProductId = "\u043a\u0443\u043f\u0438\u0442\u044c";
            }
            if (priceForProductId != null) {
                button.setText((CharSequence)priceForProductId);
            }
        }

        public static int getIconId(final ShopDataManager.ShopItem shopDataManagerShopShopItemsSetShopItem, final ShopDataManager.ShopProductType shopDataManagerShopProductType) {
            final String iconName = shopDataManagerShopShopItemsSetShopItem.getIconName();
            if (iconName == null) {
                if (shopDataManagerShopProductType == ShopDataManager.ShopProductType.HINTS) {
                    return R.drawable.tip12;
                }
                return R.drawable.month1;
            }
            else {
                if (iconName.equals("hint5")) {
                    return R.drawable.tip500;
                }
                if (iconName.equals("hint4")) {
                    return R.drawable.tip110;
                }
                if (iconName.equals("hint3")) {
                    return R.drawable.tip30;
                }
                if (iconName.equals("hint2")) {
                    return R.drawable.tip12;
                }
                if (iconName.equals("hint1")) {
                    return R.drawable.tip5;
                }
                if (iconName.equals("vip2")) {
                    return R.drawable.year;
                }
                if (iconName.equals("vip1")) {
                    return R.drawable.month1;
                }
                if (shopDataManagerShopProductType == ShopDataManager.ShopProductType.HINTS) {
                    return R.drawable.tip12;
                }
                return R.drawable.month1;
            }
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        protected void bindRowViewInSection(Context var1, int var2, int var3, View var4) {
            ShopDataManager.ShopItem var5 = this.getItemInSectionAndRow(var2, var3);
            if (var5 != null) {
                this.buildRowView(var4, var5);
            } else {
                this.buildAdditionalRowView(var4, this.getViewTypeForRowInSection(var2, var3));
            }
        }

        protected void bindSectionView(Context var1, int var2, View var3) {
            if (this.mType != ShopProductType.HINTS || !MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType) || var2 != 0) {
                if (var3 != null) {
                    if (this.mType == ShopProductType.HINTS) {
                        ((TextView)var3.findViewById(R.id.deleteSetLabel)).setText("варианты пакетов:");
                    } else if (var2 == 0) {
                        ((TextView)var3.findViewById(R.id.setLabel)).setText("премиум-доступ – это");
                    } else {
                        ((TextView)var3.findViewById(R.id.setLabel)).setText("варианты доступа:");
                    }
                }
            }
        }


        public ShopDataManager.ShopItem getItemInSectionAndRow(final int n, final int n2) {
            if (this.mType == ShopDataManager.ShopProductType.SUBSCRIPTION && n == 0) {
                return null;
            }
            if (this.mType == ShopDataManager.ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType) && n == 0) {
                return null;
            }
            if (n2 >= this.objectsList.size()) {
                return null;
            }
            return (ShopDataManager.ShopItem)this.getItem(n2);
        }

        protected int getNumberOfRowsInSection(int n) {
            final ShopDataManager.ShopProductType mType = this.mType;
            final ShopDataManager.ShopProductType subscription = ShopDataManager.ShopProductType.SUBSCRIPTION;
            final int n2 = 1;
            if (mType == subscription && n == 0) {
                n = n2;
                if (MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType)) {
                    n = 2;
                }
                int n3 = n;
                if (MainDataManager.getInstance().getUserData().isVip()) {
                    n3 = n + 1;
                }
                return n3;
            }
            if (this.mType == ShopDataManager.ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType) && n == 0) {
                return 1;
            }
            if (this.mType == ShopDataManager.ShopProductType.SUBSCRIPTION && n == 1 && MainDataManager.getInstance().getUserData().isVip()) {
                return this.objectsList.size() + 1;
            }
            return this.objectsList.size();
        }

        protected int getNumberOfSections() {
            if (this.mType == ShopDataManager.ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType)) {
                return 2;
            }
            if (this.mType == ShopDataManager.ShopProductType.SUBSCRIPTION) {
                return 2;
            }
            return 1;
        }

        protected int getNumberOfViewTypeForRows() {
            return 6;
        }

        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            return super.getView(n, view, viewGroup);
        }

        protected int getViewTypeForRowInSection(final int n, final int n2) {
            if (this.mType == ShopDataManager.ShopProductType.SUBSCRIPTION) {
                if (n == 0) {
                    if (n2 == 0) {
                        return 5;
                    }
                    if (n2 == 1 && MainDataManager.getInstance().getUserData().isVip()) {
                        return 2;
                    }
                    return 3;
                }
                else {
                    if (n2 >= this.objectsList.size()) {
                        return 4;
                    }
                    return 1;
                }
            }
            else {
                if (MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType) && n == 0) {
                    return 3;
                }
                return 1;
            }
        }

        public boolean isEnabled(final int n) {
            return this.getRowForPosition(n) >= 0;
        }

        protected View newRowViewInSection(final Context context, int n, final int n2, final ViewGroup viewGroup) {
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            final ShopDataManager.ShopProductType mType = this.mType;
            final ShopDataManager.ShopProductType hints = ShopDataManager.ShopProductType.HINTS;
            final int n3 = R.layout.shop_item_sale_timer;
            if (mType == hints) {
                if (MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType) && n == 0) {
                    n = n3;
                    return from.inflate(n, viewGroup, false);
                }
            }
            else {
                if (this.getViewTypeForRowInSection(n, n2) == 5) {
                    n = R.layout.premium_description_item_layout;
                    return from.inflate(n, viewGroup, false);
                }
                if (this.getViewTypeForRowInSection(n, n2) == 3) {
                    n = n3;
                    return from.inflate(n, viewGroup, false);
                }
                if (this.getViewTypeForRowInSection(n, n2) == 2) {
                    n = R.layout.premium_timer_item_layout;
                    return from.inflate(n, viewGroup, false);
                }
                if (this.getViewTypeForRowInSection(n, n2) == 4) {
                    n = R.layout.shop_cancel_subscription_item_row;
                    return from.inflate(n, viewGroup, false);
                }
            }
            n = R.layout.shop_item_row;
            return from.inflate(n, viewGroup, false);
        }

        protected View newSectionView(final Context context, final int n, final ViewGroup viewGroup) {
            if (this.mType == ShopDataManager.ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(this.mType) && n == 0) {
                return new View(this.getContext());
            }
            return ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header, viewGroup, false);
        }

        public void notifyDataSetChanged() {
            this.premiumTimerItemView = null;
            this.saleItemView = null;
            super.notifyDataSetChanged();
        }

        public void updateTimerItems() {
            final View saleItemView = this.saleItemView;
            if (saleItemView != null && saleItemView.findViewById(R.id.itemNameLabel) != null) {
                final Resources resources = this.getContext().getResources();
                final TextView textView = (TextView)this.saleItemView.findViewById(R.id.itemNameLabel);
                final StringBuilder sb = new StringBuilder();
                sb.append(resources.getString(R.string.shop_item_sale_label));
                sb.append(" ");
                sb.append(Utils.secondsToTimerType(MainDataManager.getInstance().getShopDataManager().getSaleTimeLeft()));
                textView.setText((CharSequence)sb.toString());
            }
            if (this.mType != ShopDataManager.ShopProductType.SUBSCRIPTION) {
                return;
            }
            final boolean vip = MainDataManager.getInstance().getUserData().isVip();
            final boolean b = this.premiumTimerItemView != null;
            if (b != vip) {
                this.notifyDataSetChanged();
                return;
            }
            if (vip && b) {
                final TextView textView2 = (TextView)this.premiumTimerItemView.findViewById(R.id.itemNameLabel);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Премиум активен:\n");
                sb2.append(Utils.secondsToMaxTimeType(MainDataManager.getInstance().getUserData().lastVip()));
                textView2.setText((CharSequence)sb2.toString());
            }
        }

        class ShopItemsAdapter1 implements View.OnClickListener {
            final /* synthetic */ ShopItemsAdapter this0;

            ShopItemsAdapter1(final ShopItemsAdapter this0) {
                super();
                this.this0 = this0;
            }

            public void onClick(final View view) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.this0.mContext, R.style.Theme_AppCompat_DayNight);
                alertDialogBuilder.setMessage("Для отмены подписки необходимо перейти в Google Wallet.").setPositiveButton("Перейти", (DialogInterface.OnClickListener)new ShopItemsAdapter11(this)).setNegativeButton("Отмена", (DialogInterface.OnClickListener)null);
                alertDialogBuilder.create().show();
            }

            class ShopItemsAdapter11 implements DialogInterface.OnClickListener {
                final /* synthetic */ ShopItemsAdapter1 this1;

                ShopItemsAdapter11(final ShopItemsAdapter1 this1) {
                    super();
                    this.this1 = this1;
                }

                public void onClick(final DialogInterface dialogInterface, final int n) {
                    this.this1.this0.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://wallet.google.com/manage/#subscriptions:")));
                }
            }
        }
    }

    class MenuShop1 implements EventListener {
        final /* synthetic */ MenuShop this0;

        MenuShop1(final MenuShop this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            if (this.this0.getActivity() != null && MenuShop.access000(this.this0) != null) {
                this.this0.getActivity().runOnUiThread((Runnable)new MenuShop1.MenuShop11(this));
            }
        }

        class MenuShop11 implements Runnable {
            final /* synthetic */ MenuShop1 this1;

            MenuShop11(final MenuShop1 this1) {
                super();
                this.this1 = this1;
            }

            @Override
            public void run() {
                MenuShop.access000(this.this1.this0).updateTimerItems();
            }
        }
    }

    class MenuShop2 implements EventListener {
        final /* synthetic */ MenuShop this0;

        MenuShop2(final MenuShop this0) {
            super();
            this.this0 = this0;
        }

        public void onEvent(final Event event) {
            this.this0.update();
        }
    }


    static class ShopItemsAdapter2 implements View.OnClickListener {
        final /* synthetic */ ShopItemsAdapter this0;
        final /* synthetic */ ShopDataManager.ShopItem valitem;

        ShopItemsAdapter2(final ShopItemsAdapter this0, final ShopDataManager.ShopItem valitem) {
            super();
            this.this0 = this0;
            this.valitem = valitem;
        }

        public void onClick(final View view) {
            if (MainNetworkManager.getInstance().hasNetworkConnection(this.this0.mContext)) {
                InAppManager.getInstance().purchaseProduct(this.valitem.getId(), this.this0.mContext, InAppManager.PurchaseSource.SHOP);
            }
            final SmappsScanwords.ActionType user_ACTION = SmappsScanwords.ActionType.USER_ACTION;
            final StringBuilder sb = new StringBuilder();
            sb.append("click_menu_shop_buy_");
            sb.append(this.valitem.getId());
            SmappsScanwords.sendGAEvent(user_ACTION, sb.toString());
        }
    }
}
