package org.skanword.and.network.socialnetworks;

import java.util.List;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;

public interface ISocialNetworkManagerDelegate {
    void socialNetworkAuthoriseFailed(boolean z);

    void socialNetworkAuthorised(SocialUser socialUser);

    void socialNetworkFriendsGot(List<String> list);
}
