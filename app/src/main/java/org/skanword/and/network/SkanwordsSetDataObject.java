package org.skanword.and.network;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.skanword.and.datamanager.SkanwordSet;

public class SkanwordsSetDataObject implements Serializable {
    private static final long serialVersionUID = 8630112335338511777L;
    private List<Map<String, Integer>> requestdSetIds;
    @SerializedName("save")
    private final List<List<String>> saveSkanwords;
    @SerializedName("set_list")
    private final List<SkanwordSet> setsList;
    @SerializedName("solved")
    private final List<List<String>> solvedSkanwords;

    public SkanwordsSetDataObject(List<SkanwordSet> list, List<List<String>> list2, List<List<String>> list3) {
        this.setsList = list;
        this.solvedSkanwords = list2;
        this.saveSkanwords = list3;
    }

    public List<SkanwordSet> getSetsList() {
        return this.setsList;
    }

    public SkanwordSet getFristScanwordSet() {
        List list = this.setsList;
        return list != null ? (SkanwordSet) list.get(0) : null;
    }

    public List<List<String>> getSolvedSkanwords() {
        List<List<String>> list = this.solvedSkanwords;
        return list != null ? list : new ArrayList();
    }

    public List<List<String>> getSaveSkanwords() {
        List<List<String>> list = this.saveSkanwords;
        return list != null ? list : new ArrayList();
    }

    public List<Map<String, Integer>> getRequestdSetIds() {
        return this.requestdSetIds;
    }

    public void setRequestdSetIds(List<Map<String, Integer>> list) {
        this.requestdSetIds = list;
    }
}
