package org.skanword.and.network;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;

public class UserObject implements Serializable {
    private static final long serialVersionUID = -1797044308341728223L;
    @SerializedName("ads_off")
    private boolean adsOff;
    private int hints;
    @SerializedName("cross_user_place")
    private final int placeScanwords;
    @SerializedName("cross_save")
    private final String scanwordsProgress;
    @SerializedName("cross_save_hash")
    private final String scanwordsProgressHash;
    @SerializedName("cross_user_score")
    private final int scoreScanwords;
    @SerializedName("show_hints_video_offer")
    private final boolean showHintsVideoOffer;
    @SerializedName("updated_time")
    private long updatedTime;
    private long vip;
    @SerializedName("vip_id")
    private String vipId;

    public void setVipId(String str) {
        this.vipId = str;
    }

    public UserObject(int i, boolean z, String str, long j, int i2, int i3, boolean z2, String str2, String str3, long j2) {
        this.updatedTime = 0;
        this.hints = i;
        this.adsOff = z;
        this.vipId = str;
        this.vip = j;
        this.scoreScanwords = i2;
        this.placeScanwords = i3;
        this.showHintsVideoOffer = z2;
        this.scanwordsProgressHash = str2;
        this.scanwordsProgress = str3;
        this.updatedTime = j2;
    }

    public UserObject() {
        this.updatedTime = 0;
        this.hints = 0;
        this.adsOff = false;
        String str = "";
        this.vipId = str;
        this.vip = 0;
        this.scoreScanwords = 0;
        this.scanwordsProgress = str;
        this.placeScanwords = 0;
        this.showHintsVideoOffer = false;
        this.scanwordsProgressHash = str;
        this.updatedTime = 0;
    }

    public boolean isShowHintsVideoOffer() {
        return this.showHintsVideoOffer;
    }

    public int getHints() {
        return this.hints;
    }

    public void setAdsOff(boolean z) {
        this.adsOff = z;
    }

    public boolean isAdsOff() {
        return this.adsOff;
    }

    public String getVipId() {
        return this.vipId;
    }

    public long getVip() {
        return this.vip;
    }

    public long lastVip() {
        return this.vip - ((new Date().getTime() / 1000) - getUpdatedTime());
    }

    public boolean isVip() {
        return lastVip() > 0;
    }

    public long getUpdatedTime() {
        return this.updatedTime;
    }

    public void setUpdatedTime(long j) {
        this.updatedTime = j;
    }

    public String getScanwordsProgress() {
        return this.scanwordsProgress;
    }

    public String getScanwordsProgressHash() {
        return this.scanwordsProgressHash;
    }

    public int getPlaceScanwords() {
        return this.placeScanwords;
    }

    public int getScoreScanwords() {
        return this.scoreScanwords;
    }

    public void setHints(int i) {
        this.hints = i;
    }

    public void setVip(long j) {
        this.vip = j;
    }
}
