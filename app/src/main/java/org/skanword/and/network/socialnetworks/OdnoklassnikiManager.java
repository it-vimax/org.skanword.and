package org.skanword.and.network.socialnetworks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;
import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;
import ru.ok.android.sdk.util.OkScope;

public class OdnoklassnikiManager extends SocialNetworkManager implements OkTokenRequestListener
{
    public static final String APP_ID = "184762112";
    public static final String APP_PUBLIC_KEY = "CBAQDEAMABABABABA";
    public static final String APP_SECRET_KEY = "2D530094866EA5CA4658B6C8";
    private Odnoklassniki mOdnoklassniki;

    public OdnoklassnikiManager() {
        super();
        this.mOdnoklassniki = null;
    }

    static /* synthetic */ Odnoklassniki access000(final OdnoklassnikiManager odnoklassnikiManager) {
        return odnoklassnikiManager.mOdnoklassniki;
    }

    public void authorise(final Activity activity) {
        this.mOdnoklassniki.requestAuthorization((Context)activity, false, new String[] { "VALUABLE ACCESS" });
    }

    public void clearSocialNetworkManager() {
        if (this.initted) {
            this.mOdnoklassniki.removeTokenRequestListener();
        }
    }

    public void getCurrentUser() {
        (new org.skanword.and.network.socialnetworks.OdnoklassnikiManager.GetCurrentUserTask(this)).execute(new Void[0]);
    }

    public void getUserFriends() {
        (new org.skanword.and.network.socialnetworks.OdnoklassnikiManager.GetUserFriendsTask(this)).execute(new Void[0]);
    }

    public void initSocialNetworkManager(final ISocialNetworkManagerDelegate socialNetworkManagerDelegate, final Context context) {
        super.initSocialNetworkManager(socialNetworkManagerDelegate, context);
        (this.mOdnoklassniki = Odnoklassniki.createInstance(context, "184762112", "2D530094866EA5CA4658B6C8", "CBAQDEAMABABABABA")).setTokenRequestListener((OkTokenRequestListener)this);
        this.initted = true;
    }

    public void logout() {
    }

    public void onCancel() {
        Log.v("APIOK", "Cancel");
        this.delegate.socialNetworkAuthoriseFailed(true);
    }

    public void onError() {
        Log.v("APIOK", "Error");
        this.delegate.socialNetworkAuthoriseFailed(false);
    }

    public void onSuccess(final String s) {
        Log.v("APIOK", "Success");
        this.getCurrentUser();
    }

    public final class GetCurrentUserTask extends AsyncTask {
        // FF: synthetic field
        final OdnoklassnikiManager this0;

        protected GetCurrentUserTask(OdnoklassnikiManager var1) {
            this.this0 = var1;
        }

        protected void onPostExecute(String var1) {
            if (this.this0.delegate != null) {
                if (var1 != null) {
                    StringBuilder var2 = new StringBuilder();
                    var2.append("user - ");
                    var2.append(var1);
                    Log.v("APIOK", var2.toString());

                    JSONObject var6;
                    try {
                        var6 = new JSONObject(var1);
                    } catch (JSONException var3) {
                        var3.printStackTrace();
                        var6 = null;
                    }

                    if (var6 == null) {
                        this.this0.delegate.socialNetworkAuthoriseFailed(false);
                    } else {
                        JSONException var10000;
                        label49: {
                            boolean var10001;
                            String var7;
                            label39: {
                                try {
                                    var7 = var6.getString("pic_3");
                                    if (!var7.contains("stub_")) {
                                        break label39;
                                    }
                                } catch (JSONException var5) {
                                    var10000 = var5;
                                    var10001 = false;
                                    break label49;
                                }

                                var7 = null;
                            }

                            try {
                                this.this0.socialUser = new SocialUser((new Date()).getTime(), OdnoklassnikiManager.access000(this.this0).getCurrentAccessToken(), SocialNetworkManager.getSocialNetworkStringId(SocialNetwork.SOCIAL_NETWORK_OK), var6.getString("first_name"), var6.getString("last_name"), var6.getString("uid"), var7);
                                this.this0.getUserFriends();
                                return;
                            } catch (JSONException var4) {
                                var10000 = var4;
                                var10001 = false;
                            }
                        }

                        JSONException var8 = var10000;
                        var8.printStackTrace();
                    }
                } else {
                    this.this0.delegate.socialNetworkAuthoriseFailed(false);
                }
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                HashMap var3 = new HashMap();
                var3.put("emptyPictures", "true");
                var3.put("fields", "uid,first_name,last_name,gender,pic_1,pic_3");
                String var4 = OdnoklassnikiManager.access000(this.this0).request("users.getCurrentUser", var3, "get");
                return var4;
            } catch (Exception var2) {
                Log.e("Odnoklassniki", "Failed to get current user info", var2);
                if (this.this0.delegate != null) {
                    this.this0.delegate.socialNetworkAuthoriseFailed(false);
                }

                return null;
            }
        }
    }


    protected final class GetUserFriendsTask extends AsyncTask<Void, Void, String> {
        final /* synthetic */ OdnoklassnikiManager this0;

        protected GetUserFriendsTask(OdnoklassnikiManager odnoklassnikiManager) {
            this.this0 = odnoklassnikiManager;
        }

        protected String doInBackground(Void ... object) {
            try {
                HashMap object2 = new HashMap();
                object2.put("emptyPictures", "true");
                return OdnoklassnikiManager.access000((OdnoklassnikiManager)this.this0).request("friends.get", (Map)object2, "get");
            }
            catch (Exception exception) {}
            this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
            return null;
        }

        protected void onPostExecute(String string) {
            if (string != null) {
                ArrayList<String> arrayList = null;
                JSONArray string2;
                try {
                    string2 = new JSONArray(string);
                    if (string != null) {
                        try {
                            arrayList = new ArrayList<String>();
                            for (int i = 0; i < string.length(); ++i) {
                                arrayList.add(string2.getString(i));
                            }
                            this.this0.delegate.socialNetworkFriendsGot(arrayList);
                        }
                        catch (JSONException jSONException) {}
                    }
                }
                catch (JSONException jSONException) {
                    jSONException.printStackTrace();
                }
            }
            this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
        }
    }
}
