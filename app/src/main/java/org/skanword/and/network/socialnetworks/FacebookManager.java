package org.skanword.and.network.socialnetworks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONArrayCallback;
import com.facebook.GraphRequest.GraphJSONObjectCallback;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;

public class FacebookManager extends SocialNetworkManager
{
    private AccessToken accessToken;
    private AccessTokenTracker accessTokenTracker;
    CallbackManager callbackManager;

    public FacebookManager() {
        super();
    }

    static /* synthetic */ void access000(final FacebookManager facebookManager) {
        facebookManager.getToken();
    }

    static /* synthetic */ AccessToken access100(final FacebookManager facebookManager) {
        return facebookManager.accessToken;
    }

    static /* synthetic */ AccessToken access102(final FacebookManager facebookManager, final AccessToken accessToken) {
        return facebookManager.accessToken = accessToken;
    }

    private void getToken() {
        this.accessTokenTracker = (AccessTokenTracker)new FacebookManager.FacebookManager2(this);
        this.accessToken = AccessToken.getCurrentAccessToken();
        if (this.accessToken != null) {
            this.getCurrentUser();
        }
    }

    public void authorise(final Activity activity) {
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(activity.toString());
        Log.v("", sb.toString());
        LoginManager.getInstance().logInWithReadPermissions(activity, (Collection)Arrays.asList("public_profile", "user_friends", "user_about_me"));
    }

    public void clearSocialNetworkManager() {
    }

    public void getCurrentUser() {
        final GraphRequest meRequest = GraphRequest.newMeRequest(this.accessToken, (GraphRequest.GraphJSONObjectCallback)new FacebookManager.FacebookManager3(this));
        final Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,picture");
        meRequest.setParameters(parameters);
        meRequest.executeAsync();
    }

    public void getUserFriends() {
        GraphRequest.newMyFriendsRequest(this.accessToken, (GraphRequest.GraphJSONArrayCallback)new FacebookManager.FacebookManager4(this)).executeAsync();
    }

    public void initSocialNetworkManager(final ISocialNetworkManagerDelegate socialNetworkManagerDelegate, final Context context) {
        super.initSocialNetworkManager(socialNetworkManagerDelegate, context);
        while (true) {
            try {
                final Signature[] signatures = SmappsScanwords.getContext().getPackageManager().getPackageInfo("org.skanword.and", PackageManager.GET_SIGNATURES).signatures;
                for (int length = signatures.length, i = 0; i < length; ++i) {
                    final Signature signature = signatures[i];
                    final MessageDigest instance = MessageDigest.getInstance("SHA");
                    instance.update(signature.toByteArray());
                    Log.d("KeyHash:", Base64.encodeToString(instance.digest(), 0));
                }
                Log.d("Facebook SDK Version:", FacebookSdk.getSdkVersion());
                this.callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().registerCallback(this.callbackManager, (FacebookCallback)new FacebookManager.FacebookManager1(this));
                this.initted = true;
            }
            catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ex) {
                continue;
            }
            break;
        }
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }

    public void onActivityResult(final Activity activity, final int n, final int n2, final Intent intent) {
        Log.v("", "onActivityResult facebook");
        super.onActivityResult(activity, n, n2, intent);
        this.callbackManager.onActivityResult(n, n2, intent);
    }

    class FacebookManager2 extends AccessTokenTracker {
        final /* synthetic */ FacebookManager this0;

        FacebookManager2(final FacebookManager this0) {
            super();
            this.this0 = this0;
        }

        protected void onCurrentAccessTokenChanged(final AccessToken accessToken, final AccessToken accessToken2) {
            FacebookManager.access102(this.this0, accessToken2);
        }
    }

    class FacebookManager3 implements GraphRequest.GraphJSONObjectCallback {
        final /* synthetic */ FacebookManager this0;

        FacebookManager3(final FacebookManager this0) {
            super();
            this.this0 = this0;
        }

        public void onCompleted(JSONObject var1, GraphResponse var2) {
            if (this.this0.delegate != null) {
                String var5 = var2.getRawResponse();
                StringBuilder var6 = new StringBuilder();
                var6.append("getCurrentUser ");
                var6.append(var5);
                Log.v("FacebookManager", var6.toString());
                if (var5 != null) {
                    String var7 = null;

                    try {
                        var1 = new JSONObject(var5);
                    } catch (JSONException var4) {
                        var4.printStackTrace();
                        var1 = null;
                    }

                    if (var1 != null) {
                        try {
                            if (!var1.getJSONObject("picture").getJSONObject("data").getBoolean("is_silhouette")) {
                                var7 = var1.getJSONObject("picture").getJSONObject("data").getString("url");
                            }

                            this.this0.socialUser = new SocialUser((new Date()).getTime(), FacebookManager.access100(this.this0).getToken(), SocialNetworkManager.getSocialNetworkStringId(SocialNetwork.SOCIAL_NETWORK_FB), var1.getString("first_name"), var1.getString("last_name"), var1.getString("id"), var7);
                            this.this0.getUserFriends();
                        } catch (JSONException var3) {
                            var3.printStackTrace();
                        }
                    } else {
                        this.this0.delegate.socialNetworkAuthoriseFailed(false);
                    }
                }
            }
        }
    }

    class FacebookManager4 implements GraphRequest.GraphJSONArrayCallback {
        final /* synthetic */ FacebookManager this0;

        FacebookManager4(final FacebookManager this0) {
            super();
            this.this0 = this0;
        }

        public void onCompleted(JSONArray jsonArray, final GraphResponse graphResponse) {
            final String rawResponse = graphResponse.getRawResponse();
            if (rawResponse == null) {
                this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
                return;
            }
            jsonArray = null;
            try {
                jsonArray = new JSONObject(rawResponse).getJSONArray("data");
            }
            catch (JSONException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append(" ");
                sb.append(rawResponse);
                Log.v("", sb.toString());
                ex.printStackTrace();
                this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
            }
            if (jsonArray != null) {
                try {
                    final ArrayList<String> list = new ArrayList<String>();
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        list.add(jsonArray.getJSONObject(i).getString("id"));
                    }
                    this.this0.delegate.socialNetworkFriendsGot((List)list);
                    this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
                    return;
                }
                catch (JSONException ex2) {
                    ex2.printStackTrace();
                    return;
                }
            }
            this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
        }
    }

    class FacebookManager1 implements FacebookCallback {
        // FF: synthetic field
        final FacebookManager this0;

        FacebookManager1(FacebookManager var1) {
            this.this0 = var1;
        }

        @Override
        public void onSuccess(Object o) {
            FacebookManager.access000(this.this0);
        }

        public void onCancel() {
            this.this0.delegate.socialNetworkAuthoriseFailed(true);
        }

        public void onError(FacebookException var1) {
            this.this0.delegate.socialNetworkAuthoriseFailed(false);
        }
    }
}