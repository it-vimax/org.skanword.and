package org.skanword.and.network.socialnetworks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public abstract class SocialNetworkManager
{
    protected Context appContext;
    protected ISocialNetworkManagerDelegate delegate;
    protected final Gson gson;
    protected boolean initted;
    protected SocialNetworkManager.SocialUser socialUser;

    public SocialNetworkManager() {
        super();
        this.gson = new Gson();
    }

    public static String getSocialNetworkStringId(final SocialNetworkManager.SocialNetwork socialNetwork) {
        final int n = SocialNetworkManager.SocialNetworkManager1.socialNetwork[socialNetwork.ordinal()];
        if (n == 1) {
            return "cm";
        }
        if (n == 2) {
            return "fb";
        }
        if (n != 3) {
            return "";
        }
        return "vk";
    }

    public static SocialNetworkManager.SocialNetwork getSocialNetworkfromName(final String s) {
        if (s.equals("cm")) {
            return SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK;
        }
        if (s.equals("fb")) {
            return SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB;
        }
        if (s.equals("vk")) {
            return SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK;
        }
        return SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_NO;
    }

    public abstract void authorise(final Activity p0);

    public abstract void clearSocialNetworkManager();

    public abstract void getCurrentUser();

    public abstract void getUserFriends();

    public void initSocialNetworkManager(final ISocialNetworkManagerDelegate delegate, final Context appContext) {
        this.delegate = delegate;
        this.appContext = appContext;
    }

    public abstract void logout();

    public void onActivityResult(final Activity activity, final int n, final int n2, final Intent intent) {
    }

    public static class SocialUser implements Serializable
    {
        private static final long serialVersionUID = 7793063125969187103L;
        @SerializedName("avatar_url")
        private String avatarUrl;
        @SerializedName("first_name")
        private String firstName;
        @SerializedName("second_name")
        private String secondName;
        private String socialid;
        private String token;
        private String uid;
        @SerializedName("updated_time")
        private long updatedTime;

        public SocialUser(final long updatedTime, final String token, final String socialid, final String firstName, final String secondName, final String uid, final String avatarUrl) {
            super();
            this.updatedTime = 0L;
            this.updatedTime = updatedTime;
            this.socialid = socialid;
            this.firstName = firstName;
            this.secondName = secondName;
            this.uid = uid;
            this.token = token;
            this.avatarUrl = avatarUrl;
        }

        public String getAvatarUrl() {
            return this.avatarUrl;
        }

        public String getFirstName() {
            return this.firstName;
        }

        public String getSecondName() {
            return this.secondName;
        }

        public String getSocialId() {
            return this.socialid;
        }

        public String getToken() {
            return this.token;
        }

        public String getUid() {
            return this.uid;
        }

        public long getUpdatedTime() {
            return this.updatedTime;
        }

        public void setUpdatedTime(final long updatedTime) {
            this.updatedTime = updatedTime;
        }
    }

    public enum SocialNetwork {
        SOCIAL_NETWORK_NO,
        SOCIAL_NETWORK_OK,
        SOCIAL_NETWORK_FB,
        SOCIAL_NETWORK_VK

    }

    static class SocialNetworkManager1 {
    static final /* synthetic */ int[] socialNetwork;

    static {
        socialNetwork = new int[SocialNetworkManager.SocialNetwork.values().length];
        try {
            SocialNetworkManager1.socialNetwork[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK.ordinal()] = 1;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            SocialNetworkManager1.socialNetwork[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB.ordinal()] = 2;
        }
        catch (NoSuchFieldError noSuchFieldError) {}
        try {
            SocialNetworkManager1.socialNetwork[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK.ordinal()] = 3;
        }
        catch (NoSuchFieldError noSuchFieldError) {
        }
    }
}

}
