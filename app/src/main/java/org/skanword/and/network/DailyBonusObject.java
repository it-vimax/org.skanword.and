package org.skanword.and.network;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DailyBonusObject implements Serializable {
    private static final long serialVersionUID = 231081669275352021L;
    private long downloadedTime;
    private int mScratchedPosition = 0;
    private List<Map<String, Object>> notifications;
    private long time;
    @SerializedName("timeout")
    private int timeLeft;
    private List<Integer> variants;
    private int win;

    public DailyBonusObject(int i, int i2, List<Integer> list, List<Map<String, Object>> list2, long j) {
        this.win = i;
        this.timeLeft = i2;
        this.variants = list;
        this.notifications = list2;
        this.time = j;
    }

    public long getDownloadedTime() {
        return this.downloadedTime;
    }

    public void setWin(int i) {
        this.win = i;
    }

    public void setVariants(List<Integer> list) {
        this.variants = list;
    }

    public void setDownloadedTime(long j) {
        this.downloadedTime = j;
    }

    public int getWin() {
        return this.win;
    }

    public int getTimeLeft() {
        return this.timeLeft;
    }

    public List<Integer> getVariants() {
        return this.variants;
    }

    public int getOtherVariant(int paramInt) {
        List<Integer> list = getVariants();
        StringBuilder stringBuildernull = new StringBuilder();
        stringBuildernull.append("vars  ");
        stringBuildernull.append(list);
        stringBuildernull.append("  ");
        stringBuildernull.append(getWin());
        Log.v("SkanwordsFunc", stringBuildernull.toString());
        for (Integer integer : list) {
            if (integer.intValue() == getWin()) {
                list.remove(integer);
                break;
            }
        }
        try {
            return ((Integer)list.get(paramInt - 1)).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public List<Map<String, Object>> getNotification() {
        return this.notifications;
    }

    public void setTimeLeft(int i) {
        this.timeLeft = i;
    }

    public void setScratchedPosition(int i) {
        this.mScratchedPosition = i;
    }

    public int getScratchedPosition() {
        return this.mScratchedPosition;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long j) {
        this.time = j;
    }
}
