package org.skanword.and.network;

import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;

public class UserAuth {
    private final String authKey;
    private String avatarUrl = null;
    private String fullName = null;
    private UserObject loginUserState = null;
    private SocialNetwork socialNetwork;
    private final String userId;
    private final String userSocialId;
    private final String vendorId;

    public UserAuth(String str, String str2, String str3, String str4, SocialNetwork socialNetwork) {
        this.userSocialId = str3;
        this.vendorId = str;
        this.userId = str2;
        this.authKey = str4;
        this.socialNetwork = socialNetwork;
    }

    public UserObject getLoginUserState() {
        return this.loginUserState;
    }

    public void setLoginUserState(UserObject userObject) {
        this.loginUserState = userObject;
    }

    public String getAvatarUrl() {
        return this.avatarUrl;
    }

    public void setAvatarUrl(String str) {
        this.avatarUrl = str;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String str) {
        this.fullName = str;
    }

    public SocialNetwork getSocialNetwork() {
        return this.socialNetwork;
    }

    public void setSocialNetwork(SocialNetwork socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public String getUserSocialId() {
        return this.userSocialId;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getAuthKey() {
        return this.authKey;
    }

    public String getVendorId() {
        return this.vendorId;
    }
}
