package org.skanword.and.network;

import android.graphics.BitmapFactory;
import android.util.Log;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.MainDataManager.IDowloadSetProgress;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DownloaderArhiveFile {
    private int endProgressDownload = 90;
    private String fileName;
    private IDowloadSetProgress progress;
    private int startProgress;
    private String unZipPath;
    private String zipFilePath;

    private void unzip() {
        Log.d("ANDRO_ASYNC", "unzip");
        File file = new File(this.unZipPath);
        if (!file.exists() && file.mkdir())
            Log.d("ANDRO_ASYNC", "create unzip dir");
        try {
            int i;
            ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(this.zipFilePath)));
            while (true) {
                try {
                    ZipEntry zipEntry = zipInputStream.getNextEntry();
                    i = 0;
                    if (zipEntry != null) {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        byte[] arrayOfByte = new byte[1024];
                        String  string = zipEntry.getName();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("unzip ");
                        stringBuilder.append(this.unZipPath);
                        stringBuilder.append("/");
                        stringBuilder.append(string);
                        Log.d("ANDRO_ASYNC", stringBuilder.toString());
                        stringBuilder = new StringBuilder();
                        stringBuilder.append(this.unZipPath);
                        stringBuilder.append("/");
                        stringBuilder.append(string);
                        FileOutputStream fileOutputStream = new FileOutputStream(stringBuilder.toString());
                        while (true) {
                            i = zipInputStream.read(arrayOfByte);
                            if (i != -1) {
                                byteArrayOutputStream.write(arrayOfByte, 0, i);
                                fileOutputStream.write(byteArrayOutputStream.toByteArray());
                                byteArrayOutputStream.reset();
                                continue;
                            }
                            break;
                        }
                        fileOutputStream.close();
                        zipInputStream.closeEntry();
                        continue;
                    }
                } catch (IOException e) {}

                break;
            }

            try {
                zipInputStream.close();
                File[] arrayOfFile = file.listFiles();
                int j = arrayOfFile.length;
                while (j < j) {
                    if (BitmapFactory.decodeFile(arrayOfFile[j].getAbsolutePath()) == null) {
                        if (this.progress != null)
                            this.progress.onProgress(-1);
                        File file1 = new File(this.unZipPath);
                        if (file1.exists())
                            file1.delete();
                        break;
                    }
                    j++;
                }
            } catch (IOException e) {}


        } catch (FileNotFoundException e) {
            if (this.progress != null)
                this.progress.onProgress(-1);
            File file2 = new File(this.unZipPath);
            if (file2.exists())
                file2.delete();
        } finally {
            (new File(this.zipFilePath)).delete();
        }
        file.delete();
    }

    public void dowload(URL paramURL, String paramString) throws Exception {
        FileOutputStream fileOutputStream;
        if (paramString != null) {
            this.fileName = getFileName(paramString);
            this.unZipPath = getUnzipPath(paramString);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("fileName of file: ");
            stringBuilder.append(paramURL);
            Log.v("ANDRO_ASYNC", stringBuilder.toString());
            URLConnection Connection  = paramURL.openConnection();
            Connection.connect();
            int i = Connection.getContentLength();
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Lenght of file: ");
            stringBuilder2.append(i);
            Log.v("ANDRO_ASYNC", stringBuilder2.toString());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(paramURL.openStream());
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("input ");
            stringBuilder3.append(stringBuilder2);
            Log.v("ANDRO_ASYNC", stringBuilder3.toString());
            StringBuilder stringBuilder4 = new StringBuilder();
            stringBuilder4.append(SmappsScanwords.CROSSWORDS_DIR.getPath());
            stringBuilder4.append("/");
            stringBuilder4.append(this.fileName);
            this.zipFilePath = stringBuilder4.toString();
            StringBuilder stringBuilder5 = new StringBuilder();
            stringBuilder5.append("zipFilePath ");
            stringBuilder5.append(this.zipFilePath);
            Log.v("ANDRO_ASYNC", stringBuilder5.toString());
            File file = new File(this.zipFilePath);
            fileOutputStream = new FileOutputStream(this.zipFilePath);
            StringBuilder stringBuilde6 = new StringBuilder();
            stringBuilde6.append("input file: ");
            stringBuilde6.append(file);
            stringBuilde6.append(" output file = ");
            stringBuilde6.append(fileOutputStream);
            Log.v("ANDRO_ASYNC", stringBuilde6.toString());
            byte[] arrayOfByte = new byte[1024];
            long l = 0L;
            while (true) {
                int j = bufferedInputStream.read(arrayOfByte);
                if (j != -1) {
                    l += j;
                    MainDataManager.IDowloadSetProgress iDowloadSetProgress1 = this.progress;
                    if (iDowloadSetProgress1 != null)
                        iDowloadSetProgress1.onProgress(this.startProgress + Math.round((float)(100L * l)) / i * (this.endProgressDownload - this.startProgress) / 100);
                    fileOutputStream.write(arrayOfByte, 0, j);
                    continue;
                }
                break;
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            bufferedInputStream.close();
            unzip();
            MainDataManager.IDowloadSetProgress iDowloadSetProgress = this.progress;
            if (iDowloadSetProgress != null)
                iDowloadSetProgress.onProgress(100);
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" dowload  ");
//        stringBuilder.append(fileOutputStream);
        Log.v("", stringBuilder.toString());
        throw new Exception();
    }

    public static String getFileName(String str) {
        String[] str2 = str.split("/");
        return str2[str2.length - 1];
    }

    public static String getUnzipPath(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SmappsScanwords.CROSSWORDS_DIR.getPath());
        stringBuilder.append("/");
        stringBuilder.append(getFolderName(str));
        return stringBuilder.toString();
    }

    public static String getFolderName(String str) {
        return getFileName(str).split(".zip")[0];
    }

    public void setProgress(IDowloadSetProgress iDowloadSetProgress, int i) {
        this.progress = iDowloadSetProgress;
        this.startProgress = i;
    }
}
