package org.skanword.and.network.socialnetworks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKProgressType;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.util.VKUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;

public class VkontakteManager extends SocialNetworkManager
{
    private static String[] sMyScope;
    private static String sTokenKey = "VK_ACCESS_TOKEN";
    VKAccessTokenTracker vkAccessTokenTracker;

    static {
        VkontakteManager.sMyScope = new String[] { "offline" };
    }

    public VkontakteManager() {
        super();
        this.vkAccessTokenTracker = (VKAccessTokenTracker)new VkontakteManager.VkontakteManager1(this);
    }

    public void authorise(final Activity activity) {
        VKSdk.login(activity, VkontakteManager.sMyScope);
    }

    public void clearSocialNetworkManager() {
    }

    public void getCurrentUser() {
        VKApi.users().get(VKParameters.from(new Object[] { "fields", "photo_100" })).executeWithListener((VKRequest.VKRequestListener)new VkontakteManager.VkontakteManager3(this));
    }

    public void getUserFriends() {
        VKApi.friends().get(VKParameters.from(new Object[] { "fields", "photo_100" })).executeWithListener((VKRequest.VKRequestListener)new VkontakteManager.VkontakteManager4(this));
    }

    public void initSocialNetworkManager(final ISocialNetworkManagerDelegate socialNetworkManagerDelegate, final Context context) {
        super.initSocialNetworkManager(socialNetworkManagerDelegate, context);
        this.vkAccessTokenTracker.startTracking();
        final String[] certificateFingerprint = VKUtil.getCertificateFingerprint(context, context.getPackageName());
        final StringBuilder sb = new StringBuilder();
        sb.append("initSocialNetworkManager  ");
        sb.append(certificateFingerprint[0]);
        Log.v("", sb.toString());
    }

    public void logout() {
    }

    public void onActivityResult(final Activity activity, final int n, final int n2, final Intent intent) {
        if (!VKSdk.onActivityResult(n, n2, intent, (VKCallback)new VkontakteManager.VkontakteManager2(this))) {
            super.onActivityResult(activity, n, n2, intent);
        }
    }

    class VkontakteManager1 extends VKAccessTokenTracker {
        final /* synthetic */ VkontakteManager this0;

        VkontakteManager1(final VkontakteManager this0) {
            super();
            this.this0 = this0;
        }

        public void onVKAccessTokenChanged(final VKAccessToken vkAccessToken, final VKAccessToken vkAccessToken2) {
            if (vkAccessToken2 == null) {
                this.this0.getCurrentUser();
            }
        }
    }

    class VkontakteManager2 implements VKCallback<VKAccessToken> {
        final /* synthetic */ VkontakteManager this0;

        VkontakteManager2(final VkontakteManager this0) {
            super();
            this.this0 = this0;
        }

        public void onError(final VKError vkError) {
            this.this0.delegate.socialNetworkAuthoriseFailed(true);
        }

        public void onResult(final VKAccessToken vkAccessToken) {
            this.this0.getCurrentUser();
        }
    }

    class VkontakteManager3 extends VKRequest.VKRequestListener {
        final /* synthetic */ VkontakteManager this0;

        VkontakteManager3(final VkontakteManager this0) {
            super();
            this.this0 = this0;
        }

        public void attemptFailed(final VKRequest vkRequest, final int n, final int n2) {
            this.this0.delegate.socialNetworkAuthoriseFailed(false);
        }

        public void onComplete(VKResponse var1) {
            Object var3 = null;

            JSONObject var7;
            label36: {
                JSONException var2;
                label35: {
                    try {
                        var7 = var1.json.getJSONArray("response").getJSONObject(0);
                    } catch (JSONException var6) {
                        var2 = var6;
                        var7 = null;
                        break label35;
                    }

                    StringBuilder var8 = new StringBuilder();
                    var8.append("");
                    var8.append(var7.toString());
                    Log.v("", var8.toString());
                    break label36;

                }

                var2.printStackTrace();
            }

            if (var7 != null) {
                String var9 = (String)var3;

                try {
                    if (var7.getString("photo_100") != null) {
                        var9 = var7.getString("photo_100");
                    }

                    this.this0.socialUser = new SocialUser((new Date()).getTime(), VKSdk.getAccessToken().accessToken, SocialNetworkManager.getSocialNetworkStringId(SocialNetwork.SOCIAL_NETWORK_VK), var7.getString("first_name"), var7.getString("last_name"), var7.getString("id"), var9);
                    this.this0.getUserFriends();
                } catch (JSONException var4) {
                    var4.printStackTrace();
                }
            } else {
                this.this0.delegate.socialNetworkAuthoriseFailed(false);
            }
        }

        public void onError(final VKError vkError) {
            this.this0.delegate.socialNetworkAuthoriseFailed(false);
        }

        public void onProgress(final VKRequest.VKProgressType vkRequestVKProgressType, final long n, final long n2) {
        }
    }

    class VkontakteManager4 extends VKRequest.VKRequestListener {
        final /* synthetic */ VkontakteManager this0;

        VkontakteManager4(final VkontakteManager this0) {
            super();
            this.this0 = this0;
        }

        public void attemptFailed(final VKRequest vkRequest, final int n, final int n2) {
            this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
        }

        public void onComplete(final VKResponse vkResponse) {
            JSONArray jsonArray;
            try {
                jsonArray = vkResponse.json.getJSONArray("items");
            }
            catch (JSONException ex) {
                ex.printStackTrace();
                jsonArray = null;
            }
            if (jsonArray != null) {
                try {
                    final ArrayList<String> list = new ArrayList<String>();
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        list.add(jsonArray.getJSONObject(i).getString("id"));
                    }
                    this.this0.delegate.socialNetworkFriendsGot((List)list);
                    this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
                    return;
                }
                catch (JSONException ex2) {
                    ex2.printStackTrace();
                    return;
                }
            }
            this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
        }

        public void onError(final VKError vkError) {
            this.this0.delegate.socialNetworkAuthorised(this.this0.socialUser);
        }

        public void onProgress(final VKRequest.VKProgressType vkRequestVKProgressType, final long n, final long n2) {
        }
    }
}
