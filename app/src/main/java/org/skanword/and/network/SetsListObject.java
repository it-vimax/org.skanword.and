package org.skanword.and.network;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListDirrectionType;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.datamanager.SkanwordsSetInfo;

public class SetsListObject implements Serializable {
    private static final long serialVersionUID = -6152007683222991068L;
    private TaskListDirrectionType dirrectionType;
    @SerializedName("set_id_first")
    private final int latestSetId;
    private TaskListType listType;
    @SerializedName("set_id_last")
    private final int oldestSetId;
    @SerializedName("page_next")
    private final boolean pageNextAvailable;
    @SerializedName("page_previous")
    private final boolean pagePreviousAvailable;
    @SerializedName("list")
    private final List<SkanwordsSetInfo> setsList;
    @SerializedName("set_tomorrow")
    private final SkanwordsSetInfo tomorrowSet;

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public SetsListObject(boolean z, boolean z2, int i, int i2, List<SkanwordsSetInfo> list, SkanwordsSetInfo skanwordsSetInfo) {
        this.pagePreviousAvailable = z;
        this.pageNextAvailable = z2;
        this.latestSetId = i;
        this.oldestSetId = i2;
        this.setsList = list;
        this.tomorrowSet = skanwordsSetInfo;
    }

    public boolean isPagePreviousAvailable() {
        return this.pagePreviousAvailable;
    }

    public boolean isPageNextAvailable() {
        return this.pageNextAvailable;
    }

    public int getLatestSetId() {
        return this.latestSetId;
    }

    public int getOldestSetId() {
        return this.oldestSetId;
    }

    public List<SkanwordsSetInfo> getSetsList() {
        return this.setsList;
    }

    public SkanwordsSetInfo getTomorrowSet() {
        return this.tomorrowSet;
    }

    public TaskListType getListType() {
        return this.listType;
    }

    public void setListType(TaskListType taskListType) {
        this.listType = taskListType;
    }

    public TaskListDirrectionType getDirrectionType() {
        return this.dirrectionType;
    }

    public void setDirrectionType(TaskListDirrectionType taskListDirrectionType) {
        this.dirrectionType = taskListDirrectionType;
    }
}
