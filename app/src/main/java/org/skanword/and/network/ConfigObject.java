package org.skanword.and.network;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.skanword.and.etc.Utils;

public class ConfigObject implements Serializable {
    private static final long serialVersionUID = -2846031599572231609L;
    @SerializedName("app_ads")
    private final AppAds appAds;
    @SerializedName("app_offers")
    private final AppOffers appOffers;
    @SerializedName("local_notifications")
    private final LocalNotification localNotifications;
    @SerializedName("news_hash")
    private final String newsHash;
    @SerializedName("payment_track")
    private final boolean paymentTrack;
    @SerializedName("push_token_exist")
    private final boolean pushTokenExist;
    @SerializedName("shop_hash")
    private final String shopHash;
    @SerializedName("version")
    private final Versions versions;

    public static class AppAds implements Serializable {
        private static final long serialVersionUID = -8690415063978369826L;
        @SerializedName("cross")
        private final List<AppAdsObject> crossBanners;
        @SerializedName("show_popup")
        private final boolean isShowPopUp;
        @SerializedName("main")
        private final List<AppAdsObject> mainShows;
        @SerializedName("questions")
        private final List<AppAdsObject> questionsShows;
        @SerializedName("show_delay")
        private final int showDelay;

        public static class AppAdsObject implements Serializable {
            private static final long serialVersionUID = -8159363141808903804L;
            @SerializedName("app_id")
            private final String appId;
            @SerializedName("app_signature")
            private final String appSignature;
            private final int coefficient;
            private final String name;

            public static long getSerialversionuid() {
                return serialVersionUID;
            }

            public AppAdsObject(String str, String str2, String str3, int i) {
                this.name = str;
                this.appSignature = str2;
                this.appId = str3;
                this.coefficient = i;
            }

            public String getName() {
                return this.name;
            }

            public String getAppSignature() {
                return this.appSignature;
            }

            public String getAppId() {
                return this.appId;
            }

            public int getCoefficient() {
                return this.coefficient;
            }
        }

        public AppAds(List<AppAdsObject> list, List<AppAdsObject> list2, List<AppAdsObject> list3, boolean z, int i) {
            this.crossBanners = list3;
            this.mainShows = list;
            this.questionsShows = list2;
            this.isShowPopUp = z;
            this.showDelay = i;
        }

        public AppAds() {
            this.mainShows = new ArrayList();
            this.questionsShows = new ArrayList();
            this.crossBanners = new ArrayList();
            this.showDelay = 0;
            this.isShowPopUp = false;
        }

        public List<AppAdsObject> getMainShows() {
            return this.mainShows;
        }

        public List<AppAdsObject> getQuestionsShows() {
            return this.questionsShows;
        }

        public List<AppAdsObject> getCrossBanners() {
            return this.crossBanners;
        }

        public boolean isShowPopUp() {
            return this.isShowPopUp;
        }

        public int getShowDelay() {
            return this.showDelay;
        }
    }

    public static class AppOffers implements Serializable {
        private static final long serialVersionUID = 8736484659189829995L;
        private final List<AppAds.AppAdsObject> video;
        @SerializedName("video_hints")
        private final List<AppAds.AppAdsObject> videoHints;
        @SerializedName("video_interstitial")
        private final List<AppAds.AppAdsObject> videoInterstitial;

        public AppOffers(List<AppAds.AppAdsObject> list, List<AppAds.AppAdsObject> list2, List<AppAds.AppAdsObject> list3) {
            this.videoHints = list;
            this.video = list2;
            this.videoInterstitial = list3;
        }

        public AppOffers() {
            this.videoHints = new ArrayList();
            this.video = new ArrayList();
            this.videoInterstitial = new ArrayList();
        }

        public List<AppAds.AppAdsObject> getVideoHints() {
            return this.videoHints;
        }

        public List<AppAds.AppAdsObject> getVideo() {
            return this.video;
        }

        public List<AppAds.AppAdsObject> getVideoInterstitial() {
            return this.videoInterstitial;
        }
    }

    public static class LocalNotification implements Serializable {
        private static final long serialVersionUID = -2370775632301098549L;
        @SerializedName("remind_off")
        private final List<NotificationRemind> remindOff;
        @SerializedName("remind_on")
        private final List<NotificationRemind> remindOn;

        public LocalNotification(List<NotificationRemind> list, List<NotificationRemind> list2) {
            this.remindOff = list;
            this.remindOn = list2;
        }

        public LocalNotification() {
            this.remindOn = new ArrayList();
            this.remindOff = new ArrayList();
        }

        public List<NotificationRemind> getRemindOff() {
            return this.remindOff;
        }

        public List<NotificationRemind> getRemindOn() {
            return this.remindOn;
        }

        public boolean compareWithNotifications(LocalNotification localNotification) {
            Gson gson = new Gson();
            return Utils.md5(gson.toJson(localNotification)).equals(Utils.md5(gson.toJson(this)));
        }
    }

    public static class NotificationRemind implements Serializable {
        private static final long serialVersionUID = 155091005774545474L;
        private final List<Integer> days;
        private final String text;

        public NotificationRemind(List<Integer> list, String str) {
            this.days = list;
            this.text = str;
        }

        public List<Integer> getDays() {
            return this.days;
        }

        public String getText() {
            return this.text;
        }
    }

    public static class Version implements Serializable {
        private static final long serialVersionUID = -2006107567450886910L;
        private final int build;
        private final String text;

        public Version(int i, String str) {
            this.build = i;
            this.text = str;
        }

        public int getBuild() {
            return this.build;
        }

        public String getText() {
            String str = this.text;
            return str == null ? "" : str;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Version version = (Version) obj;
            if (getBuild() != version.getBuild() || getText().equals(version.getText()) == false) {
                z = false;
            }
            return z;
        }
    }

    public static class Versions implements Serializable {
        private static final long serialVersionUID = 2627868444307832476L;
        private final Version inform;
        private final Version lock;

        public Versions(Version version, Version version2) {
            this.inform = version;
            this.lock = version2;
        }

        public Version getInform() {
            Version version = this.inform;
            return version == null ? new Version(0, "") : version;
        }

        public Version getLock() {
            Version version = this.lock;
            return version == null ? new Version(0, "") : version;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Versions versions = (Versions) obj;
            if (!getInform().equals(versions.getInform()) || getInform().equals(versions.getLock()) == false) {
                z = false;
            }
            return z;
        }
    }

    public ConfigObject(LocalNotification localNotification, boolean z, AppAds appAds, AppOffers appOffers, boolean z2, String str, String str2, Versions versions) {
        this.localNotifications = localNotification;
        this.pushTokenExist = z;
        this.appAds = appAds;
        this.appOffers = appOffers;
        this.paymentTrack = z2;
        this.newsHash = str;
        this.shopHash = str2;
        this.versions = versions;
    }

    public String getNewsHash() {
        return this.newsHash;
    }

    public String getShopHash() {
        return this.shopHash;
    }

    public boolean paymentTrack() {
        return this.paymentTrack;
    }

    public AppOffers getAppOffers() {
        return this.appOffers;
    }

    public Versions getVersions() {
        return this.versions;
    }

    public AppAds getAppAds() {
        return this.appAds;
    }

    public LocalNotification getLocalNotifications() {
        return this.localNotifications;
    }

    public boolean isPushTokenExist() {
        return this.pushTokenExist;
    }
}
