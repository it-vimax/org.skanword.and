package org.skanword.and.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.skanword.and.R;
import org.skanword.and.datamanager.NewsDataManager;
import org.skanword.and.datamanager.RatingDataManager;
import org.skanword.and.datamanager.ShopDataManager;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.libs.musselwhizzle.mvc.events.SimpleEvent;

import com.vk.sdk.VKSdk;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.menu.AuthorisationAcceptActivity;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.socialnetworks.FacebookManager;
import org.skanword.and.network.socialnetworks.ISocialNetworkManagerDelegate;
import org.skanword.and.network.socialnetworks.OdnoklassnikiManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager;
import org.skanword.and.network.socialnetworks.VkontakteManager;

public class MainNetworkManager implements ISocialNetworkManagerDelegate
{
    private static final String EVENT_NETWORK_STATE_CHANGED = "EVENT_NETWORK_STATE_CHANGED";
    public static final String EVENT_NETWORK_STATE_CHANGED_ALL = "EVENT_NETWORK_STATE_CHANGED_ALL";
    public static final int INFO_NEWS = 1;
    public static final int INFO_RATING = 4;
    public static final int INFO_SHOP = 2;
    private static final String SERVER_URL_PROD = "https://skanword.org/rest/";
    private static final String SERVER_URL_TEST = "https://skanword.smapps.net/rest/";
    public static final int SKAN_DATA_DATA = 2;
    public static final int SKAN_DATA_PROGRESS = 1;
    private static final String USER_AUTH_PREFS_NAME = "UserAuthPrefs";
    private static final String VENDOR_KEY_PROD = "23599e3e23eb187798e2f6cedb44eb91";
    private static final String VENDOR_KEY_TEST = "d81b5f222115939769a88a690843d04c";
    private static MainNetworkManager instance;
    static Integer mParsingMutex;
    private final Gson gson;
    private boolean isInited;
    private ConnectivityManager mConnectivityManager;
    private Activity mCurrentAuthActivity;
    private Map<String, HttpPost> mCurrentRequests;
    private boolean mInAuthProcess;
    private SocialNetworkManager.SocialUser mLastSocialUser;
    private boolean mShowingConnectionAlert;
    private SocialNetworkManager mSocialNetworkManager;
    private String mStoreName;
    private boolean mTrackPayment;
    private UserAuth mUserAuth;
    private ProgressDialog mWaitingProgressDialog;

    static {
        MainNetworkManager.mParsingMutex = 1;
    }

    protected MainNetworkManager() {
        super();
        this.gson = new Gson();
        this.mUserAuth = null;
        this.mSocialNetworkManager = null;
        this.mLastSocialUser = null;
        this.mCurrentAuthActivity = null;
        this.mStoreName = null;
        this.mInAuthProcess = false;
        this.mShowingConnectionAlert = false;
        this.mTrackPayment = false;
        this.mCurrentRequests = Collections.synchronizedMap(new HashMap<String, HttpPost>());
        this.isInited = false;
    }

    static /* synthetic */ ProgressDialog access000(final MainNetworkManager mainNetworkManager) {
        return mainNetworkManager.mWaitingProgressDialog;
    }

    static /* synthetic */ ProgressDialog access002(final MainNetworkManager mainNetworkManager, final ProgressDialog mWaitingProgressDialog) {
        return mainNetworkManager.mWaitingProgressDialog = mWaitingProgressDialog;
    }

    static /* synthetic */ SocialNetworkManager.SocialUser access100(final MainNetworkManager mainNetworkManager) {
        return mainNetworkManager.mLastSocialUser;
    }

    static /* synthetic */ Activity access200(final MainNetworkManager mainNetworkManager) {
        return mainNetworkManager.mCurrentAuthActivity;
    }

    static /* synthetic */ UserAuth access300(final MainNetworkManager mainNetworkManager) {
        return mainNetworkManager.mUserAuth;
    }

    static /* synthetic */ UserAuth access302(final MainNetworkManager mainNetworkManager, final UserAuth mUserAuth) {
        return mainNetworkManager.mUserAuth = mUserAuth;
    }

    static /* synthetic */ boolean access402(final MainNetworkManager mainNetworkManager, final boolean mInAuthProcess) {
        return mainNetworkManager.mInAuthProcess = mInAuthProcess;
    }

    static /* synthetic */ boolean access500(final MainNetworkManager mainNetworkManager) {
        return mainNetworkManager.mShowingConnectionAlert;
    }

    static /* synthetic */ boolean access502(final MainNetworkManager mainNetworkManager, final boolean mShowingConnectionAlert) {
        return mainNetworkManager.mShowingConnectionAlert = mShowingConnectionAlert;
    }

    static /* synthetic */ UserObject access600(final MainNetworkManager mainNetworkManager, final boolean b, final Activity activity) {
        return mainNetworkManager.purchaseSend(b, activity);
    }

    private List<NameValuePair> addParamsWithUpdates(final List<NameValuePair> list) {
        List<NameValuePair> list2 = list;
        if (list == null) {
            list2 = new ArrayList<NameValuePair>();
        }
        final String json = this.gson.toJson(UserUpdatesManager.getInstance().saveCurrentData());
        final StringBuilder sb = new StringBuilder();
        sb.append("addParamsWithUpdates  ");
        sb.append(json);
        Log.v("SkanwordsNetwork", sb.toString());
        list2.add((NameValuePair)new BasicNameValuePair("updates", Base64.encodeToString(json.getBytes(), 0)));
        return list2;
    }

    private void checkResponseForDailyBonus(JsonElement value) {
        value = value.getAsJsonObject().get("bonus_hourly");
        if (value != null) {
            MainDataManager.getInstance().dailyBonusObjectRecieved((DailyBonusObject)this.gson.fromJson(value, DailyBonusObject.class));
        }
    }

    private void checkResponseForPaymentMessage(JsonElement value) {
        value = value.getAsJsonObject().get("payment_message");
        if (value != null && TopActivityManager.get().getTopActivity() != null) {
            TopActivityManager.get().getTopActivity().runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager12(this, value));
        }
    }

    private void checkResponseForServerMessage(final JsonElement jsonElement) throws JsonSyntaxException {
        this.checkResponseForServerMessage(jsonElement, TopActivityManager.get().getTopActivity());
    }

    private void checkResponseForServerMessage(final JsonElement jsonElement, final Activity activity) throws JsonSyntaxException {
        JsonObject asJsonObject;
        if (jsonElement.getAsJsonObject().get("message") != null) {
            asJsonObject = jsonElement.getAsJsonObject().get("message").getAsJsonObject();
        }
        else {
            asJsonObject = null;
        }
        if (asJsonObject != null && activity != null) {
            activity.runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager13(this, activity, asJsonObject));
        }
    }

    private void checkResponseForUpdatesHashes(JsonElement value) {
        value = value.getAsJsonObject().get("updates");
        List list;
        if (value != null) {
            list = this.gson.fromJson(value, new MainNetworkManager.MainNetworkManager11(this).getType());
        }
        else {
            list = null;
        }
        UserUpdatesManager.getInstance().clearUpdateDataForHashes((List)list);
    }

    private void checkResponseForUserData(JsonElement value) {
        value = value.getAsJsonObject().get("user");
        if (value != null) {
            MainDataManager.getInstance().userDataReceived((UserObject)this.gson.fromJson(value, UserObject.class));
        }
    }

    private void checkServerResponse(final JsonElement jsonElement) {
        this.checkResponseForPaymentMessage(jsonElement);
        this.checkResponseForServerMessage(jsonElement);
        this.checkResponseForUpdatesHashes(jsonElement);
        this.checkResponseForUserData(jsonElement);
        this.checkResponseForDailyBonus(jsonElement);
    }

    public static MainNetworkManager getInstance() {
        if (MainNetworkManager.instance == null) {
            MainNetworkManager.instance = new MainNetworkManager();
        }
        final MainNetworkManager instance = MainNetworkManager.instance;
        if (!instance.isInited) {
            instance.init();
        }
        return MainNetworkManager.instance;
    }

    public static DefaultHttpClient getThreadSafeClient() {
        final DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        final ClientConnectionManager connectionManager = defaultHttpClient.getConnectionManager();
        final HttpParams params = defaultHttpClient.getParams();
        return new DefaultHttpClient((ClientConnectionManager)new ThreadSafeClientConnManager(params, connectionManager.getSchemeRegistry()), params);
    }

    public static String getVendorKey() {
        if (isRelese()) {
            return "23599e3e23eb187798e2f6cedb44eb91";
        }
        return "d81b5f222115939769a88a690843d04c";
    }

    public static String getserverUrl() {
        if (isRelese()) {
            return "https://skanword.org/rest/";
        }
        return "https://skanword.smapps.net/rest/";
    }

    private void illegalJSON(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("illegal json ");
        sb.append(s);
        Log.v("", sb.toString());
        final Activity topActivity = TopActivityManager.get().getTopActivity();
        if (topActivity == null) {
            return;
        }
        topActivity.runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager8(this, topActivity));
    }

    private void init() {
        if (MainMenuActivity.hasPermissions()) {
            this.isInited = true;
            this.mUserAuth = this.getLocalUserAuth();
        }
    }

    private boolean isConnected() {
        final NetworkInfo activeNetworkInfo = this.mConnectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static boolean isRelese() {
        return true;
    }

    private JsonElement parseRequestResult(final String json, final boolean b) {
        while (true) {
            while (true) {
                synchronized (this) {
                    final JsonParser jsonParser = new JsonParser();
                    if (json != null) {
                        final JsonElement parse = jsonParser.parse(json);
                        if (b && parse != null) {
                            this.checkServerResponse(parse);
                        }
                        return parse;
                    }
                }
                final JsonElement parse = null;
                continue;
            }
        }
    }

    private UserObject purchaseSend(boolean z, Activity activity) {
        Collection purchasesRestored;
        if (z) {
            purchasesRestored = MainDataManager.getInstance().getPurchasesData().getPurchasesRestored();
        } else {
            purchasesRestored = MainDataManager.getInstance().getPurchasesData().getPurchases();
        }
        List arrayList = new ArrayList(purchasesRestored);
        UserObject userObject = null;
        if (checkAuth(activity) && hasNetworkConnection(activity) != false && arrayList.size() > 0) {
            List<NameValuePair> activity2 = addParamsWithUpdates(arrayList);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("");
            stringBuilder.append(Integer.valueOf(0));
            arrayList.add(new BasicNameValuePair("restored", stringBuilder.toString()));
            arrayList.add(new BasicNameValuePair("receipts", this.gson.toJson(arrayList)));
            JsonElement z2 = makePostRequestForProtocol("payment", arrayList, true).getJsonResponse();
            Object z3 = z ? z2.getAsJsonObject().get("user") : false;
            if (z) {
                userObject = (UserObject) this.gson.fromJson(z2, UserObject.class);
            }
            MainDataManager.getInstance().getPurchasesData().clearPurcheses(arrayList);
            MainDataManager.getInstance().storePurchases();
        }
        return userObject;
    }

    private void requestError() {
        this.requestError(TopActivityManager.get().getTopActivity());
    }

    private void requestError(final Activity activity) {
        if (activity == null) {
            return;
        }
        activity.runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager10(this, activity));
    }

    public void acceptAuthorisation() {
        this.acceptAuthorisation(null);
    }

    public void acceptAuthorisation(final Activity activity) {
        this.acceptAuthorisation(activity, false);
    }

    public void acceptAuthorisation(final Activity activity, final boolean b) {
        if (this.mWaitingProgressDialog == null) {
            this.showWaitingDialog(this.getCurrentAuthActivity(), "Идет авторизация...");
        }
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.MainNetworkManager3(this, activity));
    }

    public boolean checkAuth(final Activity activity) {
        if (this.mUserAuth == null) {
            this.mUserAuth = this.getLocalUserAuth();
            if (this.mUserAuth == null) {
                this.mUserAuth = this.login(null, activity, null);
                if (this.mUserAuth == null) {
                    return false;
                }
                this.saveAuthData();
            }
        }
        return true;
    }

    public void clear() {
        final SocialNetworkManager mSocialNetworkManager = this.mSocialNetworkManager;
        if (mSocialNetworkManager != null) {
            mSocialNetworkManager.clearSocialNetworkManager();
        }
    }

    public String connectionType() {
        final NetworkInfo[] allNetworkInfo = ((ConnectivityManager)SmappsScanwords.getContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getAllNetworkInfo();
        for (int length = allNetworkInfo.length, i = 0; i < length; ++i) {
            final NetworkInfo networkInfo = allNetworkInfo[i];
            if (networkInfo.getTypeName().equalsIgnoreCase("WIFI") && networkInfo.isConnected()) {
                return "wi-fi";
            }
            if (networkInfo.getTypeName().equalsIgnoreCase("MOBILE") && networkInfo.isConnected()) {
                return "mobile_internet";
            }
        }
        return "no_sync_type";
    }

    public List<NameValuePair> genCommonRequestParams() {
        PackageInfo packageInfo = null;
        Label_0035: {
            try {
                packageInfo = SmappsScanwords.getContext().getPackageManager().getPackageInfo(SmappsScanwords.getContext().getPackageName(), 0);
                break Label_0035;
            }
            catch (NullPointerException ex) {
                ex.printStackTrace();
            }
            catch (PackageManager.NameNotFoundException ex2) {
                ex2.printStackTrace();
            }
            packageInfo = null;
        }
        final ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        final UserAuth mUserAuth = this.mUserAuth;
        if (mUserAuth != null) {
            list.add(new BasicNameValuePair("auth_key", mUserAuth.getAuthKey()));
            list.add(new BasicNameValuePair("user_id", this.mUserAuth.getUserId()));
            list.add(new BasicNameValuePair("vendor_id", this.mUserAuth.getVendorId()));
            list.add(new BasicNameValuePair("last_sync_type", this.connectionType()));
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getNetId());
            sb.append("_");
            sb.append(this.mUserAuth.getVendorId());
            sb.append("_");
            sb.append(getVendorKey());
            list.add(new BasicNameValuePair("vendor_key", Utils.md5(sb.toString())));
        }
        final HashMap<String, String> src = new HashMap<String, String>();
        src.put("device_system_version", System.getProperty("os.version"));
        src.put("device", System.getProperty(Build.DEVICE));
        src.put("device_model", System.getProperty(Build.MODEL));
        src.put("device_product", System.getProperty(Build.PRODUCT));
        if (packageInfo != null) {
            final String versionName = packageInfo.versionName;
            final int versionCode = packageInfo.versionCode;
            src.put("app_version", versionName);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(versionCode);
            src.put("app_build", sb2.toString());
        }
        list.add(new BasicNameValuePair("google_advertising_id", SmappsScanwords.instance.getAdvertisingId()));
        list.add(new BasicNameValuePair("google_android_id", Utils.getAndroidId()));
        list.add(new BasicNameValuePair("google_device_id", Utils.getDeviceId()));
        list.add(new BasicNameValuePair("device_info", Base64.encodeToString(this.gson.toJson(src).getBytes(), 0)));
        return list;
    }

    public Activity getCurrentAuthActivity() {
        return this.mCurrentAuthActivity;
    }

    public SocialNetworkManager.SocialUser getLastSocialUser() {
        return this.mLastSocialUser;
    }

    public UserAuth getLocalUserAuth() {
        final SharedPreferences appSharedPreferences = SmappsScanwords.getAppSharedPreferences("UserAuthPrefs");
        final String string = appSharedPreferences.getString("user_id", (String)null);
        final String string2 = appSharedPreferences.getString("user_auth_key", (String)null);
        final String string3 = appSharedPreferences.getString("vendor_key", (String)null);
        final String string4 = appSharedPreferences.getString("full_name", (String)null);
        final String string5 = appSharedPreferences.getString("avatar_url", (String)null);
        final String string6 = appSharedPreferences.getString("user_social_id", (String)null);
        final SocialNetworkManager.SocialNetwork socialNetworkManagerSocialNetwork = SocialNetworkManager.SocialNetwork.values()[appSharedPreferences.getInt("social_network", SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_NO.ordinal())];
        if (string == null) {
            return null;
        }
        final UserAuth userAuth = new UserAuth(string3, string, string6, string2, socialNetworkManagerSocialNetwork);
        userAuth.setFullName(string4);
        userAuth.setAvatarUrl(string5);
        return userAuth;
    }

    public String getNetId() {
        final String mStoreName = this.mStoreName;
        if (mStoreName != null) {
            return mStoreName;
        }
        String string = SmappsScanwords.getAppSharedPreferences("UserAuthPrefs").getString("store_name", (String)null);
        if (string != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("store name restored ");
            sb.append(string);
            Log.v("", sb.toString());
            return this.mStoreName = string;
        }
        final String storeName = InAppManager.getInstance().getStoreName();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("  storename name ");
        sb2.append(storeName);
        Log.v("", sb2.toString());
        final String[] split = storeName.split("\\.");
        if (split.length > 1) {
            string = split[1];
        }
        else if (split.length > 0) {
            string = split[0];
        }
        if (string != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("store name saved ");
            sb3.append(string);
            Log.v("", sb3.toString());
            SmappsScanwords.getAppSharedPreferencesEditor("UserAuthPrefs").putString("store_name", string).commit();
            this.mStoreName = string;
        }
        return this.mStoreName;
    }

    public UserAuth getUserAuth() {
        return this.mUserAuth;
    }

    public String getUserName() {
        if (this.mUserAuth == null) {
            return"Неизвестно";
        }
        if (this.isUserAuthorised()) {
            return "Some Name";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mUserAuth.getUserId());
        sb.append("");
        return sb.toString();
    }

    public boolean hasNetworkConnection() {
        return this.hasNetworkConnection(null);
    }

    public boolean hasNetworkConnection(final Activity activity) {
        final boolean connected = this.isConnected();
        if (!connected && activity != null) {
            activity.runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager9(this, activity));
        }
        return connected;
    }

    public void hideWaitingDialog(final Activity activity) {
        final MainNetworkManager.MainNetworkManager1 mainNetworkManager1 = new MainNetworkManager.MainNetworkManager1(this);
        if (Looper.myLooper() != Looper.getMainLooper() && activity != null) {
            activity.runOnUiThread((Runnable)mainNetworkManager1);
            return;
        }
        MainMenuActivity.instance.runOnUiThread((Runnable)mainNetworkManager1);
    }

    public boolean isUserAuthorised() {
        final UserAuth mUserAuth = this.mUserAuth;
        boolean b = false;
        if (mUserAuth != null) {
            b = b;
            if (mUserAuth.getSocialNetwork() != SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_NO) {
                b = true;
            }
        }
        return b;
    }

    public UserAuth login(final SocialNetworkManager.SocialUser socialNetworkManagerSocialUser, final Activity activity, final MainNetworkManager.RequestCompletionBlock requestCompletionBlock) {
        final boolean hasNetworkConnection = this.hasNetworkConnection(activity);
        boolean b = false;
        final UserAuth userAuth = null;
        String uid = null;
        UserAuth userAuth2;
        if (hasNetworkConnection) {
            final ArrayList<NameValuePair> list = new ArrayList<NameValuePair>(4);
            list.add((NameValuePair)new BasicNameValuePair("vendor_id", SmappsScanwords.getVendorId()));
            list.add((NameValuePair)new BasicNameValuePair("google_advertising_id", SmappsScanwords.instance.getAdvertisingId()));
            list.add((NameValuePair)new BasicNameValuePair("google_android_id", Utils.getAndroidId()));
            list.add((NameValuePair)new BasicNameValuePair("google_device_id", Utils.getDeviceId()));
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getNetId());
            sb.append("_");
            sb.append(SmappsScanwords.getVendorId());
            sb.append("_");
            sb.append(getVendorKey());
            list.add((NameValuePair)new BasicNameValuePair("vendor_key", Utils.md5(sb.toString())));
            if (socialNetworkManagerSocialUser != null) {
                list.add((NameValuePair)new BasicNameValuePair("suid", socialNetworkManagerSocialUser.getUid()));
                list.add((NameValuePair)new BasicNameValuePair("access_token", socialNetworkManagerSocialUser.getToken()));
                list.add((NameValuePair)new BasicNameValuePair("social_id", socialNetworkManagerSocialUser.getSocialId()));
            }
            if (this.mUserAuth == null) {
                final String string = SmappsScanwords.getContext().getSharedPreferences("referrer", 0).getString("referrer", (String)null);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("referrer - ");
                sb2.append(string);
                Log.v("SkanwordsFunc", sb2.toString());
                if (string != null) {
                    list.add((NameValuePair)new BasicNameValuePair("referrer", string));
                }
            }
            final JsonElement jsonResponse = this.makePostRequestForProtocol("login", list, false).getJsonResponse();
            if (jsonResponse != null) {
                final JsonObject asJsonObject = jsonResponse.getAsJsonObject().get("auth").getAsJsonObject();
                final String vendorId = SmappsScanwords.getVendorId();
                final String asString = asJsonObject.get("user_id").getAsString();
                if (socialNetworkManagerSocialUser != null) {
                    uid = socialNetworkManagerSocialUser.getUid();
                }
                final String asString2 = asJsonObject.get("auth_key").getAsString();
                SocialNetworkManager.SocialNetwork socialNetworkManagerSocialNetwork;
                if (socialNetworkManagerSocialUser == null) {
                    socialNetworkManagerSocialNetwork = SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_NO;
                }
                else {
                    socialNetworkManagerSocialNetwork = SocialNetworkManager.getSocialNetworkfromName(socialNetworkManagerSocialUser.getSocialId());
                }
                userAuth2 = new UserAuth(vendorId, asString, uid, asString2, socialNetworkManagerSocialNetwork);
                final JsonElement value = jsonResponse.getAsJsonObject().get("user");
                if (value != null) {
                    final UserObject loginUserState = this.gson.fromJson(value, UserObject.class);
                    userAuth2.setLoginUserState(loginUserState);
                    if (loginUserState.getScoreScanwords() == 0 && TutorialManager.getInstance().currentTutorialStep() == TutorialManager.TutorialStep.TUTORIAL_DISABLED) {
                        TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_START);
                    }
                    else {
                        TutorialManager.getInstance().setTutorialStep(TutorialManager.TutorialStep.TUTORIAL_COMPLETE);
                    }
                }
                b = true;
            }
            else {
                this.hideWaitingDialog((Activity)MainMenuActivity.instance);
                userAuth2 = userAuth;
            }
        }
        else {
            this.hideWaitingDialog((Activity)MainMenuActivity.instance);
            userAuth2 = userAuth;
        }
        if (!b) {
            this.requestError(activity);
        }
        if (requestCompletionBlock != null) {
            requestCompletionBlock.complete(b);
        }
        return userAuth2;
    }

    public void loginInSocialNetwork(final SocialNetworkManager.SocialNetwork socialNetworkManagerSocialNetwork, final Activity currentAuthActivity, final boolean b) {
        this.setCurrentAuthActivity(currentAuthActivity);
        if (b && this.mInAuthProcess) {
            return;
        }
        if (b) {
            if (!this.hasNetworkConnection(currentAuthActivity)) {
                return;
            }
            this.mInAuthProcess = true;
        }
        final SocialNetworkManager mSocialNetworkManager = this.mSocialNetworkManager;
        if (mSocialNetworkManager != null) {
            mSocialNetworkManager.logout();
            this.mSocialNetworkManager.clearSocialNetworkManager();
        }
        this.mSocialNetworkManager = null;
        final int n = MainNetworkManager.MainNetworkManager19.socialNetwork[socialNetworkManagerSocialNetwork.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n == 4) {
                        this.mLastSocialUser = null;
                    }
                }
                else {
                    this.mSocialNetworkManager = (SocialNetworkManager)new VkontakteManager();
                }
            }
            else {
                this.mSocialNetworkManager = (SocialNetworkManager)new FacebookManager();
            }
        }
        else {
            this.mSocialNetworkManager = (SocialNetworkManager)new OdnoklassnikiManager();
        }
        final SocialNetworkManager mSocialNetworkManager2 = this.mSocialNetworkManager;
        if (mSocialNetworkManager2 != null) {
            mSocialNetworkManager2.initSocialNetworkManager((ISocialNetworkManagerDelegate)this, SmappsScanwords.getContext().getApplicationContext());
            this.mSocialNetworkManager.authorise(currentAuthActivity);
        }
        else {
            this.acceptAuthorisation(currentAuthActivity);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("need login to ");
        sb.append(socialNetworkManagerSocialNetwork);
        Log.v("", sb.toString());
    }

    public MainNetworkManager.RequestResponse makePostRequestForProtocol(final String s, final List<NameValuePair> list) {
        return this.makePostRequestForProtocol(s, list, true);
    }

    public RequestResponse makePostRequestForProtocol(String object, List<NameValuePair> nameValPairList, boolean bl) {
        NameValuePair nameValuePair;
        StringBuilder stringBuilder;
        Iterator<org.apache.http.NameValuePair> nameValuePairIter = nameValPairList.iterator();
        while (nameValuePairIter.hasNext()) {
            nameValuePair = nameValuePairIter.next();
            stringBuilder = new StringBuilder();
            stringBuilder.append("key - ");
            stringBuilder.append(nameValuePair.getName());
            stringBuilder.append("  value - ");
            stringBuilder.append(nameValuePair.getValue());
            Log.v((String)"SkanwordsNetwork", (String)stringBuilder.toString());
        }
        RequestResponse.StatusType st = RequestResponse.StatusType.FAILED;
        List nameValPairList2 = nameValPairList;
        if (bl) {
            nameValPairList2 = this.genCommonRequestParams();
            if (nameValPairList != null) {
                nameValPairList2.addAll(nameValPairList);
            }
        }

        StringBuilder stringBuilder2 = null;
        try {
            String postReqStr = "";
            try {
                postReqStr = this.makePostRequestForSuffix((String)object, nameValPairList2);
            }
            catch (JsonSyntaxException jsonSyntaxException) {}
            try {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("makePostRequestForProtocol - ");
                stringBuilder2.append((String)object);
                stringBuilder2.append(" \nresponse - ");
                stringBuilder2.append(postReqStr);
                Log.v((String)"SkanwordsNetwork", (String)stringBuilder2.toString());
                JsonElement jsRes = this.parseRequestResult(postReqStr, bl);
                st = RequestResponse.StatusType.COMPLETE;
                return new RequestResponse(st, jsRes);
            }
            catch (JsonSyntaxException jsonSyntaxException) {
            }
        }
        catch (IOException iOException) {
            iOException.printStackTrace();
        }
        catch (OutOfMemoryError outOfMemoryError) {
            outOfMemoryError.printStackTrace();
        }
        this.illegalJSON((String)object);
        return null;
    }

    public String makePostRequestForSuffix(String string, List<NameValuePair> threadSafeClient) throws ClientProtocolException, IOException, OutOfMemoryError, HttpHostConnectException {
        final StringBuilder sb = new StringBuilder();
        sb.append("make request for url suffix ");
        sb.append(string);
        Log.v("SkanwordsFunc", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(getserverUrl());
        sb2.append(string);
        sb2.append("/");
        sb2.append(this.getNetId());
        final String string2 = sb2.toString();
        final HttpPost httpPost = new HttpPost(string2);
        httpPost.setEntity((HttpEntity)new UrlEncodedFormEntity((List)threadSafeClient));
        DefaultHttpClient threadSafeClient2 = getThreadSafeClient();
        if (this.mCurrentRequests.containsKey(string2)) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("aborting previous request for url suffix ");
            sb3.append(string);
            Log.v("SkanwordsFunc", sb3.toString());
            try {
                this.mCurrentRequests.get(string2).abort();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        Label_0203: {
            if (string.equals("cross_set_data")) {
                break Label_0203;
            }
            this.mCurrentRequests.put(string2, httpPost);
            try {
                try {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("start request for url suffix ");
                    sb4.append(string);
                    Log.v("SkanwordsSpeed", sb4.toString());
                    final HttpResponse execute = threadSafeClient2.execute((HttpUriRequest)httpPost);
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("finish request for url suffix ");
                    sb5.append(string);
                    Log.v("SkanwordsSpeed", sb5.toString());
                    string = EntityUtils.toString(execute.getEntity());
                    this.mCurrentRequests.remove(string2);
                    return string;
                }
                finally {}
            }
            catch (Exception ex2) {
                Log.v("Error", ex2.toString());
                this.mCurrentRequests.remove(string2);
                return null;
            }
        }
        this.mCurrentRequests.remove(string2);
        return null;
    }

    public void onActivityResult(final Activity activity, final int n, final int n2, final Intent intent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(" main network onActivityResult  ");
        sb.append(n);
        sb.append("  ");
        sb.append(n2);
        Log.v("", sb.toString());
        if (n == 102) {
            if (n2 == 0) {
                this.mInAuthProcess = false;
            }
            this.hideWaitingDialog(activity);
        }
        final SocialNetworkManager mSocialNetworkManager = this.mSocialNetworkManager;
        if (mSocialNetworkManager != null) {
            mSocialNetworkManager.onActivityResult(activity, n, n2, intent);
        }
    }

    public void onCreate(final Context context) {
        this.mConnectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            VKSdk.initialize(context.getApplicationContext());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void purchaseComplete(final Activity activity, final boolean b) {
        if (MainDataManager.getInstance().getPurchasesData().getPurchases().size() < 1 && MainDataManager.getInstance().getPurchasesData().getPurchasesRestored().size() < 1) {
            if (activity != null) {
                activity.runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager14(this, activity));
            }
            return;
        }
        if (activity != null) {
            activity.runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager15(this, activity));
        }
        if (b) {
            getInstance().serverLog("payment_track", "5. purchaseComplete has data to send");
        }
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.PurchaseCompleteRunnable(this, activity));
    }

    public ConfigObject requestConfig(final MainNetworkManager.RequestCompletionBlock requestCompletionBlock) {
        Log.v("SkanwordsFunc", "requestConfig");
        final ConfigObject configObject = null;
        ConfigObject configObject2 = null;
        boolean b = false;
        Label_0115: {
            if (this.checkAuth(null) && this.hasNetworkConnection()) {
                final JsonElement jsonResponse = this.makePostRequestForProtocol("config", this.addParamsWithUpdates(null)).getJsonResponse();
                if (jsonResponse != null) {
                    final JsonElement value = jsonResponse.getAsJsonObject().get("settings");
                    if (value != null) {
                        configObject2 = this.gson.fromJson(value, ConfigObject.class);
                    }
                    if (configObject2 != null) {
                        this.mTrackPayment = configObject2.paymentTrack();
                    }
                    MainDataManager.getInstance().configDataReceived(configObject2);
                    b = true;
                    break Label_0115;
                }
            }
            b = false;
            configObject2 = configObject;
        }
        if (requestCompletionBlock != null) {
            requestCompletionBlock.complete(b);
        }
        return configObject2;
    }

    public void requestConfigExec() {
        this.requestConfigExecWithCompleteBlock(null);
    }

    public void requestConfigExecWithCompleteBlock(final MainNetworkManager.RequestCompletionBlock requestCompletionBlock) {
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.MainNetworkManager2(this, requestCompletionBlock));
    }

    public void requestInfo(final int n, final Activity activity) {
        this.requestInfoWithCompleteBlock(n, activity, null);
    }

    public void requestInfo(final MainNetworkManager.RequestCompletionBlock requestCompletionBlock, final int n, final Activity activity) {
        final boolean checkAuth = this.checkAuth(activity);
        boolean b2;
        final boolean b = b2 = false;
        if (checkAuth) {
            b2 = b;
            if (this.hasNetworkConnection()) {
                final ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
                final StringBuilder sb = new StringBuilder();
                sb.append(n);
                String s = "";
                sb.append("");
                list.add(new BasicNameValuePair("mask", sb.toString()));
                list.add(new BasicNameValuePair("news_hash", MainDataManager.getInstance().getNewsDataManager().getNewsHash()));
                list.add(new BasicNameValuePair("shop_hash", MainDataManager.getInstance().getShopDataManager().getShopHash()));
                list.add(new BasicNameValuePair("rating_hash", MainDataManager.getInstance().getRatingDataManager().getRatingHash()));
                if (MainDataManager.getInstance().getUserFriends() != null && MainDataManager.getInstance().getUserFriends().size() > 0) {
                    final StringBuilder sb2 = new StringBuilder();
                    for (final String s2 : MainDataManager.getInstance().getUserFriends()) {
                        sb2.append(s);
                        sb2.append(s2);
                        s = ",";
                    }
                    list.add(new BasicNameValuePair("friends", sb2.toString()));
                }
                final JsonElement jsonResponse = this.makePostRequestForProtocol("info", this.addParamsWithUpdates((List<NameValuePair>)list)).getJsonResponse();
                JsonElement jsonElement;
                if ((jsonElement = jsonResponse) != null) {
                    jsonElement = jsonResponse;
                    if (jsonResponse.getAsJsonObject().get("info") != null) {
                        final JsonElement value = jsonResponse.getAsJsonObject().get("info");
                        final JsonElement value2 = value.getAsJsonObject().get("shop");
                        if (value2 != null) {
                            MainDataManager.getInstance().updateShopData((ShopDataManager.Shop)this.gson.fromJson(value2.getAsJsonObject().get("data"), ShopDataManager.Shop.class), value2.getAsJsonObject().get("hash").getAsString());
                        }
                        final JsonElement value3 = value.getAsJsonObject().get("news");
                        if (value3 != null) {
                            MainDataManager.getInstance().updateNewsData((List)this.gson.fromJson(value3.getAsJsonObject().get("data"), new MainNetworkManager.MainNetworkManager6(this).getType()), value3.getAsJsonObject().get("hash").getAsString());
                        }
                        final JsonElement value4 = value.getAsJsonObject().get("rating");
                        jsonElement = value;
                        if (value4 != null) {
                            MainDataManager.getInstance().updateRatingData((RatingDataManager.RatingInfo)this.gson.fromJson(value4.getAsJsonObject().get("data"), RatingDataManager.RatingInfo.class), value4.getAsJsonObject().get("rating_hash").getAsString());
                            jsonElement = value;
                        }
                    }
                }
                b2 = b;
                if (jsonElement != null) {
                    b2 = true;
                }
            }
        }
        if (!b2) {
            this.requestError(activity);
        }
        if (requestCompletionBlock != null) {
            requestCompletionBlock.complete(b2);
        }
    }

    public void requestInfoWithCompleteBlock(final int n, final Activity activity, final MainNetworkManager.RequestCompletionBlock requestCompletionBlock) {
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.MainNetworkManager5(this, requestCompletionBlock, n, activity));
    }

    public void requestIssuesList(final SkanwordsDataManager.TaskListType listType, final SkanwordsDataManager.TaskListDirrectionType dirrectionType, final MainNetworkManager.RequestCompletionBlock requestCompletionBlock, final Activity activity) {
        final boolean userLoggedIn = this.userLoggedIn();
        boolean b2;
        final boolean b = b2 = false;
        Label_0421: {
            if (userLoggedIn) {
                b2 = b;
                if (this.hasNetworkConnection()) {
                    final List<NameValuePair> addParamsWithUpdates = this.addParamsWithUpdates(null);
                    addParamsWithUpdates.add((NameValuePair)new BasicNameValuePair("page", SkanwordsDataManager.dirrectionStringRepresentation(dirrectionType)));
                    addParamsWithUpdates.add((NameValuePair)new BasicNameValuePair("type", SkanwordsDataManager.listTypeStringRepresentation(listType)));
                    if (dirrectionType != SkanwordsDataManager.TaskListDirrectionType.TOP_DIRRECTION) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(MainDataManager.getInstance().getSkanwordsDataManager().getLatesIssueIdOfType(listType));
                        sb.append("");
                        addParamsWithUpdates.add((NameValuePair)new BasicNameValuePair("set_id_first", sb.toString()));
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(MainDataManager.getInstance().getSkanwordsDataManager().getOldestIssueIdOfType(listType));
                        sb2.append("");
                        addParamsWithUpdates.add((NameValuePair)new BasicNameValuePair("set_id_last", sb2.toString()));
                    }
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("start requestIssuesList ");
                    sb3.append(listType);
                    Log.v("SkanwordsSpeed", sb3.toString());
                    final MainNetworkManager.RequestResponse postRequestForProtocol = this.makePostRequestForProtocol("cross_list_data", addParamsWithUpdates);
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("start parse requestIssuesList ");
                    sb4.append(listType);
                    Log.v("SkanwordsSpeed", sb4.toString());
                    final Integer mParsingMutex = MainNetworkManager.mParsingMutex;
                    // monitorenter(mParsingMutex)
                    Label_0347: {
                        if (postRequestForProtocol == null) {
                            break Label_0347;
                        }
                        try {
                            final SetsListObject setsListObject = this.gson.fromJson(postRequestForProtocol.getJsonResponse(), SetsListObject.class);
                            if (setsListObject != null) {
                                setsListObject.setListType(listType);
                                setsListObject.setDirrectionType(dirrectionType);
                            }
                            MainDataManager.getInstance().getSkanwordsDataManager().setsListReceived(setsListObject);
                            // monitorexit(mParsingMutex)
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append("finish requestIssuesList ");
                            sb5.append(listType);
                            Log.v("SkanwordsSpeed", sb5.toString());
                            if (postRequestForProtocol.getStatusType() != RequestResponse.StatusType.COMPLETE) {
                                b2 = b;
                                if (postRequestForProtocol.getStatusType() != RequestResponse.StatusType.ABORTED) {
                                    break Label_0421;
                                }
                            }
                            b2 = true;
                        }
                        finally {
                        }
                        // monitorexit(mParsingMutex)
                    }
                }
            }
        }
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("MainNetworkManager requestIssuesList ");
        sb6.append(b2);
        Log.v("SkanwordsFunc", sb6.toString());
        if (!b2) {
            this.requestError(activity);
        }
        if (requestCompletionBlock != null) {
            requestCompletionBlock.complete(b2);
        }
    }

    public void requestIssuesListExec(final SkanwordsDataManager.TaskListType skanwordsDataManagerTaskListType, final SkanwordsDataManager.TaskListDirrectionType skanwordsDataManagerTaskListDirrectionType, final Activity activity) {
        this.requestIssuesListExecWithCompleteBlock(skanwordsDataManagerTaskListType, skanwordsDataManagerTaskListDirrectionType, null, activity);
    }

    public void requestIssuesListExecWithCompleteBlock(final SkanwordsDataManager.TaskListType skanwordsDataManagerTaskListType, final SkanwordsDataManager.TaskListDirrectionType skanwordsDataManagerTaskListDirrectionType, final MainNetworkManager.RequestCompletionBlock requestCompletionBlock, final Activity activity) {
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.MainNetworkManager7(this, requestCompletionBlock, skanwordsDataManagerTaskListType, skanwordsDataManagerTaskListDirrectionType, activity));
    }

    public void requestSync(final MainNetworkManager.RequestCompletionBlock requestCompletionBlock) {
        final boolean b = this.checkAuth(null) && this.hasNetworkConnection() && this.makePostRequestForProtocol("sync", this.addParamsWithUpdates(null)).getJsonResponse() != null;
        if (requestCompletionBlock != null) {
            requestCompletionBlock.complete(b);
        }
    }

    public void requestSyncExec() {
        this.requestSyncExecWithCompleteBlock(null);
    }

    public void requestSyncExecWithCompleteBlock(final MainNetworkManager.RequestCompletionBlock requestCompletionBlock) {
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.MainNetworkManager4(this, requestCompletionBlock));
    }

    public void saveAuthData() {
        final SharedPreferences.Editor appSharedPreferencesEditor = SmappsScanwords.getAppSharedPreferencesEditor("UserAuthPrefs");
        appSharedPreferencesEditor.putString("user_id", this.mUserAuth.getUserId());
        appSharedPreferencesEditor.putString("user_auth_key", this.mUserAuth.getAuthKey());
        appSharedPreferencesEditor.putString("vendor_key", this.mUserAuth.getVendorId());
        appSharedPreferencesEditor.putString("user_social_id", this.mUserAuth.getUserSocialId());
        appSharedPreferencesEditor.putInt("social_network", this.mUserAuth.getSocialNetwork().ordinal());
        if (this.mLastSocialUser != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mLastSocialUser.getFirstName());
            sb.append(" ");
            sb.append(this.mLastSocialUser.getSecondName());
            appSharedPreferencesEditor.putString("full_name", sb.toString());
            appSharedPreferencesEditor.putString("avatar_url", this.mLastSocialUser.getAvatarUrl());
            this.mUserAuth.setAvatarUrl(this.mLastSocialUser.getAvatarUrl());
            final UserAuth mUserAuth = this.mUserAuth;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.mLastSocialUser.getFirstName());
            sb2.append(" ");
            sb2.append(this.mLastSocialUser.getSecondName());
            mUserAuth.setFullName(sb2.toString());
        }
        appSharedPreferencesEditor.commit();
    }

    public void sendPushToken(final String s) {
        if (this.checkAuth(null) && this.hasNetworkConnection()) {
            final ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
            list.add(new BasicNameValuePair("push_token", s));
            this.makePostRequestForProtocol("push_token_set", (List<NameValuePair>)list);
        }
    }

    public void serverErrorLog(final String s, final String s2) {
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.ServerErrorLogRunnable(this, s, s2));
    }

    public void serverLog(final String s, final String s2) {
        if (s.equals("payment_track") && this.mTrackPayment) {
            SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.ServerLogRunnable(this, s, s2));
        }
    }

    public void setCurrentAuthActivity(final Activity mCurrentAuthActivity) {
        this.mCurrentAuthActivity = mCurrentAuthActivity;
    }

    public void showAuthorisationAcceptActivity(final String s, final SocialNetworkManager.SocialUser socialNetworkManagerSocialUser) {
        this.getCurrentAuthActivity().runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager17(this, s, socialNetworkManagerSocialUser));
    }

    public void showWaitingDialog(Activity topActivity, final String s) {
        topActivity = TopActivityManager.get().getTopActivity();
        if (topActivity == null) {
            return;
        }
        if (this.mWaitingProgressDialog != null) {
            return;
        }
        try {
            this.mWaitingProgressDialog = ProgressDialog.show((Context)topActivity, "Подождите, пожалуйста...", (CharSequence)s, false);
        }
        catch (Exception ex) {}
    }

    public SkanwordsSetDataObject skanwordSetGet(final Activity activity, final List<HashMap<String, Integer>> list) {
        if (this.checkAuth(activity) && this.hasNetworkConnection(activity)) {
            final String json = this.gson.toJson(list);
            final StringBuilder sb = new StringBuilder();
            sb.append("json cross set data  ");
            sb.append(json);
            Log.v("", sb.toString());
            final ArrayList<NameValuePair> list2 = new ArrayList<NameValuePair>();
            list2.add(new BasicNameValuePair("set_ids", Base64.encodeToString(json.getBytes(), 0)));
            final JsonElement jsonResponse = this.makePostRequestForProtocol("cross_set_data", (List<NameValuePair>)list2).getJsonResponse();
            if (jsonResponse != null) {
                final SkanwordsSetDataObject skanwordsSetDataObject = this.gson.fromJson(jsonResponse, SkanwordsSetDataObject.class);
                skanwordsSetDataObject.setRequestdSetIds((List)list);
                return skanwordsSetDataObject;
            }
            this.requestError();
        }
        return null;
    }

    public void socialNetworkAuthoriseFailed(final boolean b) {
        Log.v("", " socialNetworkAuthoriseFailed  ");
        this.mInAuthProcess = false;
        if (b) {
            return;
        }
        this.getCurrentAuthActivity().runOnUiThread((Runnable)new MainNetworkManager.MainNetworkManager18(this));
    }

    @Override
    public void socialNetworkAuthorised(final SocialNetworkManager.SocialUser mLastSocialUser) {
        this.mLastSocialUser = mLastSocialUser;
        this.showWaitingDialog(this.getCurrentAuthActivity(), "Идет синхронизация...");
        SmappsScanwords.getExecutorService().execute((Runnable)new MainNetworkManager.MainNetworkManager16(this, mLastSocialUser));
    }

    public void socialNetworkFriendsGot(final List<String> userFriends) {
        MainDataManager.getInstance().setUserFriends((List)userFriends);
    }

    public boolean userLoggedIn() {
        return this.mUserAuth != null;
    }

    // ---- Sub classes
    public static class RequestResponse
    {
        private final JsonElement mJsonResponse;
        private final StatusType mStatusType;

        public RequestResponse(final StatusType mStatusType, final JsonElement mJsonResponse) {
            super();
            this.mStatusType = mStatusType;
            this.mJsonResponse = mJsonResponse;
        }

        public JsonElement getJsonResponse() {
            return this.mJsonResponse;
        }

        public StatusType getStatusType() {
            return this.mStatusType;
        }

        public enum StatusType {
            ABORTED,
            COMPLETE,
            FAILED,
            NOT_PARSED;
        }

    }

    class MainNetworkManager12 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ JsonElement valo;

        MainNetworkManager12(final MainNetworkManager this0, final JsonElement valo) {
            super();
            this.this0 = this0;
            this.valo = valo;
        }

        @Override
        public void run() {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)TopActivityManager.get().getTopActivity(), R.attr.dialogTheme);
            alertDialogBuilder.setMessage((CharSequence)this.valo.getAsString()).setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainNetworkManager12.MainNetworkManager121(this)).setTitle("Поздравляем!");
            alertDialogBuilder.create().show();
        }

        class MainNetworkManager121 implements DialogInterface.OnClickListener {
            final /* synthetic */ MainNetworkManager12 this1;

            MainNetworkManager121(final MainNetworkManager12 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class MainNetworkManager13 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valcallerActivity;
        final /* synthetic */ JsonObject valo;

        MainNetworkManager13(final MainNetworkManager this0, final Activity valcallerActivity, final JsonObject valo) {
            super();
            this.this0 = this0;
            this.valcallerActivity = valcallerActivity;
            this.valo = valo;
        }

        @Override
        public void run() {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.valcallerActivity, R.attr.dialogTheme);
            alertDialogBuilder.setMessage((CharSequence)this.valo.get("text").getAsString()).setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainNetworkManager13.MainNetworkManager131(this)).setTitle((CharSequence)this.valo.get("title").getAsString());
            alertDialogBuilder.create().show();
        }

        class MainNetworkManager131 implements DialogInterface.OnClickListener {
            final /* synthetic */ MainNetworkManager13 this1;

            MainNetworkManager131(final MainNetworkManager13 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class MainNetworkManager11 extends TypeToken<List<String>> {
        final /* synthetic */ MainNetworkManager this0;

        MainNetworkManager11(final MainNetworkManager this0) {
            super();
            this.this0 = this0;
        }
    }

    class MainNetworkManager8 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valcallerActivity;

        MainNetworkManager8(final MainNetworkManager this0, final Activity valcallerActivity) {
            super();
            this.this0 = this0;
            this.valcallerActivity = valcallerActivity;
        }

        @Override
        public void run() {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.valcallerActivity, R.attr.dialogTheme);
            alertDialogBuilder.setMessage("Не удалось расшифровать ответ от сервера.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainNetworkManager8.MainNetworkManager81(this)).setTitle("Ошибка");
            alertDialogBuilder.create().show();
        }

        class MainNetworkManager81 implements DialogInterface.OnClickListener {
            final /* synthetic */ MainNetworkManager8 this1;

            MainNetworkManager81(final MainNetworkManager8 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class MainNetworkManager10 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valactivity;

        MainNetworkManager10(final MainNetworkManager this0, final Activity valactivity) {
            super();
            this.this0 = this0;
            this.valactivity = valactivity;
        }

        @Override
        public void run() {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.valactivity, R.attr.dialogTheme);
            alertDialogBuilder.setMessage("Не удалось поключится к серверу. Попробуйте еще раз.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainNetworkManager10.MainNetworkManager101(this)).setTitle("Ошибка");
            alertDialogBuilder.create().show();
        }

        class MainNetworkManager101 implements DialogInterface.OnClickListener {
            final /* synthetic */ MainNetworkManager10 this1;

            MainNetworkManager101(final MainNetworkManager10 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class MainNetworkManager3 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valactivity;

        MainNetworkManager3(final MainNetworkManager this0, final Activity valactivity) {
            super();
            this.this0 = this0;
            this.valactivity = valactivity;
        }

        @Override
        public void run() {
            this.this0.requestSync((MainNetworkManager.RequestCompletionBlock)new MainNetworkManager3.MainNetworkManager31(this));
        }

        class MainNetworkManager31 implements MainNetworkManager.RequestCompletionBlock {
            final /* synthetic */ MainNetworkManager3 this1;

            MainNetworkManager31(final MainNetworkManager3 this1) {
                super();
                this.this1 = this1;
            }

            public void complete(final boolean b) {
                if (!b) {
                    this.this1.this0.socialNetworkAuthoriseFailed(false);
                    this.this1.this0.hideWaitingDialog(this.this1.valactivity);
                    MainNetworkManager.access402(this.this1.this0, false);
                    return;
                }
                final UserAuth login = this.this1.this0.login(MainNetworkManager.access100(this.this1.this0), MainNetworkManager.access200(this.this1.this0), (MainNetworkManager.RequestCompletionBlock)null);
                if (login != null && (MainNetworkManager.access300(this.this1.this0) == null || !login.getUserId().equals(MainNetworkManager.access300(this.this1.this0).getUserId()))) {
                    MainNetworkManager.access302(this.this1.this0, login);
                    this.this1.this0.saveAuthData();
                    this.this1.this0.hideWaitingDialog(this.this1.valactivity);
                    MainNetworkManager.access402(this.this1.this0, false);
                    MainDataManager.getInstance().userDataReceived(MainNetworkManager.access300(this.this1.this0).getLoginUserState());
                    this.this1.this0.requestConfig((MainNetworkManager.RequestCompletionBlock)null);
                    SmappsScanwords.getEventsDispatcher().dispatchEvent((SimpleEvent)new SimpleEvent("UPDATE_ISSUES_LIST"));
                    return;
                }
                this.this1.this0.socialNetworkAuthoriseFailed(false);
                this.this1.this0.hideWaitingDialog(this.this1.valactivity);
                MainNetworkManager.access402(this.this1.this0, false);
            }
        }
    }

    public interface RequestCompletionBlock
    {
        void complete(final boolean p0);
    }

    class MainNetworkManager9 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valalertActivity;

        MainNetworkManager9(final MainNetworkManager this0, final Activity valalertActivity) {
            super();
            this.this0 = this0;
            this.valalertActivity = valalertActivity;
        }

        @Override
        public void run() {
            if (MainNetworkManager.access500(this.this0)) {
                return;
            }
            MainNetworkManager.access502(this.this0, true);
            Log.v("", "show network availability error");
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.valalertActivity, R.attr.dialogTheme);
            alertDialogBuilder.setMessage("Отсутствует соединение к сети Интернет.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainNetworkManager9.MainNetworkManager92(this)).setOnCancelListener((DialogInterface.OnCancelListener)new MainNetworkManager9.MainNetworkManager91(this)).setTitle("Ошибка");
            alertDialogBuilder.create().show();
        }

        class MainNetworkManager91 implements DialogInterface.OnCancelListener {
            final /* synthetic */ MainNetworkManager9 this1;

            MainNetworkManager91(final MainNetworkManager9 this1) {
                super();
                this.this1 = this1;
            }

            public void onCancel(final DialogInterface dialogInterface) {
                MainNetworkManager.access502(this.this1.this0, false);
            }
        }

        class MainNetworkManager92 implements DialogInterface.OnClickListener {
            final /* synthetic */ MainNetworkManager9 this1;

            MainNetworkManager92(final MainNetworkManager9 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
                MainNetworkManager.access502(this.this1.this0, false);
            }
        }
    }

    class MainNetworkManager1 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;

        MainNetworkManager1(final MainNetworkManager this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            if (MainNetworkManager.access000(this.this0) == null) {
                return;
            }
            MainNetworkManager.access000(this.this0).dismiss();
            MainNetworkManager.access002(this.this0, (ProgressDialog)null);
        }
    }

    static class MainNetworkManager19 {
        static final /* synthetic */ int[] socialNetwork;

        static {
            socialNetwork = new int[SocialNetworkManager.SocialNetwork.values().length];
            try {
                MainNetworkManager19.socialNetwork[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_OK.ordinal()] = 1;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                MainNetworkManager19.socialNetwork[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_FB.ordinal()] = 2;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                MainNetworkManager19.socialNetwork[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_VK.ordinal()] = 3;
            }
            catch (NoSuchFieldError noSuchFieldError) {}
            try {
                MainNetworkManager19.socialNetwork[SocialNetworkManager.SocialNetwork.SOCIAL_NETWORK_NO.ordinal()] = 4;
            }
            catch (NoSuchFieldError noSuchFieldError) {
            }
        }
    }

    class MainNetworkManager14 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valactivity;

        MainNetworkManager14(final MainNetworkManager this0, final Activity valactivity) {
            super();
            this.this0 = this0;
            this.valactivity = valactivity;
        }

        @Override
        public void run() {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.valactivity, R.attr.dialogTheme);
            alertDialogBuilder.setMessage("Вы не совершали покупок, которые можно восстановить.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainNetworkManager14.MainNetworkManager141(this));
            alertDialogBuilder.create().show();
        }

        class MainNetworkManager141 implements DialogInterface.OnClickListener {
            final /* synthetic */ MainNetworkManager14 this1;

            MainNetworkManager141(final MainNetworkManager14 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class MainNetworkManager15 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valactivity;

        MainNetworkManager15(final MainNetworkManager this0, final Activity valactivity) {
            super();
            this.this0 = this0;
            this.valactivity = valactivity;
        }

        @Override
        public void run() {
            this.this0.showWaitingDialog(this.valactivity, "Идет синхронизация...");
        }
    }

    public class PurchaseCompleteRunnable implements Runnable
    {
        private Activity mCallerActivity;
        final /* synthetic */ MainNetworkManager this0;

        public PurchaseCompleteRunnable(final MainNetworkManager this0, final Activity mCallerActivity) {
            super();
            this.this0 = this0;
            this.mCallerActivity = null;
            this.mCallerActivity = mCallerActivity;
        }

        @Override
        public void run() {
            final UserObject access600 = MainNetworkManager.access600(this.this0, false, this.mCallerActivity);
            final UserObject access601 = MainNetworkManager.access600(this.this0, true, this.mCallerActivity);
            if (access600 != null || access601 != null) {
                MainDataManager.getInstance().getShopDataManager().setShopHash("");
            }
            this.this0.hideWaitingDialog((Activity)MainMenuActivity.instance);
        }
    }

    class MainNetworkManager2 extends MainNetworkManager.RequestRunnable {
        final /* synthetic */ MainNetworkManager this0;

        MainNetworkManager2(final MainNetworkManager this0, final MainNetworkManager.RequestCompletionBlock reqCompl) {
            super(this0, reqCompl);
            this.this0 = this0;
        }

        public void run() {
            this.this0.requestConfig(this.mCompleteBlock);
        }
    }

    public abstract class RequestRunnable implements Runnable
    {
        protected MainNetworkManager.RequestCompletionBlock mCompleteBlock;
        final /* synthetic */ MainNetworkManager this0;

        public RequestRunnable(final MainNetworkManager this0) {
            super();
            this.this0 = this0;
            this.mCompleteBlock = null;
        }

        public RequestRunnable(final MainNetworkManager this0, final MainNetworkManager.RequestCompletionBlock mCompleteBlock) {
            super();
            this.this0 = this0;
            this.mCompleteBlock = mCompleteBlock;
        }
    }

    class MainNetworkManager6 extends TypeToken<List<NewsDataManager.News>> {
        final /* synthetic */ MainNetworkManager this0;

        MainNetworkManager6(final MainNetworkManager this0) {
            super();
            this.this0 = this0;
        }
    }

    class MainNetworkManager5 extends MainNetworkManager.RequestRunnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valactivity;
        final /* synthetic */ int valmask;

        MainNetworkManager5(final MainNetworkManager this0, final RequestCompletionBlock rc, final int valmask, final Activity valactivity) {
            super(this0, rc);
            this.this0 = this0;
            this.valmask = valmask;
            this.valactivity = valactivity;
        }

        public void run() {
            this.this0.requestInfo(this.mCompleteBlock, this.valmask, this.valactivity);
        }
    }

    class MainNetworkManager7 extends MainNetworkManager.RequestRunnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ Activity valactivity;
        final /* synthetic */ SkanwordsDataManager.TaskListDirrectionType valdirrectionType;
        final /* synthetic */ SkanwordsDataManager.TaskListType vallistType;

        MainNetworkManager7(final MainNetworkManager this0, final MainNetworkManager.RequestCompletionBlock reqComBlock, final SkanwordsDataManager.TaskListType vallistType, final SkanwordsDataManager.TaskListDirrectionType valdirrectionType, final Activity valactivity) {
            super(this0, reqComBlock);
            this.this0 = this0;
            this.vallistType = vallistType;
            this.valdirrectionType = valdirrectionType;
            this.valactivity = valactivity;
        }

        public void run() {
            this.this0.requestIssuesList(this.vallistType, this.valdirrectionType, this.mCompleteBlock, this.valactivity);
        }
    }

    class MainNetworkManager4 extends MainNetworkManager.RequestRunnable {
        final /* synthetic */ MainNetworkManager this0;

        MainNetworkManager4(final MainNetworkManager this0, final MainNetworkManager.RequestCompletionBlock reqComBlock) {
            super(this0, reqComBlock);
            this.this0 = this0;
        }

        public void run() {
            this.this0.requestSync(this.mCompleteBlock);
        }
    }

    public class ServerLogRunnable implements Runnable
    {
        private final String mLog;
        private final String mType;
        final /* synthetic */ MainNetworkManager this0;

        public ServerLogRunnable(final MainNetworkManager this0, final String mType, final String mLog) {
            super();
            this.this0 = this0;
            this.mType = mType;
            this.mLog = mLog;
        }

        @Override
        public void run() {
            try {
                final ArrayList<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
                list.add(new BasicNameValuePair("log", this.mLog));
                this.this0.makePostRequestForSuffix(this.mType, (List)list);
            }
            catch (Exception ex) {}
        }
    }

    class MainNetworkManager17 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ SocialNetworkManager.SocialUser valuser;
        final /* synthetic */ String valuserInfoJson;

        MainNetworkManager17(final MainNetworkManager this0, final String valuserInfoJson, final SocialNetworkManager.SocialUser valuser) {
            super();
            this.this0 = this0;
            this.valuserInfoJson = valuserInfoJson;
            this.valuser = valuser;
        }

        @Override
        public void run() {
            final Intent intent = new Intent("android.intent.action.EDIT", (Uri)null, (Context)this.this0.getCurrentAuthActivity(), (Class) AuthorisationAcceptActivity.class);
            intent.putExtra("user_info", this.valuserInfoJson);
            intent.putExtra("user_social", (Serializable)this.valuser);
            this.this0.getCurrentAuthActivity().startActivityForResult(intent, 102);
        }
    }

    public class ServerErrorLogRunnable implements Runnable
    {
        private final String mLog;
        private final String mType;
        final /* synthetic */ MainNetworkManager this0;

        public ServerErrorLogRunnable(final MainNetworkManager this0, final String mType, final String mLog) {
            super();
            this.this0 = this0;
            this.mType = mType;
            this.mLog = mLog;
        }

        @Override
        public void run() {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("send error log of type ");
                sb.append(this.mType);
                sb.append("  with log :");
                sb.append(this.mLog);
                Log.v("", sb.toString());
                final ArrayList<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
                list.add(new BasicNameValuePair("error", this.mLog));
                list.add(new BasicNameValuePair("protocol", this.mType));
                this.this0.makePostRequestForSuffix("app_error", (List)list);
            }
            catch (Exception ex) {}
        }
    }

    class MainNetworkManager18 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;

        MainNetworkManager18(final MainNetworkManager this0) {
            super();
            this.this0 = this0;
        }

        @Override
        public void run() {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context)this.this0.getCurrentAuthActivity(), R.attr.dialogTheme);
            alertDialogBuilder.setMessage("Авторизация не удалась, попробуйте еще раз.").setPositiveButton("Ок", (DialogInterface.OnClickListener)new MainNetworkManager18.MainNetworkManager181(this));
            alertDialogBuilder.create().show();
        }

        class MainNetworkManager181 implements DialogInterface.OnClickListener {
            final /* synthetic */ MainNetworkManager18 this1;

            MainNetworkManager181(final MainNetworkManager18 this1) {
                super();
                this.this1 = this1;
            }

            public void onClick(final DialogInterface dialogInterface, final int n) {
            }
        }
    }

    class MainNetworkManager16 implements Runnable {
        final /* synthetic */ MainNetworkManager this0;
        final /* synthetic */ SocialNetworkManager.SocialUser valuser;

        MainNetworkManager16(final MainNetworkManager this0, final SocialNetworkManager.SocialUser valuser) {
            super();
            this.this0 = this0;
            this.valuser = valuser;
        }

        @Override
        public void run() {
            final ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
            final StringBuilder sb = new StringBuilder();
            sb.append(this.valuser.getSocialId());
            sb.append("-");
            sb.append(this.valuser.getUid());
            list.add(new BasicNameValuePair("social_id", sb.toString()));
            try {
                if (MainNetworkManager.access300(this.this0) == null) {
                    this.this0.acceptAuthorisation((Activity)MainMenuActivity.instance);
                    return;
                }
                list.addAll(this.this0.genCommonRequestParams());
                final String postRequestForSuffix = this.this0.makePostRequestForSuffix("user_info", (List)list);
                if (new JSONObject(postRequestForSuffix).getInt("bind") == 1) {
                    this.this0.showAuthorisationAcceptActivity(postRequestForSuffix, this.valuser);
                    return;
                }
                this.this0.acceptAuthorisation((Activity)MainMenuActivity.instance);
            }
            catch (JSONException ex) {
                final MainNetworkManager this0 = this.this0;
                this0.hideWaitingDialog(this0.getCurrentAuthActivity());
                ex.printStackTrace();
            }
            catch (IOException ex2) {
                final MainNetworkManager this2 = this.this0;
                this2.hideWaitingDialog(this2.getCurrentAuthActivity());
                ex2.printStackTrace();
            }
        }
    }
}

