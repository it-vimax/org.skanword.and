package org.skanword.and.libs.musselwhizzle.mvc.events;



public interface EventListener {

    void onEvent(Event event);

}
